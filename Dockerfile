FROM ubuntu:18.04

LABEL "za.co.csir.organisation"="eGovernment, NGEI, CSIR" \
      "za.co.csir.author"="Derek Hohls" \
      "za.co.csir.email"="dhohls@csir.co.za" \
      "za.co.csir.version"="1.1"

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    build-essential \
    gdal-bin \
    libgeos-c1v5 \
    libpq-dev \
    libspatialindex-dev \
    libfreetype6-dev \
    python-dev \
    nano \
    wget \
    curl \
    git && \
    apt-get clean

RUN apt-get update && apt-get install -y \
    python3.7 \
    python3.7-distutils \
    python3-setuptools \
    python3.7-dev

RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
    && python3.7 get-pip.py \
    && pip3 install -U pip setuptools

ADD swordfish/requirements.txt /requirements.txt
RUN pip3 install --default-timeout=100  -r /requirements.txt

ADD swordfish /swordfish

ENTRYPOINT ["tail", "-f", "/dev/null"]
