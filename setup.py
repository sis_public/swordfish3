# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
swordfish setup
"""
#from setuptools import setup
#from Cython.Build import cythonize
from distutils.core import setup
from distutils.extension import Extension


MODULE_CISO = Extension('ciso8601',
                        ['swordfish/cythonext/ciso8601.c'])
CYTHON_CORE = Extension('core',
                        ['swordfish/cythonext/core.pyx'])
CYTHON_STRM = Extension('streamProcessor',
                        ['swordfish/cythonext/streamProcessor.pyx'])


def readme():
    """Enable readme display"""
    with open('README.rst') as _file:
        return _file.read()


setup(
    name='swordfish',
    version='0.1b7',
    description='Python data streaming and analytics',
    long_description=\
        'Message-oriented Python-based geospatial data streaming' +\
        ' framework which provides datastream processing primitives,' +\
        ' functions, transports and a common data model for messages',
    url='http://gitlab.com/ict4eo/swordfish',
    author='Meraka, CSIR',
    author_email='gmcferren@csir.co.za',
    license='GPLv3+',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: ' + \
            'GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 2.7',
        'Topic :: Streaming :: Analytics',
    ],
    keywords='python, analytics, spatial, geospatial, IoT, streaming,' + \
        ' geoinformatics, sensors, messaging, earth observation, datastream',
    packages=[
        'swordfish',
    ],
    include_package_data=True,
    install_requires=[
        'amqp',
        'anyjson',
        'backports.ssl-match-hostname',
        'certifi',
        'ciso8601',
        'Cython',
        'cytoolz',
        'gevent',
        'greenlet',
        'httplib2',
        'Jinja',
        'Jinja2',
        'jsonrpclib',
        'kombu',
        'libais',
        'MarkupSafe',
        'msgpack-python',
        'numpy',
        'paho-mqtt',
        'pandas',
        'protobuf',
        'psycopg2',
        'pymongo',
        'python-dateutil',
        'pytz',
        'pyzmq',
        'requests',
        'rethinkdb',
        'Rtree',
        'rpyc',
        'six',
        'suds',
        'tornado',
        'tornadorpc',
        'ujson',
        'websocket-client',
        'wheel',
        'xlwt'
    ],
    ext_modules=[
        CYTHON_CORE,
        CYTHON_STRM,
        MODULE_CISO
    ],
    zip_safe=False)
