#!/bin/bash
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
docker rm --force sf
docker build --tag swordfish:1.0 .
docker run --detach --name sf swordfish:1.0
docker exec -it sf bash
