=========
swordfish
=========

**WARNING!!!** This software is *Under Construction* and is **not** stable or
usable as yet.

About
=====

**swordfish** is a message-oriented Python-based geospatial data streaming
framework which provides datastream processing primitives, functions,
transports and a common data model for describing messages, based on the
Open Geospatial Consortium Observations and Measurements (O&M) and the
Unidata Common Data Model (CDM) standards.

**swordfish** requires skilled programmers to construct and deploy applications;
it is *not* a toolbox suitable for a non-technical end-user.

**swordfish3** is a fork of the original project, adapted for Python 3.

License
=======

This software is released under the MIT License. The license text is available
at https://mit-license.org/

Installation
============

**swordfish** is designed to be installed on a Linux machine. It has been
tested on Ubuntu 18.04.x and with Python 3.7.

Pre-requisites
---------------

You first need to ensure that gcc is up-to-date::

    sudo apt update
    sudo apt install --reinstall build-essential

Another pre-requisite is that `libspatialindex-dev` is installed on the host::

    sudo apt install libspatialindex-dev

Local usage
-----------

In order to try **swordfish** without installation, clone the repository,
run the `pip` install of the requirements, and add the project path to the
PYTHONPATH.  It is strongly recommended that you do this in a virtualenv::

    cd /path/to/swordfish3
    cd swordfish
    pip install -r requirements.txt
    export PYTHONPATH=/path/to/swordfish3:$PYTHONPATH

(Note that you need to run the export every time you launch a terminal).

pip install
-----------

Assuming that this repository is a private one, SSH keys are required to use
the code as a Python package.

To enable an install via SSH, you need to have your host's public SSH key
registered on the **swordfish** repository by its owner.

First install example (see repository for actual version numbers)::

    pip install git+ssh://git@gitlab.com/ict4eo/swordfish3.git

Second install example i.e. update/upgrade::

    pip install --upgrade git+ssh://git@gitlab.com/ict4eo/swordfish3.git

Using Docker
============

A `Dockerfile` is included for working with **swordfish** in a container.

To use it (assuming you have your Docker environment setup; see
https://docs.docker.com/engine/install/ )::

    docker build --tag swordfish:1.0 .
    docker run --detach --name sf swordfish:1.0

You can work inside the container::

    docker exec -it sf bash

(The above steps are encoded into the `start.sh` script to help with usage.)

Check which Python packages are installed::

    pip3 freeze

Test out **swordfish**::

    export PYTHONPATH=/swordfish:$PYTHONPATH
    alias py="python3.7"
    py /swordfish/examples/performance/basic.py
    py /swordfish/examples/swordfish_performance.py
    py /swordfish/examples/swordfish_api.py

When done, exit out of the container  (Ctrl-D) and then::

    docker rm --force sf

Updates
=======

If **swordfish** is used on different versions of the OS and of Python,
there could be issues.

Cython Modules
--------------

If a different version of Python is used, ensure that the `cythonext` modules
are compiled (or recompiled) for it. See
https://cython.readthedocs.io/en/latest/src/userguide/source_files_and_compilation.html

In general, you need to generate a new `.c` and `.so` file for each of the
`core.pyx` and `streamProcessor.pyx` files::

    cythonize -a -i streamProcessor.pyx
    cythonize -a -i core.pyx
