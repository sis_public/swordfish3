# -*- coding: utf-8 -*-
# cython: language_level=3
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Mon Sep 14 14:19:58 2015
@author: Terence van Zyl - tvzyl@csir.co.za

Updated: Sep 9 2020
@author: Derek Hohls - dhohls@csir.co.za

Notes:
    * To recompile:
        cythonize -a -i core.pyx
      Then:
        mv core.cpython-37m-x86_64-linux-gnu.so core.so
"""
from msgpack import loads, dumps
from swordfish.exceptions import *
from weakref import WeakValueDictionary
from cytoolz import functoolz
from collections import deque
from inspect import getargspec
from operator import iand

import cython
import logging
import threading
import multiprocessing
import operator
#__builtin__ module was renamed to builtins in Python3
#import __builtin__
import builtins
from functools import reduce  # reduce was removed from builtins in Python3
import ujson
import uuid
#from swordfish.cythonext.ciso8601 import parse_datetime
from ciso8601 import parse_datetime
from datetime import datetime
#import lz4

try:
    import zmq
except Exception:
    pass


class SwordfishMessageError(Exception):
    pass


#we can only work with python functions so we need to wrap c functions
identity = lambda x: functoolz.identity(x)
identity.__name__ = 'identity'


cdef inline __asattrtype(bint __frozen__, item):
    cdef type itemtype
    cdef result
    itemtype = type(item)
    if itemtype == dict:
        if "$date" in <dict>item:
            return parse_datetime((<dict>item)['$date'])
        else:
            return AttrDict.__new__(AttrDict,item, __frozen__)
    elif itemtype == list:
        return AttrList.__new__(AttrList,item, __frozen__)
    else:
        return item

cdef inline __asorgtype(item):
    cdef type itemtype
    itemtype = type(item)
    if itemtype == AttrDict:
        return (<AttrDict>item).data
    elif itemtype == AttrList:
        return (<AttrList>item).data
    elif itemtype == dict:
        for itm in item:
            itmtype = type(item[itm])
            if itmtype == AttrDict or itmtype == AttrList:
                item[itm] = __asorgtype(item[itm])
        return item
    elif itemtype == list:
        for i in xrange(len(item)):
            itmtype = type(item[i])
            if itmtype == AttrDict or itmtype == AttrList:
                item[i] = __asorgtype(item[i])
        return item
    elif itemtype == datetime:
        return {'$date':item.isoformat()}
    else:
        return item

cpdef void listmerge(list self, list alist):
    cdef dict tmp_dict
    cdef list tmp_list
    for item in alist:
        if isinstance(item, dict):
            tmp_dict = {}
            self.append(tmp_dict)
            dictmerge(tmp_dict, <dict>item)
        elif isinstance(item, list):
            tmp_list = []
            self.append(tmp_list)
            listmerge(tmp_list, <list>item)
        else:
            self.append(item)

cpdef void dictmerge(dict self, dict adict):
    cdef dict tmp_dict
    cdef list tmp_list
    for k, v in adict.iteritems():
        if isinstance(v, dict):
            self[k] = {}
            dictmerge(self[k], <dict>v)
        elif isinstance(v, list):
            self[k] = []
            listmerge(self[k], <list>v)
        else:
            self[k] = v


@cython.freelist(10000)
cdef class AttrList:
    def __cinit__(self, list initlist=None, bint frozen=False):
        self.__frozen__ = False
        if initlist is not None:
            self.data = initlist
        else:
            self.data = []
        if frozen:
            self.freeze()
    def __repr__(self):
        return repr(self.data)
    def __richcmp__(self, other, int op):
        if isinstance(other, AttrList):
            other = (<AttrList>other).data
        if op==0: #<
            return self.data <  other
        elif op==1: #<=
            return self.data <= other
        elif op==2: #==
            return self.data == other
        elif op==3: #!=
            return self.data != other
        elif op==4: #>
            return self.data >  other
        elif op==5: #>=
            return self.data >= other
    # __cmp__ is removed in Python3 -
    # https://docs.python.org/3.0/whatsnew/3.0.html#ordering-comparisons
    # workaround: https://stackoverflow.com/questions/8276983/why-cant-i-use-the-method-cmp-in-python-3-as-for-python-2
    #def __cmp__(self, other):
    #    if isinstance(other, AttrList):
    #        other = (<AttrList>other).data
    #    return cmp(self.data, other)
    def __contains__(self, item):
        return item in self.data
    def __len__(self):
        return len(self.data)

    ''' slices were deprecated in Python 2.0 - not used in Python 3.x
    def __getitem__(self, i):
        return __asattrtype(self.__frozen__, self.data[i])
    def __setitem__(self, i, item):
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        self.data[i] = __asorgtype( item)
    def __delitem__(self, i):
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        del self.data[i]
    def __getslice__(self, i, j):
        i = max(i, 0); j = max(j, 0)
        return Attrlist.__new__(AttrList,self.data[i:j])
    def __setslice__(self, i, j, other):
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        i = max(i, 0); j = max(j, 0)
        if isinstance(other, AttrList):
            self.data[i:j] = (<AttrList>other).data
        elif isinstance(other, type(self.data)):
            self.data[i:j] = other
        else:
            self.data[i:j] = list(other)
    def __delslice__(self, i, j):
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        i = max(i, 0); j = max(j, 0)
        del self.data[i:j]
    '''

    def __getitem__(self, item):
        if isinstance(item, slice):
            i = max(item.start, 0)
            j = max(item.stop, 0)
            return Attrlist.__new__(AttrList,self.data[i:j])
        else:
            return __asattrtype(self.__frozen__, self.data[item])

    def __setitem__(self, item, value):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        if isinstance(item, slice):
            i = max(item.start, 0)
            j = max(item.stop, 0)
            if isinstance(value, AttrList):
                self.data[i:j] = (<AttrList>value).data
            elif isinstance(value, type(self.data)):
                self.data[i:j] = value
            else:
                self.data[i:j] = list(value)
        else:
            self.data[item] = __asorgtype(value)

    def __delitem__(self, item):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        if isinstance(item, slice):
            i = max(item.start, 0)
            j = max(item.stop, 0)
            del self.data[i:j]
        else:
            del self.data[item]

    def __add__(self, other):
        if isinstance(other, AttrList):
            return Attrlist.__new__(AttrList,self.data + (<AttrList>other).data)
        elif isinstance(other, type(self.data)):
            return Attrlist.__new__(AttrList,self.data + other)
        else:
            return Attrlist.__new__(AttrList,self.data + list(other))
    def __radd__(self, other):
        if isinstance(other, AttrList):
            return Attrlist.__new__(AttrList,(<AttrList>other).data + self.data)
        elif isinstance(other, type(self.data)):
            return Attrlist.__new__(AttrList,other + self.data)
        else:
            return Attrlist.__new__(AttrList,list(other) + self.data)
    def __iadd__(self, other):
        if isinstance(other, AttrList):
            self.data += (<AttrList>other).data
        elif isinstance(other, type(self.data)):
            self.data += other
        else:
            self.data += list(other)
        return self
    def __mul__(self, n):
        return Attrlist.__new__(AttrList,self.data*n)
    __rmul__ = __mul__
    def __imul__(self, n):
        self.data *= n
        return self
    def append(self, item):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        self.data.append(__asorgtype(item))
    def insert(self, i, item):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        self.data.insert(i, __asorgtype(item))
    def pop(self, i=-1):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        return __asattrtype(self.__frozen__, self.data.pop(i))
    def remove(self, item):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        self.data.remove(__asorgtype(item))
    def count(self, item):
        return self.data.count(__asorgtype(item))
    def index(self, item, *args):
        return self.data.index(__asorgtype(item), *args)
    def reverse(self):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        self.data.reverse()
    def sort(self, *args, **kwds):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        self.data.sort(*args, **kwds)
    def extend(self, other):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        self.data.extend(__asorgtype(other))
    cpdef AttrList __deepcopy__(AttrList self, dict m):
        return Attrlist.__new__(AttrList, loads(dumps(self.data)) )
    cpdef basestring __getstate__(AttrList self):
        return dumps(self.data)
    cpdef __setstate__(AttrList self, basestring state):
        assert state is not None
        self.data = loads(state)
        self.__frozen__ = False
    cpdef void freeze(AttrList self):
        self.__frozen__ = True


@cython.freelist(10000)
cdef class AttrDict:
    property __json__:
        def __get__(self):
            if self.__jsonstring__ is None or not self.__frozen__:
                self.__jsonstring__ = ujson.dumps(self.data, ensure_ascii=False)
            return self.__jsonstring__
    property __msgpack__:
        def __get__(self):
            if self.__msgpackstring__ is None or not self.__frozen__:
                self.__msgpackstring__ = dumps(self.data)
            return self.__msgpackstring__
    property __data__:
        def __get__(self):
            return self.data
    def __cinit__(self, dict initdict=None, bint frozen=False, basestring msgpack=None, basestring json=None):
        self.__frozen__ = False
        self.__msgpackstring__ = None
        self.__jsonstring__ = None
        if msgpack is not None:
            self.data = loads(msgpack)
        elif json is not None:
            self.data = ujson.loads(json)
        elif initdict is not None:
            self.data = initdict
        else:
            self.data = {}
        if frozen:
            self.freeze()
    def __repr__(self): return repr(self.data)
    # __cmp__ is removed in Python3 -
    # https://docs.python.org/3.0/whatsnew/3.0.html#ordering-comparisons
    # workaround: https://stackoverflow.com/questions/8276983/why-cant-i-use-the-method-cmp-in-python-3-as-for-python-2
    #def __cmp__(self, dict):
    #    if isinstance(dict, AttrDict):
    #        return cmp(self.data, (<AttrDict>dict).data)
    #    else:
    #        return cmp(self.data, dict)
    __hash__ = None # Avoid Py3k warning
    def __len__(self): return len(self.data)
    cpdef setnesteditem(self, list key, value):
        cdef dict t_dictionary
        cdef dict return_dict
        cdef int i, j
        last_key = key[-1]
        i = 0
        try:
            t_dictionary = self.data
            for i in range(len(key)-1):
                k = key[i]
                t_dictionary = t_dictionary[k]
        except KeyError:
            for j in range(i, len(key)-1):
                k = key[j]
                return_dict = {}
                t_dictionary[k] = return_dict
                t_dictionary = return_dict
        t_dictionary[last_key] = __asorgtype(value)
    cpdef getnesteditem(self, list key, failobj=KeyError):
        cdef dict data
        cdef int i=-1
        cdef len_key = len(key)
        assert len_key > 0
        data = self.data
        if len_key==1:
            if key[0] not in data:
                if failobj == KeyError:
                    raise KeyError(".".join(key))
                return failobj
            return __asattrtype(self.__frozen__, data[key[0]])
        else:
            for i in range(len_key-1):
                k = key[i]
                data = data[k]
            return __asattrtype(self.__frozen__, data[key[i+1]])
    cpdef tuple getnesteditems(self, list keys):
        cdef dict data
        cdef int i
        cdef list results = []
        for i in range(len(keys)):
            key = keys[i]
            if type(key) == list:
                data = self.data
                for i in range(len(key)-1):
                    k = (<list>key)[i]
                    data = data[k]
                results.append( __asattrtype(self.__frozen__, data[key[-1]]) )
            else:
                results.append( __asattrtype(self.__frozen__, self.data[key]) )
        return tuple(results)
    def __getitem__(self, basestring keys):
        return __asattrtype(self.__frozen__, self.data[keys])
    cpdef setitem(self, basestring key, item):
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        self.data[key] = __asorgtype(item)
    def __setitem__(self, basestring key, item):
        cdef dict tmp_dict
        cdef list tmp_list
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        item = __asorgtype(item)
        if isinstance(item, dict):
            tmp_dict = {}
            self.data[key] = tmp_dict
            dictmerge(tmp_dict, <dict>item)
        elif isinstance(item, list):
            tmp_list = []
            self.data[key] = tmp_list
            listmerge(tmp_list, <list>item)
        else:
            self.data[key] = item
    def __delitem__(self, key):
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        del self.data[key]
    def clear(self):
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        self.data.clear()
    def copy(self):
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        return AttrDict.__new__(AttrDict,self.data.copy())
    def keys(self): return self.data.keys()
    def items(self): return [(key, __asattrtype(self.__frozen__, val)) for key, val in self.data.items()]
    def iteritems(self): return ((key, __asattrtype(self.__frozen__, val)) for key, val in self.data.items())
    def iterkeys(self): return self.data.iterkeys()
    def itervalues(self): return (__asattrtype(self.__frozen__,val) for val in self.data.itervalues())
    def values(self): return [__asattrtype(self.__frozen__,val) for val in self.data.values()]
    def has_key(self, key): return key in self.data
    cpdef int merge(self, AttrDict adict) except -1:
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        dictmerge( self.data, adict.data )
        return 1
    cpdef int cp_update(self, AttrDict adict) except -1:
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        if adict is not None:
            self.data.update( adict.data )
        return 1
    def update(self, dict=None, **kwargs):
        if self.__frozen__: raise SwordfishMessageError("Message frozen, use deepcopy.")
        if dict is None:
            pass
        elif isinstance(dict, AttrDict):
            self.data.update( (<AttrDict>dict).data )
        elif isinstance(dict, type({})) or not hasattr(dict, 'items'):
            self.data.update(dict)
        else:
            for k, v in dict.items():
                self[k] = v
        if len(kwargs):
            self.update(kwargs)
    def get(self, key, failobj=None):
        if key not in self.data:
            return failobj
        return self[key]
    def setdefault(self, key, failobj=None):
        if key not in self.data:
            self[key] = failobj
        return self[key]
    def pop(self, key, *args): return self.__asatttritem( self.data.pop(key, *args) )
    def popitem(self): return self.__asatttritem( self.data.popitem() )
    def __contains__(self, key):
        return key in self.data
    @classmethod
    def fromkeys(cls, iterable, value=None):
        d = cls()
        for key in iterable:
            d[key] = value
        return d
    def __iter__(self):
        return iter(self.data)
    cpdef AttrDict __deepcopy__(AttrDict self, dict m):
        if len(self.data) == 0:
            return AttrDict.__new__(AttrDict)
        else:
            return AttrDict.__new__(AttrDict, msgpack=self.__msgpack__)
    def __getattr__(self, basestring k):
        try:
            return self[k]
        except KeyError:
            raise AttributeError(k)
    def __setattr__(self, basestring k, v):
        self[k] = v
    cpdef basestring __getstate__(AttrDict self):
        return self.__msgpack__
    cpdef __setstate__(AttrDict self, basestring state):
        assert state is not None
        self.data = loads(state)
        self.__frozen__ = False
    cpdef void freeze(AttrDict self):
        self.__frozen__ = True

cdef class IG_I:
    cdef list keys
    cdef int keys_len
    cdef basestring last_key
    cdef list key_ors
    cdef basestring default
    def __cinit__(self, list keys, list key_ors=[], list key_ands=[], basestring default=None):
        self.keys = keys
        self.keys_len = len(keys)
        self.last_key = keys[-1] if len(keys)>0 else ""
        self.key_ors = key_ors
        self.default = default
    cpdef itemgetter_in(self, AttrDict dictionary):
        assert dictionary is not None
        if self.keys_len == 0: return None
        try:
            if self.keys_len == 1:
                return dictionary[self.last_key]
            else:
                return dictionary.getnesteditem(self.keys)
        except (KeyError, TypeError):
            raise KeyError(".".join(self.keys))
    cpdef itemgetter_in_questionmark(self, AttrDict dictionary):
        assert dictionary is not None
        try:
            if self.keys_len == 1:
                return dictionary[self.last_key]
            else:
                return dictionary.getnesteditem(self.keys, self.default)
        except (KeyError, TypeError):
            return self.default
    cpdef itemgetter_in_or(self, AttrDict dictionary):
        assert dictionary is not None
        for keys in self.key_ors:
            try:
                if len(keys) == 1:
                    return dictionary[keys[0]]
                else:
                    return dictionary.getnesteditem(keys)
            except (KeyError, TypeError):
                continue
        raise KeyError("|".join([".".join(keys) for keys in self.key_ors]))
    cpdef itemgetter_in_star(self, AttrDict dictionary):
        cdef basestring key
        cdef AttrDict tmpdict
        assert dictionary is not None
        tmpdict = dictionary
        for key in self.keys[:-1]:
            tmpdict = tmpdict[key]
        try:
            return tmpdict[self.last_key]
        except KeyError:
            return None
    cpdef itemgetter_in_tilde(self, AttrDict dictionary):
        cdef basestring key
        cdef AttrDict tmpdict
        assert dictionary is not None
        tmpdict = dictionary
        try:
            if self.keys_len == 1:
                dictionary[self.last_key]
            else:
                dictionary.getnesteditem(self.keys)
        except KeyError:
            return None
        raise KeyError

cpdef itemgetter_in_identity( AttrDict dictionary ):
    return dictionary

cpdef itemgetter_in(list keys): #list of lists
    """
    wildcards can only be used in the last key for now
    {keys[-1]}* matches zero or one occurances of key[-1] returns None if zero else raises KeyError
    {keys[-1]}~ matches zero occurances of key[-1] returns None if zero else raises KeyError
    {keys}? matches zero or one occurances of keys returns None if zero or the value after ?
    """
    assert len(keys) >= 1
    if len(keys) == 1:
        keys = keys[0]
        assert(type(keys) == list)
        if len(keys)>0:
            last_key = keys[-1]
            if last_key.endswith("?"):
                keys[-1] = last_key[:-1]
                return IG_I(keys).itemgetter_in_questionmark
            elif len(last_key.split("?")) == 2:
                keys[-1], default = last_key.split("?")
                return IG_I(keys, default=default).itemgetter_in_questionmark
            elif last_key.endswith("*"):
                keys[-1] = last_key[:-1]
                return IG_I(keys).itemgetter_in_star
            elif last_key.endswith("~"):
                keys[-1] = last_key[:-1]
                return IG_I(keys).itemgetter_in_tilde
            else:
                return IG_I(keys).itemgetter_in
        else:
            return itemgetter_in_identity
    else:
        return IG_I(keys=[], key_ors=keys).itemgetter_in_or

cdef class IS_I:
    cdef list keys
    cdef basestring last_key
    cdef int keys_len
    cdef AttrDict empty_dict
    def __init__(self, list keys):
        cdef AttrDict point_dict
        self.keys = keys
        self.keys_len = len(keys)
        self.empty_dict = AttrDict.__new__(AttrDict,{})
        self.last_key = keys[-1] if len(keys)>0 else ""
        if len(keys)>0:
            point_dict = self.empty_dict
            for key in keys[:-1]:
                point_dict[key] = {}
                point_dict = point_dict[key]
            point_dict[self.last_key] = None
    cpdef itemsetter_in(self, AttrDict dictionary, value):
        cdef basestring key
        cdef AttrDict t_dictionary
        cdef AttrDict return_dict
        assert dictionary is not None
        if isinstance(value, tuple):
            value = value[0]
        if self.keys_len == 1:
            dictionary[self.last_key] = value
        else:
            dictionary.setnesteditem(self.keys, value)

cpdef itemsetter_in_identity(AttrDict dictionary, value):
    assert dictionary is not None
    if isinstance(value, tuple):
        dictionary.cp_update(value[0])
    else:
        dictionary.cp_update(value)

cpdef itemsetter_in(list keys):
    assert keys is not None
    if len(keys)==0:
        return itemsetter_in_identity
    else:
        return IS_I(keys).itemsetter_in

class Builder(dict):
    """A dictionary with attribute-style access. It maps attribute access to the real dictionary.  """
    def __init__(self, name=None, func=identity):
        self.__attr_name = name
        self.__indexattrable__ = False
        if name:
            self.func = itemgetter_in( [(self.__attr_name+"?").split('.')] )
        else:
            self.func = func

    def __getstate__(self):
        return self.__dict__.items()

    def __setstate__(self, items):
        for key, val in items:
            self.__dict__[key] = val

    def __getitem__(self, name):
        if self.__attr_name:
            super(Builder, self).__setitem__(name, Builder("%s.%s"%(self.__attr_name, name)) )
        else:
            super(Builder, self).__setitem__(name, Builder("%s"%(name)) )
        item = super(Builder, self).__getitem__(name)
        return item

    def __lt__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_) < other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_) < other) )

    def __le__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_) <= other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_) <= other) )

    def __eq__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_) == other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_) == other) )

    def __ne__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_) <> other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_) <> other) )

    def __gt__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_) > other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_) > other) )

    def __ge__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_) >= other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_) >= other) )

    def __add__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_)  + other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_)  + other) )

    def __sub__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_)  - other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_)  - other) )

    def __mul__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_)  * other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_)  * other) )

    def __floordiv__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_) // other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_) // other) )

    def __mod__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_)  % other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_)  % other) )

    def __pow__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_) ** other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_) ** other) )

    def __in__(self, other):
        if callable(other):
            return Builder(func=(lambda _: self.func(_) in other(_) ) )
        else:
            return Builder(func=(lambda _: self.func(_) in other) )

#    def __lshift__(self, other):
#        return BooleanBuilder(func=(lambda _: self.func(_) << other) )
#
#    def __rshift__(self, other):
#        return BooleanBuilder(func=(lambda _: self.func(_) >> other) )

    def __not__(self):
        return Builder(func=(lambda _: not self.func(_) ) )

    def __and__(self, other):
        if isinstance(other, bool):
            return Builder(func=(lambda _: self.func(_)  & other ) )
        elif callable(other):
            return Builder(func=(lambda _: self.func(_)  & other(_) ) )
        else:
            raise TypeError()

    def __xor__(self, other):
        if isinstance(other, bool):
            return Builder(func=(lambda _: self.func(_)  ^ other ) )
        elif callable(other):
            return Builder(func=(lambda _: self.func(_)  ^ other(_) ) )
        else:
            raise TypeError()

    def __or__(self, other):
        if isinstance(other, bool):
            return Builder(func=(lambda _: self.func(_)  | other ) )
        elif callable(other):
            return Builder(func=(lambda _: self.func(_)  | other(_) ) )
        else:
            raise TypeError()

    def __call__(self, addmsg=None, delmsg=None, win=None, oldval=None, span=None, rate=None):
        return self.func(addmsg)

    __getattr__ = __getitem__

class SwordfishHandler(logging.Handler):
    """
    A handler class which sends log strings to an errorstream
    """
    def __init__(self, errorstream):
        """
        Initialize the handler
        @param errorstream: the destination stream for errors
        @type errorstream: stream.Stream
        """
        self.errorstream = errorstream
        logging.Handler.__init__(self)

    def flush(self):
        """
        does nothing for this handler
        """

    def emit(self, record):
        """
        Emit a record.

        """
        try:
            msg = self.format(record)
            self.errorstream(AttrDict.__new__(AttrDict,eval(msg)))
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

cdef class Stream(object):
    '''
    Represents the connection mecahnism and flow of messages between Processing
    Blocks e.g. StreamProcessors or StreamFilters
    params
    url: the url of the stream i.e file://foo.bar or ws://foo.bar.com
    timeout: the time in second that next will block for until it sends a None
    '''
    def __cinit__(self):
        self.pipe = {}
        self.context = {}
        self.sb = None
        self.__closed__ = False
        self.__stream_id__ = uuid.uuid4().hex

    def __init__(self, basestring url="", float timeout=1.0, bint bridge=True, *args, **kwargs):
        self.url = url
        self.timeout = timeout
        self.debug_name = url
        if bridge:
            self.sb = StreamBridge(debug_name=self.debug_name, default_stream=self)
            self.sb>>self
        #Initiate logging framework
        self.logger = logging.getLogger('%s.%s.%s'%(self.__class__.__module__, self.__class__.__name__, self.debug_name) )

    property stream_id:
        def __get__(self):
            return self.__stream_id__

    property closed:
        def __get__(self):
            return self.__closed__

    def __hash__(self):
        return id(self)

    #For RPyC
    def __proxy_attach__(self, obj, callback):
        if self.closed:
            raise SwordfishStreamError("Stream Already Closed")
        if issubclass( callback.__class__, basestring ):
            context = zmq.Context()
            pipe = context.socket(zmq.PAIR)
            pipe.set_hwm(10000) #approximatly 10MB at 1KB messages
            pipe.connect(callback)
            pipe.send(b"ello")
            def proxycallback(AttrDict msg):
                pipe.send(msg.__msgpack__)
            self.pipe[obj] = pipe
            self.context[obj] = context
            self.__attach__(obj, proxycallback)
        else:
            self.__attach__(obj, callback)

    #For RPyC
    def __proxy_detatch__(self, obj):
        if self.closed:
            return
        try:
            self.pipe[obj].close()
            self.context[obj].term()
            del self.pipe[obj]
            del self.context[obj]
        except KeyError as e:
            pass
        self.__detatch__(obj)

    #Required to be a Stream
    def __attach__(self, obj, callback):
        raise NotImplementedError("Please Implement this method")

    #Required to be a Stream
    def __detatch__(self, obj):
        raise NotImplementedError("Please Implement this method")

    def cleanup(self):
        raise NotImplementedError("Please Implement this method")

    def flush(self):
        raise NotImplementedError("Please Implement this method")

    cpdef close(self):
        for obj in self.pipe.keys():
            self.__proxy_detatch__(obj)
        self.sb<>self
        self.sb.close()
        self.cleanup()
        self.__closed__ = True

    #Required to be a Stream
    #message is an AttrDict
    def __call__(self, AttrDict message):
        if self.closed:
            raise SwordfishStreamError("Stream Already Closed")
        raise NotImplementedError("Please Implement this method")

    #Required for context with
    def __enter__(self):
        if self.closed:
            raise SwordfishStreamError("Stream Already Closed")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    #This is the primary stream to stream |
    def __or__(self, item):
        if self.closed:
            raise SwordfishStreamError("Stream Already Closed")
        if issubclass( item.__class__, Stream ):
            self>>item.sb
            self.debug( "bridged stream to stream %s", item.debug_name )
        else:
            raise TypeError("unsupported operand type(s) for >>: %s and %s"%(self, item))
        return item

    #This is the primary one >>
    def __rshift__(self, item):
        if self.closed:
            raise SwordfishStreamError("Stream Already Closed")
        if issubclass( item.__class__, StreamProcessor ):
            item._connect_to_streams_output( self )
            self.debug( "connected to streamprocessor %s", item.debug_name )
        elif issubclass( item.__class__, Stream ):
            raise TypeError("unsupported operand type(s) for >>: %s and %s"%(self, item))
        elif callable(item):
            item = filter(item, anonymous=True, debug_name="AnonymousFilter")
            self>>item
        else:
            raise TypeError("unsupported operand type(s) for >>: %s and %s"%(self, item))
        return item

    def __richcmp__(self, item, comp): #__ne__
        if self.closed:
            raise SwordfishStreamError("Stream Already Closed")
        if comp == 3:  #__ne__
            if issubclass( item.__class__, Stream ):
                self<>item.sb
                self.debug( "disconnected bridged streams" )
            elif issubclass( item.__class__, StreamProcessor ):
                item._disconnect_from_streams_output( self )
                self.info( "disconnected from streamprocessor %s", item.debug_name )
            elif issubclass( item.__class__, basestring ):
                raise NotImplementedError("Not implemented yet")
            else:
                raise TypeError("unsupported operand type(s) for <>: %s and %s"%(self, item))

    cdef _info(self, basestring debug_message, tuple args, dict kwargs):
        self.logger.info(debug_message, *args, **kwargs)

    cdef _critical(self, basestring debug_message, tuple args, dict kwargs):
        self.logger.critical(debug_message, *args, **kwargs)

    cdef _warning(self, basestring debug_message, tuple args, dict kwargs):
        self.logger.warning(debug_message, *args, **kwargs)

    cdef _error(self, basestring debug_message, tuple args, dict kwargs):
        self.logger.error(debug_message, *args, **kwargs)

    cdef _debug(self, basestring debug_message, tuple args, dict kwargs):
        self.logger.debug( debug_message, *args, **kwargs )

    def info(self, basestring debug_message, *args, **kwargs ):
        self._info(debug_message, args, kwargs)

    def warning(self, basestring debug_message, *args, **kwargs ):
        self._warning(debug_message, args, kwargs)

    def critical(self, basestring debug_message, *args, **kwargs ):
        self._critical(debug_message, args, kwargs)

    def error(self, basestring debug_message, *args, **kwargs ):
        self._error(debug_message, args, kwargs)

    def debug(self, basestring debug_message, *args, **kwargs ):
        self._debug( debug_message, args, kwargs)

cdef class SplitBifurcation(Stream):
    cdef public dict callbacks
    def __cinit__(self):
        self.callbacks = {}

    def __init__(self, basestring url="", float timeout=1.0, int bifurcations=1, bifurcation_filter=None):
        Stream.__init__(self, url, timeout)

    #Required to be a Stream
    def __attach__(self, obj, callback):
        self.callbacks[obj] = callback

    #Required to be a Stream
    def __detatch__(self, obj):
        del self.callbacks[obj]

cdef class BroadcastBifurcation(Stream):
    cdef public dict callbacks
    def __cinit__(self):
        self.callbacks = {}

    def __init__(self, basestring url="", float timeout=1.0, bifurcations=1, bifurcation_filter=None):
        Stream.__init__(self, url, timeout)

    #Required to be a Stream
    def __attach__(self, obj, callback):
        self.callbacks[obj] = callback

    #Required to be a Stream
    def __detatch__(self, obj):
        del self.callbacks[obj]

cdef class DefaultStream(Stream):
    def __cinit__(self):
        self.callbacks = {}
        self.anonymous_callback = None

    def __init__(self, basestring url="", float timeout=1.0, object anonymous_callback=None, bint bridge=True):
        Stream.__init__(self, url=url, timeout=timeout, bridge=bridge)
        self.anonymous_callback = anonymous_callback

    def cleanup(self):
        for obj in self.callbacks.keys():
            del self.callbacks[obj]
        if not self.anonymous_callback is None and len(self.callbacks) == 0:
            anonymous_callback = self.anonymous_callback
            self.anonymous_callback = None
            anonymous_callback()

    def __attach__(self, obj, callback):
        self.callbacks[obj] = callback

    def __detatch__(self, obj):
        del self.callbacks[obj]
        if not self.anonymous_callback is None and len(self.callbacks) == 0:
            anonymous_callback = self.anonymous_callback
            self.anonymous_callback = None
            anonymous_callback()

    def __call__(self, AttrDict message):
        for callback in self.callbacks.values():
            callback( message )

cdef class StreamProcessor(object):
    def __cinit__(self):
        self.__timeout = 0.1 #Timeout on the _execute queue
        self.output_streams =  {}
        self.input_streams = {} #value is a tuple (stream, stream_order, context, pipe)
        self.__stop = threading.Event()
        self.__stop.set()
        self.__process = threading.Event()
        self.queue = deque()
        self.results = None
        self._sink = False
        self._source = False
        self.messages_processed = 0
        self.__execute_thread = None
        self._default_stream = None
        self.misses = 0
        self.__closed__ = False

    def __init__(self, basestring debug_name="", bint anonymous=False, bint async=False, Stream default_stream=None, *args, **kwargs):
        self.async = async
        #If anonymous need to be cleaned up when my default stream is
        #cleaned up, i.e my default stream has no more connections
        #I will then close myself if I have only the default_stream as
        #an output_stream
        self.anonymous = anonymous
        self.debug_name = debug_name
        self._default_stream = default_stream

        #Initiate logging framework
        if self.debug_name == "":
            if self.__class__.__module__.startswith("swordfish."):
                self.logger = logging.getLogger('%s.%s'%(self.__class__.__module__, self.__class__.__name__) )
            else:
                self.logger = logging.getLogger('swordfish.%s.%s'%(self.__class__.__module__, self.__class__.__name__) )
        else:
            if self.__class__.__module__.startswith("swordfish."):
                self.logger = logging.getLogger('%s.%s.%s'%(self.__class__.__module__, self.__class__.__name__, self.debug_name) )
            else:
                self.logger = logging.getLogger('swordfish.%s.%s.%s'%(self.__class__.__module__, self.__class__.__name__, self.debug_name) )
        if self.debug_name == "":
            self.errorstream = DefaultStream("errorstream", bridge=False)
        else:
            self.errorstream = DefaultStream("errorstream.%s"%self.debug_name, bridge=False)
        sh = SwordfishHandler( self.errorstream )
        #https://google-styleguide.googlecode.com/svn/trunk/jsoncstyleguide.xml#error
        formatter = logging.Formatter(
        '{"apiVersion": "2.1", "error":{"message":"Swordfish Error","code":0,"errors":[{"message":"%(message)s", "time":"%(asctime)s", "domain":"%(name)s", "reason":"%(levelname)s"}]}}'
        )
        sh.setFormatter(formatter)
        sh.setLevel(logging.INFO)
        self.logger.addHandler(sh)

    property stopEvent:
        def __get__(self):
            return self.__stop
    property processEvent:
        def __get__(self):
            return self.__process
    property closed:
        def __get__(self):
            return self.__closed__

    def __hash__(self):
        return id(self)

    def __anonymous_callback(self):
        if self.anonymous and len(self.output_streams)==1:
            self.debug( "Anonymous Cleanup" )
            self.clear()
            self.anonymous = False

    def __kill_thread(self):
        if not self.__stop.is_set():
            self.__stop.set()
            self.__process.set()
            self.__execute_thread.join()

    def __startup_thread(self):
        if self.__stop.is_set():
            self.debug( "Starting __execute thread" )
            self.__stop.clear()
            self.__execute_thread = threading.Thread(name=self.debug_name, target=self.__execute, args=(self.async,))
            self.__execute_thread.start()

    cpdef int __process_result(self, object results) except -1:
        cdef AttrDict result
        self.messages_processed += 1
        if isinstance(results, AttrDict):
            result = <AttrDict>results
            result.freeze()
            for callback_method in self.output_streams.values():
                try:
                    callback_method( result )
                except Exception as e:
                    self.error("Clean this callback out? It's throwing around exceptions")
                    self.error(e)
                    try:
                        self._disconnect_from_streams_input( callback_method )
                    except Exception:
                        pass
                    raise
            self.results = [result,]
        elif isinstance(results, list):
            for result_itm in results:
                if not isinstance(result_itm, AttrDict):
                    raise SwordfishStreamProcessorError("self.process returned invalid message, must be AttrDict or AttrList %s"%results)
                else:
                    result = <AttrDict>result_itm
                    result.freeze()
            for callback_method in self.output_streams.values():
                try:
                    for result_itm in results:
                        result = <AttrDict>result_itm
                        callback_method( result )
                    #builtins.map( callback_method, results )
                except Exception as e:
                    self.error("Clean this callback out? It's throwing around exceptions")
                    self.error(e)
                    try:
                        self._disconnect_from_streams_input( callback_method )
                    except Exception:
                        pass
                    raise
            self.results = results
        elif results is None:
            pass
        else:
            raise SwordfishStreamProcessorError("self.process returned invalid message, must be AttrDict or AttrList: %s"%results)
        return 0

    cpdef int __execute(self, bint async=False) except -1:
        cdef AttrDict message
        cdef basestring stream_id
        get = self.queue.popleft
        #get = self.queue.pop
        __process = self.__process
        __process_result = self.__process_result
        is_set = self.__stop.is_set
        if async:
            process = self.async_process
            while not isSet():
                #try:
                if not self.queue.empty():
                    stream_id, message = get()
                else:
                #except IndexError: #Empty:
                    #This should only ever happen if we want to process
                    #when we have no messages or we are shutting down
                    self.misses += 1
                    __process.clear()
                    __process.wait(self.__timeout)
                    continue
                try:
                    message.freeze()
                except Exception:
                    self.critical("self.process returned message: %s should be a AttrDict", message)
                    raise
                try:
                    process( message, stream_id, __process_result)
                except Exception as e:
                    self.critical(e)
                    raise
        else:
            process = self.process
            while not is_set():
                try:
                    stream_id, message = get()
                except IndexError: #Empty:
                    #This should only ever happen if we want to process
                    #when we have no messages or we are shutting down
                    self.misses += 1
                    __process.clear()
                    __process.wait(self.__timeout)
                    continue
                try:
                    message.freeze()
                except Exception:
                    self.critical("self.process returned message: %s should be a AttrDict", message)
                    raise
                try:
                    __process_result(process( message, stream_id ))
                except Exception as e:
                    self.critical(e)
                    raise
        if is_set():
            self.debug( "__stop.is_set True" )
        return 0

    def __enter__(self):
        if self.closed:
            raise SwordfishStreamProcessorError("StreamProcessor Already Closed")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def __del__(self):
        self.close()

    cpdef _get_default_stream(self):
        if self._default_stream is None:
            if self.debug_name == "":
                self._default_stream = DefaultStream("errorstream", 0.0, self.__anonymous_callback)
            else:
                self._default_stream = DefaultStream("errorstream.%s"%self.debug_name, 0.0, self.__anonymous_callback)
            self._connect_to_streams_input( self._default_stream )
        return self._default_stream

    def cleanup(self):
        pass

    cpdef close(self):
        self.cleanup()
        self.clear()
        self.__closed__ = True
        #Finally lets see if we are a proxy object and we need to shut down our connection
        try:
            self.____conn__.close()
        except AttributeError:
            pass

    cpdef clear(self):
        self.debug( "Clearing StreamProcessor" )
        for stream_id in self.input_streams.keys():
            try:
                stream, _, _, _ = self.input_streams[stream_id]
                self._disconnect_from_streams_output( stream )
            except ReferenceError as re:
                del self.input_streams[stream_id]
                self.debug(re)
        #Execute thread should die once all input streams are cleared
        for stream in self.output_streams.values():
            self._disconnect_from_streams_input( stream )
        if not self._default_stream is None:
            if type(self) != StreamBridge:
                self._default_stream.close()
            self._default_stream = None

    cdef _info(self, basestring debug_message, tuple args, dict kwargs):
        self.logger.info(debug_message, *args, **kwargs)

    cdef _critical(self, basestring debug_message, tuple args, dict kwargs):
        self.logger.critical(debug_message, *args, **kwargs)

    cdef _warning(self, basestring debug_message, tuple args, dict kwargs ):
        self.logger.warning(debug_message, *args, **kwargs)

    cdef _error(self, basestring debug_message, tuple args, dict kwargs):
        self.logger.error(debug_message, *args, **kwargs)

    cdef _debug(self, basestring debug_message, tuple args, dict kwargs):
        self.logger.debug( debug_message, *args, **kwargs )

    def info(self, debug_message, *args, **kwargs):
        self.logger.info(debug_message, *args, **kwargs)

    def critical(self, debug_message, *args, **kwargs):
        self.logger.critical(debug_message, *args, **kwargs)

    def warning(self, debug_message, *args, **kwargs ):
        self.logger.warning(debug_message, *args, **kwargs)

    def error(self, debug_message, *args, **kwargs):
        self.logger.error(debug_message, *args, **kwargs)

    def debug(self, debug_message, *args, **kwargs):
        self.logger.debug( debug_message, *args, **kwargs )

    #This is the primary one >>
    def __rshift__(self, item):
        if not issubclass(self.__class__, StreamProcessor):
            #Hack to overcome cython funny behaviour with rrshift
            return item.rrshift( self )
        else:
            if self.closed:
                raise SwordfishStreamProcessorError("StreamProcessor Already Closed")
            if issubclass(item.__class__, Stream):
                self._connect_to_streams_input( item )
                self.info( "%s connected to stream %s"%(self.debug_name, item.debug_name) )
                return item
            elif issubclass(item.__class__, StreamProcessor):
                item._connect_to_streams_output( self._get_default_stream() )
                self.info( "%s connected to streamprocessor %s"%(self.debug_name, item.debug_name) )
                return item
            elif callable(item):
                item = filter(item, anonymous=True, debug_name="AnonymousFilter")
                item._connect_to_streams_output( self._get_default_stream() )
                self.info( "%s connected to streamprocessor %s"%(self.debug_name, item.debug_name) )
                return item
            elif issubclass( item.__class__, basestring ):
                raise TypeError("unsupported operand type(s) for >>: %s and %s"%(self, item))
            else:
                raise TypeError("unsupported operand type(s) for >>: %s and %s"%(self, item))

    #This is the one that fires if the other param is a tupple or something
    def __rrshift__(self, item):
        return self.rrshift(item)

    def rrshift(self, item):
        if self.closed:
            raise SwordfishStreamProcessorError("StreamProcessor Already Closed")
        if issubclass( item.__class__, tuple ):
            for idx, ite in enumerate(item):
                if issubclass(ite.__class__, StreamProcessor):
                    self._connect_to_streams_output( ite._get_default_stream(), idx )
                    self.info( "%s connected to streamprocessor %s"%(ite.debug_name, self.debug_name) )
                elif issubclass(ite.__class__, Stream):
                    self._connect_to_streams_output( ite, idx )
                    self.info( "%s connected to stream %s"%(ite.debug_name, self.debug_name) )
            return self
        elif issubclass( item.__class__, basestring):
            raise NotImplementedError("Can't connect strings properly yet")
        else:
            raise TypeError("unsupported operand type(s) for >>: %s and %s"%(item, self))

    #This is the disconnect one <>
    #Only works in one direction left side disconnects from right side
    #If right side is a stream disconnect from it
    def __richcmp__(self, item, comp): #__ne__
        if comp == 3:  #__ne__
            if issubclass( item.__class__, Stream ):
                self._disconnect_from_streams_input( item )
                self.info( "disconnected stream" )
            elif issubclass( item.__class__, StreamProcessor ):
                item._disconnect_from_streams_output( self._get_default_stream() )
                self.info( "disconnected streamprocesses" )
            elif issubclass( item.__class__, basestring ):
                raise NotImplementedError("Not implemented yet")
            else:
                raise TypeError("unsupported operand type(s) for <>: %s and %s"%(self, item))

    def _connect_to_streams_output( self, stream, stream_order=-1 ):
        self.debug( "_connect_to_streams_output" )
        if self.closed:
            raise SwordfishStreamProcessorError("StreamProcessor Already Closed")
        put = self.queue.append
        #put = self.queue.pop
        __process = self.__process
        __stop = self.__stop
        context = None
        pipe = None
        try:
            assert(stream.__metaclass__.__name__ == 'NetrefMetaclass')
            context = zmq.Context()
            pipe_name = 'ipc:///tmp/%s'%uuid.uuid4().hex
            pipe = context.socket(zmq.PAIR)
            pipe.set_hwm(10000) #approximatly 100MB at 1KB messages
            pipe.bind(pipe_name)
            stream.__proxy_attach__( self, pipe_name )
            pipe.recv()
            def piper():
                stream_id = stream.stream_id
                poller = zmq.Poller()
                poller.register(pipe, zmq.POLLIN)
                try:
                    while True:
                        if poller.poll(1000):
                            msg = AttrDict.__new__(AttrDict, msgpack=pipe.recv(zmq.DONTWAIT))
                            if not __stop.is_set():
                                put( (stream_id, msg) )
                                if not __process.is_set():
                                    __process.set()
                except zmq.ZMQError:
                    pipe.close()
                    pass
            thread = threading.Thread(target=piper)
            thread.start()
        except AttributeError, AssertionError:
            def callback(AttrDict msg):
                cdef basestring stream_id
                stream_id = stream.stream_id
                if not __stop.is_set():
                    put( (stream_id, msg) )
                    if not __process.is_set():
                        __process.set()
            stream.__proxy_attach__( self, callback )
        self.input_streams[stream.stream_id] = (stream, stream_order, context, pipe)
        if (len(self.output_streams) > 0 and len(self.input_streams) > 0) or self._sink:
            self.__startup_thread()

    def _connect_to_streams_input(self, stream):
        self.debug( "_connect_to_streams_input" )
        if self.closed:
            raise SwordfishStreamProcessorError("StreamProcessor Already Closed")
        try:
            assert(stream.__metaclass__.__name__ == 'NetrefMetaclass')
            stream.sb._connect_to_streams_output( self._get_default_stream() )
        except AttributeError, AssertionError:
            self.output_streams[stream.stream_id] = stream
        if  (len(self.output_streams) > 0 and len(self.input_streams) > 0) or self._source:
            self.__startup_thread()
#        if not self.results is None:
#            for result in self.results:
#                stream(result)

    def _disconnect_from_streams_output(self, stream):
        self.debug( "_disconnect_from_streams_output" )
        stream.__proxy_detatch__( self )
        _, _, context, pipe = self.input_streams[stream.stream_id]
        if pipe is not None:
            pipe.close()
        if context is not None:
            context.term()
        del self.input_streams[stream.stream_id]
        if (not self._source) and len(self.input_streams) == 0:
            self.__kill_thread()

    def _disconnect_from_streams_input(self, stream):
        self.debug( "_disconnect_from_streams_input" )
        try:
            assert(stream.__metaclass__.__name__ == 'NetrefMetaclass')
            stream.sb._disconnect_from_streams_output( self._get_default_stream() )
        except AttributeError, AssertionError:
            del self.output_streams[stream.stream_id]
        if (not self._sink) and len(self.output_streams) == 0:
            self.__kill_thread()

    cpdef _enqueue_message(self, AttrDict msg):
        # -1: didn't come from a stream
        # False: Not a delete message
        if not self.__stop.is_set():
            self.queue.append( ("", msg) )
            if not self.__process.is_set():
                self.__process.set()

    #The order in which input streams are attached gives them there id with
    #stream 0 being the first if more than one input stream is attached using a
    #iterable such as a tuple then the id's are assigned in the ordering of the
    #itreable
    #message is an AttrDict
    cpdef process(self, AttrDict message, basestring stream_id):
        '''override with own function
           return your result of processing the message'''
        raise NotImplementedError("Please Implement this method to add functionality")

    cpdef async_process(self, AttrDict message, basestring stream_id, callback):
        '''override with own function
           return your result of processing the message'''
        raise NotImplementedError("Please Implement this method to add functionality")

ctypedef object (*cfptr_boolean_function)(AttrDict)

cdef class filter(StreamProcessor):
    cdef object boolean_function
    def __init__(self, boolean_function, *args, **kwargs ):
        StreamProcessor.__init__(self, *args, **kwargs )
        self.boolean_function = boolean_function

    cpdef process(self, AttrDict message, basestring stream_id):
        try:
            if self.boolean_function(message):
                return message
            else:
                return None
        except KeyError:
            return None

cdef class sink(StreamProcessor):
    cdef object function
    def __init__(self, function, *args, **kwargs ):
        StreamProcessor.__init__(self, *args, **kwargs )
        self.function = function

    cpdef process(self, AttrDict message, basestring stream_id):
        self.function(message)
        return None

cdef class StreamBridge(StreamProcessor):
    cpdef process(self, AttrDict message, basestring stream_id):
        return message

listin = lambda ls, ks: reduce(iand, (i in ks for i in ls) , True)

cdef class NewFunc:
    cdef IG_I ig_i
    cdef list addargs
    cdef dict addkwargs
    cdef oldval
    cdef span
    cdef rate
    cdef wg_i
    def __cinit__(self, IG_I ig_i, list addargs, dict addkwargs, oldval, span, rate):
        self.ig_i = ig_i
        self.addargs = addargs
        self.addkwargs = addkwargs
        self.oldval = oldval
        self.span = span
        self.rate = rate

        self.wg_i = lambda win: (ig_i(i) for i in win)

    cpdef newfuncA(self, AttrDict addmsg=None, AttrDict delmsg=None, win=None, oldval=None, span=None, rate=None):
        if not addmsg is None: return self.ig_i.itemgetter_selector_in(addmsg)

    def newfuncB(self, AttrDict addmsg=None, AttrDict delmsg=None, win=None, oldval=None, span=None, rate=None):
        if oldval is None: oldval = self.oldval
        if span is None: oldval = self.span
        if rate is None: oldval = self.rate
        if not win is None: win = wg_i(win)
        if not addmsg is None:
            return func(addmsg=ig_i(addmsg), win=win, oldval=oldval, span=span, rate=rate, *self.addargs, **self.addkwargs)
        elif not delmsg is None:
            return func(delmsg=ig_i(delmsg), win=win, oldval=oldval, span=span, rate=rate, *self.addargs, **self.addkwargs)

def _make_getter_able(func, attr_name, addargs=None, addkwargs=None):
    if addargs is None: addargs = []
    if addkwargs is None: addkwargs = {}
    if attr_name=='_':
        ig_i = itemgetter_in( [[]] )
    else:
        ig_i = itemgetter_in( [ attr_name_or.split('.') for attr_name_or in attr_name.split('|') ] )
    wg_i = lambda win: (ig_i(i) for i in win)

#    cdef IG_I ig_i_o
#    ig_i_o = itemgetter_in_object( [[] if attr_name=='_' else attr_name_or.split('.') for attr_name_or in attr_name.split('|') ] )
        #newfunc = NewFunc(ig_i_o, addargs, addkwargs, None, None, None).newfuncA

    if func is None:
        #we just use the getter func
        def newfunc(AttrDict addmsg=None, **ignore_kwargs):
            if not addmsg is None: return ig_i(addmsg)
        newfunc.__name__ = "itemgetter_in"
    else:
        try:
            newfunc__name__ = func.__name__
        except AttributeError:
            newfunc__name__ =  func.__class__.__name__

        try:
            argspec = getargspec(func)
        except TypeError:
            argspec = getargspec(func.__call__)
            argspec.args.remove('self')
        #arg_count = len(argspec.args)

        if not argspec.defaults is None:
            ks = {argspec.args[-i-1]:argspec.defaults[-i-1] for i in xrange(len(argspec.defaults))}
        else:
            ks = {}

        for param in ['addmsg','delmsg', 'win', 'oldval', 'span', 'rate']:
            if param in argspec.args and not param in ks:
                raise SwordfishSyntaxError("%s() parameter %s must have default value if present"%(newfunc__name__, param))

        if not 'addmsg' in ks and not 'delmsg' in ks:
            raise SwordfishSyntaxError('Invalid function signature, must have one of addmsg or delmsg')

        if 'win' in ks or 'span' in ks or 'rate' in ks:
            if not listin(['win', 'span', 'rate'], ks):
                raise SwordfishSyntaxError("%s() must have all of parameters 'win', 'span' and 'rate'"%(newfunc__name__))

        oldval=ks.get('oldval', None); span=ks.get('span', None); rate=ks.get('rate', None)

        if listin(['addmsg', 'delmsg', 'win', 'oldval', 'span', 'rate'], ks):
            def newfunc(AttrDict addmsg=None, AttrDict delmsg=None, win=None, oldval=oldval, span=span, rate=rate):
                if not addmsg is None:
                    win = wg_i(win) if not win is None else ks['win']
                    return func(addmsg=ig_i(addmsg), win=win, oldval=oldval, span=span, rate=rate, *addargs, **addkwargs)
                elif not delmsg is None:
                    win = wg_i(win) if not win is None else ks['win']
                    return func(delmsg=ig_i(delmsg), win=win, oldval=oldval, span=span, rate=rate, *addargs, **addkwargs)
        elif listin(['addmsg', 'delmsg', 'win', 'span', 'rate'], ks):
            def newfunc(AttrDict addmsg=None, AttrDict delmsg=None, win=None, span=span, rate=rate, **ignore_kwargs):
                if not addmsg is None:
                    win = wg_i(win) if not win is None else ks['win']
                    return func(addmsg=ig_i(addmsg), win=win, span=span, rate=rate, *addargs, **addkwargs)
                elif not delmsg is None:
                    win = wg_i(win) if not win is None else ks['win']
                    return func(delmsg=ig_i(delmsg), win=win, span=span, rate=rate, *addargs, **addkwargs)
        elif listin(['addmsg', 'oldval', 'win', 'span', 'rate'], ks):
            def newfunc(AttrDict addmsg=None, oldval=oldval, win=None, span=span, rate=rate, **ignore_kwargs):
                if not addmsg is None:
                    win = wg_i(win) if not win is None else ks['win']
                    return func(addmsg=ig_i(addmsg), win=win, oldval=oldval, span=span, rate=rate, *addargs, **addkwargs)
        elif listin(['delmsg', 'oldval', 'win', 'span', 'rate'], ks):
            def newfunc(AttrDict delmsg=None, oldval=oldval, win=None, span=span, rate=rate, **ignore_kwargs):
                if not delmsg is None:
                    win = wg_i(win) if not win is None else ks['win']
                    return func(delmsg=ig_i(delmsg), win=win, oldval=oldval, span=span, rate=rate, *addargs, **addkwargs)
        elif listin(['addmsg', 'win', 'span', 'rate'], ks):
            def newfunc(AttrDict addmsg=None, win=None, span=span, rate=rate, **ignore_kwargs):
                if not addmsg is None:
                    win = wg_i(win) if not win is None else ks['win']
                    return func(addmsg=ig_i(addmsg), win=win, span=span, rate=rate, *addargs, **addkwargs)
        elif listin(['delmsg', 'win', 'span', 'rate'], ks):
            def newfunc(AttrDict delmsg=None, win=None, span=span, rate=rate, **ignore_kwargs):
                if not delmsg is None:
                    win = wg_i(win) if not win is None else ks['win']
                    return func(delmsg=ig_i(delmsg), win=win, span=span, rate=rate, *addargs, **addkwargs)
        elif listin(['addmsg', 'delmsg', 'oldval'], ks):
            def newfunc(AttrDict addmsg=None, AttrDict delmsg=None, oldval=oldval, **ignore_kwargs):
                if not addmsg is None:
                    return func(addmsg=ig_i(addmsg), oldval=oldval, *addargs, **addkwargs)
                elif not delmsg is None:
                    return func(delmsg=ig_i(delmsg), oldval=oldval, *addargs, **addkwargs)
        elif listin(['addmsg', 'delmsg'], ks):
            def newfunc(AttrDict addmsg=None, AttrDict delmsg=None, **ignore_kwargs):
                if not addmsg is None:
                    return func(addmsg=ig_i(addmsg), *addargs, **addkwargs)
                elif not delmsg is None:
                    return func(delmsg=ig_i(delmsg), *addargs, **addkwargs)
        elif listin(['addmsg', 'oldval'], ks):
            def newfunc(AttrDict addmsg=None, oldval=oldval, **ignore_kwargs):
                if not addmsg is None:
                    return func(addmsg=ig_i(addmsg), oldval=oldval, *addargs, **addkwargs)
        elif listin(['delmsg', 'oldval'], ks):
            def newfunc(AttrDict delmsg=None, oldval=oldval, **ignore_kwargs):
                if not delmsg is None:
                    return func(delmsg=ig_i(delmsg), oldval=oldval, *addargs, **addkwargs)
        elif listin(['delmsg'], ks):
            def newfunc(AttrDict delmsg=None, **ignore_kwargs):
                if not delmsg is None:
                    return func(delmsg=ig_i(delmsg), *addargs, **addkwargs)
        elif listin(['addmsg'], ks):
            def newfunc(AttrDict addmsg=None, **ignore_kwargs):
                if not addmsg is None:
                    return func(addmsg=ig_i(addmsg), *addargs, **addkwargs)
        else:
            def newfunc(**ignore_kwargs):
                return func(*addargs, **addkwargs)
        newfunc.__name__ = newfunc__name__
    newfunc.__attr_name__ = attr_name
    return newfunc


