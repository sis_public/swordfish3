# -*- coding: utf-8 -*-
# cython: language_level=3
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Mon Sep 14 14:19:58 2015

@author: tvzyl
"""

cpdef itemsetter_in(list keys)
cpdef itemgetter_in(list keys)

cpdef void listmerge(list self, list alist)
cpdef void dictmerge(dict self, dict adict)

cdef class AttrList:
    cdef list data
    cdef bint __frozen__
    cpdef AttrList __deepcopy__(AttrList self, dict m)
    cpdef basestring __getstate__(AttrList self)
    cpdef __setstate__(AttrList self, basestring state)
    cpdef void freeze(AttrList self)

cdef class AttrDict:
    cdef dict data
    cdef bint __frozen__
    cdef basestring __msgpackstring__
    cdef basestring __jsonstring__
    cpdef AttrDict __deepcopy__(AttrDict self, dict m)
    cpdef basestring __getstate__(AttrDict self)
    cpdef __setstate__(AttrDict self, basestring state)
    cpdef void freeze(AttrDict self)
    cpdef setitem(self, basestring key, item)
    cpdef setnesteditem(self, list key, value)
    cpdef getnesteditem(self, list key, failobj=?)
    cpdef tuple getnesteditems(self, list keys)
    cpdef int cp_update(self, AttrDict adict) except -1
    cpdef int merge(self, AttrDict adict) except -1

cdef class Stream(object):
    cdef public basestring url
    cdef public float timeout
    cdef public basestring debug_name
    cdef public StreamBridge sb
    cdef public object logger
    cdef public dict pipe
    cdef public dict context
    cdef bint __closed__
    cdef basestring __stream_id__
    cdef object __weakref__
    cpdef close(self)
    cdef _info(self, basestring debug_message, tuple args, dict kwargs)
    cdef _critical(self, basestring debug_message, tuple args, dict kwargs)
    cdef _warning(self, basestring debug_message, tuple args, dict kwargs)
    cdef _error(self, basestring debug_message, tuple args, dict kwargs)
    cdef _debug(self, basestring debug_message, tuple args, dict kwargs)

cdef class DefaultStream(Stream):
    cdef public object anonymous_callback
    cdef public dict callbacks

cdef class StreamProcessor(object):
    cdef bint __closed__
    cdef float __timeout
    cdef bint anonymous
    cdef public dict output_streams
    cdef public dict input_streams
    cdef object __stop
    cdef object __process
    cdef public object queue
    cdef public list results
    cdef public basestring debug_name
    cdef Stream _default_stream
    cdef public bint _sink
    cdef public bint _source
    cdef public int messages_processed
    cdef public object logger
    cdef public DefaultStream errorstream
    cdef public object __execute_thread
    cdef public long misses
    cdef public bint async
    cdef object __weakref__
    cpdef close(self)
    cpdef clear(self)
    cpdef _get_default_stream(self)
    cpdef int __process_result(self, results) except -1
    cpdef int __execute(self, bint async=*) except -1
    cdef _info(self, basestring debug_message, tuple args, dict kwargs)
    cdef _critical(self, basestring debug_message, tuple args, dict kwargs)
    cdef _warning(self, basestring debug_message, tuple args, dict kwargs )
    cdef _error(self, basestring debug_message, tuple args, dict kwargs)
    cdef _debug(self, basestring debug_message, tuple args, dict kwargs)
    cpdef process(self, AttrDict message, basestring stream_id)
    cpdef async_process(self, AttrDict message, basestring stream_id, callback)
    cpdef _enqueue_message(self, AttrDict msg)

cdef class StreamBridge(StreamProcessor):
    pass