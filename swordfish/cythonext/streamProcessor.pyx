# -*- coding: utf-8 -*-
# cython: language_level=3
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Tue Mar 11 14:59:23 2014

@author: Terence van Zyl - tvzyl@csir.co.za

Updated: Sep 9 2020
@author: Derek Hohls - dhohls@csir.co.za

Notes:
    * To recompile:
        cythonize -a -i streamProcessor.pyx
      Then:
        mv streamProcessor.cpython-37m-x86_64-linux-gnu.so streamProcessor.so
"""

from swordfish.cythonext.core cimport AttrDict, AttrList, StreamProcessor
from swordfish.cythonext.core cimport itemgetter_in, itemsetter_in

from swordfish.cythonext.core import _make_getter_able
from swordfish.cythonext.core import AttrDict, StreamProcessor, identity
from swordfish.cythonext.core import Builder

from collections import deque
import heapq

from swordfish.exceptions import SwordfishSyntaxError

#other libs

from inspect import getargspec
from cytoolz.dicttoolz import merge

class Window(object):
    def _process_external_event(self):
        #Call this method whenever you would like to process and external event
        #External events other than the arrival of new mesages which is already
        #handled, examples include time span expiring
        self.__process.acquire()

    def _set_process_event_semaphore(self, __process):
        self.__process = __process

    def _update_window(self, message, group_key=None):
        raise NotImplemented()
        #should be where you update your window will be called eachtime a new
        #message arrives for a groupkey

    def update_window(self, message, group_key=None):
        self._update_window(message, group_key)
        return self._get_deltas(group_key) + (self._get_iterable_window(group_key), self._is_return(group_key))

    def _get_iterable_window(self, group_key=None):
        raise NotImplemented()
        #should return a empty list if none exists

    def _get_deltas(self, group_key=None):
        raise NotImplemented()
        #get the deltas of added and removed messages as a tuple of lists
        #ordering should be preserved
        #also resets the deltas

    def _is_return(self, group_key):
        raise NotImplemented()
        #return true or false as to if the window requires the process result
        #to be returned

    def get_span(self):
        raise NotImplemented()
        #should return the span of this window

    def get_rate(self):
        raise NotImplemented()
        #should return the rate of this window

class heap_reiterable_generator():
    def __init__(self, iterable):
        self.iterable = iterable

    def __iter__(self):
        return (i[1] for i in self.iterable)

class IntervalWindow(Window):
    def __init__(self, attr, span, rate, parse=None, *args, **kwargs):
        Window.__init__(self, **kwargs)
        self.attr = attr
        self.span = span
        self.rate = rate
        self.start = None
        self.stop = None
        self.current = None
        self.last_span = None
        self.delta_add = {}
        self.delta_del = {}
        self.group_key_window = {}
        self.group_key_count = {}
        if parse:
            self.ig = lambda msg: parse( itemgetter_in( [attr.split('.')] )(msg) )
        else:
            self.ig = itemgetter_in( [attr.split('.')] )

    def _update_window(self, message, group_key=None):
        if message is None:
            return
        self.current = self.ig(message)
        try:
            heap = self.group_key_window[group_key]
        except KeyError:
            heap = []
            self.group_key_window[group_key] = heap
            self.group_key_count[group_key] = 0
            self.delta_add[group_key] = []
            self.delta_del[group_key] = []
            self.start = self.current - self.span
            self.stop = self.current
            self.last_span = None

        if self.current >= self.stop:
            self.stop = self.current
            heapq.heappush(heap, [self.current, message])
            self.start = self.stop - self.span
            while heap[0][0] <= self.start:
                self.delta_del[group_key].append(heapq.heappop(heap)[1])
            self.delta_add[group_key].append(message)

    def _get_iterable_window(self, group_key=None):
        try:
            return heap_reiterable_generator( self.group_key_window[group_key] )
        except KeyError:
            return []

    def _get_deltas(self, group_key=None):
        return_deltas = (self.delta_add[group_key], self.delta_del[group_key])
        self.delta_add[group_key] = []
        self.delta_del[group_key] = []
        return return_deltas

    def _is_return(self, group_key):
        if self.last_span is None:
            self.last_span = self.current
            return True
        elif self.last_span + self.rate <= self.current:
            self.last_span = self.last_span + self.rate
            return True
        else:
            return False

    def get_span(self):
        return  self.span

    def get_rate(self):
        return  self.rate

class CountWin(Window):
    def __init__(self, span, rate, *args, **kwargs):
        Window.__init__(self, **kwargs)
        try:
            self.start, self.stop = span
        except:
            self.start = 0
            self.stop = span
        self.rate = rate
        self.delta_add = {}
        self.delta_del = {}
        self.group_key_window = {}
        self.group_key_count = {}
        self.messages_processed = 0

    def _update_window(self, message, group_key=None):
        if message is None:
            return
        try:
            window = self.group_key_window[group_key]
        except KeyError:
            window = deque(maxlen=self.stop)
            self.group_key_window[group_key] = window
            self.group_key_count[group_key] = 0
            self.delta_add[group_key] = []
            self.delta_del[group_key] = []
        win_len = len(window)
        if win_len == self.stop:
            dequeued_msg = window.pop()
            self.delta_del[group_key].append(dequeued_msg)
        window.appendleft(message)
        enqued_msg = window[self.start]
        self.delta_add[group_key].append(enqued_msg)
        self.group_key_count[group_key] = (self.group_key_count[group_key]+1)%self.rate
        self.messages_processed += 1

    def _get_iterable_window(self, group_key=None):
        try:
            return self.group_key_window[group_key]
        except KeyError:
            return []

    def _get_deltas(self, group_key=None):
        return_deltas = (self.delta_add[group_key], self.delta_del[group_key])
        self.delta_add[group_key] = []
        self.delta_del[group_key] = []
        return return_deltas

    def _is_return(self, group_key):
        return self.group_key_count[group_key]%self.rate == self.rate-1

    def get_span(self):
        return  self.stop - self.start

    def get_rate(self):
        return  self.rate

class explodelist(list):
    pass

cdef class ArgumentStreamProcessor(StreamProcessor):
    cdef public AttrDict ret_msg_template
    cdef public list setget_funcs
    cdef public int setget_func_count
    def __init__( self, debug_name, anonymous, *args, **kwargs ):
        self.ret_msg_template = AttrDict.__new__(AttrDict, {})
        self._init_setget_funcs(args, kwargs)
        StreamProcessor.__init__(self, debug_name=debug_name, anonymous=anonymous, async=False, default_stream=None )

    def _init_setget_funcs(self, args, kwargs):
        self.setget_funcs = []
        self.setget_func_count = 0
        has_ = False
        for arg in args:
            if isinstance(arg, dict):
                for key, value in arg.items():
                    kwargs[key] = value
            elif isinstance(arg, tuple):
                # map( (maximum, 'attr.temp') ) ->
                # ret_msg['maximum']['attr']['temp'] = maximum(msg['attr']['temp'])
                kwargs["%s.%s"%(arg[0].__name__,arg[1])]=arg
            elif isinstance(arg, str):
                # map('attr.temp')
                # ret_msg['attr']['temp'] = msg['attr']['temp']
                # map('_')
                # ret_msg = msg
                if arg=='_':
                    kwargs['_'] = '_'
                else:
                    kwargs[arg] = arg
            elif callable(arg):
                try:
                    kwargs["%s.%s"%(arg.__name__, arg.__attr_name__)] = arg
                except AttributeError:
                    kwargs["%s"%(arg.__name__)] = arg
        for key, value in kwargs.items():
            if key != '_':
                self._init_ret_msg_template(key)
            else:
                has_= True
            if isinstance(value, str):
                # map(temp='attr.temp')
                # ret_msg['temp'] = msg['attr']['temp']
                # map(_='attr.temp')
                # ret_msg = msg['attr']['temp']
                if value == '_':
                    func =  duplicate
                else:
                    func =  _make_getter_able( None, value )
            elif isinstance(value, tuple):
                # max_temp=(maximum,'attr.temp')
                # ret_msg['max_temp'] = maximum(msg['attr']['temp'])
                # _=(maximum,'attr.temp')
                # ret_msg = maximum(msg['attr']['temp'])
                func = _make_getter_able( value[0], value[1] )
            elif hasattr(value, '__indexattrable__') and value.__indexattrable__:
                # uid=UID(seed=1234)
                # {'uid':UID(seed=1234)}
                func = value['_']
            elif callable(value):
                # uid=UID(seed=1234)
                # {'uid':(UID(seed=1234), '_'}
                func = value
            if key=='_':
                self.setget_funcs.insert(0, (itemsetter_in([]), func) )
                self.setget_func_count += 1
            elif func.__name__=='replace':
                if len(self.setget_funcs)>0:
                    self.setget_funcs.insert(1, (itemsetter_in(key.split('.')), func )  )
                    self.setget_func_count += 1
                else:
                    self.setget_funcs.append( (itemsetter_in(key.split('.')), func )  )
                    self.setget_func_count += 1
            else:
                self.setget_funcs.append( (itemsetter_in(key.split('.')), func )  )
                self.setget_func_count += 1
        if has_:
            self.ret_msg_template = AttrDict.__new__(AttrDict, {})

    def _init_ret_msg_template(self, key):
        keys = key.split('.')
        if len(keys)==0 : return
        point_dict = self.ret_msg_template
        for key in keys[:-1]:
            if not point_dict.has_key(key):
                point_dict[key] = {}
            point_dict = point_dict[key]
        if not point_dict.has_key(keys[-1]):
            point_dict[keys[-1]] = None

cpdef duplicate(AttrDict addmsg=None, AttrDict delmsg=None, win=None, oldval=None, span=None, rate=None):
    if not addmsg is None:
        return addmsg.__deepcopy__(None)
    elif not delmsg is None:
        return delmsg.__deepcopy__(None)
    else:
        raise SwordfishSyntaxError("Got an addmsg and a delmsg that are None")

class lambda_or(object):
    def __init__(self, *funcs):
        self.funcs = funcs
    def __call__(self, AttrDict addmsg=None, AttrDict delmsg=None, win=None, oldval=None, span=None, rate=None):
        for func in self.funcs:
            try:
                return func(addmsg, delmsg, win, oldval, span, rate)
            except KeyError:
                continue
        raise KeyError

class lambda_and(object):
    def __init__(self, *funcs):
        self.funcs = funcs
    def __call__(self, AttrDict addmsg=None, AttrDict delmsg=None, win=None, oldval=None, span=None, rate=None):
        for func in self.funcs:
            result = func(addmsg, delmsg, win, oldval, span, rate)
        return result

cdef class map(ArgumentStreamProcessor):
    def __init__( self, *args, **kwargs ):
        ArgumentStreamProcessor.__init__( self, "map", False, *args, **kwargs )

    cpdef process(self, AttrDict message, basestring stream_id):
        cdef list ret_msgs = [self.ret_msg_template.__deepcopy__(None)]
        cdef int i
        cdef int ret_msgs_len = 1
        for itemsetter, func in self.setget_funcs:
            vals = func( addmsg=message )
            if type(vals) == explodelist:
                ret_msgs.extend( [ret_msgs[-1].__deepcopy__(None) for i in xrange(len(vals)-ret_msgs_len)] )
                ret_msgs_len = len(ret_msgs)
                for i in range(ret_msgs_len):
                    val = vals[i%ret_msgs_len]
                    itemsetter(<AttrDict>ret_msgs[i], val)
            elif ret_msgs_len == 1:
                itemsetter(<AttrDict>ret_msgs[0], vals)
            else:
                for i in range(ret_msgs_len):
                    itemsetter(<AttrDict>ret_msgs[i], vals)
        return ret_msgs

cdef class fold(ArgumentStreamProcessor):
    cdef dict group_key_old_val
    cdef list groupkeyset
    def __init__(self, by, *args, **kwargs ):
        ArgumentStreamProcessor.__init__(self, "fold", False, *args, **kwargs )
        self.group_key_old_val = {}
        self.groupkeyset = []
        if isinstance(by, list):
            self._by(*by)
        else:
            raise SwordfishSyntaxError('by clause must be a list')

    cpdef process(self, AttrDict message, basestring stream_id):
        cdef int i, j
        cdef list group_key_old_val_per_func
        cdef list ret_msgs = []
        cdef int ret_msgs_len
        #get the group_key this msg belongs to in the grouping
        group_key = message.getnesteditems(self.groupkeyset)
        #have we already seen this group_key?
        try:
            group_key_old_val_per_func = <list>(self.group_key_old_val[group_key])
        except KeyError:
            #create the group_key old_value
            group_key_old_val_per_func = [None]*self.setget_func_count
            self.group_key_old_val[group_key] = group_key_old_val_per_func
        ret_msgs = [self.ret_msg_template.__deepcopy__(None)]
        ret_msgs_len = 1
        for j in range(self.setget_func_count):
            itemsetter, func = self.setget_funcs[j]
            oldval=group_key_old_val_per_func[j]
            if oldval is None:
                vals = func( addmsg=message )
            else:
                vals = func( addmsg=message, oldval=oldval )
            group_key_old_val_per_func[j] = vals
            if type(vals) == explodelist:
                ret_msgs.extend( [ret_msgs[-1].__deepcopy__(None) for i in range(len(vals)-ret_msgs_len)] )
                ret_msgs_len = len(ret_msgs)
                for i in range(ret_msgs_len):
                    val = vals[i%ret_msgs_len]
                    itemsetter(<AttrDict>ret_msgs[i], val)
            elif ret_msgs_len==1:
                itemsetter(<AttrDict>ret_msgs[0], vals)
            else:
                for i in range(ret_msgs_len):
                    itemsetter(<AttrDict>ret_msgs[i], vals)
        return ret_msgs

    def _by(self, *args):
        cdef list groupkeyset = []
        for arg in args:
            if arg=='_':
                self.groupkeyset = []
                return self
            groupkeyset.append([ attr_name for attr_name in arg.split('.') ])
        self.groupkeyset = groupkeyset
        return self

cdef class reduce(ArgumentStreamProcessor):
    cdef window
    cdef rate
    cdef dict group_key_old_val
    cdef list groupkeyset
    def __init__( self, by, *args, **kwargs ):
        #TODO: construct the template for speed
        #TODO: handle duplicate attribute names
        ArgumentStreamProcessor.__init__(self, "reduce", False, *args, **kwargs )
        self.window = None
        self.rate = 1
        self.group_key_old_val = {}
        self.groupkeyset = []
        if isinstance(by, list):
            self._by(*by)
        else:
            raise SwordfishSyntaxError('by clause must be a list')

    def _setWindow(self, window):
        #TODO: API 2.0 handle multiple windows
        if isinstance(window, list):
            window = window[0]
        self.window = window

    def _by(self, *args):
        cdef list groupkeyset = []
        for arg in args:
            if arg=='_':
                self.groupkeyset = []
                return self
            groupkeyset.append([ attr_name for attr_name in arg.split('.') ])
        self.groupkeyset = groupkeyset
        return self

    cpdef process(self, AttrDict message, basestring stream_id):
        #get the group_key this msg belongs to in the grouping
        cdef list group_key_old_val_per_func
        cdef int j
        cdef AttrDict msg
        group_key = message.getnesteditems(self.groupkeyset)
        #have we already seen this group_key?
        try:
            group_key_old_val_per_func = <list>(self.group_key_old_val[group_key])
        except KeyError:
            #create the group_key old_value
            group_key_old_val_per_func = [None]*self.setget_func_count
            self.group_key_old_val[group_key] = group_key_old_val_per_func
        if not self.window is None:
            delta_add, delta_del, window, isReturn = self.window.update_window( message, group_key )
            span = self.window.get_span()
            rate = self.window.get_rate()
        else:
            delta_add = [message]
            delta_del = []
            window = [message]
            span = 1
            rate = 1
            isReturn = True
        for msg in delta_del:
            for j in xrange(self.setget_func_count):
                itemsetter, func = self.setget_funcs[j]
                old_val = group_key_old_val_per_func[j]
                if old_val is None:
                    vals = func( delmsg=msg, win=window, span=span, rate=rate )
                else:
                    vals = func( oldval=old_val, delmsg=msg, win=window, span=span, rate=rate )
                group_key_old_val_per_func[j] = vals
        for msg in delta_add:
            ret_msg = self.ret_msg_template.__deepcopy__(None)
            ret_msgs = [ret_msg]
            for j in xrange(self.setget_func_count):
                itemsetter, func = self.setget_funcs[j]
                old_val = group_key_old_val_per_func[j]
                if old_val is None:
                    vals = func( addmsg=msg, win=window, span=span, rate=rate )
                else:
                    vals = func( addmsg=msg, oldval=old_val, win=window, span=span, rate=rate )
                group_key_old_val_per_func[j] = vals
                if not isReturn:
                    continue
                if type(vals) == explodelist:
                    ret_msgs.extend( [ret_msgs[-1].__deepcopy__(None) for i in xrange(len(vals)-len(ret_msgs))] )
                    for i, ret_msg in enumerate(ret_msgs):
                        val = vals[i%len(ret_msgs)]
                        itemsetter(ret_msg, val)
                else:
                    for i, ret_msg in enumerate(ret_msgs):
                        itemsetter(ret_msg, vals)
        return ret_msgs if isReturn else None

class _FULLOUTERJOIN():
    def __init__(self, merge):
        self.merge = merge
    def __repr__(self):
        return "%s MERGE FULL OUTER JOIN"%(self.merge)

class _RIGHTOUTERJOIN():
    def __init__(self, merge):
        self.merge = merge
    def __or__(self, item):
        if isinstance(item, _LEFT):
            return _FULLOUTERJOIN(self.merge)
        raise TypeError("unsupported operand type(s) for |: '%s' and '%s'"%(self, item))
    def __repr__(self):
        return "%s MERGE RIGHT OUTER JOIN"%(self.merge)

class _LEFTOUTERJOIN():
    def __init__(self, merge):
        self.merge = merge
    def __or__(self, item):
        if isinstance(item, _RIGHT):
            return _FULLOUTERJOIN(self.merge)
        raise TypeError("unsupported operand type(s) for |: '%s' and '%s'"%(self, item))
    def __repr__(self):
        return "%s MERGE LEFT OUTER JOIN"%(self.merge)

class _INNERJOIN():
    def __init__(self, merge):
        self.merge = merge
    def __or__(self, item):
        if isinstance(item, _RIGHT):
            return _RIGHTOUTERJOIN(self.merge)
        if isinstance(item, _LEFT):
            return _LEFTOUTERJOIN(self.merge)
        raise TypeError("unsupported operand type(s) for |: '%s' and '%s'"%(self, item))
    def __repr__(self):
        return "%s MERGE INNER JOIN"%(self.merge)

class _LEFT(Builder):
    def __and__(self,  item):
        return _INNERJOIN(self)
    def __repr__(self):
        return "LEFT"

class _RIGHT(Builder):
    """A dictionary with attribute-style access. It maps attribute access to the real dictionary.  """
    def __init__(self, name=None, func=identity):
        self.attr_name = name
        if name:
            self.func = itemgetter_in( [attr_name_or.split('.') for attr_name_or in self.attr_name.split('|') ] )
        else:
            self.func = func

    def __getstate__(self):
        return self.__dict__.items()

    def __setstate__(self, items):
        for key, val in items:
            self.__dict__[key] = val

    def __getitem__(self, name):
        if self.attr_name:
            super(Builder, self).__setitem__(name, Builder("%s.%s"%(self.attr_name, name)) )
        else:
            super(Builder, self).__setitem__(name, Builder("%s"%(name)) )
        item = super(Builder, self).__getitem__(name)
        return item

    def __eq__(self, other):
        return Builder(func=(lambda _: self.func(_) == other) )

    def __and__(self,  item):
        return _INNERJOIN(self)

    def __repr__(self):
        return "RIGHT"

class join(StreamProcessor):
    def __init__(self, jointype, ON,  *args, **kwargs):
        StreamProcessor.__init__(self, *args, **kwargs)
        if not isinstance(jointype, _INNERJOIN):
            raise NotImplemented("API 1.0 only implements Inner Join ")
        self._jointype = jointype
        self._primaryWindow = CountWin(1,1)
        self._secondaryWindow = CountWin(1,1)
        self._ON = ON
        self.attr_primary = ON[0]
        self.attr_secondary = ON[1]
        self._primary_hash = {}
        self._secondary_hash = {}
        if isinstance(self.attr_primary, list):
            self._primary_ig_i = self._get_on(*self.attr_primary)
        else:
            raise SwordfishSyntaxError('on clause must be a list')
        if isinstance(self.attr_secondary, list):
            self._secondary_ig_i = self._get_on(*self.attr_secondary)
        else:
            raise SwordfishSyntaxError('on clause must be a list')

    def _setWindow(self, window):
        if not len(window) == 2:
            raise SyntaxError("Must be exactly two windows ")
        self._primaryWindow = window[0]
        self._secondaryWindow = window[1]

    def _get_on(self, *args):
        itemgetter_in_list = []
        for arg in args:
            if arg=='_':
                raise SwordfishSyntaxError("Can't join on whole message")
            itemgetter_in_list.append( itemgetter_in( [attr_name_or.split('.') for attr_name_or in arg.split('|') ] ) )
        return lambda message: tuple( ig_i(message) for ig_i in itemgetter_in_list )

    def process(self, AttrDict message, basestring stream_id):
        cdef AttrDict tmp_msg
        cdef bint isReturn = True
        cdef list ret_msgs = []
        if self.input_streams[stream_id][1] == 0:  #input_streams_order
            primary_val = self._primary_ig_i(message)
            if not self._primaryWindow is None:
                delta_add, delta_del, _, isReturn = self._primaryWindow.update_window( message )
                try:
                    for msg in delta_add:
                        self._primary_hash[primary_val][id(msg)] = msg
                except KeyError:
                    self._primary_hash[primary_val] = {id(message):message}
                for msg in delta_del:
                    del self._primary_hash[self._primary_ig_i(msg)][id(msg)]
            if isReturn:
                try:
                    for msg in self._secondary_hash[primary_val].itervalues():
                        tmp_msg = msg.__deepcopy__({})
                        tmp_msg.merge( message )
                        ret_msgs.append( tmp_msg )
                except KeyError:
                    pass

        if self.input_streams[stream_id][1] == 1: #input_streams_order
            secondary_val = self._secondary_ig_i(message)
            if not self._secondaryWindow is None:
                delta_add, delta_del, _, isReturn = self._secondaryWindow.update_window( message )
                try:
                    for msg in delta_add:
                        self._secondary_hash[secondary_val][id(msg)] = msg
                except KeyError:
                    self._secondary_hash[secondary_val] = {id(message):message}
                for msg in delta_del:
                    del self._secondary_hash[self._secondary_ig_i(msg)][id(msg)]
            if isReturn:
                try:
                    for msg in self._primary_hash[secondary_val].itervalues():
                        tmp_msg = message.__deepcopy__({})
                        tmp_msg.merge( msg )
                        ret_msgs.append( tmp_msg )
                except KeyError:
                    pass

        return ret_msgs
