# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Thu Nov 26 15:19:23 2015

@author: tvzyl
"""
# lib
from threading import Thread
# thirdparty
import tornado
import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.ioloop
from tornado.websocket import WebSocketClosedError
# project
from swordfish.sink import SinkProcessor


class WebSocketHandler(tornado.websocket.WebSocketHandler):
    """WebSocketHandler"""

    def initialize(self, websockets):
        print("initialize")
        self.websockets = websockets
        if self not in self.websockets:
            self.websockets.append(self)
            print("appended")

    def check_origin(self, origin):
        """Allow cross-origin requests for specific domains.

        Tornado 4.0+ requires applications that wish to receive cross-origin
        websockets to opt-in by overriding this method. Failure to do so is
        a likely cause of 403 errors when attempting a websocket connection.
        """
        print("Origin:", origin)
        #TODO  return bool(re.match(r'^.*?\.yak\.dhcp\.meraka\.csir\.co\.za', origin))
        return True

    def on_message(self, message):
        pass

    def open(self):
        print("connected")
        if self not in self.websockets:
            self.websockets.append(self)
            print("appended")

    def on_close(self):
        print('connection closed!!')
        if self in self.websockets:
            self.websockets.remove(self)


class IndexHandler(tornado.web.RequestHandler):
    """IndexHandler"""

    @tornado.web.asynchronous
    def get(self):
        self.write("You're connected to a swordfish tornado websocket sink")
        self.finish()


class WebSocket(SinkProcessor):
    def __init__(self, url="", port=9090, timeout=None, *args, **kwargs):
        """
        #Python
        from swordfish.tornado_ws.sink import WebSocket
        from swordfish.core import AttrDict
        wso = WebSocket()
        wso._enqueue_message(AttrDict({'a':1}))
        #javascript
        #var wso = new WebSocket("ws://localhost:9090/swordfish");
        #wso.onmessage = function (evt) {alert(evt.data);};
        """
        SinkProcessor.__init__(self, *args, **kwargs)
        self.websockets = []
        self.port = port
        handlers = [
            (r'/swordfish', WebSocketHandler, dict(websockets=self.websockets)),
            (r'/', IndexHandler),
        ]
        self.ws_app = tornado.web.Application(handlers)
        self.ws_app.listen(self.port)
        self.thread = Thread(target=self.target)
        self.thread.isDaemon = True
        self.thread.start()

    def target(self):
        print("Starting Tornado")
        tornado.ioloop.IOLoop.instance().start()
        print("Tornado finished")

    def cleanup(self):
        ioloop = tornado.ioloop.IOLoop.instance()
        ioloop.add_callback(lambda x: x.stop(), ioloop)
        print("Asked Tornado to exit")

    def process(self, message, stream_id):
        """Send an AttrDict message, in JSON format, to a web socket server"""
        for wsk in self.websockets:
            try:
                wsk.write_message(message.__json__)
            except WebSocketClosedError:
                pass
        return None
