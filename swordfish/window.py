# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
from swordfish.cythonext.streamProcessor import CountWin, IntervalWindow, Window

class _wit():
    def __init__(self, item=None):
        self.item = item
    def __mod__(self, item):
        if isinstance(item, Window):
            self.item._setWindow(item)
        else:
            self.item._setWindow(item.slices)
        return self.item
    def __rmod__(self, item):
        return _wit(item)

within = _wit()


class _windowmaker():
    def __init__(self, slices=[]):
        self.slices = slices
    def __contains__(self, item):
        return 0
    def __getitem__(self, ks):
        slices = self.slices[:]
        if not isinstance(ks , list):
            ks = [ks]
        for k in ks:
            if isinstance(k, slice):
                if isinstance(k.start, complex) and isinstance(k.stop, complex) and isinstance(k.step, complex):
                    raise NotImplemented("Wall Time Window Not Implemented")
                elif isinstance(k.stop, int):
                    stop = k.stop
                    start = 0 if k.start is None else k.start
                    step = 1 if k.step is None else k.step
                    if not isinstance(start, int) or not isinstance(step, int):
                        raise NotImplemented("Mixed Windows Not Implemented")
                    slices.append( CountWin( (start, stop), step) )
                else:
                    raise NotImplemented("Mixed Count and Wall Time Windows Not Implemented")
        return _windowmaker( slices )

WINDOW = _windowmaker([])
