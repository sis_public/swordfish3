# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Wed Oct 28 11:40:34 2015

@author: tvzyl
"""
import rpyc
from rpyc.core.stream import Stream
from rpyc.utils.factory import connect_stream, interrupt_main, connect_channel
from rpyc import SlaveService
from threading import Thread, Lock
from multiprocessing import Process, Queue, Pipe, Pool
import threading
from swordfish.core import StreamProcessor, DefaultStream, AttrDict
from swordfish.streamProcessor import swordfishfunc, map
from swordfish.sink import Print

#service.execute("import swordfish.remote")

@swordfishfunc
def f(addmsg=None):
    return addmsg

ds = DefaultStream("",1)

def run():
    m = list(map(_=f['_']))
    p = Print()
    return locals()

service = rpyc.classic.connect_multiprocess({'target':run})
service.execute("ns = target()")
service.execute('for k in ns: exec "%s=ns[k]"%k')

