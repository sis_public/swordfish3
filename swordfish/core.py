# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Thu Jun 25 14:03:10 2015

@author: tvzyl
"""
#import pyximport; pyximport.install()
from swordfish.cythonext.core import itemgetter_in, itemsetter_in, identity, _make_getter_able
from swordfish.cythonext.core import AttrDict, AttrList
from swordfish.cythonext.core import StreamProcessor, Builder
from swordfish.cythonext.core import Stream, DefaultStream, BroadcastBifurcation, SplitBifurcation

from swordfish.exceptions import *

_ = Builder()

stream_lookup_dict = {}

def register_stream_scheme(scheme, clazz):
    stream_lookup_dict[scheme] = clazz

def as_StreamProcessor(thing):
    def process(self, message, stream_id):
        return thing(message)
    streamProcessor = StreamProcessor()
    streamProcessor.process = process
    return streamProcessor

class StreamProcessorInc(StreamProcessor):
    '''incrementer exemplar'''
    def process(self, message, stream_id):
        message = message+1
        return message

