# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
from distutils.core import setup, Extension
from Cython.Build import cythonize

try:
    import zmq
    extensions = [
    Extension("cythonext.ciso8601", ["cythonext/ciso8601.c"]),
    Extension("cythonext.core", ["cythonext/core.pyx"], include_dirs=zmq.get_includes()),
    Extension("cythonext.streamProcessor", ["cythonext/streamProcessor.pyx"], include_dirs=zmq.get_includes()),
    ]
except Exception:
    extensions = [
    Extension("cythonext.ciso8601", ["cythonext/ciso8601.c"]),
    Extension("cythonext.core", ["cythonext/core.pyx"]),
    Extension("cythonext.streamProcessor", ["cythonext/streamProcessor.pyx"]),
    ]

setup(
  name = 'cythonext',
  packages = ['cythonext'], # same as the name above
  version = '0.1',
  description = 'A streaming data library',
  author = 'Terence van Zyl, Graeme McFerren, Derek Hohls, Bolelang Sibolla',
  author_email = 'tvzyl@csir.co.za',
  url = 'https://github.com/ict4eo/swordfish',
  download_url = 'https://github.com/ict4eo/swordfish/tarball/0.1',
  keywords = ['streaming', 'amqp', 'mqtt', 'sink', 'source'],
  classifiers = [],
  ext_modules = cythonize(extensions)
)
