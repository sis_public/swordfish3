# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Wed Aug 12 16:23:14 2015

@author: gmcferren
"""
from threading import Event
import time

from swordfish.core import DefaultStream
from swordfish.stream import ThreadBroadcastStream
from swordfish.stream import KombuStream
from swordfish.sink import Print, SinkProcessor

from swordfish.api import map
from swordfish.api import filter
from swordfish.core import AttrDict

from swordfish.spatiotemporal import RTreeIndex
#from swordfish.spatiotemporal.filters import BinaryPredicateSpatialFilters
#from swordfish.spatiotemporal.core import SimpleFeatures



bboxes1 = [(1,(28.274081,-25.749470,28.285083,-25.741212),'north_side_csir'),
          (2,(28.282523,-25.748755,28.287706,-25.741478),'outside_csir')]

bboxes2 = [(1,(28.274081,-25.749470,28.285083,-25.741212),'north_side_csir'),
          (2,(28.274081,-25.759334,28.285671,-25.749470),'south_side_csir'),
          (3,(28.282523,-25.748755,28.287706,-25.741478),'outside_csir')]


#cpower_stream = KombuStream("amqp://csircampus:campuscsir@yak.dhcp.meraka.csir.co.za:5672/csircampus",
#                            timeout=1,exchange_name='cpower', exchange_type='topic')

p = Print()

##########################################################################
##TESTING INTERSECTS as a boolean style function used in a Swordfish Filter
#rti = RTreeIndex(bboxes1)
#cpower_stream>>rti.intersects>>p
##########################################################################

###########################################################################
##TESTING INTERSECTION as a data returning function used in a Swordfish Map
#rti = RTreeIndex(bboxes2)
#cpower_stream>>map(_=(rti.intersection,'_'))>>p
###########################################################################

###########################################################################
##TESTING JOIN as a data returning function used in a Swordfish Map
#rti = RTreeIndex(bboxes2)
#j = join( LEFT & RIGHT, ON=(idx.foi && idx.foi) ) %within% WINDOW[:1:][:1:]
#j = join( LEFT & RIGHT, ON=(idx.foi, idx.foi, func) ) %within% WINDOW[:1:][:1:]
#j = join( LEFT & RIGHT, ON=(idx.parameter.id == idx.parameter.id) ) %within% WINDOW[:1:][:1:]
#j = join( LEFT & RIGHT, ON=(['parameter.id'], ['parameter.id'], func ) ) %within% WINDOW[:1:][:1:]
#(s1, s2)>>j
###########################################################################



###########################################################################
#Performance Testing
###########################################################################
td = (AttrDict({"type": "OM_Measurement", "phenomenonTime": "2015-08-14T11:45:00+02:00", "result": {"units": "kWh", "value": 20.52}, "featureOfInterest": {"geometry": {"type": "Point", "coordinates": ["28.280743", "-25.752238"]}, "type": "Feature", "properties": {"id": "CSIR:PTA:37"}}, "observedProperty": {"type": "active_load"}, "parameter": {"id": "216e8dab-773e-46f1-9fb3-32357e6d6585"}, "procedure": {"type": "sensor", "id": "CPOWER:1450", "description": "cPowerMeter"}}),
      AttrDict({"type": "OM_Measurement", "phenomenonTime": "2015-08-14T11:35:00+02:00", "result": {"units": "kWh", "value": 49.2}, "featureOfInterest": {"geometry": {"type": "Point", "coordinates": ["28.282765", "-25.747966"]}, "type": "Feature", "properties": {"id": "CSIR:PTA:26"}}, "observedProperty": {"type": "active_load"}, "parameter": {"id": "6dc6f4c8-e669-4370-bc83-bbec29012e18"}, "procedure": {"type": "sensor", "id": "CPOWER:3399", "description": "cPowerMeter"}})
      )
#td = ('{1:2}','{3,4}')
class Timer(SinkProcessor):
    def __init__(self, idx, stop_msg=AttrDict({1:1}), end_event=None, *args, **kwargs):
        self.stop_msg = stop_msg
        self.start_time = time.time()
        self.count = 0
        self.idx = idx
        self.k_nearest = kwargs.get('knearest',1)
        if not end_event is None:
            self.end_event = end_event
        else:
            self.end_event = Event()
        SinkProcessor.__init__(self, *args, **kwargs)

    def process(self, message, stream_id):
        self.count += 1
        if message==self.stop_msg:
            self.end_time = time.time()
            self.end_event.set()
            print("Finished Timing %s msgs in %s secs: %s msgs/sec"%(self.count, self.end_time-self.start_time, self.count/(self.end_time-self.start_time) ))
            return None
        #rti.intersection(message)#just execute
        #rti.intersects(message)#just execute
        #self.idx.intersection(message)#just execute
        #self.idx.intersects(message)#just execute
        #knf = self.idx.knearest(self.k_nearest)
        #knf(message)
        return None

class Timer2(SinkProcessor):
    def __init__(self, total, stop_msg, end_event, *args, **kwargs):
        self.total = total
        self.end_event = end_event
        self.stop_msg = stop_msg
        self.count = 0
        SinkProcessor.__init__(self, *args, **kwargs)

    def process(self, message, stream_id):
        #print message
        if self.count == 0:
            self.start_time = time.time()
        self.count += 1
        if self.total == self.count:
            self.end_time = time.time()
            print("Finished Timing %s msgs in %s secs: %s msgs/sec"%(self.count, self.end_time-self.start_time, self.count/(self.end_time-self.start_time) ))
            if self.stop_msg is not None :
                if message==self.stop_msg:
                    print("Verified Last message: Passed")
                else:
                    print("Verified Last message: Failed", self.stop_msg, message)
            self.end_event.set()
        return None

###############################################################################
# Test bbox filters provisioned by rtree
###############################################################################
rti = RTreeIndex(bboxes2)
test_size=25000

#print "tbs over intersection"
end_event = Event()
ks = ThreadBroadcastStream('tbs://localhost', 1)
#ks>>Timer(rti, end_event=end_event)

ks>>( rti._intersects['_'] )>>Timer2(test_size*len(td),td[1],end_event)

run_count = 0
while run_count < test_size:
    for msg in td:
        ks(msg)
    run_count +=1
#ks(AttrDict({1:1}))
end_event.wait()
ks.close()

###############################################################################
# Test spatial filters provisioned by shapely
###############################################################################
'''bpsf = BinaryPredicateSpatialFilters(,)
tbs = ThreadBroadcastStream('tbs://localhost', 1)
p = Print()

test_line = 'LINESTRING(27.1 -27.1, 27.2 -27.2, 27.3 -27.3)'
test_point = 'POINT(27.1 -27.1)'
test_poly = 'POLYGON((27.1 -27.1, 27.2 -27.2, 27.3 -27.3, 28.1 -28.1, 28.1 -27.1,27.1 -27.1))'
test_poly_rings = 'POLYGON((27.1 -27.1, 27.2 -27.2, 27.3 -27.3, 28.1 -28.1, 28.1 -27.1,27.1 -27.1),(27.7 -27.30,27.9 -27.32, 27.95 -27.6, 27.7 -27.30))'
'''