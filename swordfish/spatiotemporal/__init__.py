# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Mon Jul 13 15:35:23 2015

@author: gmcferren
"""

from .st_core import RTreeIndex
#from aggregations import *
#from filters import *
#from joins import *
#from transforms import *
