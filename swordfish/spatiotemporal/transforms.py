# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Swordfish Spatial Transforms

Assorted Spatial Transforms via Shapely and Proj4

Calculated Property Accessors
Projection
Geoprocessing e.g. Buffers, ConvexHulls, Envelopes, Simplifications
various AffineTransforms
Created on 07/12/2015 13:30:00
@author: gmcferren

"""
from swordfish.core import _make_getter_able
from swordfish.api import indexattr, swordfishfunc, swordfishproperty
from swordfish.spatiotemporal.st_core import SimpleFeatures, ProjectionMixin

from shapely.geometry import shape #simple load of geojson coordinates

class GeomProps(ProjectionMixin):
    '''
    Intended to be used as maps and filters
    '''
    def __init__(self,stream_srs='epsg:4326',project_stream=True):
        self.project_stream = project_stream
        ProjectionMixin.__init__(self,stream_srs)
        self.foi_coordinates = _make_getter_able(None, 'featureOfInterest.geometry')

    @swordfishproperty
    def area(self,addmsg=None):
        '''
        Returns the area of a streamed message geometry as a float
        '''
        if self.project_stream == True:
            msg_geom = self.projectGeom(shape(self.foi_coordinates(addmsg)))
        else:
            msg_geom = shape(self.foi_coordinates(addmsg))

        return msg_geom.area

    @swordfishproperty
    def length(self,addmsg=None):
        '''
        Returns the length of a streamed message geometry as a float
        '''
        if self.project_stream == True:
            msg_geom = self.projectGeom(shape(self.foi_coordinates(addmsg)))
        else:
            msg_geom = shape(self.foi_coordinates(addmsg))

        return msg_geom.length

    @swordfishproperty
    def bounds(self,addmsg=None):
        '''
        Returns the bounds of a streamed message geometry as
        tuple (minx,miny,maxx,maxy) of floats
        '''
        if self.project_stream == True:
            msg_geom = self.projectGeom(shape(self.foi_coordinates(addmsg)))
        else:
            msg_geom = shape(self.foi_coordinates(addmsg))

        return list(msg_geom.bounds)

    @swordfishproperty
    def distance(self,other_geom=None):
        '''
        Returns the distance of a streamed message geometry from another
        geometry, as a float
        Please supply crs properly into other geom if it is not EPSG:4326
        '''
        #TODO: what of other_geom projection?
        other_geom = shape(other_geom)
        def distance(addmsg=None):
            if self.project_stream == True:
                msg_geom = self.projectGeom(shape(self.foi_coordinates(addmsg)))
            else:
                msg_geom = shape(self.foi_coordinates(addmsg))

            return msg_geom.distance(other_geom)
        return distance