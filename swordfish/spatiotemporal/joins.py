# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
# -*- coding: utf-8 -*-
"""
Swordfish Spatial Joins

Stream joins based on RTree index based filters and geometry based filters

Created on 07/12/2015 13:30:00
@author: gmcferren

"""
