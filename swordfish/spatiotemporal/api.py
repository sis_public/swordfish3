# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Tue Aug  4 15:10:08 2015

@author: tvzyl
"""
class SpatialIndex(object):

    def __init__(self, geoms):
        self.rtree = geoms

    def intersects(self, addmsg):
        return self.rtree.interests(addmsg.foi.geom)

    def intersection(self, addmsg):
        return self.rtree.interests(addmsg.foi.geom)


    def knearest(self, k):
        def kmearest(self, addmsg):
            return self.rtree.knearest(k, addmsg.foi.geom)

#si = SpatialIndex(geoms, 'rtree')
#s1>>filter( si.itersects )>>map( _=si.knearest(5), _=si.intersection )
