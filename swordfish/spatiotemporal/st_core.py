# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Thurs July 16 at 10:50

@author: gmcferren

 A module that provides core spatio-temporal functions for use by various
 kinds of StreamProcessors of Swordfish. Relies heavily on the Swordfish
 Common Data Model



Componentry Possibilities
http://docs.scipy.org/doc/scipy/reference/spatial.html (kdtrees, delauney/voronoi/convexhull)
http://geopandas.org
http://geodjango.org/
http://toblerity.org/shapely/manual.html
Rtree
__geo_interface__ @ https://gist.github.com/sgillies/2217756



Types of operations likely

Mappings -

Projections
AffineTransformations
Constructions eg. buffer/envelope/chull/simplification  of g1
Travel e.g. distance, direction/heading/bearing for all g in [g1...gn] in relation to gA

Filters -

Unary Predicates e.g. g1.is_valid
Binary Predicates e.g. g1.intersects.g2, either over an index or geoms
Index comparisons
Set-theoretic Methods e.g. ... WHERE g1.intersection.g2 > x
Travel e.g. distance, direction/heading/bearing for g1 in relation to gA

Joins -
Binary Predicates e.g. g1.intersects.g2
Index comparisons

Reduces -

Constructions eg. buffer/envelope/chull/simplification  of g1
Set-theoretic Methods e.g. g1.union.g2 -> g3
Index comparisons


Generally speaking, spatial data items on a stream will be individually or
collectively mapped in a unary way e.g. projected or in a binary way e.g.
compared to another geometry in via predicate function like intersects;
reduced in some way e.g. added to a geometry collection or unioned with another
geometry (set-forming)
"""
from swordfish.core import _make_getter_able
from swordfish.core import AttrDict

from swordfish.api import indexattr, swordfishfunc, swordfishproperty
from swordfish.observations.common_data_model_exceptions import SwordfishCommonDataModelError
from swordfish.observations.observation_measurement import OM_Measurement,om
from swordfish.observations.observation_measurement import geometry as gi_geometry
'''

RTREE INDEXING
Things that need to happen:
- Populate an Rtree index from stream (coords interleaved or not)
- persist or destroy an Index
- query an index
  - nearest neighbour
  - intersection
- query index metadata
  - bounds/get_bounds, leaves, count
- delete index entries
- get/set index properties

Kinds of Uses
 - fill an index off a stream (or a Window?)
 - fill an index off some other source (reservoir [file, database]), via streaming
 - compare stream messages to an index
   - but what to return?
     - the message itself if it passes test (e.g. there is an intersection)
     - a new message that joins the incoming message to the results of the comparison?
 -> type should match purpose

'''



from rtree import index
class RTreeIndex():
    def __init__(self,bboxes,work_interleaved = True):
        '''
        Constructs an internal RTree index that can be used to test streamed
        geospatial data against for intersection and nearest neighbours
        Params:
        bboxes an iterable (e.g. a generator) returning sequences of form
        (id,[xmin, ymin, zmin, xmax, ymax, zmax],object) for work_interleaved = True
        (id,(minx, maxx, miny, maxy, minz, maxz),object) for work_interleaved = False
        '''
        self.idx = index.Index()
        self.idx.interleaved = work_interleaved
        for bbox in bboxes:
            self.idx.insert(bbox[0],bbox[1],bbox[2])
        self.ig_i_coordinate = _make_getter_able(None, 'featureOfInterest.geometry.coordinates')

    def intersects(self, addmsg):
       '''
       computes true for messages whose geometry payload intersects the index
       somewhere, i.e. the return from rtree.intersection is > 0 and forwards
       only those messages that compute to True

       function used as a filter
       '''
       try:
           msg = om(init=OM_Measurement(observation=addmsg))
       except Exception as e:
           raise SwordfishCommonDataModelError(e.message)
       try:
           the_geom = [float(coord) for coord in msg.observation.featureOfInterest.geometry.coordinates]#implements __geo_interface__
       except Exception as e:
           raise SwordfishCommonDataModelError(e.message)

       result = list(self.idx.intersection(the_geom,objects="raw"))

       if len(result) > 0:
           return True
       else:
           return False

    @swordfishproperty
    def _intersects(self, addmsg=None):
       '''
       computes true for messages whose geometry payload intersects the index
       somewhere, i.e. the return from rtree.intersection is > 0 and forwards
       only those messages that compute to True

       function used as a filter
       '''
#       try:
#           msg = om(init=OM_Measurement(observation=addmsg))
#       except Exception as e:
#           raise SwordfishCommonDataModelError(e.message)
       try:
           the_geom = [float(coord) for coord in self.ig_i_coordinate(addmsg)] #implements __geo_interface__
       except Exception as e:
           raise SwordfishCommonDataModelError(e.message)

       result = list(self.idx.intersection(the_geom,objects="raw"))

       if len(result) > 0:
           return True
       else:
           return False

    def intersection(self, addmsg=None):
        '''
        computes a set of index entries that are intersected by the msg feature
        geom.
        '''
        try:
           #msg = om(init=OM_Measurement(observation=addmsg))
           msg = om(init=addmsg)
        except Exception as e:
           raise SwordfishCommonDataModelError(e.message)
        try:
           the_geom = [float(coord) for coord in msg.featureOfInterest.geometry.coordinates]#implements __geo_interface__
        except Exception as e:
           raise SwordfishCommonDataModelError(e.message)

        result = self.idx.intersection(the_geom,objects = True)
        intersections = []
        for r in result:
            gio = gi_geometry()
            gio.type = 'Polygon'
            gio.bbox = r.bbox
            gio.properties = {'name':str(r.object)}
            bl = [r.bbox[0],r.bbox[1]]
            br = [r.bbox[1],r.bbox[3]]
            tr = [r.bbox[2],r.bbox[3]]
            tl = [r.bbox[0],r.bbox[2]]
            gio.coordinates = [[bl,br,tr,tl,bl]]
            intersections.append(gio)
        msg.featureOfInterest.properties['intersections'] = intersections
        return msg

    def knearest(self, k):
        '''
        computes a set of index entries that represnet the k-nearest entries to
        the msg feature geom
        '''
        def knearest(addmsg):
            try:
                #msg = om(init=OM_Measurement(observation=addmsg))
                msg = om(init=addmsg)
            except Exception as e:
                raise SwordfishCommonDataModelError(e.message)
            try:
                the_geom = [float(coord) for coord in msg.featureOfInterest.geometry.coordinates]#implements __geo_interface__
            except Exception as e:
                raise SwordfishCommonDataModelError(e.message)

            result = self.idx.nearest(the_geom,k,objects = True)
            k_nearest = []
            for r in result:
                gio = gi_geometry()
                gio.type = 'Polygon'
                gio.bbox = r.bbox
                gio.properties = {'name':str(r.object)}
                bl = [r.bbox[0],r.bbox[1]]
                br = [r.bbox[1],r.bbox[3]]
                tr = [r.bbox[2],r.bbox[3]]
                tl = [r.bbox[0],r.bbox[2]]
                gio.coordinates = [[bl,br,tr,tl,bl]]
                k_nearest.append(gio)
            msg.featureOfInterest.properties['k_nearest'] = k_nearest
            return msg
        return knearest

import pyproj
import shapely.geometry as SG
from shapely.ops import transform
from functools import partial

class ProjectionMixin(object):
    '''
    functionality for doing projections of input geometries with proj4 library
    '''
    def __init__(self,srs='epsg:4326'):
        #do a check to see if epsg code is passed in or a proj4 string
        self.prj = self._get_proj(srs)

    def _get_proj(self,srs_in):
        if srs_in[0:5].lower() == 'epsg:':
            return pyproj.Proj(init=srs_in)
        else:
            try:
                pindex = srs_in.index('+init')
                srs = srs_in[pindex + 6].rstrip()
                return pyproj.Proj(init=srs)
            except ValueError:
                return pyproj.Proj(srs_in)

    def projectGeom(self,in_geom,to_srs):
        '''
        will project a shapely geometry into the srs required by the child
        instances.
        '''
        to_prj = self._get_proj(to_srs)

        if to_prj.srs == self.prj.srs:
            return in_geom

        project = partial(pyproj.transform, self.prj, to_prj)

        out_geom = transform(project,in_geom)
        return out_geom



class SimpleFeatures(dict,ProjectionMixin):
    '''
    Provides a data structure for working with OGC/ISO Simple features;
    in particular, supports various representations of geospatial features,
    methods for constructing them from diffrent sources

    Features are loaded in as key-value pairs where
        key is the id of the feature
        value is the geometry of the feature
    '''
    def __init__(self,srs,*args,**kwargs):
        '''
        Arguments:
        srs: the declared projection in which features must be stored and
        calculations performed. Can be an epsg code in the form 'epsg:4326'
        or proj4 string in the form '+proj=utm +zone=27 +ellps=WGS84'
        '''
        self.update(*args,**kwargs)
        #self.srs = srs
        ProjectionMixin.__init__(self,srs)

    def load_from_geojson(self, fid, gjson):
        '''
        Constructs features into this SimpleFeatures object by providing a
        feature_id and a GeoJSON geometry
        '''
        self[fid] = SG.shape(gjson)

    def parseftr_from_geojson(self, gjson):
        '''
        Constructs features into this SimpleFeatures object by parsing the
        supplied geojson object(s)
        '''
        raise NotImplementedError
    def load_from_wkt(self, fid, gwkt):
        '''
        Constructs features into this SimpleFeatures object by providing a
        feature_id and a WKT geometry
        '''

    def as_wkb(self):
        '''
        dumps geometry as wkb for example for purposes of adding to PostGIS
        '''
        raise NotImplementedError

    def projectContext(self,to_srs):
        '''
        In-place reprojection of geoms, typically to match srs of stream
        '''
        for k,v in self.items():
            self[k] = self.projectGeom(v,to_srs)


    def rtree_streamer(self, interleaved=True):
        '''
        generator of feature bounding boxes for insertion into an RTree Index
        For each feature in Simplefeatures object, works out and returns the
        (feature id, interleaved envelope coords (xmin,ymin,xmax,ymax))
        '''
        if len(list(self.keys())) > 0:
            for k,v in self.items():
                yield (k,v.bounds)

class sfGeometryOutputs(object):
    '''
    Geometry transformations against the geometries of a streamed message
    '''
    def __init__(self, srs):
        '''
        Arguments:
        srs: the projection in which calculations must take place
        '''
        self.srs = srs
    def sfConvexHull(self,addmsg):
        '''
        Returns the convex hull of the streamed geometry, in the projection
        of the constructor to this instance
        '''
        msg_geom = addmsg#TODO: get from __geo_interface__


class sfBinaryPredicates(object):
    '''
    Provides a selection of binary predicates that allow comparison of the
    geometry of a message on a stream with that of a group of geometries
    provided as an argument to the constructor of this instance

    Will usually be used as filters

    We have :
    - intersects provided by mintersects
    - crosses provided by mcrosses
    - within provided by mwithin
    '''
    def __init__(self,geoms,srs):
        '''
        Arguments:
        geoms: SimpleFeatures instance that will be used for spatial comparison
        of features entering from a stream
        srs: the projection in which calculations must take place
        '''
        self.geoms = geoms
        self.srs = srs

    def mcrosses(self,stamp_message=False):
        '''
        determines if the stream message crosses any of the supplied
        geometries

        params:
        -------
        stamp_message:indicates that the user wants to add identity of features
          that message geometry crosses to the output message. Default is not
          to stamp

          There are some uncertainties here still, around whether to stamp a
          message or to split out into new types of messages

        if stamp_message is True, places the feature_ids of the geometries into
        the stream message where any relationship is true
        '''
        def mcrosses(addmsg):
            msg_geom = addmsg#TODO: get from __geo_interface__
            if stamp_message:
                fid_list = []
                for geomk,geomv in self.geoms.items():
                    if msg_geom.crosses(geomv):
                        fid_list.append(geomk)
                if fid_list != []:
                    #TODO: how to stamp observations with intersections or
                    #split into new messages? Don't stamp for moment
                    return addmsg
            else:
                for geomk,geomv in self.geoms.items():
                    if msg_geom.crosses(geomv):
                        return addmsg
        return mcrosses

    def mintersects(self,stamp_message=False):
        '''
        Determines if the stream message intersects any of the supplied
        geometries. Most general (and commonlyused ) form of spatial predicate.
        Inverse is the disjoint.

        params:
        -------
        stamp_message:indicates that the user wants to add identity of features
          that message geometry intersects to the output message. Default is not
          to stamp

          There are some uncertainties here still, around whether to stamp a
          message or to split out into new types of messages

        if stamp_message is True, places the feature_ids of the geometries into
        the stream message where any relationship is true
        '''
        def mintersects(addmsg):
            msg_geom = addmsg#TODO: get from __geo_interface__
            if stamp_message:
                fid_list = []
                for geomk,geomv in self.geoms.items():
                    if msg_geom.intersects(geomv):
                        fid_list.append(geomk)
                if fid_list != []:
                    #TODO: how to stamp observations with intersections or
                    #split into new messages? Don't stamp for moment
                    return addmsg
            else:
                for geomk,geomv in self.geoms.items():
                    if msg_geom.intersects(geomv):
                        return addmsg
        return mintersects

    def mwithin(self,stamp_message=False):
        '''
        determines if the stream message is contained within the supplied
        geometries

        params:
        -------
        stamp_message:indicates that the user wants to add identity of features
          that message geometry falls within to the output message. Default is
          not to stamp

          There are some uncertainties here still, around whether to stamp a
          message or to split out into new types of messages

        if stamp_message is True, places the feature_ids of the geometries into
        the stream message where any relationship is true
        '''
        def mwithin(addmsg):
            msg_geom = addmsg#TODO: get from __geo_interface__
            if stamp_message:
                fid_list = []
                for geomk,geomv in self.geoms.items():
                    if msg_geom.within(geomv):
                        fid_list.append(geomk)
                if fid_list != []:
                    #TODO: how to stamp observations with intersections or
                    #split into new messages? Don't stamp for moment
                    return addmsg
            else:
                for geomk,geomv in self.geoms.items():
                    if msg_geom.within(geomv):
                        return addmsg
        return mwithin