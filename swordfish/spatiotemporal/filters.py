# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Swordfish Spatial Filters

RTree index based filters and geometry based filters

Created on 15/11/2015 09:00:00
@author: gmcferren

"""

from swordfish.core import _make_getter_able
from swordfish.api import indexattr, swordfishfunc, swordfishproperty
from swordfish.spatiotemporal.st_core import SimpleFeatures, ProjectionMixin
from swordfish.spatiotemporal.st_core import SwordfishSpatialProcessingError

from rtree import index
from shapely.geometry import shape #simple load of geojson coordinates
from shapely.ops import transform
import pyproj
from functools import partial




class RTreeIndex():
    def __init__(self,bboxes,work_interleaved = True):
        '''
        Constructs an internal RTree index that can be used to test streamed
        geospatial data against for intersection and nearest neighbours
        Params:
        bboxes an iterable (e.g. a generator) returning sequences of form
        (id,[xmin, ymin, zmin, xmax, ymax, zmax],object) for work_interleaved = True
        (id,(minx, maxx, miny, maxy, minz, maxz),object) for work_interleaved = False
        '''
        self.idx = index.Index()
        self.idx.interleaved = work_interleaved
        for bbox in bboxes:
            self.idx.insert(bbox[0],bbox[1],bbox[2])
        self.ig_i_coordinate = _make_getter_able(None, 'featureOfInterest.geometry.coordinates')
    def insert(self,bbox_obj,work_interleaved = True):
        '''
        bbox_obj is a sequence of form
        (id,[xmin, ymin, zmin, xmax, ymax, zmax],object) for work_interleaved = True
        (id,(minx, maxx, miny, maxy, minz, maxz),object) for work_interleaved = False
        '''
        self.idx.insert(bbox_obj[0],bbox_obj[1],bbox_obj[2])

    @swordfishproperty
    def rt_intersects_filter(self, addmsg=None):
       '''
       computes true for messages whose geometry payload intersects the index
       somewhere, i.e. the return from rtree.intersection is > 0 and forwards
       only those messages that compute to True

       function used as a filter, typically to compare a tream to some set of pre-existing data

       a stream-stream spatial filter is a spatial join and dealt with in the joins submodule
       '''
#       try:
#           msg = om(init=OM_Measurement(observation=addmsg))
#       except Exception as e:
#           raise SwordfishCommonDataModelError(e.message)
       try:
           the_geom = [float(coord) for coord in self.ig_i_coordinate(addmsg)] #implements __geo_interface__
           result = list(self.idx.intersection(the_geom,objects="raw"))
       except Exception as e:
           raise SwordfishSpatialProcessingError(e.message)

       if len(result) > 0:
           return True
       else:
           return False

class BinaryPredicateSpatialFilters(ProjectionMixin):
    '''
    Provides a selection of binary predicates that allow comparison of the
    geometry of a message on a stream with that of a group of geometries
    provided as an argument to the constructor of this instance

    Usage pattern is likely one of two cases:
    - streamed geometry relates to a single provided geometry
    - streamed geometry relates to a provided unordered set of geometries

    In the latter case, the test returns the streamed message if any of the
    supplied geometries has a relationship, except for the disjoint case,
    where the streamed geometry must have a disjoint relationship with all the
    supplied geometries
    '''
    def __init__(self,geoms,stream_srs='epsg:4326',project_stream=True):
        '''
        Arguments:
        geoms: SimpleFeatures instance that will be used for spatial comparison
        of features entering from a stream
        stream_srs: the projection of data on stream, default is latlon wgs84;
        project_stream:force stream data srs into same projection as test geoms
          by default  - if set to False, project geoms data to stream data srs
        '''
        self.geoms = geoms
        self.project_stream = project_stream
        ProjectionMixin.__init__(self,stream_srs)
        self.foi_coordinates = _make_getter_able(None, 'featureOfInterest.geometry')

    @swordfishproperty
    def sp_crosses_filter(self,addmsg=None):
        '''
        determines if the stream message crosses any of the supplied
        geometries

        '''
        if self.project_stream == True:
            msg_geom = self.projectGeom(shape(self.foi_coordinates(addmsg)))
        else:
            msg_geom = shape(self.foi_coordinates(addmsg))
        #for geomk,geomv in self.geoms.iteritems():
        #    if msg_geom.crosses(geomv):
        #        return True
        return any(map(msg_geom.crosses,iter(self.geoms.values())))

    @swordfishproperty
    def sp_intersects_filter(self,addmsg=None):
        '''
        Determines if the stream message intersects any of the supplied
        geometries. Most general (and commonlyused ) form of spatial predicate.
        Inverse is the disjoint.
        '''
        if self.project_stream == True:
            msg_geom = self.projectGeom(shape(self.foi_coordinates(addmsg)))
        else:
            msg_geom = shape(self.foi_coordinates(addmsg))
        #for geomk,geomv in self.geoms.iteritems():
        #    if msg_geom.intersects(geomv):
        #        return True
        return any(map(msg_geom.intersects,iter(self.geoms.values())))

    @swordfishproperty
    def sp_disjoint_filter(self,addmsg=None):
        '''
        Determines if the stream message is disjoint from all of the supplied
        geometries. Inverse is the intersection.
        '''
        disjoint = True
        if self.project_stream == True:
            msg_geom = self.projectGeom(shape(self.foi_coordinates(addmsg)))
        else:
            msg_geom = shape(self.foi_coordinates(addmsg))

        #for geomk,geomv in self.geoms.iteritems():
        #    if not msg_geom.disjoint(geomv):
        #        disjoint = False
        #        break
        #if disjoint == True:
        #    return True
        return all(map(msg_geom.disjoint,iter(self.geoms.values())))

    @swordfishproperty
    def sp_within_filter(self,addmsg=None):
        '''
        Determines if the stream message is spatially within any of the supplied
        geometries.
        '''
        if self.project_stream == True:
            msg_geom = self.projectGeom(shape(self.foi_coordinates(addmsg)))
        else:
            msg_geom = shape(self.foi_coordinates(addmsg))

        #for geomk,geomv in self.geoms.iteritems():
        #    if msg_geom.within(geomv):
        #        return True
        return any(map(msg_geom.within,iter(self.geoms.values())))

    @swordfishproperty
    def sp_contains_filter(self,addmsg=None):
        '''
        Determines if the stream message spatially contains any of the supplied
        geometries.
        '''
        if self.project_stream == True:
            msg_geom = self.projectGeom(shape(self.foi_coordinates(addmsg)))
        else:
            msg_geom = shape(self.foi_coordinates(addmsg))

        #for geomk,geomv in self.geoms.iteritems():
        #    if msg_geom.contains(geomv):
        #        return True
        return any(map(msg_geom.contains,iter(self.geoms.values())))

    @swordfishproperty
    def sp_touches_filter(self,addmsg=None):
        '''
        Determines if the stream message spatially touches any of the supplied
        geometries.
        '''
        if self.project_stream == True:
            msg_geom = self.projectGeom(shape(self.foi_coordinates(addmsg)))
        else:
            msg_geom = shape(self.foi_coordinates(addmsg))

        #for geomk,geomv in self.geoms.iteritems():
        #    if msg_geom.touches(geomv):
        #        return True
        return any(map(msg_geom.touches,iter(self.geoms.values())))
