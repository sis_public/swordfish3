# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Fri April 29 2016 at 14:30
@author: gmcferren

The purpose of this module is to allow Swordfish Streams to exist over a RethinkDB
messaging fabric. RethinkDB is a realtime database that pushes data to clients
"""
from threading import Event, Thread
import rethinkdb as r
from rethinkdb.errors import ReqlDriverError
from .core import Stream, AttrDict


class RethinkDBStream(Stream):
    '''
    RethinkDB is a realtime database with a push architecture, primarily
    designed for servicing reactive web applications. It has certain durability
    guarantees that may be manipulated to allow RethinkDB to fulfill more of
    a streaming role (ie turn on soft reliability, turn off commit acks). Also,
    since the db aims to store everything, and a stream could be infinite, some
    care should be taken to purge the db of old records. This may not be a task
    swordfish must do though, perhaps it is more of a dba role

    Needs a db server instance (host, port) to form a connection, credentials,
    a db instance,  a table/ filtered table to bind to to receive messages,
    and an optional table to use in publishing, a reliability flag (0,1,2,3)

    The reliability/QOS flag maps to RethinkDB client arguments as follows:
    {0:soft reliability; no acks,
     1:soft reliability; acks,
     2:hard reliability; no acks,
     3:hard reliability; acks} - default is 0

    Each component that attaches to a RethinkDB stream will be considered a new
    RethinkDB Client, with its own connection (NB, since python client libs are
    not threadsafe), callbacks and topics

    Messages are pushed to the RethinkDB transport via its client API, and
    received from the transport using its changeset API

    url scheme: rethinkdb://user:pass@host:28015/database
    '''
    def __init__(self, url, publish_table, timeout=1.0, name=None, qos=0,
                 immortal=False):
        '''necessary to have a publish table to be a RethinkDB stream'''
        self.rdb = r.RethinkDB()
        url, self.rdbport, self.rdbauths, self.rdb_db = self._unpack_url(url)
        #print(url)
        #print(self.rdbport)
        #print(self.rdbauths)
        #print(self.rdb_db)

        Stream.__init__(self, url, timeout)
        self.immortal = immortal
        self.qos = self._describe_qos(qos)
        self.callbacks = {}
        self.debug_name = "RethinkDBStream_%s"%(self.url)
        self.client_id = name or ''
        self.publish_table = publish_table or self.client_id
        self.rdb_producer_conn = None
        self.rdb_table = None
        self.table_changes_thread = None
        self._connect_rethinkdb_stream()
        self.subscription = None
        self.i = 0
        self.end_event = Event()
        self.finish_changes_event = Event()

    def _describe_qos(self, qos_in):
        '''responsible for parsing QOS flag into reliability and acks params'''
        if qos_in == 0:
            q_d = {'d':'soft', 'rc': False}
        elif qos_in == 1:
            q_d = {'d':'soft', 'rc': True}
        elif qos_in == 2:
            q_d = {'d':'hard', 'rc': False}
        elif qos_in == 3:
            q_d = {'d':'hard', 'rc': True}
        else:
            q_d = {'d':'soft', 'rc': False}
        return q_d

    def _unpack_url(self, url_in):
        '''
        takes the url and makes it rethinkdb ready, since it seems not to
        have a url based connect method
        '''
        url_list = url_in.rstrip('/').lstrip('rethinkdb://').split('@')
        auth = {"username":'admin', "password":''}
        port = 28015
        d_b = 'test'
        if len(url_list) == 2:

            auth_list = url_list[0].split(':')

            if len(auth_list) == 2:
                auth = {"username":auth_list[0], "password":auth_list[1]}
            else:
                auth = {"username":auth_list[0], "password":''}
            urlport_db = url_list[1].split('/')
            if len(urlport_db) == 2:
                d_b = urlport_db[1]
            url_port = urlport_db[0].split(':')
            url = url_port[0]
            if len(url_port) == 2:
                port = url_port[1]

        else:
            urlport_db = url_list[0].split('/')
            if len(urlport_db) == 2:
                d_b = urlport_db[1]
            url_port = urlport_db[0].split(':')
            url = url_port[0]
            if len(url_port) == 2:
                port = url_port[1]
        return (url, port, auth, d_b)

    def _connect_rethinkdb_stream(self):
        try:
            self.rdb_producer_conn = self.rdb.connect(
                host=self.url,
                port=self.rdbport,
                db=self.rdb_db,
                user=self.rdbauths.get('username'),
                password=self.rdbauths.get('password'))
            self.rdb_table = self.rdb.table(self.publish_table)
            print(self.rdb_table)
        except ReqlDriverError as err:
            self.error("Unable to connect to reThink:%s" % err)
        #connection related concerns
        '''
        self.mqtt_producer.on_connect = self._on_mqtt_connect
        self.mqtt_producer.connect(host=self.url, port=self.mqttport)
        self.mqtt_producer.on_publish = self._on_mqtt_publish
        '''
        for obj, callback in list(self.callbacks.items()):
            self.__detatch__(obj)
            self.__attach__(obj, callback)
    '''
    def _on_mqtt_connect(self, client, userdata, flags, rc):
        self.info("Connected with result code " + str(rc) +
                  " for client " + client._client_id)
    def _on_mqtt_subscribe(self, client, userdata, mid, granted_qos):
        self.info("Subscribed with QOS " + str(granted_qos) +
                  " for client " + client._client_id)
    def _on_mqtt_disconnect(self, client, userdata, flags, rc):
        self.info("Disconnected with result code " + str(rc) +
                  " for client " + client._client_id)
    def _on_mqtt_unsubscribe(self, client, userdata, mid):
        self.info("Unsubscribed for client %s", client._client_id)

    def _on_mqtt_publish(self, client, userdata, mid):
        self.info("Published message %s for client %s", mid, client._client_id)
    '''
    #Required to be a Stream
    def cleanup(self):
        '''rethinkdb clean up...'''
        for key in list(self.callbacks.keys()):
            self.__detatch__(key)
        try:
            self.rdb_producer_conn.close(noreply_wait=True)
            self.finish_changes_event.set()
            self.table_changes_thread.join()
            self.subscription.close()
        except AttributeError as err:
            self.error("Unable to close connection: %s" % err)
        self.rdb_table = None
        print("tct joined in cleanup")
        #self.mqtt_producer.loop_stop()
        #self.mqtt_producer.disconnect()

    def __del__(self):
        '''make sure rethinkdb bindings are cleaned up...'''
        self.cleanup()
    #between the next two functions is the meat of subscribing
    #likely have to use message_callback_add() functionality
    #or may have to have a client per attach, i think
    def _get_consumer(self):
        if self.subscription is not None:
            return

        def process_rdb_message(message):  # change params as appr.
            try:
                body_obj = AttrDict(json=message)
            except Exception:
                body_obj = AttrDict({"non-compliant":True,
                                     "value":message})
            for callback in list(self.callbacks.values()):
                callback(body_obj)

        def fetch_rdb_messages(call_back):
            '''
            RethinkDB exposes an iterator based API for receiving data; this
            does npot fit the swordfish push based approach naturally; so this
            function is passed to a thread that receives the data from the
            RethinkDB iterator and passes to a callback
            '''
            #while not self.finish_changes_event.isSet() :
            for change in self.rdb_table.changes(
                    include_types=True).run(self.rdb_producer_conn):
                if change.get("type") == "add":
                    call_back(change.get("new_val"))
                if self.finish_changes_event.isSet():
                    break
        try:
            self.subscription = self.rdb.connect(
                host=self.url,
                port=self.rdbport,
                db=self.rdb_db,
                user=self.rdbauths.get('username'),
                password=self.rdbauths.get('password'))
        except ReqlDriverError as err:
            self.error("Unable to connect to reThink:%s" % err)

        if self.subscription:
            self.table_changes_thread = Thread(
                name="table_changes",
                target=fetch_rdb_messages,
                args=(process_rdb_message,))

            #self.table_changes_thread.setDaemon(True)
            self.table_changes_thread.start()
            '''mqttclient = mqtt.Client(client_id=self.client_id,
                                     clean_session=self.clean_session)
            if self.mqttauths:
                mqttclient.username_pw_set(self.mqttauths.get('username'),
                                           self.mqttauths.get('password'))
            #connection related concerns
            mqttclient.on_connect = self._on_mqtt_connect
            mqttclient.connect(host=self.url, port=self.mqttport)
            #subscription related concerns
            #mqttclient.on_subscribe = self._on_mqtt_subscribe
            mqttclient.on_message = process_mqtt_message

            mqttclient.subscribe(self.publish_topic)
            mqttclient.loop_start()
            self.subscription = mqttclient'''

    def __attach__(self, obj, callback):
        self.callbacks[obj] = callback
        self._get_consumer()

    #Required to be a Stream
    def __detatch__(self, obj):
        self.debug("__detatch__")
        del self.callbacks[obj]
        if len(self.callbacks) == 0:
            self.finish_changes_event.set()
            if self.table_changes_thread:
                self.table_changes_thread.join()
            print("tct joined in detatch")
            if self.subscription:
                self.subscription.close()

    #Required to be a Stream
    #message is an AttrDict
    def __call__(self, message):
        '''
        so map to the RethinkDB publisher client on the Stream Instance
        '''
        try:
            payload = message.__json__
        except ValueError as err:
            self.error("Publish of rethinkdb message failed: %s" % str(err))
        else:
            #TODO:think about what to do if return_changes is True
            record = self.rdb_table.insert(
                payload, durability=self.qos['d'],
                return_changes=self.qos['rc'],
                conflict='replace').run(self.rdb_producer)
