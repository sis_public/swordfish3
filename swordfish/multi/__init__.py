# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Wed Oct 28 11:40:34 2015

@author: tvzyl
"""
import rpyc
from weakref import WeakKeyDictionary

class MultiProcess(object):
    def __init__(self, target=None):
        if target is None:
            self.service = rpyc.classic.connect_multiprocess()
        else:
            self.service = rpyc.classic.connect_multiprocess({'target':target})
            self.service.execute("ns=target()")
            self.service.execute('for k in ns: exec "%s=ns[k]"%k')
            for k in self.service.namespace['ns']:
                self.__dict__[k] = self.service.namespace[k]
        self.service.execute("import swordfish.sink")
        self.service.execute("import swordfish.source")
        self.service.execute("import swordfish.stream")
        self.sink = self.service.modules.swordfish.sink
        self.source = self.service.modules.swordfish.source
        self.stream = self.service.modules.swordfish.stream

    def close(self):
        for i in self.__dict__:
            try:
                i.close()
            except AttributeError:
                pass
        self.service.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

class stream:
    services = WeakKeyDictionary()
    @classmethod
    def MQTTStream(clazz, *args, **kwargs):
        """See swordfish.stream.MQTT"""
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.stream")
        m = service.eval("swordfish.stream.MQTTStream(*args, **kwargs)")
        clazz.services[m] = service
        return m

    @classmethod
    def ThreadBroadcastStream(clazz, *args, **kwargs):
        """See swordfish.stream.ThreadBroadcastStream"""
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.stream")
        m = service.eval("swordfish.stream.ThreadBroadcastStream(*args, **kwargs)")
        clazz.services[m] = service
        return m

    @classmethod
    def ThreadSplitStream(clazz, *args, **kwargs):
        """See swordfish.stream.ThreadSplitStream"""
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.stream")
        m = service.eval("swordfish.stream.ThreadSplitStream(*args, **kwargs)")
        clazz.services[m] = service
        return m


class sink:
    services = WeakKeyDictionary()
    @classmethod
    def Print(clazz, *args, **kwargs):
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.sink")
        m = service.eval("swordfish.sink.Print(*args, **kwargs)")
        clazz.services[m] = service
        return m

    @classmethod
    def Drain(clazz, *args, **kwargs):
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.sink")
        m = service.eval("swordfish.sink.Drain(*args, **kwargs)")
        clazz.services[m] = service
        return m

    @classmethod
    def MongoDB(clazz, *args, **kwargs):
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.sink")
        m = service.eval("swordfish.sink.MongoDB(*args, **kwargs)")
        clazz.services[m] = service
        return m

    @classmethod
    def WebSocket(clazz, *args, **kwargs):
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.sink")
        m = service.eval("swordfish.sink.WebSocket(*args, **kwargs)")
        clazz.services[m] = service
        return m

    @classmethod
    def Method(clazz, *args, **kwargs):
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.sink")
        m = service.eval("swordfish.sink.Method(*args, **kwargs)")
        clazz.services[m] = service
        return m

    @classmethod
    def SMTP(clazz, *args, **kwargs):
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.sink")
        m = service.eval("swordfish.sink.SMTP(*args, **kwargs)")
        clazz.services[m] = service
        return m

    @classmethod
    def Filer(clazz, *args, **kwargs):
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.sink")
        m = service.eval("swordfish.sink.Filer(*args, **kwargs)")
        clazz.services[m] = service
        return m

class api:
    services = WeakKeyDictionary()
    @classmethod
    def map(clazz, *args, **kwargs):
        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.api")
        m = service.eval("swordfish.api.map(*args, **kwargs)")
        clazz.services[m] = service
        return m

    @classmethod
    def fold(clazz, by, *args, **kwargs):
        service = rpyc.classic.connect_multiprocess({'by':by,'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.api")
        m = service.eval("swordfish.api.fold(by, *args, **kwargs)")
        clazz.services[m] = service
        return m

    @classmethod
    def reduce(clazz, by, *args, **kwargs):
        service = rpyc.classic.connect_multiprocess({'by':by,'args':args, 'kwargs':kwargs})
        service.execute("import swordfish.api")
        m = service.eval("swordfish.api.reduce(by, *args, **kwargs)")
        clazz.services[m] = service
        return m

#    @classmethod
#    def join(clazz, *args, **kwargs):
#        service = rpyc.classic.connect_multiprocess({'args':args, 'kwargs':kwargs})
#        service.execute("import swordfish.api")
#        m = service.eval("swordfish.api.map(*args, **kwargs)")
#        clazz.services[m] = service
#        return m

    @classmethod
    def filter(clazz, boolean_function):
        service = rpyc.classic.connect_multiprocess({'boolean_function':boolean_function})
        service.execute("import swordfish.api")
        m = service.eval("swordfish.api.filter(boolean_function)")
        clazz.services[m] = service
        return m

