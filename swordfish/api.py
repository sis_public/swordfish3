# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Tue Mar 11 14:59:23 2014

@author: tvzyl
"""
#import pyximport; pyximport.install()
from swordfish.cythonext.streamProcessor import fold, map, reduce, join, explodelist
from swordfish.cythonext.streamProcessor import _LEFT, _RIGHT, _INNERJOIN
from swordfish.cythonext.core import AttrDict, StreamProcessor, filter
from swordfish.exceptions import SwordfishSyntaxError
from swordfish.core import _make_getter_able, itemgetter_in
from swordfish.core import Builder

import builtins

#other libs
from inspect import getargspec

_ = Builder()
LEFT = _LEFT()
RIGHT = _RIGHT()

class indexattr:
    def __init__(self, func, *args, **kwargs):
        self.func = func
        self.args = args
        self.kwargs = kwargs
        self.__indexattrable__ = True

    def __getitem__(self, attr):
        return _make_getter_able(self.func, attr, self.args, self.kwargs)


class swordfishproperty(property):
    def __init__(self, fget=None, *args, **kwargs):
        def newget(*args, **kwargs):
            return indexattr(fget, *args, **kwargs)
        property.__init__(self, fget=newget, *args, **kwargs)

def swordfishfunc(func):
    argspec = getargspec(func)
    default_count = len(argspec.defaults) if argspec.defaults else 0
    arg_count = len(argspec.args) if argspec.args else 0
    args = argspec.args[0:arg_count-default_count]
    kwargs = list(zip(argspec.args[arg_count-default_count:], argspec.defaults)) if argspec.defaults else {}
    # filter changed from list to iterator in Python3
    extra_kwargs = list(
        builtins.filter(
            lambda a: a[0] not in ['addmsg', 'delmsg', 'win', 'oldval', 'span', 'rate'],
            kwargs))
    deco_func = indexattr(func)
    if len(args)+len(extra_kwargs)>0:
        s = """lambda %s: indexattr(func, %s)"""
        for arg in args:
            if arg == 'self': continue
            s = s%('%s, %s', '%s, %s')
            s = s%(arg, '%s', arg, '%s')
        for arg, default in extra_kwargs:
            s = s%('%s=%s, %s', '%s=%s, %s')
            s = s%(arg, default, '%s', arg, arg, '%s')
        s = s%('','')
        prettyfunc = eval(s, {'indexattr':indexattr, 'func':func})
        prettyfunc.__name__ = func.__name__
        prettyfunc.__doc__ = func.__doc__
        return prettyfunc
    else:
        return deco_func

@swordfishfunc
def const(constant, addmsg=None):
    return constant

@swordfishfunc
def empty(addmsg=None):
    return {}

@swordfishfunc
def null(addmsg=None):
    return None

@swordfishfunc
def replace(addmsg=None):
    return {}



"""
class HASH_EQUALS():
    def __init__(self):
        self._left_hash = {}
        self._right_hash = {}


    def __call__(self, is_left_stream, val, delta_add_vals, delta_del_vals, delta_add):
        if is_left_stream:
            _hash = self._left_hash
        else:
            _hash = self._right_hash
        try:
            for msg in delta_add:
                _hash[val][id(msg)] = msg
        except KeyError:
            _hash[val] = {id(message):message}
        for msg in delta_del:
            del _hash[self._primary_ig_i(msg)][id(msg)]
"""