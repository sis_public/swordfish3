# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Thu Nov 20 17:16:44 2014

@author: tvzyl
"""
import logging
from time import sleep
logger = logging.getLogger('swordfish')

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def warning_to_file():
    fh = logging.FileHandler('error.log')
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

#Add a console output for info
def info_to_console():
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

def debug_to_console():
    #Add a console output to do debugging
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

def keep_alive(raise_KeyboardInterrupt_again=False):
    try:
        while True:
            sleep(1)
    except KeyboardInterrupt:
        logger.info("Got a KeyboardInterrupt")
        if raise_KeyboardInterrupt_again: raise

def keep_loop(timeout=1.0, raise_KeyboardInterrupt_again=False):
    try:
        sleep(timeout)
        return True
    except KeyboardInterrupt:
        logger.info("Got a KeyboardInterrupt")
        if raise_KeyboardInterrupt_again: raise
