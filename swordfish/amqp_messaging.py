# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Fri Feb 21 15:48:08 2014

@author: gmcferren

The purpose of this module is to allow Swordfish Streams to exist over an AMQP fabric
"""
from swordfish.core import AttrDict, Stream
from kombu.connection import Connection
from kombu.messaging import Queue, Consumer, Producer, Exchange
from weakref import WeakKeyDictionary
import threading
import socket
from time import sleep
import errno
from socket import error as SocketError
from ujson import dumps, loads

from kombu import enable_insecure_serializers
print("Enabling Insecure Serializers")
enable_insecure_serializers()

class KombuStream(Stream):
    '''
    To match the notion of a Stream, this represnts the full AMQP binding

    Needs broker,channel,read_exchange [and exchange type, assume a direct
    binding to a topic exchange as default],read_queue,routing_key

    The callback on the stream is a Consumer on a Queue (an Exchange will need
    to be put down to get the full Consumer/Producer functionality)
    -> stream callback mapped to AMQP Consumer callback

    Messages received on the callback get pushed onto the internal stream queue
    for use by the iterator

    To complete the stream, a Producer needs to be put down before the exchange
    to allow automated stream deployment by StreamProcessors

    So an AMQP Stream looks like P->E->Q->callback->callback->iterator

    url scheme: amqp://user:pass@host:10000/vhost
    '''
    def __init__(self, url, timeout=1, exchange_name=None, exchange_type='topic',
                 exchange_auto_delete=False, exchange_durable=False, immortal=False,
                 queue_name='', queue_auto_delete=True, queue_durable=False,
                 routing_key=None, serializer='json'):

        Stream.__init__(self, url, timeout)
        self.exchange_name = exchange_name
        self.exchange_type = exchange_type
        self.exchange_auto_delete = exchange_auto_delete
        self.exchange_durable = exchange_durable

        self.queue_name = queue_name
        self.queue_auto_delete = queue_auto_delete
        self.queue_durable = queue_durable

        self.routing_key =routing_key

        self.serializer = serializer

        self.immortal = immortal
        self.callbacks = WeakKeyDictionary()
        self.debug_name = "KombuStream %s"%(self.url)
        self._stop = threading.Event()
        self._has_callbacks = threading.Event()
        self._has_callbacks.clear()
        self._connect_kombu_stream()
        self.backoff = 1

    def _connect_kombu_stream(self):
        self.conn = Connection(self.url, 1)
        if self.exchange_name:
            self.exchange = Exchange(self.exchange_name, self.exchange_type, self.conn,
                                     durable=self.exchange_durable, auto_delete=self.exchange_auto_delete)
        else:
            self.exchange = Exchange('amq.topic', 'topic', self.conn, durable=True)
        self.producer = Producer(self.conn, self.exchange, self.routing_key, serializer=self.serializer)
        for obj, tup in list(self.callbacks.items()):
            callback, consumer, routing_key = tup
            self.__detatch__(obj)
            self.__attach__(obj, callback)
        self._stop.clear()
        self._thread = threading.Thread( target=self.service_callbacks )
        self._thread.isDaemon = True
        self._thread.start()

    def service_callbacks(self):
        while not self._stop.isSet():
            self._has_callbacks.wait()
            for _, consumer, _ in list(self.callbacks.values()):
                while True:
                    try:
                        consumer.connection.drain_events(timeout=1)
                    except socket.timeout as st:
                        self.debug( st )
                    except SocketError as e:
                        if self.immortal:
                            self.info(e)
                            try:
                                self.info("Trying to reconnect to Kombu")
                                self.cleanup()
                                self._connect_kombu_stream()
                            except SocketError:
                                self.backoff = min(self.backoff*2, 3600)
                                self.info("Sleeping for %s seconds", self.backoff)
                                sleep(self.backoff)
                        else:
                            self.critical(e)
                            raise
                    else:
                        self.backoff = 1
                    finally:
                        break

    def flush(self):
        #producer.publish ensures deliver atleast once semantics
        return True

    #Required to be a Stream
    def cleanup(self):
        self.producer.close()
        self._stop.set()
        for key in list(self.callbacks.keys()):
            self.__detatch__(key)
        self._has_callbacks.set()
        try:
            self._thread.join()
            self._has_callbacks.clear()
        except RuntimeError as re:
            self.debug( str(re) )
        self.conn.close()

    def __del__(self):
        '''make sure amqp bindings are cleaned up...'''
        self.cleanup()

    def _get_consumer(self, callback, routing_key):
        def process_amqp_message(body, message):
            try:
                body_obj = AttrDict( body )
            except Exception as e:
                if isinstance(body, str):
                    body = body.strip().replace("'",'"')
                    try:
                        body_obj = AttrDict(loads( body ))
                        self.debug( "Got non-compliant message: %s with exception: %s, fixed it", body, e )
                    except Exception as e:
                        body_obj = AttrDict( {"non-compliant":True, "value":body} )
                        self.warning( "Got non-compliant message: %s with exception: %s", body, e )
                else:
                    self.critical( "Got non-compliant message: %s with exception: %s", body, e )
                    raise
            callback( body_obj )
            message.ack()
        conn = Connection( self.url, 1 )
        queue = Queue(name=self.queue_name, exchange=self.exchange, routing_key=routing_key,
                      channel=conn, durable=self.queue_durable, auto_delete=self.queue_auto_delete )
        consumer = Consumer( conn, queue, callbacks=[process_amqp_message], accept=[self.serializer] )
        consumer.consume()
        return consumer

    def __attach__(self, obj, callback):
        if self.routing_key is None:
            routing_key="#"
        else:
            routing_key = self.routing_key
        consumer = self._get_consumer(callback, routing_key)
        self.callbacks[obj] = (callback, consumer, routing_key)
        self._has_callbacks.set()

    #Required to be a Stream
    def __detatch__(self, obj):
        self.debug(  "__detatch__" )
        if obj in self.callbacks:
            callback, consumer, routing_key = self.callbacks[obj]
            del self.callbacks[obj]
            if len(self.callbacks) == 0:
                self._has_callbacks.clear()
            try:
                consumer.close()
            except:
                self.debug( "consumer close failed" )

    #Required to be a Stream
    #message is an AttrDict
    def __call__(self, message):
        '''so map to the AMQP callback'''
        try:
            self.producer.publish(message.__data__)
        except Exception as e:
            if self.immortal:
                self.error( str(e) )
                self.info( "A message got discarded, I'm immortal" )
            else:
                self.critical( str(e) )
                raise

