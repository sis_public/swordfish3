# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Tues August 18 2015

@author: gmcferren
"""
import subprocess
import time
from swordfish.source import SourceStreamProcessor
from swordfish.sink import Drain
from swordfish.core import AttrDict


class tailedFileSource(SourceStreamProcessor):
    '''
    Attach to a file as a stream source

    Some use cases:
    file is a reservoir that you want to turn into a stream
    file is being read from as it is being written to, e.g. a log
    a combinatation of the above, e.g. start streaming from beginning of log,
    and carry on

    Implemented over a subprocess that tails the selected file; problematic in
    that it can orphan the process
    '''
    def __init__(self, url,rate=1,*args, **kwargs ):
        SourceStreamProcessor.__init__( self, autostart=False, *args,  **kwargs )
        self.url = url
        self.rate=rate
        #self.start()
    def _sourceprocessor(self):
        print("entering sourceprocessor thread")
        #self.processor = subprocess.Popen(['/usr/bin/tail','-f',self.url,'-n 86500'], stdout=subprocess.PIPE)
        self.processor = subprocess.Popen(['/usr/bin/tail','-f',self.url,'-n %i' % self.rate], stdout=subprocess.PIPE)
        time.sleep(0.05)

        while True:
            try:
                line = self.processor.stdout.readline()
                self.put( {'value':line} )
            except Exception as e:
                self.put( {1:1} )
                print(e.message)
    def process(self, message, stream_id):
        return message
    def __del__(self):
        try:
            self.processor.kill()
        except:
            pass


class constantReadFileSource(SourceStreamProcessor):
    '''
    Attach to a file as a stream source

    Some use cases:
    file is a reservoir that you want to turn into a stream
    file is being read from as it is being written to, e.g. a log
    a combinatation of the above, e.g. start streaming from beginning of log,
    and carry on

    Implemented over a file handle
    '''
    def __init__(self, url,start_position="end",*args, **kwargs ):
        SourceStreamProcessor.__init__( self, autostart=False, *args,  **kwargs )
        self.url = url
        self.start_position=start_position

    def _sourceprocessor(self):
        with open(self.url,'r') as rf:
            if self.start_position == "end":
                #goto end of file and start reading as stream
                rf.seek(0,2)
            while True:
                line = rf.readline()
                if line:
                    self.put(AttrDict({'value':line}))
                else:
                    time.sleep(0.00001)#100 000 mps rate assumed

    def process(self, message, stream_id):
        return message


class listReadFileSource(SourceStreamProcessor):
    """
    Attach to a (growing) set of files as a stream source

    Some use cases:
    set of files is a reservoir that you want to turn into a stream
    e.g. start streaming from beginning of a set of log files, and carry on
    till the last one (with a bit of leeway to allow more data to be written
    to a given file)

    Implemented over a file handle
    """
    def __init__(self, url,start_position="end",*args, **kwargs ):
        SourceStreamProcessor.__init__( self, autostart=False, *args,  **kwargs )
        self.url = url
        self.start_position=start_position
        self.know_finished = False

    def _sourceprocessor(self):
        with open(self.url,'r') as rf:
            if self.start_position == "end":
                #goto end of file and start reading as stream
                rf.seek(0,2)
            while True:
                line = rf.readline()
                if line:
                    self.know_finished = False
                    self.put(AttrDict({'value':line}))
                else:
                    self.know_finished = True
                    #10 000 mps rate assumed,
                    #allows chance to read more
                    time.sleep(0.0001)


    def process(self, message, stream_id):
        return message
"""
#usage:
from swordfish.sink import Print,SinkProcessor
from threading import Event
from swordfish.mqtt_messaging import MQTTStream

class Timer(SinkProcessor):
    def __init__(self, stop_msg={1:1}, end_event=None, *args, **kwargs):
        self.stop_msg = stop_msg
        self.start_time = time.time()
        self.count = 0
        if not end_event is None:
            self.end_event = end_event
        else:
            self.end_event = Event()
        SinkProcessor.__init__(self, *args, **kwargs)

    def process(self, message, stream_id):
        self.count += 1
        #print message, " same as stopper: ", message == self.stop_msg
        if message==self.stop_msg:
            self.end_time = time.time()
            self.end_event.set()
            print "Finished Timing %s msgs in %s secs: %s msgs/sec"%(self.count, self.end_time-self.start_time, self.count/(self.end_time-self.start_time) )
        return None

end_event = Event()
ms = MQTTStream('mqtt://yak.dhcp.meraka.csir.co.za:1883/','seafar')
#ms = MQTTStream('mqtt://localhost:1883/','seafar')
myloc = "/home/gmcferren/ict4eo/ict4eo/trunk/python_code/swordfish_projects/seafar/data/nmea-sample"
seafar_source = tailedFileSource(myloc,86000)
#seafar_source = tailedFileSource('/tmp/xyz.csv',86000)
#msgs = [{"1":1} for i in xrange(10000)]
#p = Print()
#{'value': '{1:1}\n'}
#t = Timer(stop_msg = msgs[-1] ,end_event=end_event)
#d=Drain()

#ms.__attach__( callback, callback )
seafar_source>>ms
start_time = time.time()
seafar_source.start()
ms.end_event.wait()
end_time = time.time()
#print 50000/(end_time-start_time), (end_time-start_time)
print 85194/(end_time-start_time), (end_time-start_time)


#seafar_source>>ms>>t
#seafar_source>>t
#end_event.wait()
#ms.close()
"""

"""
def filler():
    msg_count = 50000
    count = 0
    with open('/tmp/xyz.csv','w') as f:
        while count < msg_count:
            f.write('{"%s":"%s"}\n'%("a"*1024,"a"*1024))
            f.flush()
            #time.sleep(0.00001)
            count += 1
        f.write('{1:1}\n')
        f.flush()

f = filler()
"""