# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Tue Dec 17 17:36:01 2013

@author: tvzyl
"""

import threading
import subprocess
import ujson
from queue import Queue, Empty
import websocket
import psycopg2
from time import sleep
from collections import deque
import logging
from swordfish.exceptions import StreamError
from swordfish.core import stream_lookup_dict, AttrDict
from swordfish.core import BroadcastBifurcation, SplitBifurcation
#for use by postgres classes
import datetime
import zmq
import msgpack

module_logger = logging.getLogger('swordfish.stream')

#Refactoring to place all  streams in the stream module
from swordfish.amqp_messaging import KombuStream
from swordfish.mqtt_messaging import MQTTStream
from swordfish.rethinkdb_messaging import RethinkDBStream


class ZMQStream(BroadcastBifurcation):
    def __init__(self, url,  timeout=10.0):
        self._stop = threading.Event()
        context = zmq.Context()

        self.receiver = context.socket(zmq.SUB)
        #self.receiver.bind("tcp://*:5556")
        self.receiver.bind(url)
        self.receiver.setsockopt_string(zmq.SUBSCRIBE, "")
        self.receiver.setsockopt(zmq.RCVHWM, 10000 )
        self.receiver.setsockopt(zmq.SNDHWM, 10000 )

        self.sender = context.socket(zmq.PUB)
        #self.sender.connect("tcp://localhost:5556")
        self.sender.connect(url)
        self.sender.setsockopt(zmq.RCVHWM, 10000 )
        self.sender.setsockopt(zmq.SNDHWM, 10000 )

        #Lets just synchronize the two
        synched = False
        count = 0
        while True:
            if not synched:
                self.sender.send("ello")
            count += 1
            if self.receiver.poll(timeout=10) != 0:
                out = self.receiver.recv()
                synched = True
            elif synched:
                break
        print("synched", count, out)
        self.put = self.sender.send
        BroadcastBifurcation.__init__(self, url, timeout)
        self.thread = threading.Thread(target=self.service_callbacks, args=(self.receiver, self.callbacks, self.timeout, self._stop))
        self.thread.daemon = True
        self.thread.start()
        #self.debug_name = "stream %s %s"%(self.url, self.thread.name)

    def service_callbacks(self, receiver, callbacks, timeout, _stop):
        get = receiver.recv
        poll = receiver.poll
        values = callbacks.values
        is_set = _stop.is_set
        while not is_set():
            if poll(timeout)!=0:
                body_obj = AttrDict( msgpack=get() )
                for callback in values():
                    callback( body_obj )
            elif is_set():
                break

    def cleanup(self):
        self._stop.set()
        self.thread.join()
        #self.sender.disconnect("tcp://localhost:5556")
        self.sender.disconnect(self.url)
        try:
            self.receiver.unbind(self.url)
            #self.receiver.unbind("tcp://*:5556")
        except:
            pass

    def __call__(self, message):
        if len(self.callbacks) <= 0:
            return
        self.put( message.__msgpack__ )


class PostgresTableStream(SplitBifurcation):
    '''
    Base for implementing different kinds of streams against a postgres SQL db
    '''
    def __init__(self, url, timeout, table, field_mapping=None):
        '''
        Params:
        --------
        url: pg connection string following dsn convention i.e.
        conn = psycopg2.connect("dbname=test user=postgres password=secret")
        table: the table or view from which you wish to stream
        field_mapping: a way of specifying what you want the returned data to
        look like - allows to model, for e.g. the basis of an O&M schema;
        if None, replace with *; to use effectively, user would need to know
        table structure - default is to return a dict with table_name:value kvps
        '''
        SplitBifurcation.__init__(self, url, timeout)
        self.typ_reg_dict = {}
        self.conn = None
        try:
            self.conn = psycopg2.connect(self.url)
            #sets up a registry of the common types we might want to work with
            #in data on cursors
            with self.conn.cursor() as typcurs:
                typcurs.execute("SELECT oid, typname from pg_type \
                WHERE typname in \
                ('date','time','timetz','timestamp','timestampz', \
                'geometry', 'spheroid','box3d','box2d');")
                nb_types = typcurs.fetchall()
                for typ in nb_types:
                    self.typ_reg_dict[typ[0]] = typ[1]
        except psycopg2.Error as e:
            print(e.pgerror)
            return
        self.debug_name = "stream %s"%table

    def cleanup(self):
        try:
            self.conn.close()
        except psycopg2.Error as e:
            print("PostgresTableStream closed %s"%e)

    #Required to be a Stream
    #message is an AttrDict
    def __call__(self, message):
        raise NotImplementedError("Please Implement this method")


class PostgresPollingTableStream(PostgresTableStream):

    def __init__(self, url, timeout, table, poll_freq=5000, offset=-1, orderby='', field_mapping=None):
        '''
        Params:
        ----------
        url: pg connection string following dsn convention
        timeout:
        table: the table or view from which you wish to stream
        poll_freq: time in milliseconds between polls
        offset: any of {-1, 1, x} where -1 -> all records, 1 -> most recent
                record and x-> last x recent records
        orderby: allows for records to be ordered by a known column that
        provides a table-level unique identifier e.g. serial field or timestamp

        Will only stream records that have not been put on the stream before

        Streams a record one tuple at a time

        Always puts records onto stream in ascending order of entry into
        database i.e. FIFO-like unless otherwise specified by the orderby arg

        '''
        PostgresTableStream.__init__(self, url, timeout, table, field_mapping)
        if not self.conn:
            print("Unable to connect to database:%s" % url)
            return
        self.cursor = self.conn.cursor()
        self.last_record = -999999
        self.poll_freq = poll_freq
        self.offset = int(offset)
        self.table = table
        self.orderby = orderby


        # need to work out some kind of ordering default in the absence of an
        # orderby arg; this is done by querying the table by a serial column
        # or failing that, its primary key
        if self.orderby =='':
            default_ordering_sql = "SELECT a.attname,CASE WHEN a.atttypid = ANY ('{int,int8,int2}'::regtype[])\
            AND EXISTS (\
             SELECT 1 FROM pg_attrdef ad\
             WHERE  ad.adrelid = a.attrelid\
             AND    ad.adnum   = a.attnum\
             AND    ad.adsrc = 'nextval('''\
               || (pg_get_serial_sequence (a.attrelid::regclass::text, a.attname))::regclass\
               || '''::regclass)'\
             )\
             THEN CASE a.atttypid\
                WHEN 'int'::regtype  THEN 'serial'\
                WHEN 'int8'::regtype THEN 'bigserial'\
                WHEN 'int2'::regtype THEN 'smallserial'\
             END\
             ELSE format_type(a.atttypid, a.atttypmod)\
             END AS data_type,\
             a.attnum = ANY(i.indkey) as p\
             FROM   pg_index i\
             JOIN   pg_attribute a ON a.attrelid = i.indrelid\
             WHERE  i.indrelid = '%s'::regclass;" % table

            try:
                with self.conn.cursor() as curs:
                    curs.execute(default_ordering_sql)
                    candidates = curs.fetchall()
                    print("column types")
                    print(candidates)
                #first pass, check for serials, and use first discovered
                for c in candidates:
                    if c[1] == 'serial' or c[1] == 'bigserial' or c[1] == 'smallserial':
                        self.orderby = c[0]
                        break
                #second pass, grab primary key if orderby not filled
                if self.orderby == '':
                    for c in candidates:
                        if c[2]:
                            self.orderby = c[0]
                            break
                if self.orderby == '':
                    #cannot order table, makes it tricky to stream meaningfully
                    raise StreamError('Cannot create stream ordering from Postgres table')
            except psycopg2.Error as e:
                curs.close()
                self.conn.close()
                print("Problem initialising PostgresPollingTableStream database: ", e.pgerror)
                return
        #need to make sure field_mapping has a column that matches orderby
        #argument else the WHERE clause breaks - if not, force it to be so
        if field_mapping:
            '''
            {dbfield:alias,dbfield:alias,dbfield:alias,etc}
            '''
            self.field_mapping = ''
            if field_mapping.get(self.orderby) != self.orderby:
                field_mapping[self.orderby] = self.orderby
            for key, val in field_mapping.items():
                self.field_mapping = self.field_mapping + '%s as %s,' % (key, val)
            self.field_mapping = self.field_mapping[:-1]
        else:
            self.field_mapping = '*'

        #crank off processing
        self._stop = threading.Event()
        self.thread = threading.Thread(target=self.polldb)
        self.thread.daemon = True
        self.thread.start()

    def cleanup(self):
        self._stop.set()
        self.thread.join()
        try:
            self.conn.close()
        except psycopg2.Error as e:
            print("PostgresTableStream closed")


    def polldb(self):
        offset = 0
        while not self._stop.isSet():
            if self.offset == -1:
                self.stream_sql = "SELECT %s FROM %s WHERE %s > %i ORDER BY %s" % (self.field_mapping,
                                                                                  self.table,
                                                                                  self.orderby,
                                                                                  self.last_record,
                                                                                  self.orderby)
            elif self.offset == 1:
                self.stream_sql = "SELECT %s FROM %s WHERE %s > %i ORDER BY %s DESC LIMIT %i" % (self.field_mapping,
                                                                                                self.table,
                                                                                                self.orderby,
                                                                                                self.last_record,
                                                                                                self.orderby,
                                                                                                self.offset)
            else:
                '''
                # this approach can potentially return massive numbers of
                # records and nuke memory
                self.stream_sql = "SELECT * FROM (SELECT %s FROM %s ORDER BY %s ASC \
                OFFSET (SELECT reltuples from pg_class WHERE relname='%s') - %i) as foo \
                WHERE %s > %i" % (self.field_mapping, self.table,
                                   self.orderby, self.table,
                                   self.offset, self.orderby, self.last_record)
                '''
                # limit-based - reduces impact on memory
                self.stream_sql = "SELECT %s FROM %s WHERE %s > %i" \
                " ORDER BY %s ASC LIMIT %i OFFSET %i" % (
                    self.field_mapping, self.table, self.orderby, self.last_record,
                    self.orderby, self.offset, offset)
                offset += self.offset

            #print self.stream_sql
            with self.conn.cursor() as curs:
                try:
                    curs.execute(self.stream_sql)
                    items = curs.fetchall()
                    colnames = [desc[0] for desc in curs.description]
                    for item in items:
                        #print item
                        #print "is item"
                        output = {}
                        for col in range(len(item)):
                            if isinstance(item[col],datetime.datetime):
                                output[colnames[col]] = item[col].isoformat()
                            #crude check for geojson string returned from PostGIS
                            elif isinstance(item[col],str) and (
                            ('point' in item[col].lower()) or ('polygon' in item[col].lower()) or ('line' in item[col].lower())
                            ) and ('coordinates' in item[col].lower()):
                                output[colnames[col]] = ujson.loads(item[col])
                            else:
                                output[colnames[col]] = item[col]
                            ###service callbacks on stream
                        for cb_obj in list(self.callbacks.keys()):
                            #print cb_obj
                            try:
                                self.callbacks[cb_obj]( AttrDict(output) )
                            except KeyError:
                                pass
                    # set up the last record flag for the starting offset in the stream
                    self.last_record = item[colnames.index(self.orderby)]
                except psycopg2.Error as e:
                    print(e.pgerror)
            sleep(self.poll_freq/1000)

    def __call__(self, message):
        raise NotImplementedError
        #should handle DB INSERTS

        #if len(self.callbacks) < 0:
        #    return
        #self.queue.put( {'message':message} )


class PostgresTriggerTableStream(PostgresTableStream):
    '''
    A stream that is hooked into the table via PLPython machinery;
    A function is dumped into postgres, then accessed from a trigger
    set on a table;
    Data are pumped onto stream via plpython function
    '''
    pass


class ThreadBroadcastStream(BroadcastBifurcation):
    def __init__(self, url, timeout):
        self._stop = threading.Event()
        #self.queue = Queue(0)
        #self.put = self.queue.put_nowait
        self.queue = deque()
        self.put = self.queue.append
        self.__process = threading.Event()
        BroadcastBifurcation.__init__(self, url, timeout)
        self.thread = threading.Thread(target=self.service_callbacks, args=(self.queue, self.callbacks, self.timeout, self._stop))
        self.thread.daemon = True
        self.thread.start()
        #self.debug_name = "stream %s %s"%(self.url, self.thread.name)

    def service_callbacks(self, queue, callbacks, timeout, _stop):
        #get = queue.get
        get = queue.popleft
        values = callbacks.values
        is_set = _stop.is_set
        while not is_set():
            try:
                try:
                    message = get( )
                    for callback in values():
                        body_obj = message
                        callback( body_obj )
                except IndexError:
                    self.__process.clear()
                    self.__process.wait(0.1)
                    continue
            except Empty:
                if is_set():
                    break

    def cleanup(self):
        self._stop.set()
        self.thread.join()

    def __call__(self, message):
        if len(self.callbacks) <= 0:
            return
        self.put( message )
        if not self.__process.is_set():
            self.__process.set()


class ThreadSplitStream(SplitBifurcation):
    def __init__(self, url, timeout):
        #self.queue = Queue(0)
        #self.put = self.queue.put_nowait
        self.queue = deque()
        self.put = self.queue.append
        self.__process = threading.Event()
        SplitBifurcation.__init__(self, url, timeout)
        #self.debug_name = "stream %s %s"%(self.url, "SplitBifurcation")

    def service_callback(self, callback, _stop):
        #get = self.queue.get
        get = self.queue.popleft
        while not _stop.isSet():
            try:
                try:
                    #message = get( timeout=self.timeout )
                    message = get( )
                    body_obj = message
                    callback( body_obj )
                except IndexError:
                    self.__process.clear()
                    self.__process.wait(0.1)
                    continue
            except Empty:
                if _stop.isSet():
                    break

    def __attach__(self, obj, callback):
        _stop = threading.Event()
        thread = threading.Thread(target=self.service_callback, args=(callback, _stop))
        thread.daemon = True
        thread.start()
        SplitBifurcation.__attach__(self, obj, (thread, _stop))

    def __detatch__(self, obj):
        thread, _stop = self.callbacks[obj]
        _stop.set()
        thread.join()
        SplitBifurcation.__detatch__(self, obj)

    def cleanup(self):
        keys = list(self.callbacks.keys())
        for obj in keys:
            self.__detatch__(obj)

    def __call__(self, message):
        if len(self.callbacks) <= 0:
            return
        self.put( message )
        if not self.__process.is_set():
            self.__process.set()


class FileStream(SplitBifurcation):
    def __init__(self, url):
        self.queue = Queue(0)
        SplitBifurcation.__init__(self, url)
        def blitzreader():
            self.process = subprocess.Popen(['/usr/bin/tail','-f',self.url,'-n 1'], stdout=subprocess.PIPE)
            readln = self.process.stdout.readline
            while True:
                line = readln()
                print(line)
                self.__call__( line )
        self.br = threading.Thread(target=blitzreader)
        self.br.daemon = True
        self.br.start()

    def __iter__(self):
        #print "__iter__"
        return self

    def __next__(self):
        #print "next"
        return self.queue.get()

    def __call__(self, message):
        #print "__call__"
        self.queue.put( message )


class WebSocketStream(BroadcastBifurcation):
    def __init__(self, url, timeout):
        self._stop = threading.Event()
        BroadcastBifurcation.__init__(self, url, timeout)
        self.ws = websocket.create_connection(url, self.timeout)
        self.thread = threading.Thread(target=self.service_callbacks)
        self.thread.daemon = True
        self.thread.start()
        #self.debug_name = "stream %s %s"%(self.url, self.thread.name)

    def service_callbacks(self):
        while not self._stop.isSet():
            try:
                message = self.ws.recv()
                for cb_obj in list(self.callbacks.keys()):
                    try:
                        self.callbacks[cb_obj]( AttrDict(message) )
                    except KeyError:
                        pass
            except websocket.WebSocketTimeoutException:
                #print "websocket.WebSocketTimeoutException: timed out"
                pass

    def cleanup(self):
        self._stop.set()
        self.thread.join()

    def __call__(self, message):
        if len(self.callbacks) < 0:
            return
        if isinstance(message, dict):
            self.ws.send( ujson.dumps(message) )
        elif isinstance(message, str):
            self.ws.send( message )


stream_lookup_dict['ws']=WebSocketStream
stream_lookup_dict['thread']=ThreadBroadcastStream,
stream_lookup_dict['thread+split']=ThreadSplitStream
stream_lookup_dict['file']=FileStream

