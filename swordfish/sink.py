# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
from .core import StreamProcessor
import ujson as json
import websocket
import numpy as np
import smtplib
from email.mime.text import MIMEText
try:
    from pymongo import MongoClient
except:
    print("MongoClient Unavailable")
try:
    import rethinkdb as rethink
    from rethinkdb.errors import ReqlOpFailedError, RqlRuntimeError, \
        ReqlTimeoutError, ReqlDriverError
except:
    print("RethinkDB Client Unavailable")


class SinkProcessor(StreamProcessor):
    def __init__(self, *args, **kwargs):
        StreamProcessor.__init__(self, *args, **kwargs )
        self._sink = True


class MongoDB(SinkProcessor):
    def __init__(self, clienturi="mongodb://localhost:27017/", database="swordfish", collection="messages", *args, **kwargs):
        SinkProcessor.__init__(self, *args, **kwargs)
        self.clienturi = clienturi
        self.database = database
        self.collection = collection
        self.client = MongoClient(self.clienturi)
        self.db = self.client[self.database]
        self.cl = self.db[self.collection]

    def process(self, message, stream_id):
        self.cl.insert( message.__deepcopy__({}).__data__ )
        return message


class RethinkDB(SinkProcessor):
    """see: https://github.com/rethinkdb/rethinkdb-example-flask-backbone-todo\
    /blob/master/todo.py
    """
    def __init__(self, uri="localhost", port="27017", database="swordfish",
                 table="messages", *args, **kwargs):
        SinkProcessor.__init__(self, *args, **kwargs)
        self.uri = uri
        self.port = port
        self.database = database
        self.table = table
        try:
            self.connection = rethink.connect(host=self.uri, port=self.port)
        except ReqlTimeoutError:
            raise Exception('Unable to connect to %s:%s!' %
                            (self.uri, self.port))
        try:
            rethink.db_create(self.database).run(self.connection)
        except RqlRuntimeError:
            self.warning('Database "%s" already exists.', self.database)
        try:
            rethink.db(self.database).table_create(self.table).run(self.connection)
        except RqlRuntimeError:
            self.warning('Table "%s" already exists.', self.table)

    def process(self, message, stream_id):
        insert = rethink.db(self.database).table(self.table).insert(
            message.__deepcopy__({}).__data__)
        try:
            insert.run(self.connection)
        except ReqlDriverError as err:
            self.error(err)
        return message


class EchoWebSocket(SinkProcessor):
    def __init__( self, url, timeout=None, run_websocket=False, *args, **kwargs ):
        #Example: url="ws://www.example.com/socketserver"
        SinkProcessor.__init__(self, *args, **kwargs )
        self.info('EchoWebSocket connecting to:%s', url)
        self.ws = websocket.create_connection(url, timeout)

    def process(self, message, stream_id):
        """Send an AttrDict message, in JSON format, to a web socket server"""
        #print "sink.websocket.pre", type(message), message
        try:
            #self.debug('raw_msg:%s (type:%s)', message, type(message))
            msg = message.__json__
            #print "sink.websocket.pst", type(msg), msg
            #self.debug('json_msg:%s (type:%s)', msg.replace('"',"'"), type(msg))  # debug cannot handle "
            self.ws.send(msg)
        except AttributeError as err:
            self.error('%s - unable to create JSON from:%s', err, message)
        except TypeError as err:
            self.error('%s - unable to create JSON from:%s', err, message)
        return None


class Method(SinkProcessor):
    def __init__(self, method, *args, **kwargs):
        SinkProcessor.__init__(self, *args, **kwargs )
        self.method = method

    def process(self, message, stream_id):
        self.debug(message)
        self.method(message)
        return None


class Print(SinkProcessor):
    def process(self, message, stream_id):
        self.debug( message.__json__)
        print(message.__json__)
        return message


class Drain(SinkProcessor):
    def process(self, message, stream_id):
        self.debug(message.__json__)
        return message


class SMTP(SinkProcessor):
    def __init__( self, from_address, to_addresses, subject, smtp_server='smtp.csir.co.za',  *args, **kwargs ):
        SinkProcessor.__init__(self, *args, **kwargs )
        assert isinstance(to_addresses,  list)
        self.smtp_server = smtp_server
        self.from_address = from_address
        self.to_addresses = to_addresses
        self.subject = subject

    def process(self, message, stream_id):
        COMMASPACE = ', '
        msg = MIMEText(str(message))
        msg['Subject'] = self.subject
        msg['From'] = self.from_address
        msg['To'] = COMMASPACE.join( self.to_addresses )
        s = smtplib.SMTP(self.smtp_server)
        s.ehlo()
        s.sendmail(self.from_address, self.to_addresses, msg.as_string())
        s.quit()


class Filer(SinkProcessor):
    def __init__( self, filename, reset=False, **kwargs ):
        SinkProcessor.__init__(self, **kwargs )
        self.output_file = filename
        self.reset = reset or False

    def process(self, message, stream_id):
        #print message, type(message)
        if self.output_file:
            mode = 'w' if self.reset else 'a'
            try:
                with open(self.output_file, mode) as outfile:
                    json.dump(message, outfile)
                    outfile.write('\n')
            except:
                self.error('Unable to save the data to: %s', self.output_file)
        return message


#append false unable to handle nested return values at this point, must remedy
class Plotter(SinkProcessor):
    def __init__( self, attrs, plt, animation, ticks_count=10, xticks=None, num=None, **kwargs ):
        #TODO - refactor init params into: *args, **kwargs
        SinkProcessor.__init__(self, **kwargs )
        fig, ax = plt.subplots(num=num)
        self.plt = plt
        self.fig = fig
        self.ax = ax
        self.attrs = attrs
        self.xticks = xticks
        self.x_values = list(range(ticks_count))
        self.values = []
        self.lines = []
        for attr in self.attrs:
            self.values.append( [0]*ticks_count )
            self.lines.append( ax.plot(np.zeros(ticks_count), label=attr)[0] )
        self.plt.legend()
        self.ani = animation.FuncAnimation(self.fig, self.update, interval=100)
        self.plt.show()

    def update(self, i):
        for j, attr in enumerate(self.attrs):
            self.lines[j].set_ydata(np.array(self.values[j]))
        if self.xticks:
            self.ax.set_xticklabels(self.x_values)

    def process(self, message, stream_id):
        for i, attr in enumerate(self.attrs):
            val = eval( 'message.%s'%attr )
            self.values[i].append( val )
            self.values[i].pop(0)
        if self.xticks:
            self.x_values.append(eval( 'message.%s'%self.xticks ))
            self.x_values.pop(0)
        return message
