# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Use an HTTP endpoint as a stream source

Created: Wed May 17 2017
@author: gmcferren@csir.co.za

Updates:
    2018-03-09  dhohls@csir.co.za  Added SimpleHTTPPutSource
    2018-03-14  dhohls@csir.co.za  Added logging functionality; header for Put
"""
# lib
import logging
import requests
import time
# swordfish
from swordfish.source import SourceStreamProcessor
from swordfish.core import AttrDict

module_logger = logging.getLogger('swordfish.http_source')


class SimpleHTTPGetSource(SourceStreamProcessor):
    '''
    Attach to an HTTP endpoint as a stream source, accessed via simple get
    requests. Polling approach.

    Use case:
    ---------
    Access stream-like data available from an HTTP endpoint, via HTTP GET.
    Meant to be a pretty generic getter, so full url needs to be passed in,
    with optional session info, specifically authentication credentials.

    No logic applied to payload, besides basic error handling; would need to be
    processed by implementing an application specific process() method in a
    subclass or pass result through an application specific StreamProcessor.

    Return val could be sizeable and is simply a representation of the resource
    The value can be appropriately processed (reformatted) by overriding the
    format_message() method in a subclass.

    Implemented with Python requests module

    '''
    def __init__(self, url, request_freq=1000, backoff_point=15,
                 *args, **kwargs):
        '''
        Params:
        -------
        url:
            the full url to the resource, including any KVPs
        request_freq:
            how often the url should be polled for data,
            in milliseconds, defaulting to 1000
        backoff_point:

        **kwargs

        If authentication needs to be established, provide the keys:
          `username`, with the requisite username
          `password`, with the requisite password

        If a waiting period beween message transmission is desired (e.g.
        to prevent all messages arriving in a single time interval), then set
        the `delay` parameter to a suitable value in millseconds.

        '''
        SourceStreamProcessor.__init__(self, autostart=False, *args, **kwargs)
        self.url = url
        self.request_frequency = request_freq
        self.backoff_point = backoff_point
        self.username = kwargs.get('username', None)
        self.password = kwargs.get('password', None)
        self.delay = kwargs.get('delay', 0)
        assert int(self.delay) >= 0

    def _sourceprocessor(self):
        rsession = requests.session()
        # little bit of throttling, in case the url is not responding with an
        # ok(200) result. Set to 15 seconds
        backoff_counter = 0
        if self.username and self.password:
            rsession.auth = (self.username, self.password)
        while True:
            try:
                resp = rsession.get(self.url)
                if resp.ok:
                    self.put(self.format_message(resp.text))
                    time.sleep(self.delay / 1000)
                else:
                    backoff_counter += 1
                    if backoff_counter == self.backoff_point:
                        time.sleep(15)
                        backoff_counter = 0
                time.sleep(self.request_frequency / 1000)
            except Exception as err:
                module_logger.exception("GET exception:%s", err)
        rsession.close()

    def process(self, message, stream_id):
        return message

    def format_message(self, text):
        '''Create formatted message; overide in inheriting class as needed'''
        return AttrDict({'value':text})


class SimpleHTTPPutSource(SourceStreamProcessor):
    '''
    Attach to an HTTP endpoint as a stream source, accessed via simple PUT
    requests. Polling approach.

    Use case:
    ---------
    Access stream-like data available from an HTTP endpoint, via HTTP POST.
    Meant to be very generic, so full url needs to be passed in,
    with optional session info, specifically authentication credentials (i.e.
    username and password), as well as headers (e.g. for JSON requests).

    No logic applied to payload; this can be changed or extended via the
    create_payload() method.

    Return val could be sizeable and is simply a representation of the resource
    The value can be appropriately processed (reformatted) by overriding the
    format_message() method in a subclass.

    The deliver_message() method is responsible for putting the message on
    the internal queue (for further/downstream use); again, this can be
    exended or overided as needed in inheriting classes e.g. to break up a set
    of messages in single message before sending to the queue.

    Implemented with Python requests module

    '''
    def __init__(self, url, payload, request_freq=1000, backoff_point=15,
                 *args, **kwargs):
        '''
        Params:
        -------
        url:
            the full url to the resource
        payload:
            the payload e.g. a JSON string; the payload can be changed or
            extended via the create_payload() method if needed
        request_freq:
            how often the url should be polled for data,
            in milliseconds, defaulting to 1000
        backoff_point:

        If authentication needs to be established, provide the keys:
          `username`, with the requisite username
          `password`, with the requisite password

        If a waiting period beween message transmission is desired (e.g.
        to prevent all messages arriving in a single time interval), then set
        the `delay` parameter to a suitable value in millseconds.

        If data is required/available in a specific format, then pass in
        `headers` string e.g. for JSON it could be :
            {'Content-Type': 'application/json'}
        '''
        SourceStreamProcessor.__init__(self, autostart=False, *args, **kwargs)
        self.url = url
        self.payload = payload
        self.request_frequency = request_freq
        self.backoff_point = backoff_point
        self.username = kwargs.get('username', None)
        self.password = kwargs.get('password', None)
        self.delay = int(kwargs.get('delay', 0))
        self.headers = kwargs.get('headers', '')

    def _sourceprocessor(self):
        rsession = requests.session()
        # little bit of throttling, in case the url is not responding with an
        # ok(200) result. Set to 15 seconds
        backoff_counter = 0
        if self.username and self.password:
            rsession.auth = (self.username, self.password)
        while True:
            data = self.create_payload(self.payload)
            module_logger.debug("url:%s data:%s", self.url, data)
            try:
                resp = rsession.post(self.url, data, headers=self.headers)
                if resp.ok:
                    self.deliver_message(resp)
                else:
                    warning = "POST RESPONSE:{0} ({1})".format(
                        resp.status_code, resp.content)
                    module_logger.warning(warning)
                    backoff_counter += 1
                    if backoff_counter == self.backoff_point:
                        time.sleep(15)
                        backoff_counter = 0
            except Exception as err:
                module_logger.exception("POST exception:%s", err)
            time.sleep(self.request_frequency / 1000)
        rsession.close()

    def process(self, message, stream_id):
        return message

    def create_payload(self, payload):
        '''Create payload data; overide in inheriting class as needed.'''
        return payload

    def format_message(self, text):
        '''Create formatted message; overide in inheriting class as needed.'''
        return AttrDict({'value':text})

    def deliver_message(self, resp):
        '''Put message(s) on the internal thread; wait as needed.'''
        self.put(self.format_message(resp.text))
        time.sleep(self.delay / 1000)


class ResourceCyclingHTTPGetSource(SourceStreamProcessor):
    '''
    Attach to an HTTP endpoint as a stream source, accessed via simple get
    requests. Polling approach.

    Use case:
    ---------
    Access stream-like data available from an HTTP endpoint, via HTTP GET.
    Meant to be a pretty generic getter, so url needs to be passed in,
    with optional session info, specifically authentication credentials.

    Aim of this class id to retrieve values from a parameterised ReSTful API,
    particularly if there are a number of resources that need to be queried
    in a cycle

    No logic applied to payload, besides basic error handling; would need to be
    processed by implementing an application specific process() method in a
    subclass or pass result through an application specific StreamProcessor.
    Return val could be sizeable and is simply a representation of the resource

    Implemented with Python requests module

    '''
    def __init__(self, url, url_placeholder="*", url_substitutes=None,
                 request_freq=1000, backoff_point=15, *args, **kwargs):
        '''
        Params:
        -------
        url: the full url to the resource, including any KVPs
        url_placeholder: the part of the url that needs to be substituted
        url_substitutes: List of subsitutes used to create the set of urls that
          need to be cycled through
        request_freq: how often the url should be polled for data,
          in milliseconds, defaulting to 1000
        backoff_point:
        **kwargs: if authentication needs to be established, provide keys
          username, with the requisite username
          password, with the requisite password
        '''
        SourceStreamProcessor.__init__(self, autostart=False, *args, **kwargs)
        self.url = url
        self.url_placeholder = url_placeholder
        self.url_substitutes = url_substitutes or []
        self.request_frequency = request_freq
        self.backoff_point = backoff_point
        if kwargs.get('username'):
            self.username = kwargs.get('username')
        if kwargs.get('password'):
            self.password = kwargs.get('password')
        self.urls = self._make_urls()

    def _make_urls(self):
        ''''Create full URLS.'''
        urls = []
        for sub in self.url_substitutes:
            _url = self.url.replace(self.url_placeholder, sub)
            urls.append(_url)
        return urls

    def _sourceprocessor(self):
        rsession = requests.session()
        # little bit of throttling, in case the url is not responding with an
        # ok(200) result. Set to 15 seconds
        backoff_counters = {us:0 for us in self.url_substitutes}

        if self.username and self.password:
            rsession.auth = (self.username, self.password)
        while True:
            for url in self.urls:
                current_sub = ""
                for u_s in self.url_substitutes:
                    if u_s in url:
                        current_sub = u_s
                        break
                resp = rsession.get(url)
                if resp.ok:
                    self.put(self.format_message(text=resp.content, url=url))
                    backoff_counters[current_sub] = 0
                else:
                    backoff_counters[current_sub] += 1
                    if backoff_counters[current_sub] == self.backoff_point:
                        time.sleep(15)
                        backoff_counters[current_sub] = 0
            time.sleep(self.request_frequency / 1000)
        rsession.close()

    def process(self, message, stream_id):
        return message

    def format_message(self, text, url):
        '''Create formatted message; overide in inheriting class as needed'''
        return AttrDict({'value': {'resp': text, 'url': url}})
