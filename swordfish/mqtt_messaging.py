# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Thurs July 16 2015 at 10:50

@author: gmcferren

The purpose of this module is to allow Swordfish Streams to exist over an MQTT
messaging fabric. MQTT is an important pub/sub protocol increasingly used in
the IoT domain

"""
from .core import Stream, AttrDict
import paho.mqtt.client as mqtt

from ujson import loads, dumps
#from msgpack import loads, dumps
from hashlib import sha256
from ast import literal_eval as LE
from weakref import WeakKeyDictionary
from threading import Event
from multiprocessing import Pipe, Process

class MQTTStream(Stream):
    '''
    Needs a broker (host, port) to form a connection,
    a topic to bind to to receive messages,
    and an optional topic to use in publishing, a QOS flag (0,1,2)

    Each component that attaches to an MQTT stream will be considered a new
    MQTT Client, with its own connection, MQTT callbacks and topics

    Messages received on the callback get pushed onto the internal stream queue
    for use by the iterator

    To complete the stream, a Producer needs to be put down before the exchange
    to allow automated stream deployment by StreamProcessors

    So an AMQP Stream looks like P->E->Q->callback->callback->iterator

    url scheme: mqtt://user:pass@host:1883/
    '''
    def __init__(self, url, publish_topic, timeout=1.0, name=None,
                 clean_session=True, immortal=False):
        '''necessary to have a publish topic to be an MQTTstream'''
        url, self.mqttport, self.mqttauths = self._unpack_url(url)
        Stream.__init__(self, url, timeout)
        self.clean_session = clean_session
        self.immortal = immortal
        self.callbacks = {}
        self.debug_name = "MQTTStream_%s"%(self.url)
        self.client_id = name or ''
        self.publish_topic = publish_topic or self.client_id
        self._connect_mqtt_stream()
        self.subscription = None
        self.i = 0
        self.end_event = Event()

    def _unpack_url(self, url_in):
        '''
        takes the url and makes it mqtt ready, since mqtt seems not to
        have a url based connect method
        '''
        url_list = url_in.rstrip('/').strip('mqtt://').split('@')
        auth = None
        port = 1883
        if len(url_list) == 2:

            auth_list = url_list[0].split(':')

            if len(auth_list) == 2:
                auth = {"username":auth_list[0], "password":auth_list[1]}
            else:
                auth = {"username":auth_list[0], "password":None}

            url_port = url_list[1].split(':')
            url = url_port[0]
            if len(url_port) == 2:
                port = url_port[1]

        else:
            url_port = url_list[0].split(':')
            if len(url_port) == 2:
                port = url_port[1]
            url = url_port[0]
        return (url, port, auth)

    def _connect_mqtt_stream(self):
        self.mqtt_producer = mqtt.Client(client_id=self.client_id,
                                         clean_session=self.clean_session)
        if self.mqttauths:
            self.mqtt_producer.username_pw_set(self.mqttauths.get('username'),
                                               self.mqttauths.get('password'))
        #connection related concerns
        self.mqtt_producer.on_connect = self._on_mqtt_connect
        self.mqtt_producer.connect(host=self.url, port=self.mqttport)
        self.mqtt_producer.on_publish = self._on_mqtt_publish
        for obj, callback in list(self.callbacks.items()):
            self.__detatch__(obj)
            self.__attach__(obj, callback)

    def _on_mqtt_connect(self, client, userdata, flags, rc):
        self.info("Connected with result code " + str(rc) +
                  " for client " + client._client_id)
    def _on_mqtt_subscribe(self, client, userdata, mid, granted_qos):
        self.info("Subscribed with QOS " + str(granted_qos) +
                  " for client " + client._client_id)
    def _on_mqtt_disconnect(self, client, userdata, flags, rc):
        self.error("Disconnected with result code " + str(rc) +
                  " for client " + client._client_id)
    def _on_mqtt_unsubscribe(self, client, userdata, mid):
        self.info("Unsubscribed for client %s", client._client_id)

    def _on_mqtt_publish(self, client, userdata, mid):
        self.info("Published message %s for client %s", mid, client._client_id)

    #Required to be a Stream
    def cleanup(self):
        for key in list(self.callbacks.keys()):
            self.__detatch__(key)
        self.mqtt_producer.loop_stop()
        self.mqtt_producer.disconnect()

    def __del__(self):
        '''make sure amqp bindings are cleaned up...'''
        self.cleanup()
    #between the next two functions is the meat of subscribing
    #likely have to use message_callback_add() functionality
    #or may have to have a client per attach, i think
    def _get_consumer(self):
        if self.subscription is not None:
            return
        def process_mqtt_message(client, userdata, message):  # change params as appr.
            try:
                body_obj = AttrDict(json=message.payload)
            except Exception as e:
                body_obj = AttrDict({"non-compliant":True,
                                     "topic":message.topic,
                                     "value":message.payload})
            for callback in list(self.callbacks.values()):
                callback(body_obj)

        mqttclient = mqtt.Client(client_id=self.client_id,
                                 clean_session=self.clean_session)
        if self.mqttauths:
            mqttclient.username_pw_set(self.mqttauths.get('username'),
                                       self.mqttauths.get('password'))
        #connection related concerns
        mqttclient.on_connect = self._on_mqtt_connect
        mqttclient.connect(host=self.url, port=self.mqttport)
        #subscription related concerns
        #mqttclient.on_subscribe = self._on_mqtt_subscribe
        mqttclient.on_message = process_mqtt_message

        mqttclient.subscribe(self.publish_topic)
        mqttclient.loop_start()
        self.subscription = mqttclient

    def __attach__(self, obj, callback):
        self.callbacks[obj] = callback
        self._get_consumer()

    #Required to be a Stream
    def __detatch__(self, obj):
        self.debug("__detatch__")
        del self.callbacks[obj]
        if len(self.callbacks)==0:
            self.subscription.unsubscribe(self.publish_topic)
            self.subscription.loop_stop()
            self.subscription.disconnect()

    #Required to be a Stream
    #message is an AttrDict
    def __call__(self, message):
        '''
        so map to the MQTT publisher client on the Stream Instance

        topic is usually going to be the publish_topic arg passed in via stream
        constructor method - cannot for now see how it is going to be
        populated here...
        '''
        try:
            payload = message.__json__
        except ValueError as ve:
            self.error("Publish of mqtt message failed: %s" % str(ve))
        else:
            self.mqtt_producer.publish(
                topic=self.publish_topic, payload=payload, qos=0, retain=False)
