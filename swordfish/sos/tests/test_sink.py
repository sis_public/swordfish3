# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Test endpoint for saving Swordfish CIM data structures into a SOS. """
# lib
import logging
import requests
import sys
import traceback
# local
from swordfish.sos.create_layout import LayoutGenerator


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class InsertObservation(object):
    """Test endpoint for Swordfish CIM data structures into a SOS."""

    def __init__(self, url, template=None, timeout=None, **kwargs):
        self.xml = []
        self.data = None
        self.outcome = None
        self.url = url
        self.template = template
        self.timeout = timeout  # not used ???
        self.headers = {'Content-Type': 'application/xml'}
        logger.debug('INIT: %s %s %s', url, template, timeout)

    def process(self, message, stream_id=None):
        """Message should be dict or AttrDict in SwordFish SCIM format.

        Example:
        {
            "type": "OM_Measurement",
            "phenomenonTime": "2015-08-17T09:06:52+02:00",
            "result": {
                "units": "liters",
                "value": 2000
            },
            "featureOfInterest": {
                "id": "CSIR_PTA_R2",
                "geometry": {
                    "type": "Point",
                    "coordinates": [-25.753, 28.282]
                },
                "type": "Feature",
                "properties": {
                    "name": None,
                }
            },
            "observedProperty": {
                "type": "water_quantity"
            },
            "parameter": {
                "id": "4ca9d9f4-44bf-11e5-a189-6913d2f2437f"
            },
            "procedure": {
                "type": "sensor",
                "id": "45030171",
                "description": "Sensus HRI-Mei linked to meter 14787486"
            }
        }

        Example of correct results:

        <?xml version="1.0" encoding="UTF-8"?>
        <sos:InsertObservationResponse xmlns:sos="http://www.opengis.net/sos/1.0">
          <sos:AssignedObservationId>o_50</sos:AssignedObservationId>
        </sos:InsertObservationResponse>

        Example of incorrect results:

        <?xml version="1.0" encoding="UTF-8"?>
        <ows:ExceptionReport xmlns:ows="http://www.opengis.net/ows/1.1"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            version="1.0.0"
            xsi:schemaLocation="http://www.opengis.net/ows/1.1
                http://schemas.opengis.net/ows/1.1.0/owsAll.xsd">
          <ows:Exception exceptionCode="InvalidParameterValue"
                         locator="AssignedSensorId">
            <ows:ExceptionText>
                Sensor must be registered at SOS at first before inserting
                observations into SOS!
            </ows:ExceptionText>
          </ows:Exception>
        </ows:ExceptionReport>

        """
        logger.debug("SOS JSON MESSAGE: %s: %s" % (type(message), message))
        logger.debug("SOS TEMPLATE: %s" % self.template)

        xml_template = LayoutGenerator(template=self.template)
        logger.debug("SOS XML: %s / %s " % (xml_template, type(xml_template)))
        dataset = xml_template.create_observation(data=message, swop_coords=True)
        if not isinstance(dataset, (list, tuple)):
            self.error("Error creating data for SOS: %s" % dataset)
            return

        for data in dataset:
            self.outcome = None
            self.data = data.replace("\u200b", '')  # zero width space...why?
            logger.debug("XML READY TO POST... %s", str(self.data))
            try:
                self.outcome = requests.post(
                    url=self.url, data=self.data, headers=self.headers)
            except Exception:
                e_type, e_value, trace = sys.exc_info()
                reason = "\n".join(traceback.format_exception(
                    e_type, e_value, trace, 2))
                logger.error("%s" % str(reason))
            if self.outcome:
                logger.debug("outcome codes: %s %s %s", type(self.outcome.status_code), self.outcome.status_code, str(self.outcome.status_code))
                logger.info("SOS RESPONSE Code:%s Text:%s",
                    str(self.outcome.status_code),
                    str(self.outcome.text))