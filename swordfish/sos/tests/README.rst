===========
SOS TESTING
===========

Setting up a SOS Test Environment
=================================

This setup assumes that you have an operational version of Docker installed on
your host machine; if not, please consult https://docs.docker.com/install/

SOS Install
-----------

The approach for a Docker-based install of a 52N SOS is here::

     https://hub.docker.com/r/52north/sos/

Follow the install guide; it is suggested that you use the compose file 
approach::

    nano docker-compose.yml
    docker-compose up -d

(It should be noted that the compose file uses the 4.3.8 release; but as of 
March 2018,the lastest release in the 4.3.x series is 4.3.16).

Once the compose and build process is complete, you should be able to chck
the containers via a `docker-compose ps` command::

             Name                        Command              State  
    ---------------------------------------------------------------
    sos_postgres-db-empty_1   docker-entrypoint.sh postgres   Up   
    sos_sos-service_1         catalina.sh run                 Up   

To access the SOS, open a browser to::

    http://localhost:8080/52n-sos-webapp/
    
The landing page will tell you to execute the installation procedure. In 
particular, set the following properties:

* as the `Datasource` select **PostgreSQL/PostGIS**
* in the `Database Configuration` section, set the `Host` to **postgres**
* set the username/password: **admin/admin**

Then set the allowed operations::

    http://localhost:8080/52n-sos-webapp/admin/operations

NOTE: Enable **all** operations  (i.e. all buttons green).

Disable transactional security::

    http://localhost:8080/52n-sos-webapp/admin/settings

Uncheck the `Transactional security active` checkbox and click on `Save`.

Enable the test dataset::

    http://localhost:8080/52n-sos-webapp/client?load=exampleData

(Press `Send` below the box of text that is filled in.)

Optionally, you can then also use the `insert_observation_extended.json` file 
to POST additional observations, for a longer time series, to the SOS.


SOS Basic Tests
---------------

Access the web client::

    http://localhost:8080/52n-sos-webapp/client
    
In the Request section, choose POST ~ application/json ~ application/json from
the drop-down lists.

In the Request text box (below the lists), fill in the following JSON::

    {
      "request": "GetObservation",
      "service": "SOS",
      "version": "2.0.0",
      "observedProperty": "http://www.52north.org/test/observableProperty/6",
      "featureOfInterest": "http://www.52north.org/test/featureOfInterest/6"
    }
    
Then press `Send`.

You can also run a check for history summary::

    {
      "request": "GetDataAvailability",
      "service": "SOS",
      "version": "2.0.0",
      "observedProperty": "http://www.52north.org/test/observableProperty/6",
      "featureOfInterest": "http://www.52north.org/test/featureOfInterest/"
    }

And the result should look like this (assuming you have not loaded the
extended dataset)::

    {
      "request" : "GetDataAvailability",
      "version" : "2.0.0",
      "service" : "SOS",
      "dataAvailability" : [
        {
          "featureOfInterest" : "http://www.52north.org/test/featureOfInterest/1",
          "procedure" : "http://www.52north.org/test/procedure/1",
          "observedProperty" : "http://www.52north.org/test/observableProperty/1",
          "phenomenonTime" : [
            "2012-11-19T13:00:00.000Z",
            "2012-11-19T13:09:00.000Z"
          ]
        }
      ]
    }


