# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Purpose: Test endpoint for streaming Swordfish CIM messages from a SOS.

Created: 2018-03-12
Author: dhohls@csir.co.za

Updates:

    2018-03-12  dhohls@csir.co.za  First version

Note:

 * This test assumes that the standard Test Data is installed into version
   4.3.8 of a 52N SOS instance, running on a localhost under Tomcat,
   with all of the default settings.

Example Usage:

Runs sample data at 10x speed (6 seconds between sample timestamps)::

    python ./swordfish/sos/tests/test_stream.py \
    -s http://localhost:8080/52n-sos-webapp/service/  \
    -f http://www.52north.org/test/featureOfInterest/6 \
    -n http://www.52north.org/test/observableProperty/ \
    -o 6 \
    -r 10.0 \
    -d 3000 \
    -c 180
"""
# lib
import argparse
import logging
# local
from swordfish import keep_loop
from swordfish.sink import Print  #, EchoWebSocket
from swordfish.sos.stream import SOSStream
from swordfish.sos.utils import iso_date_time


logging.basicConfig(
    level=logging.INFO,  # change to DEBUG to see all steps in data chain !
    format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)03d]'
           ' %(message)s',
    datefmt='%Y-%m-%d:%H:%M',)
logger = logging.getLogger(__name__)


class ValueNotSetException(Exception):
    """Handle incorrect value exception."""
    pass


def main(args):
    """Process args and setup streams
    """
    logger.debug("Args: %s", args)
    do_print = args.printing  # if True, send to Print stream
    sos = args.sos or None  # SOS
    sos_procedure = args.procedure or None
    sos_property = args.property or None
    sos_feature = args.feature or None
    sos_namespace = args.namespace or None
    message_delay = args.delay or 3000  # 3,000 milliseconds (3 secs)
    realtime = args.realtime or 1.0
    chunk = args.chunk or 180  # 3 minutes
    # optional
    sos_start = iso_date_time(args.start)
    sos_end = iso_date_time(args.end)

    with SOSStream(url=sos, payload="",
                   procedure=sos_procedure, property=sos_property,
                   feature=sos_feature, namespace=sos_namespace,
                   start_time=sos_start, end_time=sos_end, chunk=chunk,
                   delay=message_delay, realtime=realtime) as soss, \
            Print() as prn:
        if do_print:
            soss>>prn
        soss.start()

        while keep_loop():
            pass


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser()
    # SOS
    PARSER.add_argument(
        '-s', '--sos',
        help="Server name, with port, for the 52N SOS Service")
    PARSER.add_argument(
        '-o', '--property',
        help="Name of SOS property")
    PARSER.add_argument(
        '-f', '--feature',
        help="Name of SOS feature")
    PARSER.add_argument(
        '-n', '--namespace',
        help="Name of SOS namespace")
    # SOS - optional
    PARSER.add_argument(
        '-p', '--procedure',
        help="Name of SOS procedure")
    PARSER.add_argument(
        '-c', '--chunk',
        help="Time range for chunk of messages requested on each call")
    PARSER.add_argument(
        '-d', '--delay',
        help="Delay (in millisecs) between sending each message")
    PARSER.add_argument(
        '-r', '--realtime',
        help="Realtime speedup/slowdown for delays in sending each message")
    PARSER.add_argument(
        '-ts', '--start',
        help="Start time (UTC) for streaming period"
             " (default: first actual entry)")
    PARSER.add_argument(
        '-te', '--end',
        help="End time (UTC) for streaming period"
             " (default: last actual entry)")
    # other options
    PARSER.add_argument(
        '-i', '--printing', default=True, action='store_true',
        help="If set to true, send output to Print stream")
    ARGS = PARSER.parse_args()
    main(ARGS)
