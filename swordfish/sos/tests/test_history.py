# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Purpose: Test history for a SOS.

Created: 2018-03-12
Author: dhohls@csir.co.za

NOTE:
 * This test assumes that the standard Test Data is installed into version
   4.3.8 of a 52N SOS instance, running on a localhost under Tomcat,
   with all of the default settings.

Usage:
    python ./swordfish/sos/tests/test_history.py
"""

from swordfish.sos.utils import get_sos_history

SOURCE = {"url": "http://localhost:8080/52n-sos-webapp/service/"}
PAYLOAD = {
    "property": "6",
    "featureOfInterest": "http://www.52north.org/test/featureOfInterest/6",
    "namespace": "http://www.52north.org/test/observableProperty/",
    "start": "2012-11-19T13:00:00.000Z",
    "end":"2012-11-19T13:10:00.000Z"
}
result = get_sos_history(SOURCE, PAYLOAD)
# assertion assumes only default 52N Test Data loaded into SOS
try:
    assert result == {
        'http://www.52north.org/test/featureOfInterest/6':
            {'http://www.52north.org/test/observableProperty/6':
                {'start': '2012-11-19T13:00:00.000Z',
                 'end': '2012-11-19T13:09:00.000Z'
                }
            }
        }
except AssertionError as err:
    print(err)
print(result)
