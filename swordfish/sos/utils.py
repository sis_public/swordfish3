# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Utility functions for SOS.

Created: 2017-08-18
Author: dhohls@csir.co.za

Updates:

    2017-11-13 dhohls@csir.co.za    Patched date_handler
    2018-03-09 dhohls@csir.co.za    Moved support rountines from websocket
    2018-03-14 dhohls@csir.co.za    Default data format is now UTC
"""
# lib
import argparse
import datetime
import http.client
import json
import logging
import socket
from urllib.parse import urlparse
# third-party
import dateutil.parser
import pytz
import requests

HEADERS = {'Content-Type': 'application/json'}
TIMEZONE = 'UTC'  # internally, SOS stores data as UTC
NAMESPACE = 'http://meraka.csir.co.za/ogc/observableProperty/'  # ! end in /
DEFAULT_PROPERTY = None

logger = logging.getLogger(__name__)


class SOSRequestException(Exception):
    """Raise for an exception when attempting a SOS Request."""

    def __init__(self, message, *args, **kwargs):
        super(SOSRequestException, self).__init__(message)
        self.text = message


def iso_date_time(datetime_string, timezone=None,
                  datetime_format='%Y-%m-%d %H:%M:%S'):
    """Return a Python datetime object from an ISO-formatted string

    Args:
        datetime_string: string in the format of `datetime_format`
    """
    if datetime_string is None:
        return None
    _timezone = timezone or TIMEZONE or 'UTC'
    local_tz = pytz.timezone(_timezone)
    try:
        _datetime_string = datetime_string.replace('T', ' ')
        utc = datetime.datetime.strptime(_datetime_string, datetime_format)
        logger.debug('raw utc:%s', utc)
        # Tell the datetime object that it's in UTC time zone since
        # datetime objects are 'naive' by default
        utc = utc.replace(tzinfo=local_tz)
        return utc
    except (AttributeError, ValueError, TypeError) as err:
        logger.error('Unable to convert "%s" to a Python date object (%s).',
                     datetime_string, err)
    return None


def iso_date_string(adate, microseconds=False, timezone=None):
    """Return an ISO-formatted string from a Python datetime object.

    Args:
        adate: Python date object
        microseconds: if True, allow for microseconds in the date
        timezone: defaults to UTC if not supplied
    """
    _timezone = timezone or TIMEZONE or 'UTC'
    local_tz = pytz.timezone(_timezone)  # equivalent to pytz.utc
    try:
        if microseconds:
            if adate.tzinfo is not None:
                return adate.isoformat().replace('+00:00', 'Z')
            else:
                return local_tz.localize(adate).isoformat().replace('+00:00', 'Z')
        else:
            if adate.tzinfo is not None:
                return adate.replace(
                    microsecond=0).isoformat().replace('+00:00', 'Z')
            else:
                return local_tz.localize(adate).replace(
                    microsecond=0).isoformat().replace('+00:00', 'Z')
    except ValueError as err:  # already localised?
        return adate.isoformat().replace('+00:00', 'Z')
    except AttributeError as err:
        logger.exception(err, exc_info=True)
    except Exception as err:
        logger.exception(err, exc_info=True)
    return None


def allowed_port(value, port_min=1000, port_max=32000):
    """Validate port number argument."""
    ivalue = int(value)
    if port_min <= ivalue <= port_max:
        return ivalue
    else:
        raise argparse.ArgumentTypeError(
            "%s is not a valid port value" % value)


def check_url(url, timeout=10):
    """Validate that a URL returns an HTTP status of less than 400.

    Args:
        url: address string for validation
        timeout: seconds
    """
    try:
        pth = urlparse(url)
        conn = http.client.HTTPConnection(pth.netloc, timeout=timeout)
        conn.request('HEAD', pth.path)
        resp = conn.getresponse()
        logger.debug({'msg': 'response',
                      'status': resp.status, 'reason':  resp.reason})
        return resp.status < 400
    except Exception as err:
        logger.error({'error': err}, exc_info=True)
    except socket.error as err:
        logger.error({'error': 'socket.error for: %s' % url})
    return False


def date_handler(date_string=None, now=False, timezone=None):
    """Return a (Python date; ISO string) tuple from date-as-string.

    `date-as-string` could be:
        2017-11-10T14:55:00+02:00
        2017-11-10T14:55:00Z
        2017-11-10 14:55:00
        2017-11-10
    """
    _timezone = timezone or TIMEZONE or 'UTC'
    if not date_string:
        if not now:
            return None, None
        else:
            _datetime = datetime.datetime.now()
    else:
        _datetime = dateutil.parser.parse(date_string)
    iso = iso_date_string(_datetime, timezone=_timezone)
    if not iso:  # tzinfo already set
        iso = date_string
    return _datetime, iso


def data_availability_to_json(results_dict):
    """Reformat a GetDataAvailability result from a 52N SOS Service."""
    outcome = {}
    available = results_dict.get("dataAvailability", [])
    for item in available:
        foi = item.get("featureOfInterest", None)
        prop = item.get("observedProperty", None)
        if foi and prop:
            try:
                outcome[foi]
            except KeyError:
                outcome[foi] = {}
            _time = item.get("phenomenonTime", [])
            if _time and len(_time) > 0:
                outcome[foi][prop] = {"start": _time[0], "end": _time[1]}
    return outcome


def create_json_request(payload, request_type="GetObservation"):
    """Format payload into JSON string usable by a 52N SOS Service.

    For payload format, see `post_sos_request()`
    """
    logger.debug({"payload": payload, "request_type": request_type})
    try:
        assert request_type in ["GetObservation", "GetDataAvailability"]
    except AssertionError:
        raise NotImplementedError("Unable to handle request:%s", request_type)
    req = None
    _dict = payload  # json.loads(payload)
    procedure = _dict.get('procedure', "")
    offering = _dict.get('offering', "")
    foi = _dict.get('featureOfInterest', "")
    namespace = _dict.get('namespace', NAMESPACE)
    obs_property = _dict.get('property', DEFAULT_PROPERTY)
    logger.debug(
        {"raw": 'SOS params', "procedure": procedure,
         "offering": offering, "foi": foi, "offering": offering,
         "namespace": namespace, "obs_property": obs_property})
    try:
        delta = int(_dict.get('delta', 3600))  # over the last hour
    except Exception as err:
        logger.warn({'msg': 'Unable to convert delta', 'error': err})
        delta = 3600
    # single date OR date range
    equals = _dict.get('equals', None)
    start, start_date = None, None
    end, end_date = None, None
    logger.debug(
        {"raw": 'SOS Params', "equals": equals, "delta": delta,
         "obs_property": obs_property})
    if not request_type == "GetDataAvailability":
        start, start_date = date_handler(_dict.get('start', ''), now=False,
                                         timezone=TIMEZONE)
        end, end_date = date_handler(_dict.get('end', ''), now=True,
                                     timezone=TIMEZONE)
        logger.debug(
            {"msg": 'Dates', 'start': start, 'end': end, 'delta': delta})
        if not start:
            start = end - datetime.timedelta(seconds=delta)
            start_date = iso_date_string(start, timezone=TIMEZONE)
            logger.debug({'msg': 'calculated dates',
                          'start': start, 'start_date': start_date})
    logger.debug(
        {"msg": 'SOS params', "procedure": procedure,
         "offering": offering, "foi": foi, "delta": delta,
         "start_date": start_date, "end_date": end_date})
    if (procedure or foi or offering) and obs_property:
        logger.debug({'msg': 'Call to SOS'})
        # set JSON for POST request to SOS
        req = {
            "request": request_type,
            "service": "SOS",
            "version": "2.0.0"}
        try:
            assert type(obs_property) == list
            props = obs_property
        except AssertionError:
            props = [obs_property]
        req["observedProperty"] = [
            "%s%s" % (namespace, _prop) for _prop in props]
        if procedure:
            try:
                assert type(procedure) == list
                procs = procedure
            except AssertionError:
                procs = [procedure]
            req["procedure"] = ["%s" % _proc for _proc in procs]
        if foi:
            try:
                assert type(foi) == list
                fois = foi
            except AssertionError:
                fois = [foi]
            req["featureOfInterest"] = ["%s" % _foi for _foi in fois]
        if offering:
            req["offering"] = "%s" % offering
        if equals:
            req["temporalFilter"] = [
                {"equals":
                    {"ref": "om:phenomenonTime",
                     "value": "%s" % equals
                     }
                 }
            ]
        elif start_date and end_date:
            req["temporalFilter"] = [
                {"during":
                    {"ref": "om:phenomenonTime",
                     "value": [
                         "%s" % start_date,
                         "%s" % end_date
                         ]
                     }
                 }
            ]
        else:
            pass   # no date filter
        logger.info({'msg': 'JSON for SOS POST', 'data': json.dumps(req)})
    else:
        logger.warning(
            {'msg': 'Unable to construct the SOS request!  Need procedure or'
            '  offering or feature-of-interest, and also the property.'})
    return req


def get_sos_history(source, payload, as_json=True):
    """Wrapper for key parameters to access GetDataAvailability for a SOS.

    Returns:
        List of results as JSON:
        {"foi_name":
            {"property_name":
                {start: 2010-01-13, "end": }
            }
        }

        If the correct sink parameters are supplied, then data is supplied
        directly to that sink.

    Args:
        source: dict
            Must contain a `url` key
        payload: dict
            See post_sos_request() for details
        as_json: Boolean
            If true, then reformat the GetDataAvailability result as JSON
    """
    logger.debug({'msg': 'get_history', 'source': source, 'payload': payload})
    results = None
    try:
        debug_only = payload.get('debug', False)
    except (AttributeError, TypeError, ValueError) as err:
        logger.exception(
            'Invalid payload error:%s. Is type dictionary (%s) with data (%s)?',
            err, type(payload), payload)
        return None
    try:
        target = source.get('url', None)
    except (AttributeError, TypeError, ValueError) as err:
        logger.exception(
            'Invalid source error:%s. Is type dictionary (%s) with data (%s)?',
            err, type(source), source)
        return None
    try:
        results = post_sos_request(
            target, payload, request_type="GetDataAvailability")
        if debug_only:
            return results
        logger.debug({'msg': 'Results from SOS: POST', 'results': results})
        results_dict = json.loads(results.text)
        logger.debug({'msg': 'Results from SOS: dict', 'results': results_dict})
        if as_json:
            results = data_availability_to_json(results_dict)
            logger.debug({'msg': 'Data availability', 'results': results})
            return results
        else:
            return results_dict
    except SOSRequestException as err:
        logger.error({'msg': 'SOS request failed', 'error': err.text,
                      'source': source, 'payload': payload})
        return None
    except AttributeError as err:
        logger.error({'msg': 'Unable to get text from results',
                      'error': err, 'data': results})
        return None
    except (TypeError, ValueError) as err:
        logger.error({'msg': 'Unable to JSON load SOS results',
                      'error': err, 'data': results})
        return None
    except Exception as err:
        logger.error({'msg': 'Unable to process SOS results',
                      'error': err})
        return None


def post_sos_request(url, payload, request_type="GetObservation"):
    """POST a request to a SOS at `url` with a `payload` dictionary.

    This function converts the supplied payload dict into a JSON structure
    suitable for sending to a 52N SOS as a JSON string. String is POSTed
    and results are returned (these will also be JSON) if debug parameter
    is False.

    Args:
        url: string
            http address for SOS service endpoint
        payload: dict
            keys:
                `property` - the parameter(s) being measured or observed
                `procedure` - name of SOS procedure(s) (aka sensor)
                `offering` - name of SOS offering
                `namespace` - namespace used as prefix for SOS property
                `featureOfInterest` - feature(s) being sensed/observed
                `start` - datetime string for start of a range
                `end` - datetime string for end of a range
                `delta`- seconds before now OR `start` (default is 3600)
                `equals` - an exact datetime string
                `format` - string: [json | csv ]; defaults to json
                `model` - the required Swordfish Common Information Model
                    format [curently only OM_Measurement is supported] for
                    JSON-format results
                `debug` - defaults to False; only return the SOS payload
                    as a JSON string (this could be used directly in the
                    web inteface for the SOS, for example)
                `timezone` - name as a string (e.g. 'Africa/Johannesburg')
            NOTE:
              * in the payload, `property` is required
    """
    results = None
    debug_only = payload.get('debug', False)
    logger.debug({'msg': 'SOS request', 'request_type':request_type,
                  'payload': payload, 'payload_type': type(payload)})
    req = create_json_request(payload=payload, request_type=request_type)
    logger.debug("\nreq==%s \nurl==%s \nheaders==%s", req, url, HEADERS)
    if req:
        # post request to SOS
        try:
            if debug_only:
                results = json.dumps(req)  # send back formatted request
            else:
                results = requests.post(
                    url, data=json.dumps(req), headers=HEADERS)
                logger.debug(
                    {'msg': 'Results from SOS',
                     'code': results.status_code,
                     'data': results.text})
                if results.status_code != requests.codes.ok:
                    logger.error(
                        {'msg': 'Error returned by SOS',
                         'code': results.status_code,
                         'data': results.text})
        except requests.ConnectionError as err:
            logger.exception({'msg': 'SOS Service not available', 'error': err})
    return results
