# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Purpose: Generate XML (or other) file for location-based information.

Authors: Derek Hohls <dhohls@csir.co.za>; Terence van Zyl <tvanzyl@csir.co.za>
Organisation: CSIR, Meraka
Version: 1.0
Date: 2015-07-09
Purpose :
    Generate an XML (or other) file for location-based information.  By default
    this handles the layout for an OGC SOS 2.0 InsertObservation XML-based POST
Updates:
    2017-08-25 dhohls@csir.co.za  Create XML outside of create_observation
    2017-12-14 dhohls@csir.co.za  Added handler for OM_ComplexObservation
"""
# lib
import copy
import json
from jinja2 import Template
import logging
import string
# project
from swordfish.core import AttrDict
# local
from swordfish.sos.templates import InsertObservation, InsertObservationArray,\
    InsertObservation_1_0

log = logging.getLogger(__name__)


def load_from_file(name):
    """Load strings from a file."""
    _file = None
    try:
        with open(name, "r") as myfile:
            _file = myfile.read().replace('\n', '')
    except IOError:
        pass
    return _file


class LayoutGenerator(object):
    """Generate one or more location-based formatted observations from JSON data
    """

    def __init__(self, template=None):
        """
        Parameters:
            template : string
                the template to be used; defaults to 'InsertObservationArray'
                (for SOS 2.0); if template name does not match any existing
                templates then the system will attempt to load the template
                string from a file instead.
        """
        loader = self.load_template(template)
        if loader:
            self.template = Template(loader)
        else:
            log.error('Template or file "%s" is unknown.', template)
        self.observation = None
        self.xml = []
        self.xml_data = {}

    def load_template(self, name):
        """Load named template or try and load from a file."""
        if name == 'InsertObservation':
            return InsertObservation
        elif name == 'InsertObservationArray':
            return InsertObservationArray
        elif name == 'InsertObservation_1_0':
            return InsertObservation_1_0
        elif not name:
            return InsertObservationArray
        else:
            return load_from_file(name)

    def load_data(self, data=None, filename=None):
        """Load JSON data from a string/dict OR file."""
        _data_dict = None
        if filename:
            with open(filename) as data_file:
                _data_dict = json.load(data_file)
        elif data:
            if isinstance(data, AttrDict):
                _data_dict = data
            elif isinstance(data, dict):
                _data_dict = data
            elif isinstance(data, str):
                _data_dict = json.loads(data)
            else:
                log.error("Data '%s' not supported; expected"
                          " 'AttrDict', 'dict' or 'str'", type(data))
        else:
            log.error('Either JSON data or filename must be supplied!')
        log.debug('_data_dict:%s', _data_dict)
        return _data_dict

    def create_id(self, plain_id):
        """Create XML-suitable ID: replace all punctuation and remove spaces.

        In namespace conformant XML documents, all names must be either
        qualified names or NCNames. The xs:ID in SOS XML must be an NCName.

        Note that `translate` works with ASCII, not unicode - hence the rather
        simplistic conversion below!

        For alternates see:
            http://stackoverflow.com/questions/1324067/~
            how-do-i-get-str-translate-to-work-with-unicode-strings
        """
        try:
            float(plain_id)
            return str(plain_id)
        except Exception as err:
            log.info(err)
        if plain_id:
            replace_punctuation = string.maketrans(string.punctuation,
                                                   '_'*len(string.punctuation))
            plain_id = str(plain_id).translate(
                replace_punctuation).replace(" ", "")
            # TODO - check first char is alphanumeric!
        return plain_id

    def get_coordinates(self, geo_dict, swop=False):
        """Create a coords string in OGC {lon lat, lon lat, ...} notation.

        Args:
            geo_dict
                dictionary containing a "feature of interest"
            swop: boolean
                if True, then coords need to be swopped to create lon,lat
        """
        coordinates = None
        try:
            _geometry = geo_dict.get('featureOfInterest', {}).\
                get('geometry', {})
            foi_shape = _geometry.get('type')
            foi_coords = _geometry.get('coordinates')  # array eg. [23.0, 36.1]
            if foi_shape == 'Point':
                if swop:
                    foi_coords.reverse()
                coordinates = " ".join(str(coord) for coord in foi_coords)
            elif foi_shape == 'Line':
                _str = ' '
                for foic in foi_coords:
                    if swop:
                        foic.reverse()
                    _str += '%s, ' % " ".join(str(coord) for coord in foic)
                coordinates = _str.strip(', ')
            else:
                # TODO - handle polygon as list of lists
                log.error('Geometry type "%s" is not supported!', foi_shape)
        except Exception:
            log.exception('Unable to get coordinates from:%s (%s)',
                          geo_dict, type(geo_dict))
        return coordinates

    def create_xml_om_measurement(self, observation=None):
        """Create XML string for OGC SOS 2.0 InsertObservation POST
        for Swordfish OM_Measurement data

        Note:
            Use self.observation if observation not supplied
        """
        _xml = None
        _observation = observation or self.observation
        log.debug('OM_Measurement:%s', _observation)
        result = _observation.get('result')
        log.debug('result type:%s', type(result))
        if isinstance(result['value'], bool) or \
                result['value'] in ['True', 'False', 'true', 'false']:
            self.xml_data['result_type'] = 'xs:boolean'
            self.xml_data['om_type'] = 'OM_TruthObservation'
            result['value'] = str(result['value']).lower()
        else:
            self.xml_data['result_type'] = 'gml:MeasureType'
            self.xml_data['om_type'] = 'OM_Measurement'
        _xml = self.template.render(
            offering=self.xml_data['offering'],
            description=_observation.get(''),
            timestamp=_observation.get('phenomenonTime'),
            foi=self.xml_data['foi'],
            property=_observation.get('observedProperty'),
            procedure=self.xml_data['proc'],
            point=self.xml_data['point'],
            coordinates=self.xml_data['coordinates'],
            result=result,
            result_type=self.xml_data['result_type'],
            om_type=self.xml_data['om_type'],
            _result=result)
        return _xml

    def create_xml_om_complexobservation(self):
        """Create XML string for OGC SOS 2.0 InsertObservation POST
        for Swordfish OM_ComplexMeasurement data

        	 {"type": "OM_ComplexObservation",
        	  "parameter": {"id": "ed221bf0-d075-012d-287e-34159e211071"},
        	  "phenomenonTime": "2017-11-12T13:45:00Z",
        	  "procedure": {"id": "MSG", "type": "sensor"},
        	  "featureOfInterest": { ...},
        	  "observedProperty": {"type": "strike"},
        	  "resultQuality": 0.95,
        	  "result": {
        	  	"fdi": {"value": "high"},
        		"frp": {"value": 200, "units": "mW", "error": 2.5}}
           }
        """
        _xml = []
        results = self.observation.get('result', {})
        for key in results:
            _observation = copy.deepcopy(self.observation)
            _observation.result = results[key]
            _observation.observedProperty = {"type": key}
            obs_xml = self.create_xml_om_measurement(observation=_observation)
            _xml.append(obs_xml)
        return _xml

    def create_xml_cf_simpleobservation(self):
        """Create list of XML strings for OGC SOS 2.0 InsertObservation POST
        for Swordfish CF_SimpleObservation data.
        """
        results = []
        # TODO - determine correct result_type for CF_Simple_...
        log.debug('CF_SimpleObservation:%s', self.observation)
        times = {}
        _results = self.observation.get('result', {}).get('data', {})
        log.debug('_results:%s', _results)
        _times = _results.get('time', [])
        __times = sorted(_times)
        try:
            times['start'] = __times[0]
            times['end'] = __times[-1]
        except Exception as err:
            times['start'], times['end'] = None, None
        # loop through properties, then each result in each results array
        _vars = self.observation.get('result', {}).get('variables', {})
        for key in list(_vars.keys()):
            _property = {}
            if key != 'time':
                _property = {
                    'type': key,
                    'units': _vars.get(key).get('units', ''),
                    'class': 'Quantity'  # TODO - handle other classes?
                }
                for rkey, _result in enumerate(_results.get(key, [])):
                    result = {}
                    result['units'] = _vars.get(key, {}).get('units', '')
                    try:
                        result['datetime'] = _times[rkey]
                    except (NameError, TypeError, IndexError):
                        result['datetime'] = None
                    result['value'] = _result
                    _xml = self.template.render(
                        coordinates=self.xml_data['coordinates'],
                        foi=self.observation.get('featureOfInterest'),
                        procedure=self.xml_data['proc'],
                        offering=self.xml_data['offering'],
                        timestamp=result['datetime'],
                        result=result,
                        property=_property,
                        point=self.xml_data['point'],
                        # TODO - Handle quality in template !!!
                        quality=self.observation.get('resultQuality'),
                        result_type=self.xml_data['result_type'],
                        om_type=self.xml_data['om_type'])
                    results.append(_xml)
        return results

    def create_observation(self, data=None, filename=None, swop_coords=False):
        """Create list of XML strings for OGC SOS 2.0 InsertObservation POST.

        Details of what data is stored where varies between observation types;
        hence this method acts an 'intermediary' processing step to ensure that
        the template receives the same type of data in all cases.
        """
        self.xml = []
        self.observation = copy.deepcopy(
            self.load_data(data=data, filename=filename))
        log.debug('observation:%s', self.observation)
        if not self.observation:
            log.warn("No observation dict could be created!")
            return self.xml
        # create a common data dictionary used by all observation types
        self.xml_data = {}
        self.xml_data['obs_type'] = self.observation.get('type')
        self.xml_data['result_type'] = 'gml:MeasureType'
        self.xml_data['om_type'] = 'OM_Measurement'
        self.xml_data['foi'] = self.observation.get('featureOfInterest', {})
        self.xml_data['foi_id'] = \
            self.create_id(self.xml_data['foi'].get('id'))
        self.xml_data['foi']['properties'] = \
            self.xml_data['foi'].get('properties', {})
        self.xml_data['foi']['properties']['name'] = \
            self.xml_data['foi'].get('name') or \
            self.xml_data['foi'].get('properties', {}).get('name') or \
            ''
        try:
            self.xml_data['proc'] = self.observation.get('procedure').get('id')
            if not self.xml_data['proc']:
                self.xml_data['proc'] = self.xml_data['foi_id']
        except Exception as err:
            self.xml_data['proc'] = self.xml_data['foi_id']
        try:
            self.xml_data['offering'] = \
                self.xml_data['foi'].get('properties').get('offering').get('id')
        except Exception as err:
            self.xml_data['offering'] = None
        # fallback: have offering match the procedure
        if not self.xml_data['offering']:
            try:
                self.xml_data['offering'] = self.xml_data['proc']
                if not self.xml_data['offering']:
                    self.xml_data['offering'] = self.xml_data['foi_id']
            except Exception as err:
                self.xml_data['offering'] = self.xml_data['foi_id']
        log.debug('FOI:%s PROC:%s OFF:%s FOID:%s',
                  self.xml_data['foi'], self.xml_data['proc'],
                  self.xml_data['offering'], self.xml_data['foi_id'])
        self.xml_data['point'] = self.xml_data['foi_id']
        self.xml_data['coordinates'] = self.get_coordinates(self.observation,
                                                            swop_coords)
        # create the XML based on observation type
        _xml = None
        if self.xml_data['obs_type'] == "OM_Measurement":
            _xml = self.create_xml_om_measurement()
        elif self.xml_data['obs_type'] == "CF_SimpleObservation":
            _xml = self.create_xml_cf_simpleobservation()
        elif self.xml_data['obs_type'] == "OM_ComplexObservation":
            _xml = self.create_xml_om_complexobservation()
        elif self.xml_data['obs_type'] is None:
            log.error('Observation type has not been specified!')
        else:
            log.error(
                'Observation of type "%s" is not catered for!',
                self.xml_data['obs_type'])
        # create XML list
        if _xml:
            log.debug('_xml:%s\n', _xml)
            self.xml.append(_xml)  # could expect a string or a list ...
        return self.xml
