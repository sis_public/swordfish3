# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""SOS data historical summary.

Purpose: Extract data from a SOS and create historical summaries.
Created: 2018-04-10
Author:  dhohls@csir.co.za

Description:


Examples:

This example shows data types and assumes the daily update is required::

    python summary.py \
    --url=SOS_URL \
    --payload='{
        "procedure":PROCEDURE,
        "property_name":PROPERTY,
        "property_units":UNITS,
        "datetime":DATETIME,
        "period":PERIOD,
        "precision":3}'
    --daily

Only the 'property_name' is reqired in the payload dictionary.

This example has a typical payload (assuming the daily update is required, and
the code is being run from inside the source code's directory)::

    python summary.py \
    --url=http://127.0.0.1/sos/service \
    --prefix=nnr \
    --payload='{
        "procedure":"rad1",
        "property_name":"rad1_value",
        "property_units":"Bq",
        "datetime":"2018-05-11T06:01:01.963Z",
        "statistic":"AVERAGE",
        "precision":3,
        "chunk":3}' \
    --daily \
    --loglevel=INFO

Notes:
  * the above example will have a default period of MONTH days, and chunk of 7
  * the JSON output file will have a prefix of `nnr`
  * passing in an argument of `debug` will mean the SOS POST is not called.
  * as no timezone is specified in the payload, it will be assumed that all
    datetime data in and out of the function will be in UTC format - to use
    other timezones see:
    https://stackoverflow.com/questions/13866926/python-pytz-list-of-timezones

Updates:

    2018-05-11 dhohls@csir.co.za    All date formats default to UTC
                                    Added new statistic options
"""


# lib
import argparse
from collections import OrderedDict
import datetime
import json
import logging
import sys
import time
# third-party
import pytz
import requests
from dateutil.parser import parse
from dateutil.tz import gettz
# swordfish
from .utils import post_sos_request, iso_date_string
from .utils import TIMEZONE, NAMESPACE

logger = logging.getLogger(__name__)

SOS_URL = 'http://127.0.0.1/'
LOG_LEVEL = 'WARN'
PREFIX = 'sos'
MONTH = 30  # month size is defined as number of days
CHUNK_DELAY = 0.5  # delay in seconds beween retrieving each chunk


class SOSHistoricalSummary(object):
    """Extract data from a SOS and create historical summaries."""

    def __init__(self, **kwargs):
        """Create class-wide data properties.

        Keywords args:
            source: string
                full URl for SOS Service
            prefix: string
                prefix for JSON files
            procedure: string
                identity of SOS procedure
            property: string
                identity of SOS property (variable) whose data is required
            units: string
                units for the SOS property (variable)
            namespace: string
                the namespace prefix used in the SOS
            datetime_string: string
                ISO-formatted datetime string representing the key date around
                which all date calculations will be made; defaults to 'now'
            run_daily: boolean
                if True, create a daily summary for each day in the `period`
                (defaults to False)
            period: integer
                number of time intervals covered by the summary
            chunk: integer
                small sizes into which the period is broken up in order to
                reduce data load on the SOS
            statistic: string
                the method used to summarise the daily data;
                default is 'AVERAGE'
            precision: integer
                the precision (number of decimal places), if needed,
                for the reported statistic value
            timezone: string
                the name of the pytz timezone (for dattime being passed in,
                as well as the formatting for the output dates). See also:
                https://stackoverflow.com/questions/13866926/python-pytz-list-of-timezones

        """
        logger.debug('kwargs:%s', kwargs)
        self.source = kwargs.get('source', None)
        if not self.source:
            raise RuntimeError('Unable to find/set the SOS URL')
        self.sos_data = None
        self.loglevel = kwargs.get('loglevel', 'ERROR')
        self.filename_prefix = kwargs.get('prefix', PREFIX)
        self.statistic = kwargs.get('statistic', 'AVERAGE')
        self.precision = kwargs.get('precision', None)
        self.procedure = kwargs.get('procedure', None)
        self.property_name = kwargs.get('property', None)
        self.property_units = kwargs.get('units', None)
        self.namespace = kwargs.get('namespace', '')
        self.run_daily = kwargs.get('run_daily', False)
        self.debug_mode = kwargs.get('debug', False)
        self.timezone = kwargs.get('timezone', TIMEZONE)
        self.datetime_string = kwargs.get(
            'datetime_string',
            iso_date_string(
                datetime.datetime.now(),
                microseconds=True,
                timezone=self.timezone)
            )
        if not self.property_name:
            raise RuntimeError('Unable to find/set a property_name')
        if not self.procedure:
            raise RuntimeError('Unable to find/set the procedure')
        if self.precision:
            try:
                self.precision = int(self.precision)
            except (TypeError, ValueError) as err:
                raise RuntimeError('Unable to process precision:%s (%s)' %
                                   (self.precision, err))
        try:
            _period = kwargs.get('period') or 0
            self.period = int(_period)
        except (TypeError, ValueError) as err:
            raise RuntimeError('Unable to process period:%s (%s)' %
                               (_period, err))
        _chunk = kwargs.get('chunk')
        if _chunk:
            try:
                self.chunk = int(_chunk)
            except (TypeError, ValueError) as err:
                raise RuntimeError('Unable to process chunk:%s (%s)' %
                                   (_chunk, err))
        else:
            self.chunk = None
        logger.debug('proc:%s prop:%s stat:%s time:%s', self.property_name,
                     self.property_name, self.statistic, self.datetime_string)

    def load_from_json(self, filename):
        """Load historical summary from JSON file.

        Args:
            filename: string
                the filename prefix (.json will be automatically added)
        """
        try:
            with open('%s.json' % filename) as json_file:
                data = json.load(json_file, object_pairs_hook=OrderedDict)
            return data
        except Exception as err:
            logger.error(err)
        return None

    def write_to_json(self, results, datetime_string, summary=None):
        """Write historical summary to a JSON file.

        Args:
            results: dict
                result data (stations & sets of datatime,value tuples)
            datetime_string: string
                ISO-formatted datetime
            summary: string
                one of [DAILY|LATEST]

        """
        def decimals(value):
            if self.precision:
                try:
                    return round(value, self.precision)
                except Exception as err:
                    logger.error('Cannot round:%s (%s)', value, err)
                    return value
            else:
                return value

        try:
            assert isinstance(summary, str) is True
        except AssertionError:
            raise RuntimeError('Invalid `summary` supplied for JSON write!')
        if not results:
            logger.warn('Null `results` supplied for JSON %s write!', summary)
            return
        # header
        history = OrderedDict()
        history["doctype"] = "eositts"
        history["version"] = "1.0"
        history["date_created"] = iso_date_string(datetime.datetime.now(),
                                                  microseconds=True)
        history["date_end"] = datetime_string
        history["datasets"] = []

        filename = '%s_%s_%s_%s' % (
            self.filename_prefix, summary.lower(), self.statistic.lower(),
            self.property_name)

        if summary == 'LATEST':
            history_hourly = OrderedDict()
            history_hourly["id"] = "latest",
            history_hourly["header"] = dict(
                property=self.property_name,
                units=self.property_units,
                time_format="DATETIME",
                data_type="NUMBER",
                statistic=self.statistic
            )
            history_hourly["data"] = []
            for result in results:
                #logger.debug(results[result])
                _data = [{"d": data[0], "v": decimals(data[1])}
                         for data in list(results[result].items())]
                device_data = OrderedDict()
                device_data["device"] = {'id': result}
                device_data["series"] = _data
                history_hourly["data"].append(device_data)
            history["datasets"] = history_hourly

        elif summary == 'DAILY':
            history_daily = OrderedDict()
            history_daily["id"] = "thirtyday_daily_statistic"
            history_daily["header"] = dict(
                property=self.property_name,
                units=self.property_units,
                time_format="DATE",
                data_type="NUMBER",
                aggregate=self.statistic
            )
            history_daily["data"] = []
            for result in results:
                #logger.debug(results[result])
                _data = [{"d": data[0], "v": decimals(data[1])}
                         for data in list(results[result].items())]
                device_data = OrderedDict()
                device_data["device"] = {'id': result}
                device_data["series"] = _data
                history_daily["data"].append(device_data)
            history["datasets"] = history_daily

        else:
            raise NotImplementedError(
                'Processing of "%s" summary is not handled' % summary)

        # save data
        try:
            _file = '%s.json' % filename
            with open(_file, 'w') as outfile:
                json.dump(history, outfile, indent=4)
            logger.info('%s data written to %s at %s',
                        summary, _file, datetime.datetime.now())
            return True
        except Exception as err:
            logger.exception('Unable to write JSON file (%s)', err)
        return False

    def calculation(self, alist, statistic=None):
        """Calculate a statistic over a list of values.

        Testing:
        for stat in ['AVERAGE', 'MEDIAN', 'FIRST', 'LAST', 'COUNT', 'foo', ]:
            calculation(alist=None, statistic=stat)
            calculation(alist=[], statistic=stat)
            calculation(alist=[0,], statistic=stat)
            calculation(alist=[1,], statistic=stat)
            calculation(alist=[0, 1], statistic=stat)
        """
        if statistic is None:
            statistic = self.statistic
        try:
            if statistic == 'AVERAGE' or statistic == 'AVG':
                return sum(alist) / len(alist)
            elif statistic == 'TOTAL' or statistic == 'TOT' or statistic == 'SUM':
                return sum(alist)
            elif statistic == 'COUNT':
                return len(alist)
            elif statistic == 'FIRST':
                return alist[0]
            elif statistic == 'LAST':
                return alist[-1]
            elif statistic == 'MIN' or statistic == 'MINIMUM':
                return min(alist)
            elif statistic == 'MAX' or statistic == 'MAXIMUM':
                return max(alist)
            elif statistic == 'MED' or statistic == 'MEDIAN':
                llen = len(alist)
                if llen % 2 == 1:
                    return sorted(alist)[llen // 2]
                else:
                    return sum(sorted(alist)[llen // 2 - 1:llen // 2 + 1]) / 2.0
                return None
            else:
                raise NotImplementedError(
                    'Statistic type %s is not supported' % statistic)
        except (ValueError, AttributeError, TypeError,
                ZeroDivisionError, IndexError):
            logger.error('Unable to process list:%s', alist)
            return None

    def process_sos_observations(
            self, data_dict, timescale='DAY'):
        """Create a summary dictionary from a SOS GetObservations request.

        Args:
            data_dict: dictionary
                a set of JSON-based results from a SOS GetObservation request
            timescale: string
                timescale at which to return data dictionary summary;
                    [HOUR | DAY | MONTH | YEAR]
        """
        if not data_dict:
            logger.warn('Null data supplied for the observations processing!')
            return
        observations = data_dict.get('observations', [])
        logger.info('No. of observations is %s for %s',
                    len(observations), timescale)
        results = {}
        # consolidate observations
        for observation in observations:
            py_time, py_date = None, None
            foi = observation.get('featureOfInterest', {}).\
                get('identifier', {}).get('value', None)
            _time = observation.get('resultTime', None)
            try:
                py_time = parse(_time)
                logger.debug('SOS Time from~%s to~%s', _time, py_time)
            except Exception as err:
                logger.exception('Cannot parse time:%s (%s)', _time, err)
            if py_time:
                try:
                    if timescale == 'HOUR':
                        py_date = py_time.replace(
                            tzinfo=gettz(self.timezone)).replace(
                                microsecond=0, second=0, minute=0).isoformat()
                    elif timescale == 'DAY':
                        py_date = "%s-%s-%s" % (
                            py_time.year, py_time.month, py_time.day)
                    elif timescale == 'MONTH':
                        py_date = "%s-%s" % (py_time.year, py_time.month)
                    elif timescale == 'YEAR':
                        py_date = "%s" % py_time.year
                    else:
                        logger.warn('No timescale specifed:%s', timescale)
                        py_date = py_time
                except Exception as err:
                    logger.exception(
                        'Cannot process time:%s(%s) for timescale:%s in'\
                        ' timezone:%s (%s)',
                        py_time, type(py_time), timescale, self.timezone, err)
            value = observation.get('result', {}).get('value', None)
            if foi and py_date and value:
                if not results.get(foi):
                    results[foi] = {}
                if not results[foi].get(py_date):
                    results[foi][py_date] = []
                results[foi][py_date].append(value)
        # calculate statistic
        calcs = {}
        for feature in list(results.keys()):
           if not calcs.get(feature):
               calcs[feature] = {}
           dates = results[feature]
           logger.debug('feature:%s type:%s', feature, type(feature))
           for adate in dates:
               alist = results[feature][adate]
               calculated = self.calculation(alist)
               calcs[feature][adate] = calculated
        logger.debug('calcs:%s', calcs)
        # return results
        return calcs

    def create_summary_statistic(self, timescale='DAY', period=None,
                                 chunk=None, whole=True):
        """Create a property's statistic for all stations over a period.

        Keyword Args:
            timescale: string
                timescale at which to process the data range
                    [HOUR | DAY | MONTH | YEAR]
            period: numeric (int or float)
                the span over which the statistic will be calculated;
                defaults to `self.period`
            chunk: numeric (int or float)
                the time interval over which the calls to the SOS will be made;
                this is useful for large data sets, or long ranges of data
                processing, or where the frequency of data collection is high.
                NOTE:
                  * the timecale is the same as for period.
                  * the chunk defaults to `period` and cannot be greater
                    than the period value.
            whole: boolean:
                if True, use whole days for extracting data (0am to midnight)

        """
        def merge_two_dicts(x, y):
            """Merge two dicts into a new dict as a shallow copy."""
            z = x.copy()
            z.update(y)
            return z

        def end_of_day(adate):
            """Set a datetime to be the last second of the day."""
            try:
                adate = adate.replace(hour=23, minute=59, second=59, microsecond=0)
                return adate
            except Exception as err:
                logger.exception(err)
                return None

        results = None
        if not period:
            period = self.period
        if not chunk:
            chunk = self.chunk or period
        if chunk is None or period is None:
            raise RuntimeError('Both period (%s) and chunk (%s) must be set' %
                               (period, chunk))
        if chunk > period:
            raise RuntimeError('Chunk (%s) must be less than period (%s)' %
                               (chunk, period))
        try:
            start_datetime = parse(self.datetime_string)
        except (AttributeError, TypeError, ValueError) as err:
            start_datetime = None
            logger.exception('Unable to decode `key_date`:%s (%s)',
                             self.datetime_string, err)
        if not start_datetime:
            return None
        # loop through chunks to create full data dictionary
        data_dict = {}
        key_datetime = start_datetime
        chunk_end = 0
        chunk_size = chunk
        logger.debug("PRE-LOOP datetime:%s", key_datetime)
        while True:
            # chunk size - check for partials
            if chunk_end + chunk > period:
                chunk_size = period - chunk_end
                chunk_end = period  # ensure loop breaks at boundary
            else:
                chunk_size = chunk
                chunk_end = chunk_end + chunk
            # datetime changes
            if timescale == 'DAY':
                date_start = key_datetime - datetime.timedelta(days=chunk_size)
            elif timescale == 'HOUR':
                date_start = key_datetime - datetime.timedelta(hours=chunk_size)
            elif timescale == 'MONTH':
                date_start = key_datetime - datetime.timedelta(days=chunk_size * MONTH)
            elif timescale == 'YEAR':
                date_start = key_datetime - datetime.timedelta(years=chunk_size)
            else:
                raise NotImplementedError(
                    'Processing of "%s" timescale is not handled' % timescale)
            date_end = key_datetime
            if timescale in ['DAY', 'MONTH'] and whole:
                # use "whole" days
                date_end = end_of_day(date_end)
                date_start = end_of_day(date_start) + datetime.timedelta(seconds=1)
            logger.debug("DATES start:%s end:%s", date_start, date_end)
            logger.debug("timezone:%s", self.timezone)
            iso_date_start = iso_date_string(date_start, timezone=self.timezone)
            iso_date_end = iso_date_string(date_end, timezone=self.timezone)
            logger.debug("DATES ISO_start:%s ISO_end:%s",
                         iso_date_start, iso_date_end)
            payload = {
                "procedure": self.procedure,
                "property": self.property_name,
                "namespace": self.namespace,
                "start": iso_date_start,
                "end": iso_date_end
            }
            logger.debug('sos:%s summary payload:%s', self.source, payload)
            logger.debug("summary:%s payload:%s period:%s",
                         timescale, payload, period)
            # request to SOS
            if self.debug_mode:
                print('POST:%s TO:%s' % (payload, self.source))
            else:
                try:
                    response = post_sos_request(
                        self.source, payload, request_type="GetObservation")
                    if response and response.status_code:
                        logger.debug('SOS Response Code:%s', response.status_code)
                    if response and response.status_code == requests.codes.ok:
                        data = response.text
                        logger.debug('SOS Response Size:%s', len(data))
                        new_dict = json.loads(data)
                        if not data_dict:
                            data_dict = new_dict  # first pass
                        else:
                            data_dict.get("observations", []).extend(
                                new_dict.get("observations", []))
                        # ------------------ BEGIN DEBUG ONLY ------------------
                        if self.loglevel == 'DEBUG':
                            raw_file = self.filename_prefix + \
                                '_raw_data_%s_TO_%s.json' % (timescale, iso_date_start)
                            logger.debug('Writing data to file:%s', raw_file)
                            with open(raw_file, 'w') as outfile:
                                json.dump(new_dict, outfile, indent=2)
                        # ------------------ END DEBUG ONLY --------------------
                    else:
                        if response and response.status_code:
                            logger.warning(
                                'No data returned from call to SOS! (code:%s)',
                                response.status_code)
                        else:
                            logger.warning(
                                'Unable to access SOS - please check the URL.')
                except requests.exceptions.MissingSchema as err:
                    logger.exception('Unable to get data from SOS (%s)', err)
                except Exception as err:
                    logger.exception('Unexpected SOS error (%s)', err)
            # new key datetime
            if timescale == 'DAY':
                key_datetime = key_datetime - datetime.timedelta(
                    days=chunk_size)
            elif timescale == 'HOUR':
                key_datetime = key_datetime - datetime.timedelta(
                    hours=chunk_size)
            elif timescale == 'MONTH':
                key_datetime = key_datetime - datetime.timedelta(
                    days=chunk_size * MONTH)
            elif timescale == 'YEAR':
                key_datetime = key_datetime - datetime.timedelta(
                    years=chunk_size)
            else:
                pass
            # check boundary
            if chunk_end >= period:
                break
            time.sleep(CHUNK_DELAY)  # delay (prevent SOS overload)

        if data_dict:
            results = self.process_sos_observations(
                data_dict=data_dict, timescale=timescale)
        else:
           logger.warning('No data in data_dict for timescale:%s!'
                          ' (Results NOT created)', timescale)
        return results

    def process(self):
        """Call the class methods to create the output data."""
        raise NotImplementedError('Please override in an inherited class.')
        """
        # Example:
        logger.debug('key_date:%s', self.datetime_string)
        # create output data for recent (last hour) data
        latest = self.create_summary_statistic(timescale='HOUR', period=1)
        self.write_to_json(
            latest,
            self.datetime_string,
            summary='LATEST')
        # create output data for daily data (over specified period)
        if self.run_daily:
            daily = self.create_summary_statistic(timescale='DAY')
            self.write_to_json(
                daily,
                self.datetime_string,
                summary='DAILY')
        """


def main(args):
    """Setup args and process request."""
    logging.basicConfig(
        stream=sys.stdout,
        format='%(asctime)s - {%(filename)s:%(lineno)d} %(levelname)s'
               ' - %(name)s - %(message)s',
        level=getattr(logging, args.loglevel))
    # send data from args to class
    logger.debug({'msg': 'Args', 'data': args})
    try:
        payload = json.loads(args.payload, object_pairs_hook=OrderedDict)
    except ValueError as err:
        logger.exception('Unable to process payload: %s', err)
        return
    sos_history = SOSHistoricalSummary(
        source=args.url,
        prefix=args.prefix,
        run_daily=args.daily,  # default is FALSE
        namespace=args.namespace or NAMESPACE,
        timezone=args.timezone or TIMEZONE,
        property=payload.get('property_name'),
        procedure=payload.get('procedure'),
        units=payload.get('property_units'),
        period=payload.get('period'),
        chunk=payload.get('chunk'),
        statistic=payload.get('statistic'),
        precision=payload.get('precision'),
        datetime_string=payload.get('datetime'),
        loglevel=args.loglevel,
        debug_mode=args.debug
    )
    sos_history.process()


if __name__ == '__main__':
    # handle command-line call
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-u', '--url', default=SOS_URL,
        help="Full Server Address for SOS service"
             " (default is %s)" % SOS_URL)
    parser.add_argument(
        '-f', '--prefix',
        help="Prefix, including path, for output JSON file"
             " (default is %s)" % PREFIX)
    parser.add_argument(
        '-p', '--payload', default='{}',
        help="Payload as JSON string")
    parser.add_argument(
        '-t', '--datetime', default=None,
        help="The datetime for which the request is being run (default: now")
    parser.add_argument(
        '-z', '--timezone', default=TIMEZONE,
        help="Default timezone for encoding dates (%s)" % TIMEZONE)
    parser.add_argument(
        '-n', '--namespace', default=NAMESPACE,
        help="Default namespace [prefix for observed properties (%s)]" %
              NAMESPACE)
    parser.add_argument(
        '-d', '--daily', default=False, action='store_true',
        help="If set, then update daily summaries")
    parser.add_argument(
        '-ll', '--loglevel', default=LOG_LEVEL,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        help="Set log level for service (%s)" % LOG_LEVEL)
    parser.add_argument(
        '-g', '--debug', default=False, action='store_true',
        help="If set, then run in debug mode")
    # set global variables
    ARGS = parser.parse_args()
    NAMESPACE = ARGS.namespace
    LOG_LEVEL = ARGS.loglevel
    main(ARGS)
