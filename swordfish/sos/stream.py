# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Streaming function for SOS.

Created: 2018-03-08
Author: dhohls@csir.co.za

Updates:

    2018-03-09  dhohls@csir.co.za Inherit from SimpleHTTPPutSource
    2018-03-14  dhohls@csir.co.za Working without dynamic delays
"""

# lib
import datetime
import logging
import json
import time
# swordfish
# from swordfish.exceptions import StreamError
# from swordfish.core import AttrDict
from swordfish.http_source import SimpleHTTPPutSource
from swordfish.observations import OM_Measurement
# local
from .utils import get_sos_history, create_json_request, iso_date_time, \
    iso_date_string
from .utils import HEADERS, NAMESPACE

module_logger = logging.getLogger('swordfish.sos.stream')


class SOSStream(SimpleHTTPPutSource):
    '''
    A stream to retrieve data from an OGC Sensor Observation Service

    Records are converted into Common Information Model messages
    '''
    def __init__(self, url,  payload, request_freq=0, backoff_point=15,
                 *args, **kwargs):
        '''
        Params
        ------
        url: full URL connection to the SOS client
        request_freq: default time in milliseconds between calls to the SOS;
            if `realtime` param is set, then the chunked data, together with
            required time intervals will be used to determine the frequency
            of the call (default is 1 minute)

        **kwargs
        --------
        procedure: the ID of the SOS procedure
        property:  the ID of the SOS property (required)
        offering:  the ID of the SOS offering
        feature:   the ID of the SOS feature-of-interest (required)
        realtime: if set, represents the fraction by which intervals between
            transmition of messages is calculated e.g.:
                  1.0 - realtime ("actual") speed
                  0.5 - slowed to half the realtime speed
                  2.0 - speeded up to double realtime speed
            default is 1.0
        chunk: number of seconds worth of data to request at a time
            (default is 5 minutes), starting with the time of the first
            required data object; this should be set to a value that ensures
            a "reasonable" number of messages are retrieved at a one time
            e.g. for "one-per-minute" sampling frequency, set this to an
            hour sized-chunk, but for "one-per-second" frequency, a 5 or
            perhaps 10 minute sized-chunk is appropriate
        timezone: if using a local timezone (not UTC) specify it here
        delay: delay between sending messages - this is based on the actual
            time interval between the original messages; this may
            be changed by the `realtime` setting
        start_time: starting time, as Python datetime object; if None, then
            use the first available time; if start_time is before the first
            available time, then use first available time instead
        end_time: starting time, as Python datetime object; if None, then
            use the last (most recent) available time; if end_time is after the
            last available time, then use last available time instead

        Notes
        -----
        *  The `_sourceprocessor` method the SimpleHTTPPutSource will send one
           message at a time via the internal swordfish queue; with a delay
           of `request_freq` milliseconds between each call to the SOS - it is
           suggested that for effect of simulation this is set to 0 (zero),
           as delays will be inherent in the whole process because of waiting
           to send each message
        *  Stream will contain one observation per message
        '''
        self.url = url
        self.request_freq = request_freq
        self.backoff_point = backoff_point
        self.payload = payload
        self.headers = kwargs.get('headers', HEADERS)
        self.delay = kwargs.get('delay', 3000)  # 3 seconds
        SimpleHTTPPutSource.__init__(
            self,
            url=self.url,
            payload=self.payload,
            request_freq=self.request_freq,
            backoff_point=self.backoff_point,
            headers=self.headers,
            delay=self.delay)
        # kwargs
        module_logger.debug("SOSStream kwargs:%s", kwargs)
        self.timezone = kwargs.get('timezone', None)
        self.start_time = kwargs.get('start_time', None)
        self.end_time = kwargs.get('end_time', None)
        self.chunk = int(kwargs.get('chunk', "300"))  # default: 5 minutes
        self.namespace = kwargs.get('namespace', NAMESPACE)
        self.procedure = kwargs.get('procedure', None)
        self.property = kwargs.get('property', None)
        self.offering = kwargs.get('offering', None)
        self.feature = kwargs.get('feature', None)
        self.realtime = float(kwargs.get('realtime', "1.0"))  # default: real
        # validation
        if not self.property:
            raise ValueError("SOS Property must be set via kwargs")
        if not (self.feature or self.procedure):
            raise ValueError(
                "Either SOS Procedure or Feature must be set via kwargs")
        if self.namespace and self.namespace[-1] != "/":
            raise ValueError(
                'Namespace must end in a "/"')
        # initialisation
        self.current_time = None
        self.current_time_plus_chunk = None
        self.previous_message_time, self.current_message_time = None, None
        self.current_end_time, self.current_start_time = None, None
        # params: time range
        first_available, last_available = self.get_date_range()
        #module_logger.debug("first:%s,last:%s",first_available,last_available)
        if self.start_time is None:
            self.start_time = first_available
        elif self.start_time < first_available:
            self.start_time = first_available
        if self.end_time is None:
            self.end_time = last_available
        elif self.end_time > last_available:
            self.end_time = last_available
        if  self.start_time is None or self.end_time is None:
            raise ValueError("Unable to get or set times")

    def create_payload(self, payload):
        '''Create payload data; overide of parent class.'''
        payload = {
            "property": self.property
        }
        if self.namespace:
            payload["namespace"] = self.namespace
        if self.procedure:
            payload["procedure"] = self.procedure
        if self.offering:
            payload["offering"] = self.offering
        if self.feature:
            payload["featureOfInterest"] = self.feature
        module_logger.debug("payload1:%s", payload)
        # start/end must vary by steps of chunk size until end_time is reached
        # then reset and start again ...
        self.set_message_range()
        payload["start"] = iso_date_string(self.current_time,
                                           timezone=self.timezone)
        payload["end"] = iso_date_string(self.current_time_plus_chunk,
                                         timezone=self.timezone)
        module_logger.debug("payload2:%s", payload)
        req = create_json_request(payload, request_type='GetObservation')
        module_logger.debug("json_req:%s", req)
        return json.dumps(req)

    def format_message(self, text):
        '''Create formatted message; overide of parent class.'''
        return text  # was: AttrDict({"value": text})

    def deliver_message(self, resp):
        '''Put message(s) on the internal thread; wait as needed.'''
        try:
            module_logger.debug("resp.content:%s", resp.content)
            _message_dict = json.loads(resp.content)
            message_list = OM_Measurement().sos_to_observation(
                observations=_message_dict)
            for message in message_list:
                self.current_message_time = iso_date_time(
                    datetime_string=message.get('phenomenonTime'),
                    datetime_format='%Y-%m-%d %H:%M:%S.%fZ')
                if self.previous_message_time:
                    delta = self.current_message_time - self.previous_message_time
                    self.delay = delta.total_seconds() * 1000.0 / self.realtime
                    module_logger.info("self.delay:%s msec", self.delay)
                    time.sleep(float(self.delay / 1000.0))   # sleep in secs
                #module_logger.debug("message:%s type:%s", message, type(message))
                self.put(self.format_message(text=message))  # is an AttrDict
                self.previous_message_time = self.current_message_time
            message_list = []
            self.previous_message_time = None
        except Exception as err:
            module_logger.exception(err)

    # ======================================================================
    # UTILITY METHODS
    # ======================================================================

    def set_message_range(self):
        """Set the start/end times for data request from the SOS"""
        if not self.current_time:  # first time only
            self.current_time = self.start_time
        else:
            self.current_time = self.current_time + \
                datetime.timedelta(0, self.chunk)  # days, seconds
        module_logger.debug("curr:%s type:%s",
                            self.current_time, type(self.current_time))
        self.current_time_plus_chunk = self.current_time + \
            datetime.timedelta(0, self.chunk)
        if self.current_time_plus_chunk > self.end_time:
            # reset
            self.current_time = self.start_time
            self.current_time_plus_chunk = self.current_time + \
                datetime.timedelta(0, self.chunk)

    def get_date_range(self):
        """Return start/end times for a property from the SOS."""
        source = {'url': self.url}
        payload = {
            "property": self.property
        }
        if self.namespace:
            payload["namespace"] = self.namespace
        if self.procedure:
            payload["procedure"] = self.procedure
        if self.offering:
            payload["offering"] = self.offering
        if self.feature:
            payload["featureOfInterest"] = self.feature
        module_logger.debug("source:%s payload%s", source, payload)
        history = get_sos_history(source, payload) or {}
        if history == {}:
            module_logger.warn("Unable to get time range from SOS")
        _property = '%s%s' % (self.namespace, self.property)
        foi_data = history.get(self.feature, {}).get(_property, {})
        module_logger.debug("foi_data history:%s for %s", foi_data, _property)
        _start_time = iso_date_time(
            foi_data.get('start', None),
            datetime_format='%Y-%m-%d %H:%M:%S.%fZ')
        _end_time = iso_date_time(
            foi_data.get('end', None),
            datetime_format='%Y-%m-%d %H:%M:%S.%fZ')
        module_logger.debug("start:%s end:%s", _start_time, _end_time)
        return _start_time, _end_time
