# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Load standard Observation templates from local directory"""
import os

PATH = os.path.dirname(os.path.realpath(__file__))

with open(os.path.join(PATH, "InsertObservation.xml"), "r") as myfile:
    InsertObservation = myfile.read().replace('\n', '')

with open(os.path.join(PATH, "InsertObservationArray.xml"), "r") as myfile:
    InsertObservationArray = myfile.read().replace('\n', '')

with open(os.path.join(PATH, "InsertObservation1.0.xml"), "r") as myfile:
    InsertObservation_1_0 = myfile.read().replace('\n', '')
