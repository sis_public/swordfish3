# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Provide a sink endpoint for Swordfish CIM data structures into a SOS.
"""
# lib
import json
import os
import requests
import sys
import traceback
# project
from swordfish.sink import SinkProcessor
# local
from swordfish.sos.create_layout import LayoutGenerator


class InsertObservation(SinkProcessor):
    """Sink endpoint for Swordfish CIM data structures into a SOS."""

    def __init__(self, *args, **kwargs):
        SinkProcessor.__init__(self, *args, **kwargs)
        self.url = kwargs.get('url', None)
        if not self.url:
            raise IOError('No URL specified!')
        self.template = kwargs.get('template', None)
        if not os.path.isfile(self.template):
            raise IOError('Template file "%s" does not exist!' % self.template)
        self.timeout = kwargs.get('timeout', None)
        # defaults / initialisation
        self.headers = {'Content-Type': 'application/xml'}
        self.xml = []
        self.data = None
        self.outcome = None
        self.info("SOS_URL: %s", self.url)

    def process(self, message, stream_id=None):
        """Message should be in SwordFish OM_Measurement format.

        Message Example (NB: Do not put "Z" in the date!)
        ---------------
        {
            "type": "OM_Measurement",
            "phenomenonTime": "2015-08-17T09:06:52+02:00",
            "result": {
                "units": "liters",
                "value": 2000
            },
            "featureOfInterest": {
                "id": "CSIR_PTA_R2",
                "geometry": {
                    "type": "Point",
                    "coordinates": [-25.753, 28.282]
                },
                "type": "Feature",
                "properties": {
                    "name": None,
                }
            },
            "observedProperty": {
                "type": "water_quantity"
            },
            "parameter": {
                "id": "4ca9d9f4-44bf-11e5-a189-6913d2f2437f"
            },
            "procedure": {
                "type": "sensor",
                "id": "45030171",
                "description": "Sensus HRI-Mei linked to meter 14787486"
            }
        }

        Example of correct results
        --------------------------
        <?xml version="1.0" encoding="UTF-8"?>
        <sos:InsertObservationResponse xmlns:sos="http://opengis.net/sos/1.0">
          <sos:AssignedObservationId>o_50</sos:AssignedObservationId>
        </sos:InsertObservationResponse>

        Example of incorrect results
        ----------------------------
        <?xml version="1.0" encoding="UTF-8"?>
        <ows:ExceptionReport xmlns:ows="http://www.opengis.net/ows/1.1"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            version="1.0.0"
            xsi:schemaLocation="http://www.opengis.net/ows/1.1
                http://schemas.opengis.net/ows/1.1.0/owsAll.xsd">
          <ows:Exception exceptionCode="InvalidParameterValue"
                         locator="AssignedSensorId">
            <ows:ExceptionText>
                Sensor must be registered at SOS at first before inserting
                observations into SOS!
            </ows:ExceptionText>
          </ows:Exception>
        </ows:ExceptionReport>

        """
        if not message:
            return  # ignore null messages
        self.debug("JSON_MESSAGE: %s: %s" % (type(message), message))
        self.debug("TEMPLATE: %s" % self.template)
        self.debug("STREAM_ID: %s" % stream_id)

        xml_template = LayoutGenerator(template=self.template)
        self.debug("XML: %s\n%s " % (xml_template, type(xml_template)))
        dataset = xml_template.create_observation(
            data=message, swop_coords=True)
        if not isinstance(dataset, (list, tuple)):
            self.error("Invalid data for SOS:%s" % json.dumps(dataset))
            return
        # skip NULL results
        if message and message.get('type', None) == "OM_Measurement":
            if message.get("result", {}).get("value") is None:
                self.warning("OM_Measurement - NULL result value: %s" %
                             message)
                return
        elif message and message.get('type', None) == "CF_SimpleObservation":
            self.warning("CF_SimpleObservation NOT handled!")
            return
        elif message and message.get('type', None) is not None:
            self.warning("Unknown message type:%s" % message.get('type'))
            return
        else:
            pass

        if not dataset:
            self.error("Dataset is empty!")

        for data in dataset:
            self.outcome = None
            if data:
                self.data = data.replace("\u200b", '')  # zero width space???
                self.debug('XML_TO_POST:%s', json.dumps(self.data))
                try:
                    self.outcome = requests.post(
                        url=self.url, data=self.data, headers=self.headers)
                except Exception:
                    e_type, e_value, trace = sys.exc_info()
                    reason = "\n".join(traceback.format_exception(
                        e_type, e_value, trace, 2))
                    self.error({'ERROR POST_RESPONSE': '%s' %
                                json.dumps(reason)})
                if self.outcome:
                    self.info({'NORMAL_POST_RESPONSE': '%s::%s' %
                               (self.outcome.status_code,
                                self.outcome.text.replace('"', '~'))})
            else:
                self.warning("WARNING: NO data to POST")
