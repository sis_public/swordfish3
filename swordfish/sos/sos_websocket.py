# -*- coding: utf-8 -*-# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""SOS Web Socket Server.

Purpose: Create and run a Web Socket Server for data extraction from a SOS.
Created: 2017-02-01
Author:  dhohls@csir.co.za

Description:
    Tornado-based web Socket server, designed to handle RPC requests and either
    return response to the calling client, or a third-party service
    (e.g. a stream)

    Data returned are RPC-compliant; with the `results` payload containing
    Swordfish Common Information Model (JSON) format (by default), or a
    user-specified format.

Updates:

    2017-08-11 : add logging
    2017-08-14 : improved handling of dates; microseconds are problematic for
                 calls to SOS
    2017-08-15 : added format; patched bugs; enabled logging to actually work
    2017-11-13 : added support for CSV data being returned
    2017-11-14 : added support for multiple parameters, FOIs and procedures
    2018-02-08 : added GetDataAvailability support; refactored code
    2018-03-09 : moved support routines into utils module
    2018-05-04 : added support for CF_SimpleObservation response
    2018-07-11 : added support for timezone-based formatting of SOS data

"""

# lib
import argparse
import csv
import datetime
import json
import logging
import sys
# third-party
# tornado
import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.ioloop
from tornadorpc.json import JSONRPCHandler
from tornadorpc import start_server
try:
    # ver 2 of kombu
    from kombu.exceptions import StdChannelError as ChannelError
except:
    # ver 3 of kombu
    try:
        from amqp.exceptions import ChannelError
    except:
        from amqp import ChannelError
# project
from swordfish.amqp_messaging import KombuStream
# from swordfish.mqtt_messaging import MQTTStream
# from swordfish.observations import OMObservation
from swordfish.observations import OM_Measurement, CFSimpleObservation
# local
from .utils import SOSRequestException, iso_date_string, allowed_port, \
    get_sos_history, post_sos_request
from .utils import TIMEZONE, NAMESPACE

VERSION = "1.1.2"  # increase when functionality changes/updates

ACCESS_URL = 'http://127.0.0.1/'
DEFAULT_PROPERTY = "temperature"
PORT = 9292  # often 8080 or 9191
LOG_LEVEL = 'WARN'
IS_CSV_HEADER = True  # display a header if returning data as CSV

websockets = []  # track connections to the server


class IndexHandler(tornado.web.RequestHandler):
    """Handle websocket root calls."""

    @tornado.web.asynchronous
    def get(self):
        self.write("Connection Established")
        self.finish()


class Proxy(object):
    """RPC and support functions to handle calls to SOS."""

    def __init__(self, *args, **kwargs):
        self.args = args
        self.origin = kwargs.get('origin', 'http://localhost')

    def ping(self):
        """Return current ISO datetime, without microseconds, as JSON."""
        _date_str = iso_date_string(datetime.datetime.now(), timezone=TIMEZONE)
        return json.dumps(_date_str)

    def version(self):
        return json.dumps(VERSION)

    def _sos_request(self, url, payload, request_type="GetObservation"):
        """POST a request to a SOS at `url` with a `payload` dictionary.

        Examples of `payload`:
            payload = {
                'property': 'temperature',  # auto-prefixed by the namespace
                'procedure': 'wes',
                'featureOfInterest': 'A3044',
                'start': '2018-05-02 12:00:00Z',
                'end': '2018-05-02 13:00:00Z',
                'model': 'CF_SimpleObservation',
                'timezone': 'Africa/Johannesburg'
            }
            payload = {
                'property': 'temperature',  # auto-prefixed by the namespace
                'procedure': 'wes',
                'featureOfInterest': 'A3044',
                'delta': 3600,
            }
        """
        results = post_sos_request(url, payload, request_type)
        if results:
            return results
        else:
            # TODO - figure how how to set error message in response ???

            # self.log_exception(
            #   'params', 'Not all params were set properly', None)
            """
            parser = self._RPC_.faults
            parser.codes[-39801] = 'Not all params were set properly'
            #return
            """
            # self._RPC_.faults.messages['error'] =  \
            # {'message': 'Internal Error', 'code': -32603}
            raise SOSRequestException(
                "Not all request params were set properly")
        # except:
        #    return {"error":"Unknown error"}
        return results

    # ======================== available RPC methods ========================

    def get_history(self, source, payload):
        return get_sos_history(source, payload)

    def get_observation(self, source, payload, sink=None):
        """Wrapper for key parameters to access GetObservations for a SOS.

        Returns:
            List of SOS results as Swordfish observations (Common Information
            Model format) if no `sink` and payload is 'json'; otherwise return
            in specified payload `format`.

            If the correct sink parameters are supplied, then data is supplied
            directly to that sink.

        Args:
            source: dict
                Must contain a `url` key
            payload: dict
                See _sos_request() for details
            sink: dict (optional)
                keys:
                    `url` for the end-point;
                    `type` of endpoint (e.g. a KombuStream);
                    `topic` used for streaming data destination

        """
        logging.debug({'msg': 'get_observation', 'source': source,
                       'payload': payload, 'sink': sink})
        results = None
        try:
            target = source.get('url', None)
            results = self._sos_request(target, payload,
                                        request_type="GetObservation")
            debug_only = payload.get('debug', False)
            time_zone = payload.get('timezone', None)
            if debug_only:
                return results
            swordfish_model = payload.get('model', 'OM_Measurement')
            data_format = payload.get('format', 'json')
            logging.debug({'msg': 'Results from SOS', 'data': results})
            results_dict = json.loads(results.text)
        except SOSRequestException as err:
            logging.error({'msg': 'SOS request failed', 'error': err.text,
                           'source': source, 'payload': payload})
            return None
        except AttributeError as err:
            logging.error({'msg': 'Unable to get text from results',
                           'error': err, 'data': results})
            return None
        except (TypeError, ValueError) as err:
            logging.error({'msg': 'Unable to JSON load SOS results',
                           'error': err, 'data': results})
            return None
        except Exception as err:
            logging.error({'msg': 'Unable to process SOS results',
                           'error': err})
            return None

        if sink and sink.get('url'):
            # data to stream (or other target)
            _type = sink.get('type', 'KombuStream')
            if _type == 'KombuStream':
                try:
                    so = KombuStream(
                        sink.get('url'), 1, sink.get('topic', None))
                except ChannelError as err:
                    logging.error({'msg': 'Unable to sink to stream',
                                   'error': err})
                    return None
            else:
                logging.warning(
                    {'msg': 'Stream type "%s" is not implemented!' % _type})
                return None
            # pv = Print("Message")
            if swordfish_model == 'OM_Measurement':
                sos_list = OM_Measurement().sos_to_observation(
                    observations=results_dict,
                    is_json=False,
                    timezone=time_zone)
                # so>>pv
                for item in sos_list:
                    so(item)
            elif swordfish_model == 'CF_SimpleObservation':
                sos_dict = CFSimpleObservation().sos_to_observation(
                    observations=results_dict,
                    is_json=False,
                    timezone=time_zone)
                so(sos_dict)
            else:
                logging.error({'error': 'The "%s" model is not supported' %
                               swordfish_model})
            so.close()
            return True
        else:
            if data_format == 'json':
                if swordfish_model == 'OM_Measurement':
                    results = OM_Measurement().sos_to_observation(
                        observations=results_dict,
                        is_json=True,
                        timezone=time_zone)
                    logging.debug({
                        'msg': 'JSON %s results from SOS' % swordfish_model,
                        'data': results})
                    # return processed SOS data in JSON format to user
                    return results
                elif swordfish_model == 'CF_SimpleObservation':
                    results = CFSimpleObservation().sos_to_observation(
                        observations=results_dict,
                        is_json=True,
                        timezone=time_zone)
                    logging.debug({
                        'msg': 'JSON %s results from SOS' % swordfish_model,
                        'data': results})
                    return results
                else:
                    logging.error(
                        {'error': 'The "%s" model is not supported' %
                         swordfish_model})
                    # return null (no data)
                    return None
            elif data_format == 'csv':
                if swordfish_model == 'OM_Measurement':
                    try:
                        _file = OM_Measurement().sos_to_csv(
                            sos_data=results_dict,
                            header=IS_CSV_HEADER,
                            timezone=time_zone)
                        reader = csv.reader(_file)
                        output = [row for row in reader]
                        _output = output[1] if len(output) > 1 else 'None'
                        logging.debug({
                            'msg': 'CSV results from SOS',
                            'data': _output,
                            'timezone': time_zone})
                         # return processed SOS data in CSV format to user
                        return output
                    except Exception as err:
                        logging.error(
                            {'msg': 'Unable to process and create CSV results',
                             'error': err})
                        return None
                else:
                    logging.error(
                        {'error': 'The "%s" model is not supported for CSV' %
                         swordfish_model})
                    # return null (no data)
                    return None
            else:
                logging.error({'error': 'The "%s" format is not supported' %
                              data_format})
                return None


class RPCHandler(JSONRPCHandler):
    """Handle JSON RPC."""

    def __init__(self, *args, **kwargs):
        JSONRPCHandler.__init__(self, *args, **kwargs)
        self.allowed_origin = kwargs.get('origin', ORIGIN)

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", ORIGIN)

    proxy = Proxy()


def main(args):
    """Start the web socket service."""
    logging.basicConfig(
        stream=sys.stdout,
        format='%(asctime)s - {%(filename)s:%(lineno)d} %(levelname)s'
               ' - %(name)s - %(message)s',
        level=getattr(logging, args.loglevel))
    # version alert
    logging.info({'msg': 'Version', 'data': VERSION})
    # test
    logging.debug({'msg': 'Args', 'data': args})
    handlers = [
        (r'/', IndexHandler),
        (r'/rpc', RPCHandler),
    ]
    start_server(handlers, route="/rpc", port=args.port)


if __name__ == '__main__':
    """Handle command-line call."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-o', '--origin', default=ACCESS_URL,
        help="Server name that is allowed access to the service; "
             "use * for all (%s)" % ACCESS_URL)
    parser.add_argument(
        '-p', '--port', default=PORT, type=allowed_port,
        help="Port number on which to serve data from websocket (%s)" % PORT)
    parser.add_argument(
        '-z', '--timezone', default=TIMEZONE,
        help="Default timezone for encoding dates (%s)" % TIMEZONE)
    parser.add_argument(
        '-r', '--property', default=DEFAULT_PROPERTY,
        help="Default property to which observed data can be assigned (%s)" %
              DEFAULT_PROPERTY)
    parser.add_argument(
        '-n', '--namespace', default=NAMESPACE,
        help="Default namespace [prefix for observed properties (%s)]" %
              NAMESPACE)
    parser.add_argument(
        '-ll', '--loglevel', default=LOG_LEVEL,
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        help="Set log level for service (%s)" % LOG_LEVEL)
    parser.add_argument(
        '-d', '--debug', default=False, action='store_true',
        help="If set, then run in debug mode")
    # set global variables
    ARGS = parser.parse_args()
    ACCESS_URL = ARGS.origin
    ORIGIN = ARGS.origin
    PORT = ARGS.port
    TIMEZONE = ARGS.timezone
    DEFAULT_PROPERTY = ARGS.property
    NAMESPACE = ARGS.namespace
    LOG_LEVEL = ARGS.loglevel
    main(ARGS)
