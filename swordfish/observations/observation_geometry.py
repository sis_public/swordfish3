# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Class wrappers for implementation of functionality for the GeoJSON objects
defined in the Swordfish API.

Created on 07/07/2015 10:00:00
@author: dhohls
"""
# lib
import json
import datetime
import pytz
import uuid
# local
from .observation import OMObservation, Procedure, Measure
from swordfish.core import AttrDict

class geo_interface(AttrDict):
    @property
    def __geo_interface__(self):
        return self

class om_interface(AttrDict):
    @property
    def __om_interface__(self):
        return self

class geometry(geo_interface):
    pass

class SwordfishInformationModelError(Exception): pass

class procedure(AttrDict):
    def __cinit__(self, initdict=None):
        init = initdict or {}
        AttrDict.__init__(self, initdict=init)
        try:
            self.type
            self.id
        except KeyError as ke:
            raise SwordfishInformationModelError(ke)

class observedProperty(AttrDict):
    pass

class featureOfInterest(geo_interface):
    def __cinit__(self, initdict=None):
        init = initdict or {}
        for k, v in list(init.items()):
            self[k] = v

    def __setitem__(self, key, value):
        if key=='geometry':
            AttrDict.__setitem__(self, key, geometry(value) )
        else:
            AttrDict.__setitem__(self, key, value)

class OM_GeometryObservation(om_interface):
    def __init__(self, initdict=None):
        init = initdict or {}
        for k,v in list(init.items()):
            self[k] = v
        self["type"] = "OM_GeometryObservation"

    def __setitem__(self, key, value):
        if key=='featureOfInterest':
            AttrDict.__setitem__(self, key, featureOfInterest(value) )
        elif key=='result':
            AttrDict.__setitem__(self, key, geo_interface(value) )
        elif key=='observedProperty':
            AttrDict.__setitem__(self, key, observedProperty(value) )
        elif key=='procedure':
            AttrDict.__setitem__(self, key, procedure(value) )
        else:
            AttrDict.__setitem__(self, key, value)


eg = OM_GeometryObservation({
"type": "OM_Measurement",
"phenomenonTime": "2015-06-26T10:44:11.044824+02:00",
"result": {"type": "Point", "coordinates": [28.280400, -25.756686]},
"featureOfInterest": {
"id": "CSIR_PTA_44",
"geometry": {
"type": "Point",
"coordinates": [28.280400, -25.756686]},
"type": "Feature",
"properties": {
"name": "CSIR:PTA:44"}},
"observedProperty": {
"type": "active_load"},
"parameter": {
"id": "da424015-0b98-4f32-a4a1-369bb8bdcda3"},
"procedure": {
"type": "sensor",
"id": "CPOWER:124",
"description": "cPowerMeter"}})


class OM_GeometryObservation_old(OMObservation):

    def __init__(self, **kwargs):
        super(OM_GeometryObservation, self ).__init__(**kwargs)
        if self.abort:  # OMObservation ended prematurely
            return

        self.observation["type"] = "OM_GeometryObservation"
        #TODO - create the result!

    def _set_phenomema(self):
        phenomena = {}
        phenomena['property'] = self.observation.get('observedProperty').get('type')
        phenomena['value'] = self.observation.get('result')
        phenomena['units'] = None
        #print "phenomena:", phenomena
        return phenomena
