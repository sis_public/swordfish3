# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Author: Derek Hohls
Created on: 2016-02-24
Purpose: Testing of OM_Measurement
Usage:
* pip install pytest
* cd (to this file's directory)
* py.test
"""
# lib
import csv
import io
import json
# third party
import pytest
# local
from swordfish.observations import OM_Measurement


FIRST = {
	'type': 'OM_Measurement',
	'phenomenonTime': '2015-10-01T00:05:00.000Z',
	'result': {
		'units': 'ppm',
		'value': 16.0
	},
	'featureOfInterest': {
		'geometry': {
			'type': 'Point', 'coordinates': [-25.804706, 27.929009]
		}, 'id': 'NNR_PD_04'
	},
	'observedProperty': {
		'type': 'http://meraka.csir.co.za/ogc/observableProperty/CO'
	},
	'parameter': {
		'id': None
	},
	'procedure': {
		'type': 'sensor',
		'id': 'NNR_PD_04',
		'description': ''
	}
}

def simulate_sos():
    """Provide the same return data expected from a POST query to a SOS"""
    with open('sos_results.json') as _file:
        data = json.load(_file)
    return data


def test_simulate_sos():
    """Test that simulated data exits and is of correct type"""
    data = simulate_sos()
    assert type(data) == type({})


def test_sos_to_observation_blank():
    """Test os_to_observation method with params set to None"""
    omm = OM_Measurement()
    with pytest.raises(Exception) as execinfo:
        omm.sos_to_observation(sos_data=None)
    assert execinfo.value.message == \
        'Unable to extract observations from sos_data'


def test_sos_to_observation():
    """Test sos_to_observation with simulated data"""
    omm = OM_Measurement()
    data = simulate_sos()
    result = omm.sos_to_observation(sos_data=data)
    #assert cmp(result[0], FIRST) == 0  # does not work for dict?
    assert type(result) == type([])
    assert "%s" % FIRST == "%s" % result[0]
    assert len(result) == 115


def test_sos_to_csv_blank():
    """Test sos_to_csv method with params set to None"""
    omm = OM_Measurement()
    with pytest.raises(Exception) as execinfo:
        omm.sos_to_csv(sos_data=None)
    assert execinfo.value.message == \
        'Unable to extract observations from sos_data'


def test_sos_to_csv():
    """Test sos_to_csv method with simulated data"""
    omm = OM_Measurement()
    data = simulate_sos()
    result = omm.sos_to_csv(sos_data=data)
    assert type(result) == type(io.StringIO())
    reader = csv.reader(result)
    output = [row for row in reader]
    assert len(output) == 115
    assert output[0] == ['NNR_PD_04',
                         'http://meraka.csir.co.za/ogc/observableProperty/CO',
                         '2015-10-01T00:05:00.000Z',
                         '16.0']


def test_sos_to_csv_header():
    """Test sos_to_csv method with simulated data and header set ON"""
    omm = OM_Measurement()
    data = simulate_sos()
    result = omm.sos_to_csv(sos_data=data, header=True)
    assert type(result) == type(io.StringIO())
    reader = csv.reader(result)
    output = [row for row in reader]
    assert len(output) == 116
    assert output[0] == ['ID', 'Parameter', 'Date/Time', 'Value']
    assert output[1] == ['NNR_PD_04',
                         'http://meraka.csir.co.za/ogc/observableProperty/CO',
                         '2015-10-01T00:05:00.000Z',
                         '16.0']
