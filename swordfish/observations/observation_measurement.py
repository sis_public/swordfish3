# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Class wrappers for implementation of functionality for the GeoJSON-ish objects
defined in the Swordfish API.

Created on 07/07/2015 10:00:00
@author: dhohls
"""

# lib
import datetime
import logging
import io
# third-party
import xlwt
# project
from swordfish.core import AttrDict
# local
from .observation import OMObservation, utc_to_local

logger = logging.getLogger('swordfish.observations.OM')


class geo_interface(AttrDict):
    """geo"""
    @property
    def __geo_interface__(self):
        return self


class om_interface(AttrDict):
    """om"""
    @property
    def __om_interface__(self):
        return self


class geometry(geo_interface):
    """geometry"""
    pass


class featureOfInterest(geo_interface):
    """FOI"""
    def __cinit__(self, initdict=None):
        init = initdict or {}
        for key, value in list(init.items()):
            self[key] = value

    def __setitem__(self, key, value):
        if key == 'geometry':
            AttrDict.__setitem__(self, key, geometry(value))
        else:
            AttrDict.__setitem__(self, key, value)


class om(om_interface):
    """om"""

    def __cinit__(self, initdict=None):
        init = initdict or {}
        for key, value in list(init.items()):
            self[key] = value

    def __setitem__(self, key, value):
        if key == 'featureOfInterest':
            AttrDict.__setitem__(self, key, featureOfInterest(value))
        else:
            AttrDict.__setitem__(self, key, value)


EXAMPLE = om({
    "type": "OM_Measurement",
    "phenomenonTime": "2015-06-26T10:44:11.044824+02:00",
    "result": {
        "units": "kW",
        "value": 101},
    "featureOfInterest": {
        "id": "CSIR_PTA_44",
        "geometry": {
            "type": "Point",
            "coordinates": [28.280400, -25.756686]},
        "properties": {
            "name": "CSIR:PTA:44"}},
    "observedProperty": {
        "type": "active_load"},
    "parameter": {
        "id": "da424015-0b98-4f32-a4a1-369bb8bdcda3"},
    "procedure": {
        "type": "sensor",
        "id": "CPOWER:124",
        "description": "cPowerMeter"}
})


class OM_Measurement(OMObservation):
    """Specialised class for observation."""

    def __init__(self, observation=None, **kwargs):
        """Example:
        {
            "type": "OM_Measurement",
            "phenomenonTime": "2015-06-26T10:44:11.044824+02:00",
            "result": {
                "units": "kW",
                "value": 101
            },
            "featureOfInterest": {
                "id": "CSIR_PTA_44",
                "geometry": {
                    "type": "Point",
                    "coordinates": ["28.280400", "-25.756686"]
                },
                "type": "Feature",
                "properties": {
                    "name": "CSIR:PTA:44",
                    "offering": {
                        "name": "Ground Stations",
                        "id": "ground_stations"
                    }
                }
            },
            "observedProperty": {
                "type": "active_load"
            },
            "parameter": {
                "id": "da424015-0b98-4f32-a4a1-369bb8bdcda3"
            },
            "procedure": {
                "type": "sensor",
                "id": "CPOWER:124",
                "description": "cPowerMeter"
            }
        }
        """
        super(OM_Measurement, self).__init__(observation=observation, **kwargs)
        # OMObservation ends prematurely; see OMObservation
        if self.abort:
            return
        # initialise
        self.value = kwargs.get('value', None)
        # no futher if no kwargs...
        if not len(list(kwargs.keys())):
            self.observation = {}
            raise ValueError('Observation **args must be set')
        # validate (min requirements)
        if not self.value:
            raise ValueError('Parameter "value" must be set')
        if not self.procedure_id:
            raise ValueError('Parameter "procedure_id" must be set')

        self.observation["type"] = "OM_Measurement"
        self.observation["result"] = {}
        self.observation["result"]["units"] = self.units
        self.observation["result"]["value"] = self.value


    def _template(self, result=True):
        """Return a blank ObservationMeasurement AttrDict as a 'template'"""
        om_template = {
            "type": "OM_Measurement",
            "phenomenonTime": None,
            "featureOfInterest": {
                "type": "Feature",
                "id": None,
                "geometry": {
                    "type": "Point",
                    "coordinates": []
                },
                "properties": {
                    "name": None,
                    "offering": None
                }
            },
            "observedProperty": {
                "type": None
            },
            "parameter": {
                "id": None
            },
            "procedure": {
                "type": "sensor",
                "id": None,
                "description": None
            }
        }
        if result:
            om_template["result"] = {}
            om_template["result"]["units"] = None
            om_template["result"]["value"] = None
        return AttrDict(om_template)

    def _set_phenomema(self):
        phenomena = {}
        phenomena['property'] = self.observation.get('observedProperty', {}).\
                                get('type')
        phenomena['value'] = self.observation.get('result', {}).get('value')
        phenomena['units'] = self.observation.get('result', {}).get('units')
        #print("omm:196 phenomena:", phenomena)
        return phenomena

    def phenomenon_time(self, datetime_format='%Y-%m-%dT%H:%M:%S'):
        """Return phenomenonTime as a Python datetime object."""
        try:
            _time = self.observation.get('phenomenonTime', None)
            if _time is not None:
                _times = _time.split('+')
                p_time = datetime.datetime.strptime(_times[0], datetime_format)
                return p_time
        except Exception as err:
            logger.error(err)
        return None

    def list_observation(self, observation=None):
        """Create a list of (feature,procedure,property,datetime,value) tuples
        from a single observation measurement dictionary.

        Valid for a single property and a single 'time' variable
        """
        observation = observation or self.observation
        output = []
        if observation:
            foi = observation.get('featureOfInterest', {})
            fid = foi.get('id') or \
                foi.get('properties', {}).get('id') or \
                foi.get('properties', {}).get('name') or \
                foi.get('identifier', {}).get('value') or \
                'Unknown'
            proc = observation.get('procedure', {})
            proc_id = proc.get('id') or proc.get('description') or 'Unknown'
            observed_property = observation.get('observedProperty', {})
            param_name = observed_property.get('type')
            result = observation.get('result')
            datum = result.get('value')
            _time = observation.get('phenomenonTime')
            logger.debug('proc%s param:%s time:%s', proc_id, param_name, _time)
            output.append((fid, proc_id, param_name, _time, datum))
        return output

    def sos_to_observation(self, observations=None, is_json=False,
                           timezone=None):
        """Return OM_Measurement from an array of JSON-formatted (dictionary)
        SOS observation results.

        Args:
            observations: array (of dicts)
                see below for example
            is_json: boolean
                if True, then return data in JSON format (i.e. a string;
                not a list)
                if False, then return data as a list of Swordfish AttrDicts
            timezone: string
                official timezone name (https://www.iana.org/time-zones)

        Example of `observations`:

        {
          "request" : "GetObservation",
          "version" : "2.0.0",
          "service" : "SOS",
          "observations" : [
            {...},
            {...}
          ]
        }

        Example of a `data_array`:

        [
            {
              "type" :
                 "http://www.opengis.net/def/observationType/OGC-OM/2.0/"
                 "OM_Measurement",
              "procedure" : "CSIR_PTA_R1",
              "offering" : "CSIR_PTA_R1",
              "observableProperty" :
              "http://meraka.csir.co.za/ogc/observableProperty/water_quantity",
              "featureOfInterest" : {
                "identifier" : {
                  "codespace" : "http://www.opengis.net/def/nil/OGC/0/unknown",
                  "value" : "A3048",
                },
                "geometry" : {
                  "type" : "Point",
                  "coordinates" : [28.281425, -25.751745]
                }
              },
              "phenomenonTime" : "2015-08-18T05:44:45.000Z",
              "resultTime" : "2015-08-18T05:44:45.000Z",
              "result" : {
                "uom" : "liters",
                "value" : 3500.0
              }
            },
            ...
        ]
        """
        logger.debug('using timezone:"%s"', timezone)
        results, data_array = [], []
        try:
            data_array = observations.get("observations", [])
            logger.debug("processing %s messages...", len(data_array))
        except AttributeError:
            raise AttributeError(
                "Unable to extract observations from:"
                "%s\n%s" % (type(observations), observations))
        for swe in data_array:
            measure = self._template()  # AttrDict
            try:
                FID = swe["featureOfInterest"]["identifier"]["value"]
            except Exception as err:
                logger.error('Unable to extract FOI ID from %s (%s)',
                             swe["featureOfInterest"], err)
                FID = swe["procedure"]
            if timezone is not None:
                measure["phenomenonTime"] = utc_to_local(
                    swe["phenomenonTime"], timezone)
                logger.debug('converted UTC:"%s" to local:"%s"',
                             swe["phenomenonTime"], measure["phenomenonTime"])
            else:
                measure["phenomenonTime"] = swe["phenomenonTime"]
            measure["featureOfInterest"] = swe["featureOfInterest"]
            measure["featureOfInterest"]["id"] = FID
            measure["observedProperty"]["type"] = swe["observableProperty"]
            measure["procedure"]["id"] = swe["procedure"]
            measure["procedure"]["description"] = ''  # ???
            # data
            measure["result"] = {}
            if "OM_Measurement" in swe['type']:
                measure["result"]["value"] = swe["result"]["value"]
                measure["result"]["units"] = swe["result"]["uom"]
            elif "OM_SWEArray" in swe["type"]:
                _values = swe["result"]["values"]
                _fields = swe["result"]["fields"]
                time_key, param_key = -1, -1
                for key, field in enumerate(_fields):
                    if field["type"] == 'time':
                        time_key = key
                    else:
                        param_key = key
                if param_key:
                    measure["result"]["value"] = _values[0][param_key]
                    measure["result"]["units"] = _fields[param_key].get("uom")
                #TODO - implement time_key processing
            else:
                raise NotImplementedError(
                    'Observation of type "%s" not catered for!' % swe['type'])
            if is_json:
                results.append(measure.__json__)
            else:
                results.append(measure)
        if is_json:
            _results = ','.join(results)
            return '[%s]' % _results
        else:
            return results

    def sos_to_csv(self, sos_data=None, header=False, timezone=None):
        """Return a CSV file of OM_Measurements from SOS JSON list of dicts"""
        observation_list = self.sos_to_observation(
            observations=sos_data, timezone=timezone)
        output = io.StringIO()  # create a file-like object
        if header:
            output.write('FOI,Procedure,Parameter,Date/Time,Value' + '\n')
        for observation in observation_list:
            _list = self.list_observation(observation=observation)
            for _tuple in _list:
                logger.debug('CSV _tuple:%s', _tuple)
                _line = ",".join(str(item) for item in _tuple)
                output.write(_line + '\n')
        output.seek(0)
        return output

    def sos_to_xls(self, sos_data=None, header=False, timezone=None):
        """Return a XLS file of OM_Measurements from SOS JSON list of dicts"""
        observation_list = self.sos_to_observation(
            sos_data=sos_data, timezone=timezone)
        output = io.StringIO()  # create a file-like object
        # date_format = xlwt.XFStyle()
        # date_format.num_format_str = 'yyyy-mm-dd hh:mm:ss'
        wbk = xlwt.Workbook()
        wk_sheet = wbk.add_sheet('Data')
        if header:
            headers = ('FOI,Procedure,Parameter,Date/Time,Value')
            for key, head in enumerate(headers):
                wk_sheet.write(0, key, head)
        for row, observation in enumerate(observation_list):
            _tuple = self.list_observation(observation=observation)
            for col, item in enumerate(_tuple):
                wk_sheet.write(row, col, str(item))
        wbk.save(output)
        return output
