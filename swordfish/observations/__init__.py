# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Allows direct import of all classes from parent name
e.g. from swordfish.observations import OM_Measurement
"""
from .observation import OMObservation

from .observation_category import OM_CategoryObservation
from .observation_complex import OM_ComplexObservation
from .observation_geometry import OM_GeometryObservation
from .observation_measurement import OM_Measurement,om
from .observation_simple import CFSimpleObservation, CFSimpleMeasurement
from .observation_truth import OM_TruthObservation
