# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/

class SwordfishCommonDataModelError(Exception):
    def __init__(self, value):
        self.value= value
    def __str__(self):
        return repr(self.value)

