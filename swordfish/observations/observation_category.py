# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Class wrappers for implementation of functionality for the GeoJSON objects
defined in the Swordfish API.

Created on 07/07/2015 10:00:00
@author: dhohls
"""
# lib
import json
import datetime
import pytz
import uuid
# local
from .observation import OMObservation, Procedure, Measure


class OM_CategoryObservation(OMObservation):

    def __init__(self, **kwargs):
        super(OM_CategoryObservation, self ).__init__(**kwargs)
        if self.abort:  # OMObservation ended prematurely
            return

        self.observation["type"] = "OM_CategoryObservation"
        #TODO - create the result!

    def _set_phenomema(self):
        phenomena = {}
        phenomena['property'] = self.observation.get('observedProperty').get('type')
        phenomena['value'] = self.observation.get('result')
        phenomena['units'] = None
        #print "phenomena:", phenomena
        return phenomena
