# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Class wrappers for the GeoJSON objects.

Purpose: Define Python class-based wrappers for implementation of
functionality for the GeoJSON objects defined in the Swordfish API.

Created: 08/06/2015
Author: dhohls@csir.co.za

Updates:
    18/08/2017 : updated logging; refactored to use clean_ogc_string;
                 code tidy-up
"""
# lib
import datetime
import json
import logging
import pytz
import uuid
# project
from swordfish.core import AttrDict
# third-party

logger = logging.getLogger(__name__)


def utc_to_local(date_string, timezone=None):
    """Convert an UTC-based date string to a localised date string.

    Args:
        date_string: string
            a UTC date string in ISO format e.g. 2018-07-06T10:11:12Z
        timezone: string
            a pytz timezone name e.g. 'Africa/Johannesburg'
            (see https://www.iana.org/time-zones for list)

    Returns:
        ISO-formatted string with local time offset

    Tests:
    >>> assert utc_to_local(\
        date_string='2018-07-06T10:11:12Z', timezone='Africa/Johannesburg') ==\
        '2018-07-06T12:11:12+02:00'
    >>> assert utc_to_local(\
        date_string='2018-07-06T10:11:12Z', timezone=None) ==\
        '2018-07-06T10:11:12Z'
    >>> assert utc_to_local( \
        date_string='2018-07-06T10:11:12.987Z', timezone='Africa/Johannesburg') ==\
        '2018-07-06T12:11:12.987000+02:00'
    >>> assert utc_to_local( \
        date_string='2018-07-06T10:11:12.000Z', timezone='Africa/Johannesburg') == \
        '2018-07-06T12:11:12+02:00'
    """
    localised_date = date_string
    if timezone:
        try:
            try:
                python_date = datetime.datetime.strptime(
                    date_string, '%Y-%m-%dT%H:%M:%SZ')
            except ValueError:  # possibly microseconds in the string
                python_date = datetime.datetime.strptime(
                    date_string, '%Y-%m-%dT%H:%M:%S.%fZ')
            date_tz = pytz.UTC.localize(python_date)  # make timezone aware
            local_tz = pytz.timezone(timezone)  # make a timezone
            date_with_tz = date_tz.astimezone(local_tz)
            localised_date = date_with_tz.isoformat()
        except Exception as err:
            logger.error(err)
    else:
        logger.warn('No timezone supplied to function!')
    return localised_date


class Procedure(object):

    def __init__(self, **kwargs):
        """
        type: string
            as defined in self.PROCEDURE_TYPE
        id: any
            The unique id
        error: numeric
            Error bounds on this procedure
        description: string
            "predictive", "descriptive" or name of the sensor
        """
        self.PROCEDURE_TYPE = ["sensor", "model", "simulation", "vgi", "lab"]
        if not len(list(kwargs.keys())):
            self.procedure = {}
            return
        self.type = kwargs.get('type', None)
        self.id = kwargs.get('id', None)
        self.error = kwargs.get('error', None)
        self.description = kwargs.get('description', None)
        # validate (min requirements)
        if not self.type:
            raise ValueError('Parameter "type" must be set')
        if self.type not in self.PROCEDURE_TYPE:
            raise ValueError('Parameter "type" not valid')
        if not self.id:
            raise ValueError('Parameter "id" must be set')
        self.procedure = {
            'type': self.type,
            'id': self.id,
            'description': self.description,
            'error': self.error
        }


class Measure(object):

    def __init__(self, **kwargs):
        """
        value: numeric
            A measured, calculated or observed value for a property
        units: string
            Units applicable to the measured, calculated or observed value
        quality: numeric
            Probability the measurement is correct
        error: numeric
            Error in the value assumed to be +/-
        """
        if not len(list(kwargs.keys())):
            self.measure = {}
            return
        self.value = kwargs.get('value', None)
        self.units = kwargs.get('units', None)
        self.quality = kwargs.get('quality', None)
        self.error = kwargs.get('error', None)
        # validate (min requirements)
        if not self.value:
            raise ValueError('Parameter "value" must be set')
        self.measure = {
            'value': self.value,
            'units': self.units,
            'quality': self.quality,
            'error': self.error
        }


class OMObservation(AttrDict):
    """Base class for all observations."""

    def __init__(self, observation=None, **kwargs):
        # print "o:91 kwargs", kwargs
        # initialisation if existing OMObservation is passed via observation kw
        if observation is not None:
            self.observation = observation
            self.abort = True
            return
        # default initialisation
        self.TIME_ZONE = kwargs.get('time_zone', 'Africa/Johannesburg')
        self.observations = []
        self.phenomena = None
        self.output_file = kwargs.get('output', None)
        self.time_zone = kwargs.get('time_zone', self.TIME_ZONE)
        self.tz = pytz.timezone(self.time_zone)
        self.phenomenon_time = kwargs.get('phenomenon_time', None)
        # date conversion
        try:
            self.phenomenon_time = self.tz.localize(self.phenomenon_time).isoformat()
        except:
            pass
        self.units = kwargs.get('units', '')
        self.geometry_type = kwargs.get('geometry_type', 'Point')
        self.geometry_coordinates = kwargs.get('geometry_coords', [])  # [lon, lat]
        self.foi_type = kwargs.get('foi_typw', 'Feature')  # type of place/thing
        self.foi_id = kwargs.get('foi_id', '')  # place/thing being monitored
        self.observed_property = kwargs.get('observed_property', '')
        self.parameter_id = kwargs.get('parameter_id', str(uuid.uuid4()))
        self.procedure_id = kwargs.get('procedure_id', '')   # sensor - manufacturer ID
        self.procedure_name = kwargs.get('procedure_name', '')   # sensor - local name
        # TODO - validate that validTime is a [] of date1, date2 values
        self.valid_time = kwargs.get('valid_time', None)
        self.result_time = kwargs.get('result_time', None)
        self.result_quality = kwargs.get('result_quality', None)

        # no futher if no kwargs...
        # print "o:119"
        if not len(list(kwargs.keys())):
            self.observation = {}
            self.abort = True
            return
        self.abort = False

        # validate (min requirements)
        if not self.foi_id:
            raise ValueError('Parameter "foi_id" must be set')
        if not self.observed_property:
            raise ValueError('Parameter "observed_property" must be set')
        if not self.phenomenon_time:
            raise ValueError('Parameter "phenomenon_time" must be set')

        self.observation = {
            "type": "OM_Observation",
            "parameter": {
                "id": self.parameter_id
            },
            "phenomenonTime": self.phenomenon_time,
            "featureOfInterest": {
                "id": self.foi_id,
                "type": self.foi_type,
                "geometry": {
                    "type": self.geometry_type,
                    "coordinates": self.geometry_coordinates
                },
                "properties": {
                }
            },
            "observedProperty": {
                "type": self.observed_property
            },
            "validTime": self.valid_time,
            "resultQuality": self.result_quality,
            "resultTime": self.result_time,
            "procedure": {
                "type": "sensor",
                "id": self.procedure_id,
                "description": self.procedure_name
            }
        }

        """Example:
        {
            "type":"OM_Observation",
            "parameter":{"id":"ed221bf0-d075-012d-287e-34159e211071"},
            "phenomenonTime": "2010-11-12T13:45:00Z",
            "featureOfInterest":{
                "type":"Feature",
                "geometry":null,
                "properties":null
            },
            "observedProperty":{"type":"Observation"},
            "resultQuality":0.5,
            "result": "You should use a specialized subclass of OM_Observation"
        }
        """

    def _template(self, result=True):
        """Return a blank Observation AttrDict as a 'template'"""
        raise NotImplementedError("Define in inherited class!")

    def logit(self, message):
        """Output or print a message."""
        print(message)

    def count(self):
        """Number of items in the list of observations."""
        return len(self.observations)

    def add(self, observation=None):
        """Append a new observation to the list of observations."""
        _observation = observation or self.observation
        self.observations.append(_observation)

    def clean_ogc_string(self, string):
        """Remove and/or convert OGC data string for JSON-compatibility."""
        try:
            return string.replace("'", '"').replace('}u"', '} "').\
                replace(', u"', ', "').replace(': u"', ': "').\
                replace('{u"', '{"').replace('[u"', '["').strip('\n')
        except Exception as err:
            logger.error(err)
        return None

    def load_file(self, json_file=None):
        """Load single observation (JSON format) from file."""
        if json_file:
            fin = open(json_file, 'r')
            if fin:
                self.observation = json.load(fin)
                fin.close()

    def load_json(self, json_data=None):
        """Load single observation (JSON format) from file."""
        if json_data:
            self.observation = json.loads(json_data)

    def ogc_coordinate_string(self):
        """Return co-ordinates in OGC string format"""
        coordinates = None
        foi_shape = self.observation.get('featureOfInterest').get('geometry').get('type')
        foi_coords = self.observation.get('featureOfInterest').get('geometry').get('coordinates')
        if foi_shape == 'Point':
            coordinates = " ".join(map(str, foi_coords))
        elif foi_shape == 'Polygon':
            _st = " "
            for fco in foi_coords:
                _st += '%s, ' % " ".join(map(str, fco))
            coordinates = _st.strip(', ')
        else:
            raise TypeError('Geometry type %s is not supported' % foi_shape)
        return coordinates

    def save_to_file(self, observation=None, reset=False):
        """Save multiple observations (JSON format) to output file."""
        if self.observation:
            data = [observation, ]
        else:
            data = self.observations
        if self.output_file:
            mode = 'w' if reset else 'a'
            try:
                with open(self.output_file, mode) as outfile:
                    for datum in data:
                        json.dump(datum, outfile)
                        outfile.write('\n')
            except:
                self.logit('Unable to save data to: %s' % self.output_file)

    def load_from_file(self, filename=None, clean=False):
        """Load multiple observations (JSON format) from file."""
        self.observations = []
        _filename = filename or self.output_file
        odf = open(_filename, 'r')
        try:
            observation = None
            for line in odf.readlines():
                if line and line.strip('\n'):
                    try:
                        observation = json.loads(line)
                    except ValueError:
                        if clean:
                            _line = self.clean_ogc_string(line)
                            try:
                                observation = json.loads(_line)
                            except Exception as err:
                                if line:
                                    self.logit(
                                        'Unable to load cleaned data in:'
                                        ' %s: %s' % (line, err))
                                else:
                                    self.logit(err)
                    if observation:
                        self.observations.append(observation)
            odf.close()
        except:
            self.logit('Unable to load data from: %s' % _filename)

    def _set_phenomema(self):
        raise NotImplementedError("Define in inherited class!")

    def list_observation(self, observation=None):
        raise NotImplementedError("Define in inherited class!")

    def sos_to_observation(self, observations=None, is_json=False):
        """Return GeoJSON (Swordfish) messages from SOS JSON array

        Args:
            is_json: boolean
                if True, then return data in JSON format (i.e. a string;
                not a list)
                if False, then return data as a list of Swordfish AttrDicts
        """
        raise NotImplementedError("Define in inherited class!")

    def csv_observation(self, observation=None, titles=False):
        """Create string with comma separated values"""
        observation = observation or self.observation
        result = ''
        if not observation:
            return result
        _list = self.list_observation(observation)
        for _tuple in _list:
            result += ','.join(str(x) for x in _tuple)
        return result

    # DO NOT DELETE THIS METHOD - USED IN CPOWER/SWIM/NNR CODE !!!
    def dict_obs_time(self, observation=None, time_offset='+00:00'):
        """Create an AttrDict with a key 'data' that contains list of
        dictionaries; each dictionary containing parameters
        (observedProperty type), datetime, feature ID (possibly in properties),
        and result (value, units) from the original Observation(s).

        Useful for "downstream" services that require a minimal payload.

        Args:
            observation: AtrDict
            time_offset: string
                offset in HH:MM or HHMM format from UTC; default is UTC

        Example result:
        [{
            "observedProperty": {"type": "active_load"},
            "phenomenonTime": "2015-06-19T09:35:00+00:00",
            "result": {"units": "kW", "value": 127.2},
            "properties": {"id": "CSIR:CPT:C42"},
            "coordinates": ["28.277656", "-25.755048"]
        }]
        """
        def tz(time):
            if time:
                if '+' not in time and 'Z' not in time:
                    time = '%s%s' % (time, time_offset)
                    time = time.replace('Z+', '+')
            return time

        output = AttrDict()
        output.data = []
        logger.debug('observation:%s', observation)
        # TODO - validate that observation is SF CIM compliant ...
        try:
            obs_type = observation.get('type')
            foi = observation.get('featureOfInterest', {})
            fid = foi.get('id', None)
            if not fid:  # check for old-style ID
                fid = foi.get('properties', {}).get('id', '')
            observed_property = observation.get('observedProperty')
            coordinates = foi.get('geometry', {}).get('coordinates', [0, 0])
            result = observation.get('result')
            procedure = observation.get('procedure', {}).get('id', '')
            if obs_type == 'CF_SimpleObservation':
                data = result['data']
                variables = result.get('variables')
                var_keys = list(data.keys())
                if 'time' in var_keys or 'time' in var_keys:
                    times = data['time']
                for var in var_keys:
                    if str(var) != 'time':
                        dataset = data[var]
                        for key, datum in enumerate(dataset):
                            result = {
                                'units': variables.get(var, {}).get('units',
                                                                    ''),
                                'value': datum
                            }
                            _dict = AttrDict()
                            _dict.observedProperty = {'type': var}
                            _dict.phenomenonTime = tz(times[key])
                            _dict.result = result
                            _dict.properties = {}
                            _dict.properties.id = fid
                            _dict.properties.procedure = procedure
                            _dict.coordinates = coordinates
                            output.data.append(_dict)
            else:
                phen_time = observation.get('phenomenonTime')
                _dict = AttrDict()
                _dict.observedProperty = observed_property
                _dict.phenomenonTime = tz(phen_time)
                _dict.result = result
                _dict.properties = {}
                _dict.properties.id = fid
                _dict.properties.procedure = procedure
                _dict.coordinates = coordinates
                output.data.append(_dict)
        except TypeError:
            logger.exception('Unable to create obs/time list from:%s', observation)
        except AttributeError:
            logger.exception('Unable to create obs/time list from:%s', observation)
        return output


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    import doctest
    doctest.testmod()