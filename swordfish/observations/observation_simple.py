# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Class wrappers for implementation of functionality for the GeoJSON objects
defined in the Swordfish API.

Created: 07/07/2015
Updated: 05/03/2016
@author: dhohls
"""
# lib
from dateutil.tz import tzlocal
import datetime
import logging
# local
from swordfish.core import AttrDict
from .observation import OMObservation, utc_to_local

logger = logging.getLogger('swordfish.observations.CF')


class CFSimpleObservation(OMObservation):
    """CFSimpleObservation subclass of OMObservation"""

    def __init__(self, **kwargs):
        """Example:
        {
            "type":"CF_SimpleObservation",
            "parameter":{"id":"ed221bf0-d075-012d-287e-34159e211071"},
            "phenomenonTime": [
                "2010-11-12T13:40:00Z","2010-11-12T14:00:00Z"
            ],
            "procedure":{
                "id":"CP_123",
                "type":"Sensor"},
            "featureOfInterest":{
                 "id":"43_CSIR_PRETORIA"
            		"type":"Feature",
            		"geometry":{"type":"Point", "coordinates":[27.98226, -26.23482]},
            		"properties":{"name":"43:CSIR:PRETORIA"}
            	},
            "observedProperty":{"type":"TimeSeries"},
            "result":{
                	"dimensions":{
                		"time":3
            	},
            	"variables":{
            		"time":{"dimensions":["time"], "units":"isoTime"},
            		"active_load":{"dimensions":["time"], "units":"kW"}
            	},
            	"data":{
            		"time":["2010-11-12T13:40:00Z","2010-11-12T13:50:00Z",
            				"2010-11-12T14:00:00Z"],
            		"active_load":[123.7,125.1,126.0]
            	}
        }
        """
        super(CFSimpleObservation, self).__init__(**kwargs)
        if self.abort:  # OMObservation ended prematurely
            return

        self.value = kwargs.get('value', None)
        self.date_time = kwargs.get('date_time', None)

        # validate (min requirements)
        if not self.value:
            raise ValueError('Parameter "value" must be set')
        if not self.procedure_id:
            raise ValueError('Parameter "procedure_id" must be set')

        self.observation["type"] = "CF_SimpleObservation"

        result = {
            "dimensions": {
                "time": len(self.date_time)
            },
            "variables": {
	            "time": {
	                "dimensions":["time"],
	                "units":"isoTime"
	            },
	            self.observed_property: {
	                "dimensions": ["time"],
	                "units": self.units
	            }
            },
            "data":{
	            "time": [self.date_time,],
	            self.observed_property: [self.value,]
            }
        }
        self.observation["result"] = result

    def _template(self, result=True):
        """Return a blank SimpleObservation AttrDict as a 'template'"""
        om_template = {
            "type": "CF_SimpleObservation",
            "parameter": {
                "id": None
            },
            "resultTime": None,
            "phenomenonTime": [],  # start, end
            "procedure": {
                "type": "sensor",
                "id": None,
                "description": None
            },
            "featureOfInterest": {
                "type": "Feature",
                "id": None,
                "geometry": {
                    "type": "Point",
                    "coordinates": []
                },
                "properties": {
                    "name": None
                }
            },
            "observedProperty": {
                "type": "TimeSeries"
            }
        }
        if result:
            om_template["result"] = {
                	"dimensions": {
                		"time": 1
                	},
                	"variables": {
                		"time": {
                          "dimensions": ["time"],
                          "units": "isoTime"
                      }
                		#"load":{"dimensions":["time"], "units":"kW"}
                	},
                	"data": {
                 }
            }
        return AttrDict(om_template)

    def _set_phenomema(self):
        """Define phenomema used."""
        raise NotImplementedError("Not yet implemented!")

    def list_obs_time(self, observation=None):
        """Creates a list of (feature,procedure,property,datetime,value) tuples
        from single observation dictionary.

        Works for a single variable and a multiple 'time' variable
        """
        observation = observation or self.observation
        output = []
        if observation:
            foi = observation.get('featureOfInterest', {})
            properties = foi.get('properties', {})
            fid = properties.get('id') or properties.get('name') or  \
                properties.get('value')
            proc = observation.get('procedure', {})
            proc_id = proc.get('id') or proc.get('description') or 'Unknown'
            result = observation.get('result', {})
            data = result.get('data')
            variables = result.get('variables', {})
            _vars = list(variables.keys())
            param = None
            for _var in _vars:
                if _var == 'time':
                    _time = data.get(_var)
                else:
                    param = data.get(_var)
                    param_name = _var
            logger.debug('proc%s param:%s time:%s', proc_id, param_name, _time)
            if param:
                for i, datum in enumerate(param):
                    output.append((fid, proc_id, param_name, _time[i], datum))
        return output

    def sos_to_observation(self, observations=None, is_json=False,
                           timezone=None):
        """Return CFSimpleObservation from an array of JSON-formatted
        (dictionary) SOS observation results.

        Args:
            observations: array (of dicts)
                see below for example
            is_json: boolean
                if True, then return data in JSON format (i.e. a string;
                not a list)
                if False, then return data as a list of Swordfish AttrDicts
            timezone: string
                official timezone name (https://www.iana.org/time-zones)


        Example of `observations`:

        {
          "request" : "GetObservation",
          "version" : "2.0.0",
          "service" : "SOS",
          "observations" : [
            {...},
            {...}
          ]
        }

        Example of a `data_array`:

        [
            {
              "type" :
                 "http://www.opengis.net/def/observationType/OGC-OM/2.0/"
                 "OM_Measurement",
              "procedure" : "CSIR_PTA_R1",
              "offering" : "CSIR_PTA_R1",
              "observableProperty" :
              "http://meraka.csir.co.za/ogc/observableProperty/water_quantity",
              "featureOfInterest" : {
                "geometry" : {
                  "type" : "Point",
                  "coordinates" : [28.281425, -25.751745]
                }
              },
              "phenomenonTime" : "2015-08-18T05:44:45.000Z",
              "resultTime" : "2015-08-18T05:44:45.000Z",
              "result" : {
                "uom" : "liters",
                "value" : 3500.0
              }
            },
            ...
        ]
        """
        logger.debug('Using timezone:"%s"', timezone)

        def localise(timestring):
            result = timestring
            if timezone is not None:
                result = utc_to_local(timestring, timezone)
            return result

        results, data_array = [], []
        try:
            data_array = observations.get("observations", [])
            logger.debug("processing %s messages..." % len(data_array))
        except AttributeError:
            raise AttributeError(
                "Unable to extract observations from:"
                "%s\n%s" % (type(observations), observations))
        simple = self._template()  # AttrDict
        _now = datetime.datetime.now(tzlocal()).replace(
            microsecond=0).isoformat().replace('+00:00', 'Z')
        _last = len(data_array) - 1
        for key, swe in enumerate(data_array):
            if key == 0:
                # initialise meta-data
                # NOTE - we are assuming data from a SINGLE variable is returned!
                _property = swe["observableProperty"]
                if "OM_Measurement" in swe['type']:
                    units = swe["result"]["uom"]
                elif "OM_SWEArray" in swe["type"]:
                    units = ""  # TODO
                else:
                    raise NotImplementedError(
                        'Observation of type "%s" not catered for!' % swe['type'])

                simple["phenomenonTime"] = [localise(swe["phenomenonTime"])]  # first
                simple["resultTime"] = _now
                simple["featureOfInterest"] = swe["featureOfInterest"]
                simple["featureOfInterest"]["id"] = swe["procedure"]  # offering?
                simple["observedProperty"]["type"] = "TimeSeries" # Fixed! swe["observableProperty"]
                simple["procedure"]["id"] = swe["procedure"]
                simple["procedure"]["description"] = ''  # ???
                simple["result"] = {
                    "dimensions": {
                        "time": 3
                    },
                		"variables": {
                			"time": {
                				"dimensions": ["time"],
                				"units": "isoTime"
                			},
                			_property: {
                				"dimensions": ["time"],
                				"units": units
                			}
                		},
                    "data": {
                        "time": [],
                        _property : []}
                }
            if key == _last:
                simple["phenomenonTime"].append(localise(swe["phenomenonTime"]))
            # check not another variable!
            if _property and _property != swe["observableProperty"]:
                logger.error("Multiple properties encountered in CFSimpleMeasurement")
                return []
            # data; append to time and value arrays
            if "OM_Measurement" in swe['type']:
                simple["result"]["data"]["time"].append(localise(swe["phenomenonTime"]))
                simple["result"]["data"][_property].append(swe["result"]["value"])
            elif "OM_SWEArray" in swe["type"]:
                _values = swe["result"]["values"]
                _fields = swe["result"]["fields"]
                time_key, param_key = -1, -1
                for key, field in enumerate(_fields):
                    if field["type"] == 'time':
                        time_key = key
                    else:
                        param_key = key
                if param_key:
                    simple["result"]["data"]["time"].append(time_key)
                    simple["result"]["data"][_property].append(_values[0][param_key])
            else:
                raise NotImplementedError(
                    'Observation of type "%s" not catered for!' % swe['type'])
        if is_json:
            return simple.__json__
        else:
            return simple


class CFSimpleMeasurement(CFSimpleObservation):
    """CFSimpleMeasurementsubclass of CFSimpleObservation"""

    def __init__(self, **kwargs):
        super(CFSimpleMeasurement, self).__init__()

    def set_phenomema(self):
        """Define phenomema used."""
        raise NotImplementedError("Not yet implemented!")
