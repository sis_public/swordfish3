# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Class wrappers for implementation of functionality for the GeoJSON
OM_ComplexObservation models defined in the Swordfish API.

Created: 2015/07/07
Updated: 2018/11/26
Author: dhohls@csir.co.za
"""
# lib
import json
import datetime
import pytz
import uuid
# local
from swordfish.core import AttrDict
from .observation import OMObservation


class OM_ComplexObservation(OMObservation):

    def __init__(self, **kwargs):
        super(OM_ComplexObservation, self ).__init__(**kwargs)
        if self.abort:  # OMObservation ended prematurely
            return

        self.observation["type"] = "OM_ComplexObservation"
        #TODO - create the result!

        """Example:
        {
            "type":"OM_ComplexObservation",
            "parameter":{"id":"ed221bf0-d075-012d-287e-34159e211071"},
            "phenomenonTime": "2010-11-12T13:45:00Z",
            "procedure":{"id":"MSG", "type":"sensor"},
            "featureOfInterest":{
            		"type":"Feature",
            		"geometry":{
                     "type": "Point",
                     "coordinates": [27.98226, -26.23482]
                 },
            		"properties":null
            	},
            "observedProperty":{"type":"activeFire"},
            "resultQuality":0.5,
            "result":{
                "probability":{"value":0.9},
                "brightness_temperature":{"value":357, "units":"kelvin"},
                "frp":{"value":200, "units":"mW"}
            	}
        }
        """

    def _template(self, result=True):
        """Return a blank ComplexObservation AttrDict as a 'template'"""
        om_template = {
            "type": "OM_ComplexObservation",
            "parameter": {
                "id": None
            },
            "resultTime": None,
            "phenomenonTime": [],  # start, end
            "procedure": {
                "type": "sensor",
                "id": None,
                "description": None
            },
            "featureOfInterest": {
                "type": "Feature",
                "id": None,
                "geometry": {
                    "type": "Point",
                    "coordinates": []
                },
                "properties": {}
            },
            "observedProperty": {
                "type": None
            },
            "resultQuality": None
        }
        if result:
            om_template["result"] = {}
        return AttrDict(om_template)

    def _set_phenomema(self):
        phenomenas = self.observation.get('result')
        return phenomenas

    def list_observation(self, observation=None):
        """Create a list of (id,parameter,datetime,value) tuples
        from a single, complex observation dict

        Works for multiple variables and a single 'time' variable
        """
        observation = observation or self.observation
        output = []
        if observation and observation.get('type') == 'OM_ComplexObservation':
            foi = observation.get('featureOfInterest', {})
            fid = foi.get('id', 'Unknown')
            #observed_property = observation.get('observedProperty', {})
            _time = observation.get('phenomenonTime')
            params = observation.get('result', {})
            #print 'omc:71 DEBUG', _time, params
            for param_name in list(params.keys()):
                result = params.get(param_name)
                if result:
                    datum = result.get('value')
                    output.append((fid, param_name, _time, datum))
        return output
