# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Thu Apr 19 15:42:43 2018

@author: gmcferren
"""
#for smtp alerting
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from swordfish.sink import SinkProcessor

class SimpleEmailSink(SinkProcessor):
    """
    A simple SMTP based e-mail sink. Purpose is to notify e-mail receipitents of
    a data item off a stream, e.g. a threshold being breached.

    This is non-templated, very basic plain text e-mail functionality
    """
    def __init__(self, *args,**kwargs):
        """
        configured with kwargs for:
        receipients: a list of 1 or more receipients
        sender: a sender e-mail address
        smtp_url: a SMTP server url
        smtp_port: a SMTP server port
        username: a sender username if necessary
        pwd: a sender pwd if necessary
        alertstatement: a string describing what alert is for e.g.
         "exceeded threshold"; used in creating e-mail subject & text
        """
        SinkProcessor.__init__(self, *args, **kwargs)
        self.receivers = kwargs.get('receipients',[])
        self.sender = kwargs.get('sender',"")
        self.sender_un = kwargs.get('username',"")
        self.sender_pwd = kwargs.get('pwd',"")
        self.smtp_url = kwargs.get('smtp_url',"")
        self.smtp_port = kwargs.get('smtp_port',"")
        self.alert_statement = kwargs.get('alertstatement',"threshold exceeded")

    def _formatter(self,msg):
        """
        Takes message off stream and runs it through basic MIME prep
        Params:
        -------
        msg: a Swordfish Common Info Model AttrDict (non-ComplexObservation)

        Basic behaviour is to extract FOI_ID,resulttime, result and Phenom parts of the
        AttrDict and use them to make a statement along lines of:
        'Rad Value 0.5uSv at A3047 exceeded threshold at 2017-04-19T14:25:00'

        obm0 = OM_Measurement()._template()
        obm0.phenomenonTime = ret_msg.phenomenonTime
        obm0.procedure.id = ret_msg.procedure.id
        obm0.featureOfInterest.id = ret_msg.featureOfInterest.id
        obm0.featureOfInterest.geometry.coordinates = \
            ret_msg.featureOfInterest.geometry.coordinates
        obm0.observedProperty.type = "radiation_alarm_status"
        obm0.result.type = 5
        obm0.result.value = ras
        """
        COMMASPACE = ", "
        f_opt =  msg.observedProperty.type.capitalize()
        f_pt = msg.phenomenonTime
        f_rv = msg.result.value
        f_as = self.alert_statement.capitalize()
        f_foid = msg.featureOfInterest.id
        subj = ""
        try:
            html_template ="<html><body>" \
            "<h2>Alert Statement: %s</h2>" \
            "<table>" \
            "<tr><td>Indicator</td><td>%s</td></tr>" \
            "<tr><td>Value</td><td>%s</td></tr>" \
            "<tr><td>Place</td><td>%s</td></tr>" \
            "<tr><td>Date and Time</td><td>%s</td></tr>" \
            "</table>" \
            "</body></html>"
            subj = "%s: %s %s at %s at %s" % (f_as,f_opt,f_rv,f_foid,f_pt)
            text = "Alert Statement: %s \n"\
            "Indicator: %s \n"\
            "Value: %s \n"\
            "Place: %s \n"\
            "DateTime: %s" % (f_as,f_opt,f_rv,f_foid,f_pt)
            html = html_template % (f_as,f_opt,f_rv,f_foid,f_pt)

            emailmsg = MIMEMultipart('alternative')
            emailmsg['Subject'] = subj
            emailmsg['From'] = self.sender
            emailmsg['To'] = COMMASPACE.join(self.receivers)

            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html')

            # Attach parts into message container.
            # According to RFC 2046, the last part of a multipart message, in this case
            # the HTML message, is best and preferred.
            emailmsg.attach(part1)
            emailmsg.attach(part2)

            return emailmsg.as_string()
        except Exception as e:
            if subj != "":
                self.error("Failed simple email formatter({}, {}, {}): {}".format(
                self.sender, self.receivers, subj, e))
            else:
                self.error("Failed simple email formatter({}, {}): {}".format(
                self.sender, self.receivers, e))
            return None

    def _sender(self,payload):
        """SMTP comms"""
        try:
            #s = smtplib.SMTP('localhost')
            s = smtplib.SMTP(self.smtp_url)
            if self.sender_un != "" and self.sender_pwd != "":
                try:
                    s.login(self.sender_un, self.sender_pwd)
                except smtplib.SMTPHeloError as shee:
                    self.error("Failed sendmail HELO Error({}, {}, {}): {}".format(
                        self.sender, self.receivers, payload, shee))
                except smtplib.SMTPAuthenticationError as saee:
                    self.error("Failed sendmail AUTH Error({}, {}, {}): {}".format(
                        self.sender, self.receivers, payload, saee))
                except smtplib.SMTPException as see:
                    self.error("Failed sendmail LOGIN ERROR({}, {}, {}): {}".format(
                        self.sender, self.receivers, payload, see))

            # sendmail function takes 3 arguments: sender's address, recipient's address
            # and message to send - here it is sent as one string.
            s.sendmail(self.sender, self.receivers, payload)
            s.quit()
            return True
        except Exception as e:
            self.error("Failed sendmail({}, {}, {}): {}".format(
                self.sender, self.receivers, payload, e))
            return False

    def process(self, message, stream_id=None):
        payload = self._formatter(message)
        if payload is not None:
            #untile we get creds:
            #self.error("Mockup of email send: %s" % (payload))
            sent = self._sender(payload)


