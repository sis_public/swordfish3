# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
from .core import StreamProcessor, AttrDict
from threading import Thread


class SourceProcessor(StreamProcessor):
    def __init__(self, *args, **kwargs ):
        StreamProcessor.__init__(self, *args, **kwargs)
        self._source = True

    def __le__(self, item):
        return StreamProcessor.__rshift__(self, item)

    def __rshift__(self, item):
        return StreamProcessor.__rshift__(self, item)

    def __rrshift__(self, item):
        raise TypeError("%s can't  >> to SourceProcessor %s"%(item, self))


class Tap(SourceProcessor):
    def process(self, message, stream_id):
        return message


class SourceStreamProcessor(SourceProcessor):
    def __init__(self, *args, **kwargs ):
        autostart = kwargs.get('autostart', True)
        SourceProcessor.__init__(self, *args, **kwargs)
        self._sourceprocessor_thread = Thread(target=self.__sourceprocessor_thread)
        self._sourceprocessor_thread.daemon = True
        if autostart:
            self._sourceprocessor_thread.start()

    def start(self):
        self._sourceprocessor_thread.start()
        #self._sourceprocessor_thread.join()

    #This is executed by _sourceprocessor_thread thread each time before a message is fetched
    def __sourceprocessor_thread(self):
        while True:
            self._sourceprocessor()
            if self.stopEvent.isSet():
                self.info( "_sourceprocessor.stop.isSet True" )
                break

    #This will be executed for you in an infinite loop
    #You do not need to concern yourself with termination
    #Only ensure that you allow this to execute PERIODICALLY
    #use this to add messages to the message queue using _put()
    def _sourceprocessor(self):
        raise NotImplementedError("Please Implement this method to add new messages")
        #self.put(AttrDict({}))
        #eg. sleep(1) or someEvent.wait()

    def put(self, message):
        self.debug( message )
        self._enqueue_message(message)

    def _put(self, message):
        self.debug( message )
        self._enqueue_message(message)

    def process(self, message, stream_id):
        #return message
        raise NotImplementedError("Please Implement this method to add new messages")


class SourceProcessorMixin():
    def __init__(self, *args, **kwargs ):
        autostart = kwargs.get('autostart', False)
        if autostart:
            self.start()

    def start(self):
        self._sourceprocessor_thread = Thread(target=self.__sourceprocessor)
        self._sourceprocessor_thread.daemon = True
        self._sourceprocessor_thread.start()

    #This is executed by execute thread each time before a message is fetched
    def __sourceprocessor(self):
        while True:
            message = self._sourceprocessor()
            self._enqueue_message(message)
            if self._stop.isSet():
                self.info( "_sourceprocessor.stop.isSet True" )
                break

    #This will be executed for you in an infinite loop
    #You do not need to concern yourself with termination
    #Only ensure that you allow this to execute PERIODICALLY
    def _sourceprocessor(self):
        #should return next message
        raise NotImplementedError("Please Implement this method to add new messages")

    def process(self, message, stream_id):
        raise NotImplementedError("Please Implement this method to add new messages")


class IterableSource(SourceProcessor):
    def __init__( self, iterable=[], *args, **kwargs ):
        SourceProcessor.__init__(self, *args, **kwargs )
        self.setStream(iterable)

    def setStream(self, iterable):
        self.iterable = iterable
        for i in self.iterable:
            self._enqueue_message(AttrDict(i))

    def reStream(self):
        self.setStream(self.iterable)

    def process(self, message, stream_id):
        return message