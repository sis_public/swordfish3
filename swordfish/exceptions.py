# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Mon Sep 14 14:22:33 2015

@author: tvzyl
"""

class SwordfishSyntaxError(Exception):
    pass

class SwordfishMessageError(Exception):
    pass

class SwordfishStreamError(Exception):
    pass

class SwordfishStreamProcessorError(Exception):
    pass

class StreamError(Exception):
    def __init__(self, value):
        self.value= value
    def __str__(self):
        return repr(self.value)