# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Thu Dec  3 15:48:42 2015

@author: tvzyl
"""
from .core import StreamProcessor, AttrDict
#from swordfish.streamProcessor import map
import ujson as json
import websocket
import numpy as np
import smtplib
from email.mime.text import MIMEText
try:
    from pymongo import MongoClient
except:
    print("MongoClient Unavailable")

class MongoDB(StreamProcessor):
    def __init__(self, clienturi="mongodb://localhost:27017/", database="swordfish", collection="messages", *args, **kwargs):
        StreamProcessor.__init__(self, *args, **kwargs)
        self.clienturi = clienturi
        self.database = database
        self.collection = collection
        self.client = MongoClient(self.clienturi)
        self.db = self.client[self.database]
        self.cl = self.db[self.collection]

    def process(self, message, stream_id):
        return message