# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created: April 29 2016 at 14:30
@author: gmcferren

Updated: September 11 2020
@author: Derek Hohls - dhohls@csir.co.za

Test area for experimenting with new transports

Setting up Rethink
------------------

The command-line Docker startup below will create a running instance called
"rethink_test".

"-v" sets the database directory; in this case a subdirectory of the location
where the Docker is being run.

The version number was the most current as of Sept 2020.

Startup::

    docker run --name rethink_test -v "$PWD:/rethink" -d rethinkdb:2.4.1

You can access the admin console by starting a browser::

    firefox "http://$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' rethink_test):8080"

for more see: https://rethinkdb.com/docs/cli-options/

Testing with rethink
--------------------

NOTE: The server's IP address needs to be changed to match your own.

With this script running in one terminal, open another terminal and start a
Python session. In the session do::

    from rethinkdb import RethinkDB
    r = RethinkDB()
    r.connect("172.17.0.2", 28015).repl()
    r.db("test").table_create("messages").run()
    r.table("messages").insert([
        {"name":"message1"},
        {"name":"message2"},
        {"name":"message3"}
    ]).run()

You should see the messages being echoed, with the RethinkDB IDs, in the first
terminal.

"""
from swordfish.rethinkdb_messaging import RethinkDBStream
from swordfish.sink import Print
from swordfish import keep_alive


rdb_url = 'rethinkdb://admin:@172.17.0.2:28015/test'
with RethinkDBStream(rdb_url, 'messages') as rdbs, Print() as sink:
    rdbs>>sink
    keep_alive()
