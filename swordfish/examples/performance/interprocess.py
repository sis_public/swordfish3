# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Dependencies:
    pip install tabulate simplejson python-cjson ujson yajl msgpack-python
"""

from timeit import timeit
from tabulate import tabulate

import simplejson
import ujson
# cjson does not work under Python3
#import cjson
import msgpack
import rpyc
from swordfish.core import AttrDict

loops = 15000

setup = '''
from multiprocessing import Process
from toolz.functoolz import identity
from msgpack import dumps, loads
from blosc import compress, decompress
from swordfish.core import AttrDict

loops = 15000

d = {"type": "CF_SimpleObservation","parameter":{"id": "fb600634-5ad7-11e5-945f-65303a4d6e97"},"phenomenonTime": "2015-09-14T11:58:58.649575", "procedure": {"type":"sensor","id": "45030171","description": "Sensus HRI-Mei linked to meter 14787486"},"featureOfInterest": {"type": "Feature","geometry": {"type":"Point", "coordinates":[-25.753, 28.280]},"properties":{"id":"Top Reservoir"} },"observedProperty": {"type": "TimeSeries"},"result": {"dimensions":{"time":5},"variables":{"time":{"dimensions":["time"],"units":"isoTime"},"pulse_value":{"dimensions":["int"],"units":"litres"}},"data":{"time":["2015-09-14T11:58:01.305564", "2015-09-14T11:47:06.808586", "2015-09-14T11:54:55.782008", "2015-09-14T11:43:58.603956", "2015-09-14T11:50:58.623827"], "pulse_value":[500, 500, 500, 0, 500]}}}
e = AttrDict(d)

#dd = dumps(d)
#dl = len(dd)
'''

setup_queue='''
%s
from multiprocessing import Queue
qt = Queue(10)
qu = Queue(10)

def t(qt):
    for i in range(loops):
        r = qt.get()

def u(qu):
    for i in range(loops):
        qu.put(d)

o = Process(target=t, args=(qt,))
p = Process(target=u, args=(qu,))
o.daemon = True
p.daemon = True
o.start()
p.start()
'''% setup


setup_queue_bytes='''
%s
from multiprocessing import Queue
qt = Queue(10)
qu = Queue(10)

def t(qt):
    for i in range(loops):
        r = loads(qt.get())

def u(qu):
    for i in range(loops):
        qu.put(dumps(d))

o = Process(target=t, args=(qt,))
p = Process(target=u, args=(qu,))
o.daemon = True
p.daemon = True
o.start()
p.start()
'''% setup

setup_simplequeue='''
%s
from multiprocessing.queues import SimpleQueue
qt = SimpleQueue()
qu = SimpleQueue()

def t(qt):
    for i in range(loops):
        r = qt.get()

def u(qu):
    for i in range(loops):
        qu.put(d)

o = Process(target=t, args=(qt,))
p = Process(target=u, args=(qu,))
o.daemon = True
p.daemon = True
o.start()
p.start()
'''% setup

setup_simplequeue_bytes='''
%s
from multiprocessing.queues import SimpleQueue
qt = SimpleQueue()
qu = SimpleQueue()

def t(qt):
    for i in range(loops):
        r = loads(qt.get())

def u(qu):
    for i in range(loops):
        qu.put(dumps(d))

o = Process(target=t, args=(qt,))
p = Process(target=u, args=(qu,))
o.daemon = True
p.daemon = True
o.start()
p.start()
'''% setup

setup_simplequeue_bytes_blosc='''
%s
from multiprocessing.queues import SimpleQueue
qt = SimpleQueue()
qu = SimpleQueue()

def t(qt):
    for i in range(loops):
        r = loads(decompress(qt.get()))

def u(qu):
    for i in range(loops):
        qu.put(compress(dumps(d), 1))

o = Process(target=t, args=(qt,))
p = Process(target=u, args=(qu,))
o.daemon = True
p.daemon = True
o.start()
p.start()
'''% setup

setup_pipe='''
%s
from multiprocessing import Pipe
pti, pto = Pipe()
pui, puo = Pipe()

def t(pti):
    for i in range(loops):
        r = pti.recv()

def u(pui):
    for i in range(loops):
        pui.send(d)

o = Process(target=t, args=(pti,))
p = Process(target=u, args=(pui,))
o.daemon = True
p.daemon = True
o.start()
p.start()
'''% setup

setup_pipe_bytes='''
%s
from multiprocessing import Pipe
pti, pto = Pipe()
pui, puo = Pipe()

def t(pti):
    for i in range(loops):
        r = loads(pti.recv_bytes())

def u(pui):
    for i in range(loops):
        pui.send_bytes(dumps(d))

o = Process(target=t, args=(pti,))
p = Process(target=u, args=(pui,))
o.daemon = True
p.daemon = True
o.start()
p.start()
'''% setup

setup_pipe_bytes_raw='''
%s
from multiprocessing import Pipe
pti, pto = Pipe()
pui, puo = Pipe()

def t(pti):
    for i in range(loops):
        r = pti.recv_bytes()

def u(pui):
    for i in range(loops):
        pui.send_bytes(d)

o = Process(target=t, args=(pti,))
p = Process(target=u, args=(pui,))
o.daemon = True
p.daemon = True
o.start()
p.start()
'''% setup

setup_shared='''
%s
from multiprocessing import RawArray, RawValue, Semaphore, Event
at = RawArray('c',1024*1024)
vt = RawValue('i')
au = RawArray('c',1024*1024)
vu = RawValue('i')
st = Event()
et = Event()
su = Event()
eu = Event()

def t(at, vt):
    for i in range(loops):
        et.wait()
        s = at[0:vt.value]
        et.clear()
        st.set()

def u(au, vu):
    for i in range(loops):
       au[0:dl]=dd
       vu=dl
       eu.set()
       su.wait()
       su.clear()

o = Process(target=t, args=(at,vt))
p = Process(target=u, args=(au,vu))
o.daemon = True
p.daemon = True
o.start()
p.start()

'''% setup

setup_pool = '%sfrom multiprocessing import Pool; pool = Pool(processes=1)' % setup

setup_zmq ='''
%s
import zmq

context = zmq.Context()

sender = context.socket(zmq.PAIR)
sender.connect('ipc://123')

def t():
    context = zmq.Context()
    receiver = context.socket(zmq.PAIR)
    receiver.bind('ipc://123')
    receiver.recv()
    for i in range(loops):
        r = loads(receiver.recv())

receiver = context.socket(zmq.PAIR)
receiver.bind('ipc://345')
def u():
    context = zmq.Context()
    sender = context.socket(zmq.PAIR)
    sender.connect('ipc://345')
    sender.recv()
    for i in range(loops):
        sender.send(dumps(d))

o = Process(target=t)
p = Process(target=u)
o.daemon = True
p.daemon = True
o.start()
p.start()

receiver.send(b"")
sender.send(b"")

'''% setup



tests = [
    # (title, setup, enc_test, dec_test)
    #('pool apply()', '%s' % setup_pool, 'pool.apply(identity, args=(d,))', 'pool.apply(identity, args=(d,))'),
#    ('Queue', '%s' % setup_queue, 'qt.put(d)', 'qu.get()'),
    ('Queue Bytes', '%s' % setup_queue_bytes, 'qt.put(dumps(d))', 'loads(qu.get())'),
#    ('Simple Queue', '%s' % setup_simplequeue, 'qt.put(d)', 'qu.get()'),
    ('Simple Queue Bytes', '%s' % setup_simplequeue_bytes, 'qt.put(dumps(d))', 'loads(qu.get())'),
#    ('Pipe', '%s' % setup_pipe, 'pto.send(d)', 'puo.recv()'),
    ('Pipe Bytes', '%s' % setup_pipe_bytes, 'pto.send_bytes(dumps(d))', 'loads(puo.recv_bytes())'),
    ('ZMQ Bytes', '%s' % setup_zmq, 'sender.send(dumps(d))', 'loads(receiver.recv())'),
    #('Pipe Bytes Raw', '%s' % setup_pipe_bytes_raw, 'pto.send_bytes(dd)', 'puo.recv_bytes()'),
    #('Shared Memory', '%s' % setup_shared, 'at[0:dl]=dd; vt=dl; et.set(); st.wait(); st.clear()', 'eu.wait(); s = at[0:vt.value]; eu.clear(); su.set()'),

]

enc_table = []
dec_table = []

print("Running tests (%d loops each)" % loops)

for title, mod, enc, dec in tests:
    print(title)
    exec(mod)

    print("  [Send]", enc)
    result = timeit(enc, mod, number=loops)
    enc_table.append([title, result, loops/result])

    print("  [Recv]", dec)
    result = timeit(dec, mod, number=loops)
    dec_table.append([title, result, loops/result])

    as_str = '%s'%dec
    exec("print(d); print('=='); print(as_str)")
    exec("assert(str(d)==str(%s))"%dec)

enc_table.sort(key=lambda x: x[1])
enc_table.insert(0, ['Package', 'Seconds', 'msg/sec'])

dec_table.sort(key=lambda x: x[1])
dec_table.insert(0, ['Package', 'Seconds', 'msg/sec'])

print("\nProcess Send Test (%d loops)" % loops)
print(tabulate(enc_table, headers="firstrow"))

print("\nProcess Recv Test (%d loops)" % loops)
print(tabulate(dec_table, headers="firstrow"))