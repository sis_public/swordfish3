# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""Auto-generated unit tests."""
import unittest
import performance


class Test_performance(unittest.TestCase):

    def testd_Basics(self):
        p_b = performance.d()
        p_b.words = '1'
        self.assertEqual('1', p_b.words)
        p_b.list = (2,)
        self.assertEqual((2,), p_b.list)
        p_b.int = 4
        self.assertEqual(4, p_b.int)
        p_b.float = 5.0
        self.assertEqual(5.0, p_b.float)
        p_b2 = performance.d()
        p_b2.ParseFromString(p_b.SerializeToString())
        self.assertEqual(p_b.words, p_b2.words)
        self.assertEqual(p_b.list, p_b2.list)
        self.assertEqual(p_b.dict, p_b2.dict)
        self.assertEqual(p_b.int, p_b2.int)
        self.assertEqual(p_b.float, p_b2.float)

def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(Test_performance))
    return suite
