# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Tue Sep  1 13:50:05 2015
@author: Terence van Zyl - tvzyl@csir.co.za

Updated: Sep 15 2020
@author: Derek Hohls - dhohls@csir.co.za
"""

from datetime import datetime
from ciso8601 import parse_datetime
from msgpack import loads, dumps
import cython
from copy import deepcopy


class SwordfishMessageError(Exception):
    pass


cdef inline __asattrtype(bint __frozen__, item):
    cdef type itemtype
    cdef result
    itemtype = type(item)
    if itemtype == dict:
        if "$date" in <dict>item:
            return parse_datetime((<dict>item)['$date'])
        else:
            return AttrDict.__new__(AttrDict,item, __frozen__)
    elif itemtype == list:
        return AttrList3.__new__(AttrList3,item, __frozen__)
    else:
        return item


cdef inline __asorgtype(item):
    cdef type itemtype
    itemtype = type(item)
    if itemtype == AttrDict:
        return (<AttrDict>item).data
    elif itemtype == AttrList3:
        return (<AttrList3>item).data
    elif itemtype == dict:
        for itm in item:
            itmtype = type(item[itm])
            if itmtype == AttrDict or itmtype == AttrList3:
                item[itm] = __asorgtype(item[itm])
        return item
    elif itemtype == list:
        for i in xrange(len(item)):
            itmtype = type(item[i])
            if itmtype == AttrDict or itmtype == AttrList3:
                item[i] = __asorgtype(item[i])
        return item
    elif itemtype == datetime:
        return {'$date':item.isoformat()}
    else:
        return item

cdef class AttrList3
cdef class AttrDict3

cdef class AttrList3:
    cdef list data
    cdef bint __frozen__
    def __cinit__(self, list initlist=[], bint frozen=False):
        self.__frozen__ = False
        if initlist is not None:
            self.data = initlist
        else:
            self.data = []
        if frozen:
            self.freeze()
    cdef __asattrtype(AttrList3 self, item):
        itemtype = type(item)
        if itemtype == dict:
            return AttrDict3(item, self.__frozen__)
        elif itemtype == list:
            return AttrList3(item, self.__frozen__)
        else:
            return item
    cdef __asorgtype(AttrList3 self, item):
        itemtype = type(item)
        if itemtype == AttrDict3:
            return (<AttrDict3>item).data
        elif itemtype == AttrList3:
            return (<AttrList3>item).data
        else:
            return item
    def __repr__(self): return repr(self.data)
    def __richcmp__(self, other, int op):
        if isinstance(other, AttrList3):
            other = (<AttrList3>other).data
        if op==0: #<
            return self.data <  other
        elif op==1: #<=
            return self.data <= other
        elif op==2: #==
            return self.data == other
        elif op==3: #!=
            return self.data != other
        elif op==4: #>
            return self.data >  other
        elif op==5: #>=
            return self.data >= other
    # __cmp__ is removed in Python3 -
    # https://docs.python.org/3.0/whatsnew/3.0.html#ordering-comparisons
    #def __cmp__(self, other):
    #    if isinstance(other, AttrList3):
    #        other = (<AttrList3>other).data
    #   return cmp(self.data, other)
    __hash__ = None # Mutable sequence, so not hashable
    def __contains__(self, item): return item in self.data
    def __len__(self): return len(self.data)
    '''
    def __getitem__(self, i): return self.__asattrtype(self.data[i])
    def __setitem__(self, i, item): self.data[i] = self.__asorgtype(item)
    def __delitem__(self, i): del self.data[i]
    def __getslice__(self, i, j):
        i = max(i, 0); j = max(j, 0)
        return AttrList3(self.data[i:j])
    def __setslice__(self, i, j, other):
        i = max(i, 0); j = max(j, 0)
        if isinstance(other, AttrList3):
            self.data[i:j] = (<AttrList3>other).data
        elif isinstance(other, type(self.data)):
            self.data[i:j] = other
        else:
            self.data[i:j] = list(other)
    def __delslice__(self, i, j):
        i = max(i, 0); j = max(j, 0)
        del self.data[i:j]
    '''

    def __getitem__(self, item):
        if isinstance(item, slice):
            i = max(item.start, 0)
            j = max(item.stop, 0)
            return AttrList3.__new__(AttrList3,self.data[i:j])
        else:
            return __asattrtype(self.__frozen__, self.data[item])

    def __setitem__(self, item, value):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        if isinstance(item, slice):
            i = max(item.start, 0)
            j = max(item.stop, 0)
            if isinstance(value, AttrList3):
                self.data[i:j] = (<AttrList3>value).data
            elif isinstance(value, type(self.data)):
                self.data[i:j] = value
            else:
                self.data[i:j] = list(value)
        else:
            self.data[item] = __asorgtype(value)

    def __delitem__(self, item):
        if self.__frozen__:
            raise SwordfishMessageError("Message frozen, use deepcopy.")
        if isinstance(item, slice):
            i = max(item.start, 0)
            j = max(item.stop, 0)
            del self.data[i:j]
        else:
            del self.data[item]

    def __add__(self, other):
        if isinstance(other, AttrList3):
            return AttrList3(self.data + (<AttrList3>other).data)
        elif isinstance(other, type(self.data)):
            return AttrList3(self.data + other)
        else:
            return AttrList3(self.data + list(other))
    def __radd__(self, other):
        if isinstance(other, AttrList3):
            return AttrList3((<AttrList3>other).data + self.data)
        elif isinstance(other, type(self.data)):
            return AttrList3(other + self.data)
        else:
            return AttrList3(list(other) + self.data)
    def __iadd__(self, other):
        if isinstance(other, AttrList3):
            self.data += (<AttrList3>other).data
        elif isinstance(other, type(self.data)):
            self.data += other
        else:
            self.data += list(other)
        return self
    def __mul__(self, n):
        return AttrList3(self.data*n)
    __rmul__ = __mul__
    def __imul__(self, n):
        self.data *= n
        return self
    def append(self, item): self.data.append(self.__asorgtype(item))
    def insert(self, i, item): self.data.insert(i, self.__asorgtype(item))
    def pop(self, i=-1): return self.__asattrtype(self.data.pop(i))
    def remove(self, item): self.data.remove(self.__asorgtype(item))
    def count(self, item): return self.data.count(self.__asorgtype(item))
    def index(self, item, *args): return self.data.index(self.__asorgtype(item), *args)
    def reverse(self): self.data.reverse()
    def sort(self, *args, **kwds): self.data.sort(*args, **kwds)
    def extend(self, other):
        self.data.extend(self.__asorgtype(other))
    cpdef AttrList3 __deepcopy__(AttrList3 self, dict m):
        return AttrList3( loads(dumps(self.data)) )
    cpdef str __getstate__(AttrList3 self):
        return dumps(self.data)
    cpdef __setstate__(AttrList3 self, str state):
        self.data = loads(state)
        self.__frozen__ = False
        self.__msgpack__ = ''
    cpdef freeze(AttrList3 self):
        self.__frozen__ = True

cdef class AttrDict3:
    cdef dict data
    cdef bint __frozen__
    cdef str __msgpack__
    property __data__:
        def __get__(self):
            return self.data
    def __cinit__(self, dict initdict={}, bint frozen=False, **kwargs):
        self.__msgpack__ = ''
        self.__frozen__ = False
        if initdict is not None:
            self.data = initdict
        else:
            self.data = {}
        if len(kwargs):
            self.update(kwargs)
        if frozen:
            self.freeze()
    cdef __asattrtype(AttrDict3 self, item):
        itemtype = type(item)
        if itemtype == dict:
            return AttrDict3(item, self.__frozen__)
        elif itemtype == list:
            return AttrList3(item, self.__frozen__)
        else:
            return item
    cdef __asorgtype(AttrDict3 self, item):
        itemtype = type(item)
        if itemtype == AttrDict3:
            return (<AttrDict3>item).data
        elif itemtype == AttrList3:
            return (<AttrList3>item).data
        else:
            return item
    def __repr__(self): return repr(self.data)
    # __cmp__ is removed in Python3 -
    # https://docs.python.org/3.0/whatsnew/3.0.html#ordering-comparisons
    #def __cmp__(self, dict):
    #    if isinstance(dict, AttrDict3):
    #        return cmp(self.data, (<AttrDict3>dict).data)
    #    else:
    #        return cmp(self.data, dict)
    __hash__ = None # Avoid Py3k warning
    def __len__(self): return len(self.data)
    def __getitem__(self, str key):
        return self.__asattrtype(self.data[key])
    def __setitem__(self, str key, item):
        if self.__frozen__: raise SyntaxError()
        self.data[key] = self.__asorgtype(item)
    def __delitem__(self, key):
        if self.__frozen__: raise SyntaxError()
        del self.data[key]
    def clear(self):
        if self.__frozen__: raise SyntaxError()
        self.data.clear()
    def copy(self):
        if self.__frozen__: raise SyntaxError()
        return AttrDict3(self.data.copy())
    def keys(self): return self.data.keys()
    def items(self): return [(key, self.__asattrtype(val)) for key, val in self.data.items()]
    def iteritems(self): return ((key, self.__asattrtype(val)) for key, val in self.data.items())
    def iterkeys(self): return self.data.iterkeys()
    def itervalues(self): return (self.__asattrtype(val) for val in self.data.itervalues())
    def values(self): return [self.__asattrtype(val) for val in self.data.values()]
    def has_key(self, key): return key in self.data
    def update(self, dict=None, **kwargs):
        if self.__frozen__: raise SyntaxError()
        if dict is None:
            pass
        elif isinstance(dict, AttrDict3):
            self.data.update((<AttrDict3>dict).data)
        elif isinstance(dict, type({})) or not hasattr(dict, 'items'):
            self.data.update(dict)
        else:
            for k, v in dict.items():
                self[k] = v
        if len(kwargs):
            self.update(kwargs)
    def get(self, key, failobj=None):
        if key not in self:
            return failobj
        return self[key]
    def setdefault(self, key, failobj=None):
        if key not in self:
            self[key] = failobj
        return self[key]
    def pop(self, key, *args): return self.__asatttritem( self.data.pop(key, *args) )
    def popitem(self): return self.__asatttritem( self.data.popitem() )
    def __contains__(self, key):
        return key in self.data
    @classmethod
    def fromkeys(cls, iterable, value=None):
        d = cls()
        for key in iterable:
            d[key] = value
        return d
    def __iter__(self):
        return iter(self.data)
    cpdef AttrDict3 __deepcopy__(AttrDict3 self, dict m):
        return AttrDict3(loads(dumps(self.data)))
    def __getattr__(self, str k):
        try:
            return self[k]
        except KeyError:
            raise AttributeError
    def __setattr__(self, str k, v):
        self[k] = v
    cpdef str __getstate__(AttrDict3 self):
        return dumps(self.data)
    cpdef __setstate__(AttrDict3 self, str state):
        self.data = loads(state)
        self.__frozen__ = False
        self.__msgpack__ = ''
    cpdef freeze(AttrDict3 self):
        self.__frozen__ = True

cdef class AttrDict2(dict):
    cdef bint __frozen__
    cdef str __msgpack__

    def __cinit__(AttrDict2 self,  dict init={}, bint frozen=False):
        cdef int i
        self.__frozen__ = frozen
        self.__msgpack__ = ''
        for i, t in enumerate(init.items()):
            if type(t[1]) == dict:
                (<dict>self)[t[0]] = AttrDict2(t[1])
            else:
                (<dict>self)[t[0]] = t[1]

    def __getstate__(AttrDict2 self):
        return dumps(self)

    def __setstate__(AttrDict2 self, str state):
        cdef list keys
        cdef list values
        init = loads(state)
        self.__frozen__ = False
        self.__msgpack__ = ''
        for i, t in enumerate(init.items()):
            if type(t[1]) == dict:
                dict.__setitem__(self, t[0], AttrDict2(t[1]))
            else:
                dict.__setitem__(self, t[0], t[1])

    def freeze(AttrDict2 self):
        self.__frozen__ = True

    cpdef AttrDict2 __deepcopy__(AttrDict2 self, dict m):
        if self.__frozen__:
            if self.__msgpack__ == '':
                self.__msgpack__ = dumps(self)
            return loads(self.__msgpack__, object_hook=AttrDict2)
        else:
            return loads(dumps(self), object_hook=AttrDict2)

    def __setitem__(AttrDict2 self, str k, v):
        if self.__frozen__: raise SyntaxError()
        if type(v) == dict:
            dict.__setitem__(self, k, AttrDict2(v))
        else:
            dict.__setitem__(self, k, v)

    cpdef list __recursivelist__(AttrDict2 self, list l):
        cdef int i
        for i, v in enumerate(l):
            if type(v) == dict:
                l[i] = AttrDict2(v, self.__frozen__)
            elif type(v) == list:
                 l[i] = self.__recursivelist__(v)
        return l

    def __getitem__(AttrDict2 self, str k):
        cdef int i
        result = dict.__getitem__(self, k)
        if self.__frozen__:
            if type(result) == AttrDict2:
                result.freeze()
            elif type(result) == list:
                for i, v in enumerate(result):
                    if type(v) == AttrDict2:
                        v.freeze()
                    elif type(v) == dict:
                        result[i] = AttrDict2(v, True)
                    elif type(v) == list:
                        result[i] = self.__recursivelist__(v)
                result = tuple(result)
        elif type(result) == list:
            for i, v in enumerate(result):
                if type(v) == dict:
                    result[i] = AttrDict2(v)
                elif type(v) == list:
                    result[i] = self.__recursivelist__(v)
        return result

    def __getattr__(AttrDict2 self, str k):
        try:
            result = self.__getitem__(k)
        except KeyError:
            if k in['__getnewargs__','__methods__','__members__', '__deepcopy__', '__recursivelist__']: raise
            result = AttrDict2()
            dict.__setitem__(self, k, result)
        return result

    def __setattr__(AttrDict2 self, str k, v):
        self.__setitem__(k, v)

cdef class AttrList(list):
    cdef bint __frozen__
    def __cinit__(AttrList self, list init=[], bint frozen=False):
        list.__init__(self, init)
        for item in init:
            type_item = type(item)
            if type_item == list:
                list.append( self, AttrList(item) )
            elif type_item == dict:
                list.append( self, AttrDict(item) )
        self.__frozen__ = False
        if frozen:
            self.freeze()

    cpdef freeze(AttrList self):
        if self.__frozen__: return
        self.__frozen__ = True
        for v in self:
            type_v = type(v)
            if type_v == AttrDict or type_v == AttrList:
                v.freeze()

    def append(AttrList self, item):
        if self.__frozen__: raise SyntaxError("Message frozen, use deepcopy.")
        type_item = type(item)
        if type_item == AttrDict:
            list.append( self, item )
        elif type_item == list:
            list.append( self, AttrList(item) )
        elif type_item == dict:
            list.append( self, AttrDict(item) )
        else:
            list.append( self, item )

    def insert(AttrList self, index, item):
        if self.__frozen__: raise SyntaxError("Message frozen, use deepcopy.")
        type_item = type(item)
        if type_item == AttrDict:
            list.insert( self, index, item )
        elif type_item == list:
            list.insert( self, index, AttrList(item) )
        elif type_item == dict:
            list.insert( self, index, AttrDict(item) )
        else:
            list.insert( self, index, item )

    def extend(AttrList self, iterable):
        if self.__frozen__: raise SyntaxError("Message frozen, use deepcopy.")
        type_iterable = type(iterable)
        if type_iterable == AttrDict:
            list.extend( self, iterable )
        elif type_iterable == list:
            list.extend( self, AttrList(iterable) )
        elif type_iterable == dict:
            list.extend( self, AttrDict(iterable) )
        else:
            list.extend( self, iterable )

    def __setitem__(AttrList self, key, value):
        if self.__frozen__: raise SyntaxError("Message frozen, use deepcopy.")
        type_value = type(value)
        if type_value == AttrDict:
            list.__setitem__( self, key, value )
        elif type_value == list:
            list.__setitem__( self, key, AttrList(value) )
        elif type_value == dict:
            list.__setitem__( self, key, AttrDict(value) )
        else:
            list.__setitem__( self, key, value )

    def __deepcopy__(AttrList self, m):
        return loads(dumps(self), AttrList)
#        l = AttrList([])
#        l.append( (deepcopy(item, m) for item in self) )
#        return l

cdef class AttrDict(dict):
    """A dictionary with attribute-style access. It maps attribute access to
    the real dictionary.  """
    cdef bint __frozen__
    def __cinit__(AttrDict self, dict init={}, bint frozen=False):
        dict.__init__(self, init)
        for key, value in init.iteritems():
            type_value = type(value)
            if type_value == dict:
                dict.__setitem__( self, key, AttrDict(value) )
            elif type_value == list:
                dict.__setitem__( self, key, AttrList(value) )
        self.__frozen__ = False
        if frozen:
            self.freeze()

    cpdef freeze(AttrDict self):
        if self.__frozen__: return
        self.__frozen__ = True
        for k, v in self.items():
            type_v = type(v)
            if type_v == AttrDict or type_v == AttrList:
                v.freeze()

    def __setstate__(self, init):
        dict.__init__(self, init)
        for key, value in init.iteritems():
            type_value = type(value)
            if type_value == dict:
                dict.__setitem__( self, key, AttrDict(value) )
            elif type_value == list:
                dict.__setitem__( self, key, AttrList(value) )

    def __getstate__(self):
        return self

    def __setitem__(self, str key, value):
        if self.__frozen__: raise SyntaxError("Message frozen, use deepcopy.")
        type_value = type(value)
        if  type_value == AttrDict:
            dict.__setitem__( self, key, value )
        elif type_value == dict:
            dict.__setitem__( self, key, AttrDict(value) )
        elif type_value == list:
            dict.__setitem__( self, key, AttrList(value) )
        else:
            dict.__setitem__( self, key, value )

    def update(AttrDict self, E=None, F=None):
        if E:
            try:
                for k in E.keys():
                    self[k] = E[k]
            except AttributeError:
                for (k, v) in E:
                    self[k] = v
        if F:
            for k in F:
                self[k] = F[k]

    def __copy__(self):
        return AttrDict(self)

    def __deepcopy__(AttrDict self, m):
        return loads(dumps(self), AttrDict)

    def __getattr__(self, str name):
        try:
            return self.__getitem__(name)
        except KeyError:
            raise AttributeError

    def __setattr__(self, str name, v):
        self.__setitem__(name, v)

    def copy(self):
        return AttrDict(self)

d = {
'words':"""
        Lorem ipsum dolor sit amet, consectetur adipiscing
        elit. Mauris adipiscing adipiscing placerat.
        Vestibulum augue augue,
        pellentesque quis sollicitudin id, adipiscing.
        """,
'list':range(100),
'dict':{i:"a" for i in xrange(100)},
'int':100,
'float':100.123456}

d_org = {"type": "CF_SimpleObservation","parameter":{"id": "fb600634-5ad7-11e5-945f-65303a4d6e97"},"phenomenonTime": "2015-09-14T11:58:58.649575", "procedure": {"type":"sensor","id": "45030171","description": "Sensus HRI-Mei linked to meter 14787486"},"featureOfInterest": {"type": "Feature","geometry": {"type":"Point", "coordinates":[-25.753, 28.280]},"properties":{"id":"Top Reservoir"} },"observedProperty": {"type": "TimeSeries"},"result": {"dimensions":{"time":5},"variables":{"time":{"dimensions":["time"],"units":"isoTime"},"pulse_value":{"dimensions":["int"],"units":"litres"}},"data":{"time":["2015-09-14T11:58:01.305564", "2015-09-14T11:47:06.808586", "2015-09-14T11:54:55.782008", "2015-09-14T11:43:58.603956", "2015-09-14T11:50:58.623827"], "pulse_value":[500, 500, 500, 0, 500]}}}
d_at1 = AttrDict(d_org)
d_at2 = AttrDict2(d_org)
d_at3 = AttrDict3(d_org)