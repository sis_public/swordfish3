# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Dependencies:
    pip install tabulate simplejson python-cjson ujson yajl msgpack-python
"""

from timeit import timeit
from tabulate import tabulate

import simplejson
# cjson does not work under Python3
#import cjson
import rpyc
import pickle
import ujson
import msgpack
import pyximport; pyximport.install()
from swordfish.examples.performance.attrdict import AttrDict2, AttrList3, AttrDict3
from swordfish.examples.performance.attrdict import d_at1, d_at2, d_at3

print(msgpack.loads(msgpack.dumps(d_at2), AttrDict2))
print(AttrDict2(ujson.loads(ujson.dumps(d_at2))))
print(pickle.loads(pickle.dumps(d_at2, 2)))

print(AttrDict3(msgpack.loads(msgpack.dumps(d_at3.__data__))))
assert (AttrDict3(msgpack.loads(msgpack.dumps(d_at3.__data__))) == d_at3)
print(AttrDict3(ujson.loads(ujson.dumps(d_at3.__data__))))
print(pickle.loads(pickle.dumps(d_at3, 2)))


setup = '''
import pyximport; pyximport.install()
from swordfish.examples.performance.attrdict import d_org, d_at1, d_at2, d_at3
from swordfish.examples.performance.attrdict import AttrDict, AttrDict2, AttrDict3
pass'''

setupp = '''
from swordfish.examples.performance import test_pb2
p = test_pb2.d()
p.words = """
        Lorem ipsum dolor sit amet, consectetur adipiscing
        elit. Mauris adipiscing adipiscing placerat.
        Vestibulum augue augue,
        pellentesque quis sollicitudin id, adipiscing.
        """
p.list.extend(range(100))
p.dict.extend([p.Item() for i in xrange(100)])
for i in xrange(100): p.dict[i].key = str(i)
for i in xrange(100): p.dict[i].value = "a"
p.int = 100
p.float = 100.123456
'''

setup_pickle2_org   = '%s ; import cPickle ; src = cPickle.dumps(d_org, 2)' % setup
setup_pickle2_at1   = '%s ; import cPickle ; src = cPickle.dumps(d_at1, 2)' % setup
setup_pickle2_at2   = '%s ; import cPickle ; src = cPickle.dumps(d_at2, 2)' % setup
setup_pickle2_at3   = '%s ; import cPickle ; src = cPickle.dumps(d_at3, 2)' % setup
setup_json_org      = '%s ; import json; src = json.dumps(d_org)' % setup
setup_json_at1      = '%s ; import json; src = json.dumps(d_at1)' % setup
setup_json_at2      = '%s ; import json; src = json.dumps(d_at2)' % setup
setup_msgpack_org   = '%s ; src = msgpack.dumps(d_org)' % setup
setup_msgpack_at1   = '%s ; src = msgpack.dumps(d_at1)' % setup
setup_msgpack_at2   = '%s ; src = msgpack.dumps(d_at2)' % setup
setup_msgpack_at3   = '%s ; src = msgpack.dumps(d_at3.__data__)' % setup
setup_deepcopy_org  = '%s ; src = d_org' % setup
setup_deepcopy_at1  = '%s ; src = d_at1' % setup
setup_deepcopy_at2  = '%s ; src = d_at2' % setup
setup_deepcopy_at3  = '%s ; src = d_at3' % setup

setup_protobuf  = '%ssrc = p.SerializeToString(); ' % setupp

tests = [
    # (title, setup, enc_test, dec_test)
    ('cPickle (binary) d_org', 'import cPickle; %s' % setup_pickle2_org, 'cPickle.dumps(d_org, 2)', 'cPickle.loads(src)'),
    ('cPickle (binary) d_at1', 'import cPickle; %s' % setup_pickle2_at1, 'cPickle.dumps(d_at1, 2)', 'cPickle.loads(src)'),
    ('cPickle (binary) d_at2', 'import cPickle; %s' % setup_pickle2_at2, 'cPickle.dumps(d_at2, 2)', 'cPickle.loads(src)'),
    ('cPickle (binary) d_at3', 'import cPickle; %s' % setup_pickle2_at3, 'cPickle.dumps(d_at3, 2)', 'cPickle.loads(src)'),
    ('ujson %s d_org'%ujson.__version__, 'import ujson; %s' % setup_json_org, 'ujson.dumps(d_org)', 'ujson.loads(src)'),
    ('ujson %s d_at1'%ujson.__version__, 'import ujson; %s' % setup_json_at1, 'ujson.dumps(d_at1)', 'AttrDict(ujson.loads(src))'),
    ('ujson %s d_at2'%ujson.__version__, 'import ujson; %s' % setup_json_at2, 'ujson.dumps(d_at2)', 'AttrDict2(ujson.loads(src))'),
    ('msgpack-python %s.%s.%s d_org'%msgpack.version,'import msgpack; %s' % setup_msgpack_org, 'msgpack.dumps(d_org)', 'msgpack.loads(src)'),
    ('msgpack-python %s.%s.%s d_at1'%msgpack.version,'import msgpack; %s' % setup_msgpack_at1, 'msgpack.dumps(d_at1)', 'msgpack.loads(src, AttrDict)'),
    ('msgpack-python %s.%s.%s d_at2'%msgpack.version,'import msgpack; %s' % setup_msgpack_at2, 'msgpack.dumps(d_at2)', 'msgpack.loads(src, AttrDict2)'),
    ('msgpack-python %s.%s.%s d_at3'%msgpack.version,'import msgpack; %s' % setup_msgpack_at3, 'msgpack.dumps(d_at3.__data__)', 'AttrDict3(msgpack.loads(src))'),
    #('deepcopy d_org', 'import copy; %s' % setup_deepcopy_org, 'copy.deepcopy(d_org)', 'copy.deepcopy(src)'),
    #('deepcopy d_at1', 'import copy; %s' % setup_deepcopy_at1, 'copy.deepcopy(d_at1)', 'copy.deepcopy(src)'),
    #('deepcopy d_at2', 'import copy; %s' % setup_deepcopy_at2, 'copy.deepcopy(d_at2)', 'copy.deepcopy(src)'),
    ('deepcopy d_at3', 'import copy; %s' % setup_deepcopy_at3, 'copy.deepcopy(d_at3)', 'copy.deepcopy(src)'),
    #('protobuf', '%s' % setup_protobuf, 'p.SerializeToString()', 'p.ParseFromString(src)'),
]

loops = 15000
enc_table = []
dec_table = []

print("Running tests (%d loops each)" % loops)

for title, mod, enc, dec in tests:
    print(title)

    print("  [Encode]", enc)
    result = timeit(enc, mod, number=loops)
    enc_table.append([title, result, loops/result])

    print("  [Decode]", dec)
    result = timeit(dec, mod, number=loops)
    dec_table.append([title, result, loops/result])

enc_table.sort(key=lambda x: x[1])
enc_table.insert(0, ['Package', 'Seconds', 'msg/sec'])

dec_table.sort(key=lambda x: x[1])
dec_table.insert(0, ['Package', 'Seconds', 'msg/sec'])

print("\nEncoding Test (%d loops)" % loops)
print(tabulate(enc_table, headers="firstrow"))

print("\nDecoding Test (%d loops)" % loops)
print(tabulate(dec_table, headers="firstrow"))