// -*- C++ -*-
// Copyright 2020, CSIR <www.csir.co.za>
// This software is released under the MIT License
// The license text is at https://mit-license.org/
#include <Python.h>
#include <string>
#include <sstream>
#include "structmember.h"
#include "test.pb.h"

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>




static PyObject *
fastpb_convert5(::google::protobuf::int32 value)
{
    return PyLong_FromLong(value);
}

static PyObject *
fastpb_convert3(::google::protobuf::int64 value)
{
    return PyLong_FromLongLong(value);
}

static PyObject *
fastpb_convert18(::google::protobuf::int64 value)
{
    return PyLong_FromLongLong(value);
}

static PyObject *
fastpb_convert17(::google::protobuf::int32 value)
{
    return PyLong_FromLong(value);
}

static PyObject *
fastpb_convert13(::google::protobuf::uint32 value)
{
    return PyLong_FromUnsignedLong(value);
}

static PyObject *
fastpb_convert7(::google::protobuf::int32 value)
{
    return PyLong_FromLong(value);
}

static PyObject *
fastpb_convert4(::google::protobuf::uint64 value)
{
    return PyLong_FromUnsignedLong(value);
}

static PyObject *
fastpb_convert1(double value)
{
    return PyFloat_FromDouble(value);
}

static PyObject *
fastpb_convert2(float value)
{
   return PyFloat_FromDouble(value);
}

static PyObject *
fastpb_convert9(const ::std::string &value)
{
    return PyUnicode_Decode(value.data(), value.length(), "utf-8", NULL);
}

static PyObject *
fastpb_convert12(const ::std::string &value)
{
    return PyString_FromStringAndSize(value.data(), value.length());
}

static PyObject *
fastpb_convert8(bool value)
{
    return PyBool_FromLong(value ? 1 : 0);
}

static PyObject *
fastpb_convert14(int value)
{
    // TODO(robbyw): Check EnumName_IsValid(value)
    return PyInt_FromLong(value);
}





// Lets try not to pollute the global namespace
namespace {

  // Forward-declaration for recursive structures
  extern PyTypeObject d_ItemType;

  typedef struct {
      PyObject_HEAD

      performance::d_Item *protobuf;
  } d_Item;

  void
  d_Item_dealloc(d_Item* self)
  {
      delete self->protobuf;
      self->ob_type->tp_free((PyObject*)self);
  }

  PyObject *
  d_Item_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
  {
      d_Item *self;

      self = (d_Item *)type->tp_alloc(type, 0);

      self->protobuf = new performance::d_Item();

      return (PyObject *)self;
  }

  PyObject *
  d_Item_DebugString(d_Item* self)
  {
      std::string result;
      Py_BEGIN_ALLOW_THREADS
      result = self->protobuf->Utf8DebugString();
      Py_END_ALLOW_THREADS
      return PyUnicode_FromStringAndSize(result.data(), result.length());
  }


  PyObject *
  d_Item_SerializeToString(d_Item* self)
  {
      std::string result;
      Py_BEGIN_ALLOW_THREADS
      self->protobuf->SerializeToString(&result);
      Py_END_ALLOW_THREADS
      return PyString_FromStringAndSize(result.data(), result.length());
  }


  PyObject *
  d_Item_SerializeMany(void *nothing, PyObject *values)
  {
      std::string result;
      google::protobuf::io::ZeroCopyOutputStream* output =
          new google::protobuf::io::StringOutputStream(&result);
      google::protobuf::io::CodedOutputStream* outputStream =
          new google::protobuf::io::CodedOutputStream(output);

      PyObject *sequence = PySequence_Fast(values, "The values to serialize must be a sequence.");
      for (Py_ssize_t i = 0, len = PySequence_Length(sequence); i < len; ++i) {
          d_Item *value = (d_Item *)PySequence_Fast_GET_ITEM(sequence, i);

          Py_BEGIN_ALLOW_THREADS
          outputStream->WriteVarint32(value->protobuf->ByteSize());
          value->protobuf->SerializeToCodedStream(outputStream);
          Py_END_ALLOW_THREADS
      }

      Py_XDECREF(sequence);
      delete outputStream;
      delete output;
      return PyString_FromStringAndSize(result.data(), result.length());
  }


  PyObject *
  d_Item_ParseFromString(d_Item* self, PyObject *value)
  {
      std::string serialized(PyString_AsString(value), PyString_Size(value));
      Py_BEGIN_ALLOW_THREADS
      self->protobuf->ParseFromString(serialized);
      Py_END_ALLOW_THREADS
      Py_RETURN_NONE;
  }


  PyObject *
  d_Item_ParseFromLongString(d_Item* self, PyObject *value)
  {
      google::protobuf::io::ZeroCopyInputStream* input =
          new google::protobuf::io::ArrayInputStream(PyString_AsString(value), PyString_Size(value));
      google::protobuf::io::CodedInputStream* inputStream =
          new google::protobuf::io::CodedInputStream(input);
      inputStream->SetTotalBytesLimit(512 * 1024 * 1024, 512 * 1024 * 1024);

      Py_BEGIN_ALLOW_THREADS
      self->protobuf->ParseFromCodedStream(inputStream);
      Py_END_ALLOW_THREADS

      delete inputStream;
      delete input;

      Py_RETURN_NONE;
  }


  PyObject *
  d_Item_ParseMany(void* nothing, PyObject *args)
  {
      PyObject *value;
      PyObject *callback;
      int fail = 0;

      if (!PyArg_ParseTuple(args, "OO", &value, &callback)) {
          return NULL;
      }

      google::protobuf::io::ZeroCopyInputStream* input =
          new google::protobuf::io::ArrayInputStream(PyString_AsString(value), PyString_Size(value));
      google::protobuf::io::CodedInputStream* inputStream =
          new google::protobuf::io::CodedInputStream(input);
      inputStream->SetTotalBytesLimit(512 * 1024 * 1024, 512 * 1024 * 1024);

      google::protobuf::uint32 bytes;
      PyObject *single = NULL;
      while (inputStream->ReadVarint32(&bytes)) {
          google::protobuf::io::CodedInputStream::Limit messageLimit = inputStream->PushLimit(bytes);

          if (single == NULL) {
            single = d_Item_new(&d_ItemType, NULL, NULL);
          }

          Py_BEGIN_ALLOW_THREADS
          ((d_Item *)single)->protobuf->ParseFromCodedStream(inputStream);
          Py_END_ALLOW_THREADS

          inputStream->PopLimit(messageLimit);
          PyObject *result = PyObject_CallFunctionObjArgs(callback, single, NULL);
          if (result == NULL) {
              fail = 1;
              break;
          };

          if (single->ob_refcnt != 1) {
            // If the callback saved a reference to the item, don't re-use it.
            Py_XDECREF(single);
            single = NULL;
          }
      }
      if (single != NULL) {
        Py_XDECREF(single);
      }

      delete inputStream;
      delete input;

      if (fail) {
          return NULL;
      } else {
          Py_RETURN_NONE;
      }
  }





    PyObject *
    d_Item_getkey(d_Item *self, void *closure)
    {

          if (! self->protobuf->has_key()) {
            Py_RETURN_NONE;
          }

          return
              fastpb_convert9(
                  self->protobuf->key());


    }

    int
    d_Item_setkey(d_Item *self, PyObject *input, void *closure)
    {
      if (input == NULL || input == Py_None) {
        self->protobuf->clear_key();
        return 0;
      }


        PyObject *value = input;



        // string
        bool reallocated = false;
        if (PyUnicode_Check(value)) {
          value = PyUnicode_AsEncodedString(value, "utf-8", NULL);
          reallocated = true;
        }

        if (! PyString_Check(value)) {
          PyErr_SetString(PyExc_TypeError, "The key attribute value must be a string");
          return -1;
        }

        std::string protoValue(PyString_AsString(value), PyString_Size(value));
        if (reallocated) {
          Py_XDECREF(value);
        }





          self->protobuf->set_key(protoValue);



      return 0;
    }



    PyObject *
    d_Item_getvalue(d_Item *self, void *closure)
    {

          if (! self->protobuf->has_value()) {
            Py_RETURN_NONE;
          }

          return
              fastpb_convert9(
                  self->protobuf->value());


    }

    int
    d_Item_setvalue(d_Item *self, PyObject *input, void *closure)
    {
      if (input == NULL || input == Py_None) {
        self->protobuf->clear_value();
        return 0;
      }


        PyObject *value = input;



        // string
        bool reallocated = false;
        if (PyUnicode_Check(value)) {
          value = PyUnicode_AsEncodedString(value, "utf-8", NULL);
          reallocated = true;
        }

        if (! PyString_Check(value)) {
          PyErr_SetString(PyExc_TypeError, "The value attribute value must be a string");
          return -1;
        }

        std::string protoValue(PyString_AsString(value), PyString_Size(value));
        if (reallocated) {
          Py_XDECREF(value);
        }





          self->protobuf->set_value(protoValue);



      return 0;
    }


  int
  d_Item_init(d_Item *self, PyObject *args, PyObject *kwds)
  {


          PyObject *key = NULL;

          PyObject *value = NULL;


        static char *kwlist[] = {

            (char *) "key",

            (char *) "value",

          NULL
        };

        if (! PyArg_ParseTupleAndKeywords(
            args, kwds, "|OO", kwlist,
            &key,&value))
          return -1;


          if (key) {
            if (d_Item_setkey(self, key, NULL) < 0) {
              return -1;
            }
          }

          if (value) {
            if (d_Item_setvalue(self, value, NULL) < 0) {
              return -1;
            }
          }



      return 0;
  }


  PyObject *
  d_Item_richcompare(PyObject *self, PyObject *other, int op)
  {
      PyObject *result = NULL;
      if (!PyType_IsSubtype(other->ob_type, &d_ItemType)) {
          result = Py_NotImplemented;
      } else {
          // This is not a particularly efficient implementation since it never short circuits, but it's better
          // than nothing.  It should probably only be used for tests.
          d_Item *selfValue = (d_Item *)self;
          d_Item *otherValue = (d_Item *)other;
          std::string selfSerialized;
          std::string otherSerialized;
          Py_BEGIN_ALLOW_THREADS
          selfValue->protobuf->SerializeToString(&selfSerialized);
          otherValue->protobuf->SerializeToString(&otherSerialized);
          Py_END_ALLOW_THREADS

          int cmp = selfSerialized.compare(otherSerialized);
          bool value = false;
          switch (op) {
              case Py_LT:
                  value = cmp < 0;
                  break;
              case Py_LE:
                  value = cmp <= 0;
                  break;
              case Py_EQ:
                  value = cmp == 0;
                  break;
              case Py_NE:
                  value = cmp != 0;
                  break;
              case Py_GT:
                  value = cmp > 0;
                  break;
              case Py_GE:
                  value = cmp >= 0;
                  break;
          }
          result = value ? Py_True : Py_False;
      }

      Py_XINCREF(result);
      return result;
  }


  static PyObject *
  d_Item_repr(PyObject *selfObject)
  {
      d_Item *self = (d_Item *)selfObject;
      PyObject *member;
      PyObject *memberRepr;
      std::stringstream result;
      result << "d_Item(";



        result << "key=";
        member = d_Item_getkey(self, NULL);
        memberRepr = PyObject_Repr(member);
        result << PyString_AsString(memberRepr);
        Py_XDECREF(memberRepr);
        Py_XDECREF(member);


          result << ", ";

        result << "value=";
        member = d_Item_getvalue(self, NULL);
        memberRepr = PyObject_Repr(member);
        result << PyString_AsString(memberRepr);
        Py_XDECREF(memberRepr);
        Py_XDECREF(member);


      result << ")";

      std::string resultString = result.str();
      return PyUnicode_Decode(resultString.data(), resultString.length(), "utf-8", NULL);
  }


  PyMemberDef d_Item_members[] = {
      {NULL}  // Sentinel
  };


  PyGetSetDef d_Item_getsetters[] = {

      {(char *)"key",
       (getter)d_Item_getkey, (setter)d_Item_setkey,
       (char *)"",
       NULL},

      {(char *)"value",
       (getter)d_Item_getvalue, (setter)d_Item_setvalue,
       (char *)"",
       NULL},

      {NULL}  // Sentinel
  };


  PyMethodDef d_Item_methods[] = {
      {"DebugString", (PyCFunction)d_Item_DebugString, METH_NOARGS,
       "Generates a human readable form of this message, useful for debugging and other purposes."
      },
      {"SerializeToString", (PyCFunction)d_Item_SerializeToString, METH_NOARGS,
       "Serializes the protocol buffer to a string."
      },
      {"SerializeMany", (PyCFunction)d_Item_SerializeMany, METH_O | METH_CLASS,
       "Serializes a sequence of protocol buffers to a string."
      },
      {"ParseFromString", (PyCFunction)d_Item_ParseFromString, METH_O,
       "Parses the protocol buffer from a string."
      },
      {"ParseFromLongString", (PyCFunction)d_Item_ParseFromLongString, METH_O,
       "Parses the protocol buffer from a string as large as 512MB."
      },
      {"ParseMany", (PyCFunction)d_Item_ParseMany, METH_VARARGS | METH_CLASS,
       "Parses many protocol buffers of this type from a string."
      },
      {NULL}  // Sentinel
  };


  PyTypeObject d_ItemType = {
      PyObject_HEAD_INIT(NULL)
      0,                                      /*ob_size*/
      "performance.d_Item",  /*tp_name*/
      sizeof(d_Item),             /*tp_basicsize*/
      0,                                      /*tp_itemsize*/
      (destructor)d_Item_dealloc, /*tp_dealloc*/
      0,                                      /*tp_print*/
      0,                                      /*tp_getattr*/
      0,                                      /*tp_setattr*/
      0,                                      /*tp_compare*/
      d_Item_repr,                /*tp_repr*/
      0,                                      /*tp_as_number*/
      0,                                      /*tp_as_sequence*/
      0,                                      /*tp_as_mapping*/
      0,                                      /*tp_hash */
      0,                                      /*tp_call*/
      0,                                      /*tp_str*/
      0,                                      /*tp_getattro*/
      0,                                      /*tp_setattro*/
      0,                                      /*tp_as_buffer*/
      Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE | Py_TPFLAGS_HAVE_RICHCOMPARE, /*tp_flags*/
      "d_Item objects",           /* tp_doc */
      0,                                      /* tp_traverse */
      0,                                      /* tp_clear */
      d_Item_richcompare,         /* tp_richcompare */
      0,	   	                                /* tp_weaklistoffset */
      0,                   		                /* tp_iter */
      0,		                                  /* tp_iternext */
      d_Item_methods,             /* tp_methods */
      d_Item_members,             /* tp_members */
      d_Item_getsetters,          /* tp_getset */
      0,                                      /* tp_base */
      0,                                      /* tp_dict */
      0,                                      /* tp_descr_get */
      0,                                      /* tp_descr_set */
      0,                                      /* tp_dictoffset */
      (initproc)d_Item_init,      /* tp_init */
      0,                                      /* tp_alloc */
      d_Item_new,                 /* tp_new */
  };
}



// Lets try not to pollute the global namespace
namespace {

  // Forward-declaration for recursive structures
  extern PyTypeObject dType;

  typedef struct {
      PyObject_HEAD

      performance::d *protobuf;
  } d;

  void
  d_dealloc(d* self)
  {
      delete self->protobuf;
      self->ob_type->tp_free((PyObject*)self);
  }

  PyObject *
  d_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
  {
      d *self;

      self = (d *)type->tp_alloc(type, 0);

      self->protobuf = new performance::d();

      return (PyObject *)self;
  }

  PyObject *
  d_DebugString(d* self)
  {
      std::string result;
      Py_BEGIN_ALLOW_THREADS
      result = self->protobuf->Utf8DebugString();
      Py_END_ALLOW_THREADS
      return PyUnicode_FromStringAndSize(result.data(), result.length());
  }


  PyObject *
  d_SerializeToString(d* self)
  {
      std::string result;
      Py_BEGIN_ALLOW_THREADS
      self->protobuf->SerializeToString(&result);
      Py_END_ALLOW_THREADS
      return PyString_FromStringAndSize(result.data(), result.length());
  }


  PyObject *
  d_SerializeMany(void *nothing, PyObject *values)
  {
      std::string result;
      google::protobuf::io::ZeroCopyOutputStream* output =
          new google::protobuf::io::StringOutputStream(&result);
      google::protobuf::io::CodedOutputStream* outputStream =
          new google::protobuf::io::CodedOutputStream(output);

      PyObject *sequence = PySequence_Fast(values, "The values to serialize must be a sequence.");
      for (Py_ssize_t i = 0, len = PySequence_Length(sequence); i < len; ++i) {
          d *value = (d *)PySequence_Fast_GET_ITEM(sequence, i);

          Py_BEGIN_ALLOW_THREADS
          outputStream->WriteVarint32(value->protobuf->ByteSize());
          value->protobuf->SerializeToCodedStream(outputStream);
          Py_END_ALLOW_THREADS
      }

      Py_XDECREF(sequence);
      delete outputStream;
      delete output;
      return PyString_FromStringAndSize(result.data(), result.length());
  }


  PyObject *
  d_ParseFromString(d* self, PyObject *value)
  {
      std::string serialized(PyString_AsString(value), PyString_Size(value));
      Py_BEGIN_ALLOW_THREADS
      self->protobuf->ParseFromString(serialized);
      Py_END_ALLOW_THREADS
      Py_RETURN_NONE;
  }


  PyObject *
  d_ParseFromLongString(d* self, PyObject *value)
  {
      google::protobuf::io::ZeroCopyInputStream* input =
          new google::protobuf::io::ArrayInputStream(PyString_AsString(value), PyString_Size(value));
      google::protobuf::io::CodedInputStream* inputStream =
          new google::protobuf::io::CodedInputStream(input);
      inputStream->SetTotalBytesLimit(512 * 1024 * 1024, 512 * 1024 * 1024);

      Py_BEGIN_ALLOW_THREADS
      self->protobuf->ParseFromCodedStream(inputStream);
      Py_END_ALLOW_THREADS

      delete inputStream;
      delete input;

      Py_RETURN_NONE;
  }


  PyObject *
  d_ParseMany(void* nothing, PyObject *args)
  {
      PyObject *value;
      PyObject *callback;
      int fail = 0;

      if (!PyArg_ParseTuple(args, "OO", &value, &callback)) {
          return NULL;
      }

      google::protobuf::io::ZeroCopyInputStream* input =
          new google::protobuf::io::ArrayInputStream(PyString_AsString(value), PyString_Size(value));
      google::protobuf::io::CodedInputStream* inputStream =
          new google::protobuf::io::CodedInputStream(input);
      inputStream->SetTotalBytesLimit(512 * 1024 * 1024, 512 * 1024 * 1024);

      google::protobuf::uint32 bytes;
      PyObject *single = NULL;
      while (inputStream->ReadVarint32(&bytes)) {
          google::protobuf::io::CodedInputStream::Limit messageLimit = inputStream->PushLimit(bytes);

          if (single == NULL) {
            single = d_new(&dType, NULL, NULL);
          }

          Py_BEGIN_ALLOW_THREADS
          ((d *)single)->protobuf->ParseFromCodedStream(inputStream);
          Py_END_ALLOW_THREADS

          inputStream->PopLimit(messageLimit);
          PyObject *result = PyObject_CallFunctionObjArgs(callback, single, NULL);
          if (result == NULL) {
              fail = 1;
              break;
          };

          if (single->ob_refcnt != 1) {
            // If the callback saved a reference to the item, don't re-use it.
            Py_XDECREF(single);
            single = NULL;
          }
      }
      if (single != NULL) {
        Py_XDECREF(single);
      }

      delete inputStream;
      delete input;

      if (fail) {
          return NULL;
      } else {
          Py_RETURN_NONE;
      }
  }





    PyObject *
    d_getwords(d *self, void *closure)
    {

          if (! self->protobuf->has_words()) {
            Py_RETURN_NONE;
          }

          return
              fastpb_convert9(
                  self->protobuf->words());


    }

    int
    d_setwords(d *self, PyObject *input, void *closure)
    {
      if (input == NULL || input == Py_None) {
        self->protobuf->clear_words();
        return 0;
      }


        PyObject *value = input;



        // string
        bool reallocated = false;
        if (PyUnicode_Check(value)) {
          value = PyUnicode_AsEncodedString(value, "utf-8", NULL);
          reallocated = true;
        }

        if (! PyString_Check(value)) {
          PyErr_SetString(PyExc_TypeError, "The words attribute value must be a string");
          return -1;
        }

        std::string protoValue(PyString_AsString(value), PyString_Size(value));
        if (reallocated) {
          Py_XDECREF(value);
        }





          self->protobuf->set_words(protoValue);



      return 0;
    }



    PyObject *
    d_getlist(d *self, void *closure)
    {

          int len = self->protobuf->list_size();
          PyObject *tuple = PyTuple_New(len);
          for (int i = 0; i < len; ++i) {
            PyObject *value =
                fastpb_convert5(
                    self->protobuf->list(i));
            if (!value) {
              return NULL;
            }
            PyTuple_SetItem(tuple, i, value);
          }
          return tuple;


    }

    int
    d_setlist(d *self, PyObject *input, void *closure)
    {
      if (input == NULL || input == Py_None) {
        self->protobuf->clear_list();
        return 0;
      }


        if (PyString_Check(input)) {
          PyErr_SetString(PyExc_TypeError, "The list attribute value must be a sequence");
          return -1;
        }
        PyObject *sequence = PySequence_Fast(input, "The list attribute value must be a sequence");
        self->protobuf->clear_list();
        for (Py_ssize_t i = 0, len = PySequence_Length(sequence); i < len; ++i) {
          PyObject *value = PySequence_Fast_GET_ITEM(sequence, i);




        ::google::protobuf::int32 protoValue;

        // int32
        if (PyInt_Check(value)) {
          protoValue = PyInt_AsLong(value);
        } else {
          PyErr_SetString(PyExc_TypeError,
                          "The list attribute value must be an integer");
          return -1;
        }





            self->protobuf->add_list(protoValue);

        }

        Py_XDECREF(sequence);


      return 0;
    }


      PyObject *
      fastpb_convertddict(const ::google::protobuf::Message &value)
      {
          d_Item *obj = (d_Item *)
              d_Item_new(&d_ItemType, NULL, NULL);
          obj->protobuf->MergeFrom(value);
          return (PyObject *)obj;
      }


    PyObject *
    d_getdict(d *self, void *closure)
    {

          int len = self->protobuf->dict_size();
          PyObject *tuple = PyTuple_New(len);
          for (int i = 0; i < len; ++i) {
            PyObject *value =
                fastpb_convertddict(
                    self->protobuf->dict(i));
            if (!value) {
              return NULL;
            }
            PyTuple_SetItem(tuple, i, value);
          }
          return tuple;


    }

    int
    d_setdict(d *self, PyObject *input, void *closure)
    {
      if (input == NULL || input == Py_None) {
        self->protobuf->clear_dict();
        return 0;
      }


        if (PyString_Check(input)) {
          PyErr_SetString(PyExc_TypeError, "The dict attribute value must be a sequence");
          return -1;
        }
        PyObject *sequence = PySequence_Fast(input, "The dict attribute value must be a sequence");
        self->protobuf->clear_dict();
        for (Py_ssize_t i = 0, len = PySequence_Length(sequence); i < len; ++i) {
          PyObject *value = PySequence_Fast_GET_ITEM(sequence, i);





        if (!PyType_IsSubtype(value->ob_type, &d_ItemType)) {
          PyErr_SetString(PyExc_TypeError,
                          "The dict attribute value must be an instance of d_Item");
          return -1;
        }

         // .performance.d.Item
        ::performance::d::Item *protoValue =
            ((d_Item *) value)->protobuf;





            self->protobuf->add_dict()->MergeFrom(*protoValue);

        }

        Py_XDECREF(sequence);


      return 0;
    }



    PyObject *
    d_getint(d *self, void *closure)
    {

          if (! self->protobuf->has_int()) {
            Py_RETURN_NONE;
          }

          return
              fastpb_convert5(
                  self->protobuf->int());


    }

    int
    d_setint(d *self, PyObject *input, void *closure)
    {
      if (input == NULL || input == Py_None) {
        self->protobuf->clear_int();
        return 0;
      }


        PyObject *value = input;



        ::google::protobuf::int32 protoValue;

        // int32
        if (PyInt_Check(value)) {
          protoValue = PyInt_AsLong(value);
        } else {
          PyErr_SetString(PyExc_TypeError,
                          "The int attribute value must be an integer");
          return -1;
        }





          self->protobuf->set_int(protoValue);



      return 0;
    }



    PyObject *
    d_getfloat(d *self, void *closure)
    {

          if (! self->protobuf->has_float()) {
            Py_RETURN_NONE;
          }

          return
              fastpb_convert2(
                  self->protobuf->float());


    }

    int
    d_setfloat(d *self, PyObject *input, void *closure)
    {
      if (input == NULL || input == Py_None) {
        self->protobuf->clear_float();
        return 0;
      }


        PyObject *value = input;




        float protoValue;

        if (PyFloat_Check(value)) {
          protoValue = PyFloat_AsDouble(value);
        } else if (PyInt_Check(value)) {
          protoValue = PyInt_AsLong(value);
        } else if (PyLong_Check(value)) {
          protoValue = PyLong_AsLongLong(value);
        } else {
          PyErr_SetString(PyExc_TypeError,

                          "The float attribute value must be a float");

          return -1;
        }





          self->protobuf->set_float(protoValue);



      return 0;
    }


  int
  d_init(d *self, PyObject *args, PyObject *kwds)
  {


          PyObject *words = NULL;

          PyObject *list = NULL;

          PyObject *dict = NULL;

          PyObject *int = NULL;

          PyObject *float = NULL;


        static char *kwlist[] = {

            (char *) "words",

            (char *) "list",

            (char *) "dict",

            (char *) "int",

            (char *) "float",

          NULL
        };

        if (! PyArg_ParseTupleAndKeywords(
            args, kwds, "|OOOOO", kwlist,
            &words,&list,&dict,&int,&float))
          return -1;


          if (words) {
            if (d_setwords(self, words, NULL) < 0) {
              return -1;
            }
          }

          if (list) {
            if (d_setlist(self, list, NULL) < 0) {
              return -1;
            }
          }

          if (dict) {
            if (d_setdict(self, dict, NULL) < 0) {
              return -1;
            }
          }

          if (int) {
            if (d_setint(self, int, NULL) < 0) {
              return -1;
            }
          }

          if (float) {
            if (d_setfloat(self, float, NULL) < 0) {
              return -1;
            }
          }



      return 0;
  }


  PyObject *
  d_richcompare(PyObject *self, PyObject *other, int op)
  {
      PyObject *result = NULL;
      if (!PyType_IsSubtype(other->ob_type, &dType)) {
          result = Py_NotImplemented;
      } else {
          // This is not a particularly efficient implementation since it never short circuits, but it's better
          // than nothing.  It should probably only be used for tests.
          d *selfValue = (d *)self;
          d *otherValue = (d *)other;
          std::string selfSerialized;
          std::string otherSerialized;
          Py_BEGIN_ALLOW_THREADS
          selfValue->protobuf->SerializeToString(&selfSerialized);
          otherValue->protobuf->SerializeToString(&otherSerialized);
          Py_END_ALLOW_THREADS

          int cmp = selfSerialized.compare(otherSerialized);
          bool value = false;
          switch (op) {
              case Py_LT:
                  value = cmp < 0;
                  break;
              case Py_LE:
                  value = cmp <= 0;
                  break;
              case Py_EQ:
                  value = cmp == 0;
                  break;
              case Py_NE:
                  value = cmp != 0;
                  break;
              case Py_GT:
                  value = cmp > 0;
                  break;
              case Py_GE:
                  value = cmp >= 0;
                  break;
          }
          result = value ? Py_True : Py_False;
      }

      Py_XINCREF(result);
      return result;
  }


  static PyObject *
  d_repr(PyObject *selfObject)
  {
      d *self = (d *)selfObject;
      PyObject *member;
      PyObject *memberRepr;
      std::stringstream result;
      result << "d(";



        result << "words=";
        member = d_getwords(self, NULL);
        memberRepr = PyObject_Repr(member);
        result << PyString_AsString(memberRepr);
        Py_XDECREF(memberRepr);
        Py_XDECREF(member);


          result << ", ";

        result << "list=";
        member = d_getlist(self, NULL);
        memberRepr = PyObject_Repr(member);
        result << PyString_AsString(memberRepr);
        Py_XDECREF(memberRepr);
        Py_XDECREF(member);


          result << ", ";

        result << "dict=";
        member = d_getdict(self, NULL);
        memberRepr = PyObject_Repr(member);
        result << PyString_AsString(memberRepr);
        Py_XDECREF(memberRepr);
        Py_XDECREF(member);


          result << ", ";

        result << "int=";
        member = d_getint(self, NULL);
        memberRepr = PyObject_Repr(member);
        result << PyString_AsString(memberRepr);
        Py_XDECREF(memberRepr);
        Py_XDECREF(member);


          result << ", ";

        result << "float=";
        member = d_getfloat(self, NULL);
        memberRepr = PyObject_Repr(member);
        result << PyString_AsString(memberRepr);
        Py_XDECREF(memberRepr);
        Py_XDECREF(member);


      result << ")";

      std::string resultString = result.str();
      return PyUnicode_Decode(resultString.data(), resultString.length(), "utf-8", NULL);
  }


  PyMemberDef d_members[] = {
      {NULL}  // Sentinel
  };


  PyGetSetDef d_getsetters[] = {

      {(char *)"words",
       (getter)d_getwords, (setter)d_setwords,
       (char *)"",
       NULL},

      {(char *)"list",
       (getter)d_getlist, (setter)d_setlist,
       (char *)"",
       NULL},

      {(char *)"dict",
       (getter)d_getdict, (setter)d_setdict,
       (char *)"",
       NULL},

      {(char *)"int",
       (getter)d_getint, (setter)d_setint,
       (char *)"",
       NULL},

      {(char *)"float",
       (getter)d_getfloat, (setter)d_setfloat,
       (char *)"",
       NULL},

      {NULL}  // Sentinel
  };


  PyMethodDef d_methods[] = {
      {"DebugString", (PyCFunction)d_DebugString, METH_NOARGS,
       "Generates a human readable form of this message, useful for debugging and other purposes."
      },
      {"SerializeToString", (PyCFunction)d_SerializeToString, METH_NOARGS,
       "Serializes the protocol buffer to a string."
      },
      {"SerializeMany", (PyCFunction)d_SerializeMany, METH_O | METH_CLASS,
       "Serializes a sequence of protocol buffers to a string."
      },
      {"ParseFromString", (PyCFunction)d_ParseFromString, METH_O,
       "Parses the protocol buffer from a string."
      },
      {"ParseFromLongString", (PyCFunction)d_ParseFromLongString, METH_O,
       "Parses the protocol buffer from a string as large as 512MB."
      },
      {"ParseMany", (PyCFunction)d_ParseMany, METH_VARARGS | METH_CLASS,
       "Parses many protocol buffers of this type from a string."
      },
      {NULL}  // Sentinel
  };


  PyTypeObject dType = {
      PyObject_HEAD_INIT(NULL)
      0,                                      /*ob_size*/
      "performance.d",  /*tp_name*/
      sizeof(d),             /*tp_basicsize*/
      0,                                      /*tp_itemsize*/
      (destructor)d_dealloc, /*tp_dealloc*/
      0,                                      /*tp_print*/
      0,                                      /*tp_getattr*/
      0,                                      /*tp_setattr*/
      0,                                      /*tp_compare*/
      d_repr,                /*tp_repr*/
      0,                                      /*tp_as_number*/
      0,                                      /*tp_as_sequence*/
      0,                                      /*tp_as_mapping*/
      0,                                      /*tp_hash */
      0,                                      /*tp_call*/
      0,                                      /*tp_str*/
      0,                                      /*tp_getattro*/
      0,                                      /*tp_setattro*/
      0,                                      /*tp_as_buffer*/
      Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE | Py_TPFLAGS_HAVE_RICHCOMPARE, /*tp_flags*/
      "d objects",           /* tp_doc */
      0,                                      /* tp_traverse */
      0,                                      /* tp_clear */
      d_richcompare,         /* tp_richcompare */
      0,	   	                                /* tp_weaklistoffset */
      0,                   		                /* tp_iter */
      0,		                                  /* tp_iternext */
      d_methods,             /* tp_methods */
      d_members,             /* tp_members */
      d_getsetters,          /* tp_getset */
      0,                                      /* tp_base */
      0,                                      /* tp_dict */
      0,                                      /* tp_descr_get */
      0,                                      /* tp_descr_set */
      0,                                      /* tp_dictoffset */
      (initproc)d_init,      /* tp_init */
      0,                                      /* tp_alloc */
      d_new,                 /* tp_new */
  };
}



static PyMethodDef module_methods[] = {
    {NULL}  // Sentinel
};

#ifndef PyMODINIT_FUNC	// Declarations for DLL import/export.
#define PyMODINIT_FUNC void
#endif
PyMODINIT_FUNC
initperformance(void)
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    PyObject* m;




      if (PyType_Ready(&d_ItemType) < 0)
          return;

      if (PyType_Ready(&dType) < 0)
          return;


    m = Py_InitModule3("performance", module_methods,
                       "");

    if (m == NULL)
      return;




      Py_INCREF(&d_ItemType);
      PyModule_AddObject(m, "d_Item", (PyObject *)&d_ItemType);

      Py_INCREF(&dType);
      PyModule_AddObject(m, "d", (PyObject *)&dType);

}