# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Dependencies:
    pip install tabulate simplejson python-cjson ujson yajl msgpack-python
"""

from timeit import timeit
from tabulate import tabulate

import simplejson
import ujson
# cjson does not work under Python3
#import cjson
import msgpack
import rpyc
from swordfish.core import AttrDict

d = {"type": "CF_SimpleObservation","parameter":{"id": "fb600634-5ad7-11e5-945f-65303a4d6e97"},"phenomenonTime": "2015-09-14T11:58:58.649575", "procedure": {"type":"sensor","id": "45030171","description": "Sensus HRI-Mei linked to meter 14787486"},"featureOfInterest": {"type": "Feature","geometry": {"type":"Point", "coordinates":[-25.753, 28.280]},"properties":{"id":"Top Reservoir"} },"observedProperty": {"type": "TimeSeries"},"result": {"dimensions":{"time":5},"variables":{"time":{"dimensions":["time"],"units":"isoTime"},"pulse_value":{"dimensions":["int"],"units":"litres"}},"data":{"time":["2015-09-14T11:58:01.305564", "2015-09-14T11:47:06.808586", "2015-09-14T11:54:55.782008", "2015-09-14T11:43:58.603956", "2015-09-14T11:50:58.623827"], "pulse_value":[500, 500, 500, 0, 500]}}}
e = AttrDict(d)
print(d)
print()
print(e)
