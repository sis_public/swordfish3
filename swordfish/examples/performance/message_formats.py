# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Dependencies:
    pip install tabulate simplejson python-cjson ujson yajl msgpack-python
"""

from timeit import timeit
from tabulate import tabulate

import simplejson
import ujson
# cjson does not work under Python3
#import cjson
import msgpack
import rpyc
import pyximport; pyximport.install()
import swordfish.examples.performance.attrdict

setup = '''
import pyximport; pyximport.install()
from swordfish.examples.performance.attrdict import d_org, d_at1, d_at2

d=d_at2
pass'''

setupp = '''
from swordfish.examples.performance import test_pb2
p = test_pb2.d()
p.words = """
        Lorem ipsum dolor sit amet, consectetur adipiscing
        elit. Mauris adipiscing adipiscing placerat.
        Vestibulum augue augue,
        pellentesque quis sollicitudin id, adipiscing.
        """
p.list.extend(range(100))
p.dict.extend([p.Item() for i in xrange(100)])
for i in xrange(100): p.dict[i].key = str(i)
for i in xrange(100): p.dict[i].value = "a"
p.int = 100
p.float = 100.123456
'''

setup_pickle    = '%s ; import pickle ; src = pickle.dumps(d)' % setup
setup_pickle2   = '%s ; import pickle ; src = pickle.dumps(d, 2)' % setup
setup_json      = '%s ; import json; src = json.dumps(d)' % setup
setup_msgpack   = '%s ; src = msgpack.dumps(d)' % setup
setup_deepcopy  = '%s ; src = d' % setup
setup_protobuf  = '%ssrc = p.SerializeToString(); ' % setupp

tests = [
    # (title, setup, enc_test, dec_test)
    #('pickle (ascii)', 'import pickle; %s' % setup_pickle, 'pickle.dumps(d, 0)', 'pickle.loads(src)'),
    #('pickle (binary)', 'import pickle; %s' % setup_pickle2, 'pickle.dumps(d, 2)', 'pickle.loads(src)'),
    #('pickle (ascii)', 'import pickle; %s' % setup_pickle, 'pickle.dumps(d, 0)', 'pickle.loads(src)'),
    ('pickle (binary)', 'import pickle; %s' % setup_pickle2, 'pickle.dumps(d, 2)', 'pickle.loads(src)'),
    #('json', 'import json; %s' % setup_json, 'json.dumps(d)', 'json.loads(src)'),
    #('simplejson %s'%simplejson.__version__, 'import simplejson; %s' % setup_json, 'simplejson.dumps(d)', 'simplejson.loads(src)'),
    #('python-cjson %s'%cjson.__version__, 'import cjson; %s' % setup_json, 'cjson.encode(d)', 'cjson.decode(src)'),
    ('ujson %s'%ujson.__version__, 'import ujson; %s' % setup_json, 'ujson.dumps(d)', 'ujson.loads(src)'),
    #('yajl 0.3.5', 'import yajl; %s' % setup_json, 'yajl.dumps(d)', 'yajl.loads(src)'),
    ('msgpack-python %s.%s.%s'%msgpack.version,'import msgpack; %s' % setup_msgpack, 'msgpack.dumps(d)', 'msgpack.loads(src)'),
    ('deepcopy', 'import copy; %s' % setup_deepcopy, 'copy.deepcopy(src)', 'copy.deepcopy(src)'),
    #('protobuf', '%s' % setup_protobuf, 'p.SerializeToString()', 'p.ParseFromString(src)'),
]

loops = 15000
enc_table = []
dec_table = []

print("Running tests (%d loops each)" % loops)

for title, mod, enc, dec in tests:
    print(title)

    print("  [Encode]", enc, type(enc))
    result = timeit(stmt=enc, setup=mod, number=loops)
    enc_table.append([title, result, loops/result])

    print("  [Decode]", dec, type(dec))
    result = timeit(stmt=dec, setup=mod, number=loops)
    result = 1
    dec_table.append([title, result, loops/result])

enc_table.sort(key=lambda x: x[1])
enc_table.insert(0, ['Package', 'Seconds', 'msg/sec'])

dec_table.sort(key=lambda x: x[1])
dec_table.insert(0, ['Package', 'Seconds', 'msg/sec'])

print("\nEncoding Test (%d loops)" % loops)
print(tabulate(enc_table, headers="firstrow"))

print("\nDecoding Test (%d loops)" % loops)
print(tabulate(dec_table, headers="firstrow"))