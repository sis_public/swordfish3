# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
try:
  from setuptools import setup, Extension
except ImportError:
  from distutils.core import setup, Extension

setup(name="proto_wrapper",
      version="1.0",
      packages=[

      ],
      package_dir={

      },
      ext_modules=[

          Extension("performance", ["test.cc", "test.pb.cc"], libraries=['protobuf']),

      ],
      test_suite="test.suite")