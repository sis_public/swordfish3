# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Mon Jul  6 12:51:09 2015

@author: tvzyl
"""
from swordfish import debug_to_console
from swordfish.sink import SinkProcessor, Print, Drain
from swordfish.source import Tap
from threading import Event
from swordfish.multi import MultiProcess
from swordfish import multi
from swordfish.core import AttrDict, DefaultStream, _
from swordfish.streamProcessor import swordfishfunc, map, filter, fold, reduce, join
from swordfish.streamProcessor import LEFT, RIGHT
from swordfish.window import within, WINDOW, IntervalWindow
from swordfish.stream import KombuStream, MQTTStream, ThreadBroadcastStream, ThreadSplitStream, ZMQStream

import time
import timeit
from msgpack import dumps
from functools import reduce

#debug_to_console()

@swordfishfunc
def mapping(addmsg=None):
    return addmsg

class Timer(SinkProcessor):
    def __init__(self, total, stop_msg, end_event, *args, **kwargs):
        self.total = total
        self.end_event = end_event
        self.stop_msg = stop_msg
        self.count = 0
        SinkProcessor.__init__(self, *args, **kwargs)

    def process(self, message, stream_id):
        #print message
        if self.count == 0:
            self.start_time = time.time()
        self.count += 1
        if self.total == self.count:
            self.end_time = time.time()
            print("Finished Timing %s msgs in %s secs: %s msgs/sec"%(self.count, self.end_time-self.start_time, self.count/(self.end_time-self.start_time) ))
            if self.stop_msg is not None :
                if message==self.stop_msg:
                    print("Verified Last message: Passed")
                else:
                    print("Verified Last message: Failed", self.stop_msg, message)
            self.end_event.set()
        return None

test_size=50000
msg = {"type": "CF_SimpleObservation","parameter":{"id": "%s"},"phenomenonTime": "2015-09-14T11:58:58.649575", "procedure": {"type":"sensor","id": "45030171","description": "Sensus HRI-Mei linked to meter 14787486"},"featureOfInterest": {"type": "Feature","geometry": {"type":"Point", "coordinates":[-25.753, 28.280]},"properties":{"id":"Top Reservoir"} },"observedProperty": {"type": "TimeSeries"},"result": {"dimensions":{"time":5},"variables":{"time":{"dimensions":["time"],"units":"isoTime"},"pulse_value":{"dimensions":["int"],"units":"litres"}},"data":{"time":["2015-09-14T11:58:01.305564", "2015-09-14T11:47:06.808586", "2015-09-14T11:54:55.782008", "2015-09-14T11:43:58.603956", "2015-09-14T11:50:58.623827"], "pulse_value":[500, 500, 500, 0, 500]}}}
#msg = {1:1}
#msgs = [AttrDict({"o%s"%(i+10000):{'a':[i,i,i,i], 'b':'b'*1000}}) for i in xrange(1,test_size+1)]
#msgs = [AttrDict({"o%s"%(i+10000):i}) for i in xrange(1,test_size+1)]
#msgs = [AttrDict({"o":0}) for i in xrange(1,test_size+1)]
msgs = [AttrDict( eval(str(msg)%i) )  for i in range(1,test_size+1)]
#msgs = [AttrDict(msg)  for i in xrange(1,test_size+1)]
for i, msg in enumerate(msgs): msg.parameter.id = i
for msg in msgs: msg.freeze()
#msgs = [dumps(AttrDict({"o":0})) for i in xrange(1,test_size+1)]

setup="""
from copy import deepcopy
from json import loads, dumps
from swordfish.core import AttrDict
from UserDict import UserDict
#d={"o%s"%(10000):{'a':[10000,10000,10000,10000], 'b':'b'*1000}}
d = {"type": "CF_SimpleObservation","parameter":{"id": "fb600634-5ad7-11e5-945f-65303a4d6e97"},"phenomenonTime": "2015-09-14T11:58:58.649575", "procedure": {"type":"sensor","id": "45030171","description": "Sensus HRI-Mei linked to meter 14787486"},"featureOfInterest": {"type": "Feature","geometry": {"type":"Point", "coordinates":[-25.753, 28.280]},"properties":{"id":"Top Reservoir"} },"observedProperty": {"type": "TimeSeries"},"result": {"dimensions":{"time":5},"variables":{"time":{"dimensions":["time"],"units":"isoTime"},"pulse_value":{"dimensions":["int"],"units":"litres"}},"data":{"time":["2015-09-14T11:58:01.305564", "2015-09-14T11:47:06.808586", "2015-09-14T11:54:55.782008", "2015-09-14T11:43:58.603956", "2015-09-14T11:50:58.623827"], "pulse_value":[500, 500, 500, 0, 500]}}}
a=AttrDict(d)
u=UserDict(d)
class O10000():
    a = []
class O():
    o10000 = 0
o = O()
o10000 = O10000()
o10000.a = [10000, 10000, 10000, 10000]
o.o10000 = o10000
"""
"""
print "deepcopy dict"
res = timeit.timeit("deepcopy(d)", setup, number=100000)
print "Finished Timing %s msgs in %s secs: %s msgs/sec"%(100000, res, 100000/res )

print "deepcopy Attrdict"
res = timeit.timeit("deepcopy(a)", setup, number=100000)
print "Finished Timing %s msgs in %s secs: %s msgs/sec"%(100000, res, 100000/res )

print "deepcopy Userdict"
res = timeit.timeit("deepcopy(u)", setup, number=10000)
print "Finished Timing %s msgs in %s secs: %s msgs/sec"%(10000, res, 10000/res )

print "json"
res = timeit.timeit("loads(dumps(a))", setup, number=100000)
print "Finished Timing %s msgs in %s secs: %s msgs/sec"%(100000, res, 100000/res )
"""

print("streamprocessor")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
t = Timer(test_size, msgs[-1], end_event)
ks>>t
for msg in msgs:
    t._enqueue_message(msg)
end_event.wait()
ks.close()
t.close()

print("ds")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
ks>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

print("ds-ds")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
kt = DefaultStream('tbs://localhost', 1)
ks|kt
kt>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()
kt.close()

print("tbs")
end_event = Event()
ks = ThreadBroadcastStream('tbs://localhost', 1)
ks>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

print("tss")
end_event = Event()
ks = ThreadSplitStream("tss://localhost",1)
ks>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

print("ds-filter")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
ks>>list(filter(_.type == "CF_SimpleObservation"))>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

print("ds-api.map")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
ks>>list(map('_'))>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

print("ds-api.map(getset)")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
ks>>list(map('_', type='type'))>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

print("ds-api.map(func)")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
ks>>list(map(_=mapping))>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

print("ds-api.fold")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
ks>>fold(['parameter.id'], '_')>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

print("ds-api.reduce")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
ks>>reduce(['parameter.id'], '_')>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

print("ds-api.join")
end_event = Event()
ksl = DefaultStream('tbs://localhost', 1)
ksr = DefaultStream('tbs://localhost', 1)
j = join( LEFT & RIGHT, ON=(['parameter.id'], ['parameter.id']) ) %within% WINDOW[:1:][:1:]
(ksl, ksr)>>j>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ksl(msg)
    ksr(msg)
end_event.wait()
ksl.close()
ksr.close()

print("ds-sink")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
ks>>Drain()>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

print("ds-multi.StreamProcessor")
def run():
    end_event = Event()
    t = Timer(test_size, msgs[-1], end_event)
    return locals()
with MultiProcess(target=run) as mp,\
     DefaultStream('tbs://localhost', 1) as ks:
    ks>>mp.t
    for msg in msgs:
        ks(msg)
    mp.end_event.wait()

print("ds-mult.sink")
end_event = Event()
with DefaultStream('tbs://localhost', 1)  as ks,\
     multi.sink.Drain() as d, \
     Timer(test_size, msgs[-1], end_event) as t:
    ks>>d>>t
    for msg in msgs:
        ks(msg)
    end_event.wait()

print("ds-multi.map")
def run():
    end_event = Event()
    f = list(map('_'))
    t = Timer(test_size, msgs[-1], end_event)
    return locals()
with MultiProcess(target=run) as mp,\
     MultiProcess(target=run) as mq,\
     MultiProcess(target=run) as mr,\
     MultiProcess(target=run) as ms,\
     MultiProcess(target=run) as mt,\
     MultiProcess(target=run) as mu,\
     ThreadBroadcastStream('tbs://localhost', 1) as ks, \
     multi.api.map('_') as f:
    ks>>mp.f>>mp.t
    ks>>mq.f>>mq.t
    ks>>mr.f>>mr.t
    ks>>ms.f>>ms.t
    ks>>mt.f>>mt.t
    ks>>mu.f>>mu.t
    for msg in msgs:
        ks(msg)
    mp.end_event.wait()
    mq.end_event.wait()
    mr.end_event.wait()
    ms.end_event.wait()
    mt.end_event.wait()
    mu.end_event.wait()

print("ds-multi.map(in/out)")
end_event = Event()
with DefaultStream('tbs://localhost', 1) as ks, \
     multi.api.map('_') as f,\
     Timer(test_size, msgs[-1], end_event) as t:
    ks>>f>>t
    for msg in msgs:
        ks(msg)
    end_event.wait()

print("ds-multi.fold")
end_event = Event()
with DefaultStream('tbs://localhost', 1) as ks, \
     multi.api.fold(['parameter.id'],_=mapping) as f,\
     Timer(test_size, msgs[-1], end_event) as t:
    ks>>f>>t
    for msg in msgs:
        ks(msg)
    end_event.wait()

print("ds-multi.filter")
end_event = Event()
with DefaultStream('tbs://localhost', 1) as ks, \
     multi.api.filter(lambda _: True) as f,\
     Timer(test_size, msgs[-1], end_event) as t:
    ks>>f>>t
    for msg in msgs:
        ks(msg)
    end_event.wait()

print("ds-multi.reduce")
end_event = Event()
with DefaultStream('tbs://localhost', 1) as ks, \
     multi.api.reduce(['parameter.id'], '_') as f,\
     Timer(test_size, msgs[-1], end_event) as t:
    ks>>f>>t
    for msg in msgs:
        ks(msg)
    end_event.wait()

print("ds-multi(api.map)-multi(sink)")
end_event = Event()
def run():
    m=list(map(_=mapping))
    d=Drain()
    return locals()
with MultiProcess(target=run) as mp,\
     DefaultStream('tbs://localhost', 1) as ks,\
     Timer(test_size, msgs[-1], end_event) as t:
    ks>>mp.m>>mp.d>>t
    for msg in msgs:
        ks(msg)
    end_event.wait()

#print "ds-[multi.filter]"
#end_event = Event()
#ks = DefaultStream('tbs://localhost', 1)
#kt = multi.stream.ThreadSplitStream('tbs://localhost', 1)
#fs = []
#fs.append(multi.api.filter(_.parameter.id < 2500))
#fs.append(multi.api.filter((_.parameter.id >= 2500) & (_.parameter.id < 5000)))
#fs.append(multi.api.filter((_.parameter.id >= 5000) & (_.parameter.id < 7500)))
#fs.append(multi.api.filter((_.parameter.id >= 7500) & (_.parameter.id < 10000)))
#fs.append(multi.api.filter((_.parameter.id >= 10000) & (_.parameter.id < 12500)))
#fs.append(multi.api.filter((_.parameter.id >= 12500) & (_.parameter.id < 15000)))
#fs.append(multi.api.filter((_.parameter.id >= 15000) & (_.parameter.id < 17500)))
#fs.append(multi.api.filter(_.parameter.id >= 17500))
#t = Timer(test_size, msgs[-1], end_event)
#for f in fs:
#    (ks|kt)>>f>>t
#for msg in msgs:
#    ks(msg)
#end_event.wait()
#for f in fs:
#    f.close()
#kt.close()
#ks.close()
#t.close()

print("ds-[api.filter]")
end_event = Event()
ks = DefaultStream('tbs://localhost', 1)
t = Timer(test_size, msgs[-1], end_event)
ks>>list(filter(_.parameter.id < 2500))>>t
ks>>list(filter((_.parameter.id >= 2500) & (_.parameter.id < 5000)))>>t
ks>>list(filter((_.parameter.id >= 5000) & (_.parameter.id < 7500)))>>t
ks>>list(filter((_.parameter.id >= 7500) & (_.parameter.id < 10000)))>>t
ks>>list(filter((_.parameter.id >= 10000) & (_.parameter.id < 12500)))>>t
ks>>list(filter((_.parameter.id >= 12500) & (_.parameter.id < 15000)))>>t
ks>>list(filter((_.parameter.id >= 15000) & (_.parameter.id < 17500)))>>t
ks>>list(filter(_.parameter.id >= 17500))>>t
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()
t.close()

print("tbs-[api.multi(filter)]")
end_event = Event()
ks = ThreadBroadcastStream('tbs://localhost', 1)
t = Timer(test_size, msgs[-1], end_event)
ks>>multi.api.filter(_.parameter.id >= 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
ks>>multi.api.filter(_.parameter.id < 0)>>t
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()
t.close()

print("ds-multi.tbs")
end_event = Event()
with DefaultStream() as ds, \
     multi.stream.ThreadBroadcastStream("", 0.0) as ks, \
     Timer(test_size, msgs[-1], end_event) as t:
    (ds|ks)>>t
    for msg in msgs:
        ds(msg)
    end_event.wait()

print("source-multi.tbs")
end_event = Event()
with Tap() as ds, \
     multi.stream.ThreadBroadcastStream("", 0.0) as ks, \
     Timer(test_size, msgs[-1], end_event) as t:
    ds>>ks>>t
    for msg in msgs:
        ds._enqueue_message(msg)
    end_event.wait()

print("ds-mqtt")
end_event = Event()
with MQTTStream('mqtt://yak.dhcp.meraka.csir.co.za:1883/', '/performance') as ksl,\
     multi.stream.MQTTStream('mqtt://yak.dhcp.meraka.csir.co.za:1883/', '/performance') as ksr:
    ksr>>Timer(test_size, msgs[-1], end_event)
    for msg in msgs:
        ksl(msg)
    end_event.wait()

print("tss-tbs")
end_event = Event()
ss  = ThreadSplitStream("tss://localhost",1)
ks1 = ThreadBroadcastStream("tbs://localhost",1)
ks2 = ThreadBroadcastStream("tbs://localhost",1)
ks3 = ThreadBroadcastStream("tbs://localhost",1)
ks4 = ThreadBroadcastStream("tbs://localhost",1)
t = Timer(test_size, msgs[-1], end_event)
(ss|ks1)>>t
(ss|ks2)>>t
(ss|ks3)>>t
for msg in msgs:
    ss(msg)
end_event.wait()
t.close()
ss.close()
ks1.close()
ks2.close()
ks3.close()
ks4.close()

print("mqtt")
end_event = Event()
#ks = MQTTStream('mqtt://yak.dhcp.meraka.csir.co.za:1883/', '/performance')
ks = MQTTStream('mqtt://localhost:1883/', '/p')
ks>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()

#serializer='json'
#print "amqp-"+serializer
#end_event = Event()
#ks = KombuStream('amqp://csircampus:campuscsir@yak.dhcp.meraka.csir.co.za:5672/csircampus', 1, 'performance', serializer=serializer)
#ks>>Timer(test_size, msgs[-1], end_event)
#ks>>Drain()
#for msg in msgs:
#    ks(msg)
#end_event.wait()
#ks.close()

#serializer='msgpack'
#print "amqp-"+serializer
#end_event = Event()
#ks = KombuStream('amqp://csircampus:campuscsir@yak.dhcp.meraka.csir.co.za:5672/csircampus', 1, 'performance', serializer=serializer)
#ks>>Timer(test_size, msgs[-1], end_event)
#for msg in msgs:
#    ks(msg)
#end_event.wait()
#ks.close()
#
#print "redis-"+serializer
#end_event = Event()
#ks = KombuStream('redis://localhost/', 1, 'performance', serializer=serializer)
#ks>>Timer(test_size, msgs[-1], end_event)
#for msg in msgs:
#    ks(msg)
#end_event.wait()
#ks.close()

"""
print "mem-"+serializer
end_event = Event()
ks = KombuStream('memory://localhost/csircampus', 1, 'performance', serializer=serializer)
ks>>Timer(test_size, msgs[-1], end_event)
for msg in msgs:
    ks(msg)
end_event.wait()
ks.close()
"""

#from swordfish.sink import MongoDB
#print "mongodb"
#end_event = Event()
#ks = DefaultStream('redis://localhost/', 1, 'performance')
#md = MongoDB(clienturi="mongodb://localhost:27017/", database="test", collection="test")
#ks>>md>>Timer(test_size, msgs[-1], end_event)
#for msg in msgs:
#    ks(msg)
#end_event.wait()
#ks.close()
#


##############################################################################
#OLD TESTS
"""
#end_event = Event()
ks = MQTTStream('mqtt://yak.dhcp.meraka.csir.co.za:1883/', '/performance')
#i = 0
#def callback(msg):
#    global i
#    global test_size
#    i += 1
#    if i == test_size:
#        end_event.set()
#ks.__attach__( callback, callback )
#start_time = time.time()
#publish=ks.mqtt_producer.publish
#topic=ks.publish_topic
#for message in msgs:
#    publish(topic=topic, payload=dumps(message))
#end_event.wait()
#
#end_time = time.time()
#print test_size/(end_time-start_time)
i = 0
start_time = time.time()
for message in msgs:
    ks(message)
#end_event.wait()
end_time = time.time()
print test_size/(end_time-start_time)
ks.close()
"""

#print "deque"
#setup="""
#from collections import deque
#from swordfish.core import AttrDict
#test_size=10000
#q=deque()
#put = q.append
#get = q.popleft
#msgs = [AttrDict({"o%s"%(i+10000):{'a':[i,i,i,i]}}) for i in xrange(1,test_size+1)]
##msgs = [AttrDict({"o%s"%(i+10000):i}) for i in xrange(1,test_size+1)]
#"""
#stmt="""
#for msg in msgs:
#    put(msg)
#for i in xrange(test_size):
#    get()
#"""
#res = timeit.timeit(stmt, setup, number=10)
#print "Finished Timing %s msgs in %s secs: %s msgs/sec"%(test_size, res/10.0, test_size/(res/10.0) )

#print "queue"
#setup="""
#from Queue import Queue
#from swordfish.core import AttrDict
#test_size=10000
#q=Queue()
#put = q.put_nowait
#get = q.get_nowait
#msgs = [AttrDict({"o%s"%(i+10000):{'a':[i,i,i,i]}}) for i in xrange(1,test_size+1)]
##msgs = [AttrDict({"o%s"%(i+10000):i}) for i in xrange(1,test_size+1)]
#"""
#stmt="""
#for msg in msgs:
#    put(msg)
#for i in xrange(test_size):
#    get()
#"""
#res = timeit.timeit(stmt, setup, number=10)
#print "Finished Timing %s msgs in %s secs: %s msgs/sec"%(test_size, res/10.0, test_size/(res/10.0) )