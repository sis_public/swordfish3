# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
import argparse
from swordfish.stream import PostgresPollingTableStream
from swordfish.sink import Print


def stream_pg(conf):
    fm = {
        'fire_events_id':'fe_id',
        'active_fire_id':'af_id',
        'ST_ASGEOJSON(point_location)':'location',
        'observed_datetime':'acqdatetime',
        'observed_radiative_power':'frp',
        'study_region': 'study_region',
        'fire_ecotype_classification': 'fec',
        'forecast_fire_danger_index': 'ffdi',
        'days_since_last_burn': 'dslb',
        'accumulated_evi':'aevi'
    }
    pv = Print("The Value: ")
    #in below setup code, offset refers to the number of records needed:
    #   -1 is all, 1 is latest record, x is latest x records
    connect = f"dbname={conf.database} user={conf.user} " + \
        f"password={conf.password} host={conf.server} port={conf.port}"
    ppts = PostgresPollingTableStream(
        connect,
        1,
        'fire_events',
        poll_freq=5000,
        offset=10,
        orderby='fire_events_id',
        field_mapping=fm)

    ppts>>pv


def parse_args():
    """Create the parser & parse args"""
    parser = argparse.ArgumentParser(description='...')
    parser.add_argument(
        '-u', '--user',
        type=str,
        default='admin',
        help='Supply the user name (default: admin)')
    parser.add_argument(
        '-w', '--password',
        type=str,
        default='admin',
        help='Supply the user password (default: admin)')
    parser.add_argument(
        '-p', '--port',
        type=str,
        default='5433',
        help='Supply the port number (default: 5433)')
    parser.add_argument(
        '-d', '--database',
        type=str,
        default='gisdb',
        help='Supply the database name (default: gisdb)')
    parser.add_argument(
        '-s', '--server',
        type=str,
        default='localhost',
        help='Supply the server/host name (default: localhost)')
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    conf = parse_args()
    stream_pg(conf)
