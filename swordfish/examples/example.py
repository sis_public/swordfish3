# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Mon Feb 24 12:58:59 2014

@author: tvzyl
"""
"""
from Stream import StreamProcessorInc, ThreadStream
from StreamProcessor import RollingMap, Map, InnerJoin, StreamPrint, SUM, CountWin

ts = ThreadStream()

s1 = StreamProcessorInc(debug_name='s1', default_input_stream=ts)
s2 = StreamProcessorInc(debug_name='s2')
s3 = StreamProcessorInc(debug_name='s3')

s1>>s2>>s3

s1.default_stream(1)

r = RollingMap( funcs={SUM('measurements.rainfall'):'measurements.rainfall_003'}, win=CountWin( span=3 ), append=True )
s = RollingMap( funcs={SUM('measurements.rainfall'):'measurements.rainfall_004'}, win=CountWin( span=4 ), append=False )

mySpecialForecast = lambda message: 3
m = Map( funcs={mySpecialForecast:'measurements.ph_forecast'} )

(r,s)>>InnerJoin()>>m>>StreamPrint(debug_name="Results")


r.default_stream('{"measurements": {"rainfall": 10}}') #10
r.default_stream('{"measurements": {"rainfall": 20}}') #30
r.default_stream('{"measurements": {"rainfall": 30}}') #60
r.default_stream('{"measurements": {"rainfall": 40}}') #90

s.default_stream('{"measurements": {"rainfall": 10}}') #10
s.default_stream('{"measurements": {"rainfall": 20}}') #30
s.default_stream('{"measurements": {"rainfall": 30}}') #60
s.default_stream('{"measurements": {"rainfall": 40}}') #100
"""

"""
Forecast Ph
"""

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from swordfish.streamProcessor import *
import json
import pandas
import time
import locale
from threading import Thread
import copy
from swordfish import sink
from swordfish import source
# not found?
from rolling import SUM
from transform import PREVIOUS, AS_GEOJSON_FEATURE

locale.setlocale(locale.LC_ALL, 'en_GB.UTF-8')

phStream = sink.Drain()
trmmStream = sink.Drain()
learner_features=['index','ph_target', 'conductivity_target', 'sulphate_target', 'manganese_target', 'uranium_target', 'sodium_target', 'iron_target', 'rainfall_1', 'rainfall_14', 'rainfall_154', 'rainfall_365']

trmm_roll_001 = RollingMap( SUM('rainfall_1'),'measurements.rainfall_1', CountWin( span=1  ), append=False )
trmm_roll_014 = RollingMap( SUM('measurements.rainfall_1'),'measurements.rainfall_14', CountWin( span=14  ), append=True )
trmm_roll_154 = RollingMap( SUM('measurements.rainfall_1'),'measurements.rainfall_154', CountWin( span=154 ), append=True )
trmm_roll_365 = RollingMap( SUM('measurements.rainfall_1'),'measurements.rainfall_365', CountWin( span=365 ), append=True )

trmmStream>>trmm_roll_001>>trmm_roll_014>>trmm_roll_154>>trmm_roll_365

inner_join = InnerJoin( TRUE(), CountWin( span=1 ), debug_name='inner_join')

(trmm_roll_365, phStream)>>inner_join

debug_stream = sink.Print(debug_name='debug')

mySpecialForecast = lambda message: copy.deepcopy(rfr_estimator).predict( pandas.read_json(json.dumps( [message.measurements] ) )[learner_features[1:]] )[0]
forecast_ph = Map( func=mySpecialForecast, ret_val_name='measurements.ph_forecast', append=True )
prev_maps=Map( func=PREVIOUS(), ret_val_name='', append=False )

inner_join>>forecast_ph
forecast_ph>>prev_maps

result_stream=InnerJoin( TRUE(), CountWin( span=1 ))

(inner_join, prev_maps)>>result_stream

sp = sink.Plotter(['measurements.ph_forecast','measurements.ph_target'], plt, animation, xticks='measurements.index', num=None)
sp.ax.set_ylim(2, 10)
sp.ax.set_title('Acid Mine Drainage')
sp.ax.set_xlabel('time elapsed (bi-weekly)')
sp.ax.set_ylabel('ph')
sp.plt.setp( sp.plt.xticks()[1], rotation=20 )

result_stream>>sp

#websocket_stream_local = swordfish.sink.WebSocket('ws://localhost:9090')
#websocket_stream_graeme = sink.WebSocket('ws://gmcferren-nb4.dhcp:8000')

def simulate_stream():
    while True:
        locale.setlocale(locale.LC_ALL, 'en_GB.UTF-8')
        sim_stream_hist = X_train.reset_index( level=0 )
        sim_stream_real = X_test.reset_index( level=0 )
        for o in eval(sim_stream_hist[['rainfall_1']][-366:].to_json(orient='records',date_format='iso')):
            trmmStream.default_stream(o)
        for i in range(65,80):
            o = sim_stream_real[['rainfall_1']][i:i+1].to_json(orient='records',date_format='iso')[1:-1]
            p = AttrDict({})
            p.measurements = eval(sim_stream_real[learner_features[0:8]][i:i+1].to_json(orient='records',date_format='iso')[1:-1])
            trmmStream.default_stream(o)
            p.type = "amd_ph"
            p.source = {"id":"HippoPool","type":"sampling"}
            p.location = {"type":"Point", "coordinates": [ 27.72108, -26.09919 ]}
            p.timestamp = p.measurements.index
            phStream.default_stream(p)
            time.sleep(4)


anomaly_func = lambda message: {"ph_target":message.measurements.ph_target, "ph_forecast": message.measurements.ph_forecast} if abs(message.measurements.ph_forecast - message.measurements.ph_target) > 0.5 else None
anomaly = Map( func=anomaly_func, ret_val_name='alert', append=True )

result_stream>>anomaly
anomaly>>debug_stream
#anomaly<>debug_stream

as_geojson_feature = Map( func=AS_GEOJSON_FEATURE(), ret_val_name='', append=False )

anomaly>>as_geojson_feature
#result_stream>>as_geojson_feature

#as_geojson_feature>>websocket_stream_graeme
#as_geojson_feature>>websocket_stream_local

t = Thread(target=simulate_stream)
t.start()
