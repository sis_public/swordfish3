# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Tue Jun 23 12:17:06 2015
@author: tvzyl


THIS IS THE NEW FORM OF THE OPERATION:

stream_processor >> stream_processor
stream_processor >> stream
stream >> stream_procesor

source_processor <= stream_processor >= sink_processor
source_processor <= stream >= sink_processor

stream | stream
"""

import os
import logging
from swordfish.core import _, AttrDict
from swordfish.streamProcessor import map, fold, reduce, join, LEFT, RIGHT, const, null, empty, filter
from swordfish.sink import Print, Drain
from swordfish.source import IterableSource
from swordfish.window import within, WINDOW, IntervalWindow
from swordfish.streamProcessor import swordfishfunc
from swordfish import debug_to_console, info_to_console
import time
from datetime import timedelta
from swordfish.examples import data
from dateutil.parser import parse

from swordfish.amqp_messaging import KombuStream

logger = logging.getLogger('swordfish')
logger.setLevel(logging.DEBUG)
#debug_to_console()

s1 = IterableSource()
s2 = IterableSource()
p = Print('Output')
d = Drain()

maximum = lambda addmsg=None, oldval=None: max(addmsg, oldval)
maximum.__name__ = 'maximum'
maximum = swordfishfunc(maximum)

incrementer = lambda addmsg=None: int.__add__(1, addmsg)
incrementer.__name__ = 'inc'
incrementer = swordfishfunc(incrementer)

def buildlist(thelist):
    def func(addmsg=None):
        thelist.append(addmsg)
    return func

@swordfishfunc
def assertmessage(assertlist, addmsg=None):
    assert(addmsg in assertlist)
    assertlist.remove(addmsg)
    return addmsg

@swordfishfunc
def exception(addmsg=None):
    return 1.0/0.0

@swordfishfunc
def convolution(oldval=0, addmsg=None, delmsg=None, win=None, span=1, rate=1):
    delta_add = 0.0
    delta_del = 0.0
    if addmsg is not None:
        delta_add = addmsg
    if delmsg is not None:
        delta_del = delmsg
    return oldval - delta_del + delta_add

class windowsum:
    #@swordfishproperty
    def sum(self, oldval=0.0, addmsg=0.0, delmsg=0.0):
        return oldval - delmsg + addmsg

import math
@swordfishfunc
def floor(addmsg=None):
    return math.floor(addmsg)

#s1>>map(exception=exception['attr.error'])>>p
#s1.setStream(data.msgs)
#time.sleep(1)
#s1.clear()
#p.clear()
#print("")

al = [AttrDict(i) for i in [
{'i': 0, 'type': 1, 'id': 1, 'attr': {'temp': 25, 'error': 0}},
{'i': 1, 'type': 1, 'id': 2, 'attr': {'temp': 20, 'error': 0}},
{'i': 2, 'type': 1, 'id': 3, 'attr': {'temp': 15, 'error': 0}},
{'i': 3, 'type': 1, 'id': 4, 'attr': {'temp': 12, 'error': 1}},
{'i': 4, 'type': 1, 'id': 2, 'attr': {'temp': 21, 'error': 0}},
{'i': 5, 'type': 1, 'id': 3, 'attr': {'temp': 15, 'error': 0}},
{'i': 6, 'type': 1, 'id': 4, 'attr': {'temp': 11, 'error': 0}},
{'i': 7, 'type': 1, 'id': 4, 'attr': {'temp': 10, 'error': 0}},
{'i': 8, 'type': 1, 'id': 4, 'attr': {'temp': 10, 'error': 0}},
{'i': 9, 'type': 1, 'id': 5, 'attr': {'temp': 10, 'error': 0}}]]
a = assertmessage(al)
alist = []
b = buildlist(alist)
s1 >> list(map('_')) >> a['_'] >> p >> b >> Drain()
s1.setStream(data.msgs)
time.sleep(1)
s1.clear()
p.clear()
assert(len(al)==0)
print("")

al = [AttrDict(i) for i in [
{'id': 1, 'attr': {'temp': {'value': 25}}, 'temp': 25, 'inc': {'id': 2}},
{'id': 2, 'attr': {'temp': {'value': 20}}, 'temp': 20, 'inc': {'id': 3}},
{'id': 3, 'attr': {'temp': {'value': 15}}, 'temp': 15, 'inc': {'id': 4}},
{'id': 4, 'attr': {'temp': {'value': 12}}, 'temp': 12, 'inc': {'id': 5}},
{'id': 2, 'attr': {'temp': {'value': 21}}, 'temp': 21, 'inc': {'id': 3}},
{'id': 3, 'attr': {'temp': {'value': 15}}, 'temp': 15, 'inc': {'id': 4}},
{'id': 4, 'attr': {'temp': {'value': 11}}, 'temp': 11, 'inc': {'id': 5}},
{'id': 4, 'attr': {'temp': {'value': 10}}, 'temp': 10, 'inc': {'id': 5}},
{'id': 4, 'attr': {'temp': {'value': 10}}, 'temp': 10, 'inc': {'id': 5}},
{'id': 5, 'attr': {'temp': {'value': 10}}, 'temp': 10, 'inc': {'id': 6}}]]
a = assertmessage(al)
s1>>list(map('id',
        incrementer['id'],
        {'attr.temp.value':'attr.temp'},
        temp='attr.temp'
        ))>>a['_']>>p
s1.setStream(data.msgs)
time.sleep(1)
s1.clear()
p.clear()
assert(len(al)==0)
print("")

al = [AttrDict(i) for i in [
{'i': {'j': 0}, 'type': None, 'id': {'id': {}}, 'attr': 5},
{'i': {'j': 0}, 'type': None, 'id': {'id': {}}, 'attr': 5},
{'i': {'j': 0}, 'type': None, 'id': {'id': {}}, 'attr': 5},
{'i': {'j': 0}, 'type': None, 'id': {'id': {}}, 'attr': 5},
{'i': {'j': 0}, 'type': None, 'id': {'id': {}}, 'attr': 5},
{'i': {'j': 0}, 'type': None, 'id': {'id': {}}, 'attr': 5},
{'i': {'j': 0}, 'type': None, 'id': {'id': {}}, 'attr': 5},
{'i': {'j': 0}, 'type': None, 'id': {'id': {}}, 'attr': 5},
{'i': {'j': 0}, 'type': None, 'id': {'id': {}}, 'attr': 5},
{'i': {'j': 0}, 'type': None, 'id': {'id': {}}, 'attr': 5}]]
a = assertmessage(al)
s1>>list(map({'id.id':empty},
        {'attr':const(5)},
        {'type':null},
        {'i':const({'j':0})}
        ))>>a['_']>>p
s1.setStream(data.msgs)
time.sleep(1)
s1.clear()
p.clear()
assert(len(al)==0)
print("")

al = [AttrDict(i) for i in [
{'car': None, 'foo': None, 'bar': None, 'tar': 'bla', 'jar': 25},
{'car': None, 'foo': None, 'bar': None, 'tar': 'bla', 'jar': 20},
{'car': None, 'foo': None, 'bar': None, 'tar': 'bla', 'jar': 15},
{'car': None, 'foo': None, 'bar': None, 'tar': 'bla', 'jar': 12},
{'car': None, 'foo': None, 'bar': None, 'tar': 'bla', 'jar': 21},
{'car': None, 'foo': None, 'bar': None, 'tar': 'bla', 'jar': 15},
{'car': None, 'foo': None, 'bar': None, 'tar': 'bla', 'jar': 11},
{'car': None, 'foo': None, 'bar': None, 'tar': 'bla', 'jar': 10},
{'car': None, 'foo': None, 'bar': None, 'tar': 'bla', 'jar': 10},
{'car': None, 'foo': None, 'bar': None, 'tar': 'bla', 'jar': 10}]]
a = assertmessage(al)
s1>>list(map(
        {'foo':"attr.k~"},
        {'bar':"attr.k*"},
        {'car':"attr.i.k?"},
        {'tar':"attr.i.k?bla"},
        {'jar':"temp.attr|error.attr|attr.temp"},
        ))>>a['_']>>p
s1.setStream(data.msgs)
time.sleep(1)
s1.clear()
p.clear()
assert(len(al)==0)
print("")


al = [AttrDict(i) for i in [
{'car': 0, 'foo': 1, 'bar': -1, 'tar': True, 'jar': 0},
{'car': 2, 'foo': 3, 'bar': -1, 'tar': True, 'jar': 1},
{'car': 6, 'foo': 5, 'bar': -1, 'tar': True, 'jar': 8},
{'car': 12, 'foo': 7, 'bar': -1, 'tar': True, 'jar': 81},
{'car': 8, 'foo': 6, 'bar': 2, 'tar': False, 'jar': 16},
{'car': 15, 'foo': 8, 'bar': 2, 'tar': False, 'jar': 125},
{'car': 24, 'foo': 10, 'bar': 2, 'tar': False, 'jar': 1296},
{'car': 28, 'foo': 11, 'bar': 3, 'tar': False, 'jar': 2401},
{'car': 32, 'foo': 12, 'bar': 4, 'tar': False, 'jar': 4096},
{'car': 45, 'foo': 14, 'bar': 4, 'tar': False, 'jar': 59049}]]
a = assertmessage(al)
s1>>list(map(
        {'foo':_.i + _.id},
        {'bar':_.i - _.id},
        {'car':_.i * _.id},
        {'tar':_.i.__in__([0,1,2,3]) },
        {'jar':_.i ** _.id},
        ))>>a['_']>>p
s1.setStream(data.msgs)
time.sleep(1)
s1.clear()
p.clear()
assert(len(al)==0)
print("")

print ("starting fold")
al = [AttrDict(i) for i in [
{'max_error': 0, 'id': 1, 'attr': {'temp': {'max': 25}}, 'convolution': {'attr': {'temp': 25.0}}},
{'max_error': 0, 'id': 2, 'attr': {'temp': {'max': 25}}, 'convolution': {'attr': {'temp': 45.0}}},
{'max_error': 0, 'id': 3, 'attr': {'temp': {'max': 25}}, 'convolution': {'attr': {'temp': 60.0}}},
{'max_error': 1, 'id': 4, 'attr': {'temp': {'max': 25}}, 'convolution': {'attr': {'temp': 72.0}}},
{'max_error': 1, 'id': 2, 'attr': {'temp': {'max': 25}}, 'convolution': {'attr': {'temp': 93.0}}},
{'max_error': 1, 'id': 3, 'attr': {'temp': {'max': 25}}, 'convolution': {'attr': {'temp': 108.0}}},
{'max_error': 1, 'id': 4, 'attr': {'temp': {'max': 25}}, 'convolution': {'attr': {'temp': 119.0}}},
{'max_error': 1, 'id': 4, 'attr': {'temp': {'max': 25}}, 'convolution': {'attr': {'temp': 129.0}}},
{'max_error': 1, 'id': 4, 'attr': {'temp': {'max': 25}}, 'convolution': {'attr': {'temp': 139.0}}},
{'max_error': 1, 'id': 5, 'attr': {'temp': {'max': 25}}, 'convolution': {'attr': {'temp': 149.0}}}]]
a = assertmessage(al)
s1>>fold(['_'],
        convolution['attr.temp'],
        'id',
        {'attr.temp.max':maximum['attr.temp']},
        max_error=maximum['attr.error']
        )>>a['_']>>p
s1.setStream(data.msgs)
time.sleep(1)
s1.clear()
p.clear()
assert(len(al)==0)
print ("finished fold")
print("")

#s1>>fold([], exception=exception['attr.error'])>>p
#s1.setStream(data.msgs)
#time.sleep(1)
#p.close()
#print("")

al = [AttrDict(i) for i in [
{'max_error': 0, 'convolution': {'attr': {'temp': 25.0}}, 'temp': 25, 'attr': {'max_temp': 25}},
{'max_error': 0, 'convolution': {'attr': {'temp': 20.0}}, 'temp': 20, 'attr': {'max_temp': 20}},
{'max_error': 0, 'convolution': {'attr': {'temp': 15.0}}, 'temp': 15, 'attr': {'max_temp': 15}},
{'max_error': 1, 'convolution': {'attr': {'temp': 12.0}}, 'temp': 12, 'attr': {'max_temp': 12}},
{'max_error': 0, 'convolution': {'attr': {'temp': 41.0}}, 'temp': 21, 'attr': {'max_temp': 21}},
{'max_error': 0, 'convolution': {'attr': {'temp': 30.0}}, 'temp': 15, 'attr': {'max_temp': 15}},
{'max_error': 1, 'convolution': {'attr': {'temp': 23.0}}, 'temp': 11, 'attr': {'max_temp': 12}},
{'max_error': 1, 'convolution': {'attr': {'temp': 33.0}}, 'temp': 10, 'attr': {'max_temp': 12}},
{'max_error': 1, 'convolution': {'attr': {'temp': 43.0}}, 'temp': 10, 'attr': {'max_temp': 12}},
{'max_error': 0, 'convolution': {'attr': {'temp': 10.0}}, 'temp': 10, 'attr': {'max_temp': 10}}]]
a = assertmessage(al)
s1>>reduce(['id'],
           convolution['attr.temp'],
           {'attr.max_temp':maximum['attr.temp']},
           temp='attr.temp',
           max_error=maximum['attr.error'] )>>a['_']>>p
s1.setStream(data.msgs)
time.sleep(1)
s1.clear()
p.clear()
assert(len(al)==0)
print("")

t = reduce(['id'],
           convolution['attr.temp'],
           {'attr.max_temp':maximum['attr.temp']},
           temp='attr.temp',
           max_error=maximum['attr.error'] )
t %within% WINDOW[:1:]
s1>>t>>p
s1.setStream(data.msgs)
time.sleep(1)
s1.clear()
p.clear()
print("")

t = reduce(['id'],
           convolution['attr.temp'],
           {'attr.max_temp':maximum['attr.temp']},
           temp='attr.temp',
           max_error=maximum['attr.error'] )
t %within% WINDOW[:2:]
s1>>t>>p
s1.setStream(data.msgs)
time.sleep(1)
s1!=t
t!=p
s1.clear()
p.clear()
t.close()
print("")

t = reduce(['id'],
           convolution['attr.temp'],
           {'attr.max_temp':maximum['attr.temp']},
           temp='attr.temp',
           max_error=maximum['attr.error'] )
t %within% WINDOW[:2:2]
s1>>t>>p
s1.setStream(data.msgs)
time.sleep(1)
s1!=t
t!=p
s1.clear()
p.clear()
t.close()
print("")

#t = reduce(['id'], exception=exception['attr.error'] )
#t %within% WINDOW[:2:]
#s1>>t>>p
#s1.setStream(data.msgs)
#time.sleep(1)
#s1<>t
#t<>p
#t.close()
#print("")

f = s1>>(lambda _: _['attr']['temp'] > 20)
f>>p
s1.setStream(data.msgs)
time.sleep(1)
s1!=f
f!=p

f = s1>>( ((_.attr.temp+1)**2 > 400) & (_.attr.temp+1 < 25) )
f>>p
s1.setStream(data.msgs)
time.sleep(1)
s1!=f
f!=p

import ciso8601
adate = ciso8601.parse_datetime('2015-07-09T13:55:00+02:00')
#adate = ciso8601.parse_datetime('2015-07-09T12:55:00+01:00')
f = s1>>(_.date >= adate)
f>>p
s1.setStream(data.msgs_join)
time.sleep(1)
s1.clear()
p.clear()
f.close()
print("")

al = [AttrDict(i) for i in [
{"attr":{"temp":10,"error":0},"i":7,"j":0,"date":{"$date":"2015-07-09T13:50:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":8,"j":0,"date":{"$date":"2015-07-09T13:50:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":7,"j":1,"date":{"$date":"2015-07-09T13:51:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":8,"j":1,"date":{"$date":"2015-07-09T13:51:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":7,"j":2,"date":{"$date":"2015-07-09T13:52:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":8,"j":2,"date":{"$date":"2015-07-09T13:52:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":7,"j":3,"date":{"$date":"2015-07-09T13:53:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":8,"j":3,"date":{"$date":"2015-07-09T13:53:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":7,"j":4,"date":{"$date":"2015-07-09T13:54:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":8,"j":4,"date":{"$date":"2015-07-09T13:54:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":7,"j":5,"date":{"$date":"2015-07-09T13:55:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":8,"j":5,"date":{"$date":"2015-07-09T13:55:00+02:00"},"type":1,"id":4},
{"attr":{"temp":10,"error":0},"i":9,"j":6,"date":{"$date":"2015-07-09T13:56:00+02:00"},"type":1,"id":5},
{"attr":{"temp":10,"error":0},"i":9,"j":7,"date":{"$date":"2015-07-09T13:57:00+02:00"},"type":1,"id":5},
{"attr":{"temp":10,"error":0},"i":9,"j":8,"date":{"$date":"2015-07-09T13:58:00+02:00"},"type":1,"id":5},
{"attr":{"temp":10,"error":0},"i":9,"j":9,"date":{"$date":"2015-07-09T13:59:00+02:00"},"type":1,"id":5}]]
a = assertmessage(al)
j = join( LEFT & RIGHT, ON=(['id'], ['id']) ) %within% WINDOW[:3:][:3:]
(s1, s2)>>j>>a['_']>>p
s1.setStream( data.msgs )
time.sleep(1)
s2.setStream( data.msgs_join )
time.sleep(1)
s1!=j
s2!=j
s1.clear()
p.clear()
j.close()
assert(len(al)==0)
print("")

s1.close()
p.close()
os._exit(1)

