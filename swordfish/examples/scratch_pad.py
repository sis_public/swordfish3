# -*- coding: utf-8 -*-
# Copyright 2020, CSIR <www.csir.co.za>
# This software is released under the MIT License
# The license text is at https://mit-license.org/
"""
Created on Mon Apr 14 16:30:56 2014

@author: tvzyl
"""

"""
Syntax for Filters
"""
#f1 = Filter([condition1,condition2,condition3])
#f1[condition1]>>s2
#f1[condition2]>>s3

#fc1, fc2, fc3 = Filter([condition1,condition2,condition3])

#fc1 = Filter(condition1)
#fc2 = Filter(condition2)
#fc3 = Filter(condition3)

#s1>>fc1
#s1>>fc2
#s1>>fc3

"""
Syntax for Windowing/Framing Opitons
"""
#s = StreamProcessor()
#c = Count( Window( lenghth=10,rate=5 ) )
#s>>c

#Continuous Analytics and Stream Processing

#Complex Event Processing
#1 - Domain Specific Language
#2 - Continuous Query
#3 - Time or Length Windows
#4 - Temporal Pattern Matching ???

#Spatial Complex Event Processing
#1 - Domain Specific Language
#2 - Continuous Query
#3 - Time or Length Windows
#4 - Temporal Pattern Matching ???
#? - Spatial Windows
#? - Spatial Pattern Matching

#WINDOWS

#Window clause specifies the length of window of messages under consideration
        #and the rate at which that length is being applied
#the window is used to decide the two lists of messages that will be past to each
#callable(__call__) within the stream processor that is being applied over the window
        #all callables are called each time the rate parameter is met
#the two lists contain the messages that have been added to the window and the
#messages removed from the window these lists should not be modified my the callables
#if no window is present then all messages are deemed to be kept/considered
#that is each callable will recieve an added messages for each message that
#arrives and no removed messages


"""
The fire hazard fence example (range)
"""
cars = CarStream()
firehazards = FireStream()
firehazards_envelope = Envelope( 1 )

firehazards>>firehazards_envelope

#Always a Left InnerJoin takes as input two stream and produces as output a
#single stream the on clause is evaluated by passing in the window added and
#removed deltas of the left window as well as every value of the right stream
#left is the first stream connected (default_input_stream), right is the second
#stream (secondary_input_stream)

intersection = InnerJoin( on=ST_INTERSECTS('envelope', 'location'), win=RealTimeWin( len=1000 ) )

(firehazards_envelope, cars)>>intersection

intersection>>FilterUnique()>>Alert('alert_escalate')
alert_deescalate = Alert()

"""
The fire builder example (feature creation)
"""
fires1 = FiresStream1()
fires2 = FiresStream2()
fires3 = FiresStream3()

allfires = Union()

buffered = Buffer( attr='location', size=10, append=True, append_name='location_buffer' )

(fires1,fires2,fires3)>>allfires>>buffered

###----------------->ST_INTERSECTS has one param here, inconsistent
collection = CollectInto( pred=ST_INTERSECTS('location_buffer'), collect=ST_GEOMCOLLECTION, col_name='fire_id', win=LogicTimeWin('0000-00-02T00:00:00.0') )

#COLLECTBY

#Keep all messages that are within the temporal window
#Test if a new message intersects with any other messages that have been kept

dtBound>>collection

#Dynamic fire case....
collection>>ReduceBy( attr='fire_id', aggrs={ST_UNION('location'):'', TT_MAX('timestamp'):''}, win=LogicTimeWin('0000-00-02T00:00:00.0') )

#No discard fire case....
collection>>ReduceBy( attr='fire_id', aggrs={ST_UNION('location'):'', TT_MAX('timestamp'):'timestamp_max'}, having=TT_OLDER('timestamp_max','0000-00-02T00:00:00.0') )

#REDUCEBY

#for each unique attrs combination a new reduction is created
#all aggregates passed to the "agg" parameter are window callables
#any message not within the current messages i.e. reomved messages must be excluded from calculation of the aggregates
#a message being kept or considered doesnt mean that it physically needs to remain, the agregates might just remain

#having clause is tested on the most current result of the aggregates of each reduction
#each time the reduction will be modified or used
#if the having clause does not hold then reduction is discarded

#dtBound>>CollectBy( pred=('timestamp', TT_OVERLAPS, INTERVAL, 'collection_id'), win=LogicalWindow('0000-00-02T00:00:00.0') )
"""
Tag current load with predicted load (feature addition)
"""
loadStream = Stream('load')
load_cast = RollingMap( funcs={AutoRegression('measurements.load'):'meaurements.load_cast'}, win=CountWin( len=10 ) )
loadStream>>load_cast

"""
Forecast Ph
"""
phStream =   Map( funcs={}, debug_name='ph' )
trmmStream = Map( funcs={}, debug_name='trmm' )

trmm_roll_014 = trmmStream >> RollingMap( funcs={SUM('measurements.rainfall'):'measurements.rainfall_014'}, win=CountWin( span=14  ), append=False )
trmm_roll_154 = trmmStream >> RollingMap( funcs={SUM('measurements.rainfall'):'measurements.rainfall_154'}, win=CountWin( span=154 ), append=False )
trmm_roll_365 = trmmStream >> RollingMap( funcs={SUM('measurements.rainfall'):'measurements.rainfall_365'}, win=CountWin( span=365 ), append=False )

#Map takes as input functions that accept the values placed on the stream
#No window is passed to the function
#Functions should provide a 1 to 1 mapping of input to output
forecast_ph = Map( funcs={mySpecialForecast:'measurements.ph_forecast'} )

#InnerJoin() = InnerJoin(on=None, win=CountWin( len=1 ) )

(((phStream, trmm_roll_014)>>InnerJoin()
           , trmm_roll_154)>>InnerJoin()
           , trmm_roll_365)>>InnerJoin()>>forecast_ph

"""
Standard Deviation Based Ranging on Ph (range)
"""

phStream = Stream('ph')

#callable Object or function __call__(message)
rollingStdDev = RollingMap( funcs={StdDev('measurements.ph'):'measurements.mean_ph'}, win=CountWin( len=10 ) )

#Tests function for True/False, when true pass the message through on the
#any stream connected to the primary output ['in'] when a previous True test
#becomes False pass the first False message through on any streams connected
#to the secondary stream ['out']
rangeBy = RangeBy( func=RelativeThreshold( median_name='measurements.mean_ph', relative_threshold='measurements.std_ph') )

phStream>>rollingStdDev
rollingStdDev>>rollingMean
rollingMean>>rangeBy

rollingMean>>"thread://default">>rangeBy

rangeBy['in']>>first>>alert_escalate
rangeBy['out']>>alert_deescelate


from geventwebsocket import WebSocketServer, WebSocketApplication, Resource

class EchoApplication(WebSocketApplication):
    def on_open(self):
        print("Connection opened")

    def on_message(self, message):
        print(message)
        for client in list(self.ws.handler.server.clients.values()):
            client.ws.send(message)

    def on_close(self, reason):
        print(reason)

WebSocketServer(
    ('', 9090),
    Resource({'/': EchoApplication})
).serve_forever()


#$ python -m SimpleHTTPServer


hippoJson = '''{ "type": "activeAMD", "properties": { "Name": "Hippo", "description": "Hippo Pool", "timestamp": null }, "location": { "type": "Point", "coordinates": [ 27.72108, -26.09919 ] } }'''

"amqp://firetweets.csir.co.za">>doProcessing>>"http://joeso.orgo.org/"

AMQP_hotspot = AMQP_Stream("amqp://hotspots.bla/swordfish", exchange="a", queue="b", routing_key="1.*.3")

"amqp://hotspots.bla/swordfish?exchange=a&queue=b&routing_key=1.*.3">>foundFire>>"ws://localhost:9090#split,100"
__AMQP_hotspot__>>foundFire>>"ws://firetweets.csir.co.za:9090"

sp1 = Sp()
ts1 = Ts('file://a')
sp2 = Sp()

class Topology(object):
    pass

with Topolgy( sp1, ts1,sp2 ) as t:
    print(t)

SP1.LogStream>>Print()

