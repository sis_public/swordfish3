Dependencies
============
If runnning on another machine (e.g. a server)::

    pip install websocket==0.2.1
    pip install pandas


Running
=======
To startup::

    cd /path/to/trunk/python_code/
    export set PYTHONPATH=$PYTHONPATH:.
    python ./swordfish/examples/swordfish_api.py
